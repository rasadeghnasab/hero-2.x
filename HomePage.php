<?php require_once(__DIR__ . '/includes/initial.php'); ?>
<?php
$css_array = array(
  'remove' => array(
    'semantic/semantic',
    'semantic/semantic_custom',
  ),
);
?>
<?php
$description = 'هیرو اولین سایت جامع ورزش، ورزش های رزمی، اخبار ورزشی، رویدادهای ورزشی و شبکه اجتماعی ورزشی، مبادله تکنیک و فنون رزمی';
$title       = 'ورزش | بانک ورزشی | اخبار ورزشی | قهرمانان رزمی';
?>
<?php html_meta_desc_keyword_tag($description); ?>
<?php layout_template('head_section', array('css'        => $css_array,
                                            'body_class' => 'homepage'
)); ?>
<!--<div class="homePageBackground" >-->
<header class="header mainHeader">
  <div class="logo"></div>
  <ul class="menu nav">
    <!--            --><?php //if(isset($cUser->user_id)):?>
    <!--                <li><a href="#">خانه</a></li>-->
    <!--            --><?php //endif;/*else */?><!--<!---->
    <!--                <li><a href="#">خانه</a></li>-->
    <li><a href="/martial/">تکنیک</a></li>
    <li><a href="/masters/">اساتید</a></li>
    <li><a href="news/all">اخبار</a></li>
    <li><a href="/events">رویدادها</a></li>
    <!--            <li><a href="#">فروشگاه</a></li>-->
  </ul>
  <h1 class="hidden"><?= $title ?></h1>
  <form id="SignInForm" method="post" action="login/" novalidate >
    <input type="submit" id="signInHeaderBtn" class="btn btn_white signIn_btn"
           value="ورود">

    <div class="signInArea">
            	<span>
                <input class="balloon" name="username" id="userName" type="text"
                       placeholder="نام کاربری / ایمیل"><label for="userName">نامکاربری</label>
                </span> 
                <span> 
                	<input class="balloon" name="password" id="passWord"
                           type="password" placeholder="رمز عبور"
                           maxlength="32"><label for="passWord">رمز عبور</label>
                </span>

      <div class="SignInTck">
        <!--                <input type="checkbox" name="remember"><label>مرا به یاد داشته باش</label>-->
        <a href="#">فراموشی رمز عبور</a>
      </div>
    </div>

  </form>
        <span class="btn btn_white signUp_btn">
            <a href="register/" title="ثبت نام در سیستم"> ثبت نام </a>
        </span>
</header>
<?php layout_template('page-message'); ?>

<!--</div>-->
<?php layout_template('foot/foot_section'); ?>
