$(document).ready(function () {
//    window.formatToPersian;
    $('body').on('element_created', function () {
        // when an element_created event is occured this function called.
        /************** veno box ***************/
        $('.venobox').venobox({infinigall: true});
        /************** end of veno box ***************/
    });
    $('.venobox').venobox({infinigall: true});
    $('.timeago').timeago();
    $('.badge').parent().css({position: 'relative'});
    $('.no-title').title_remover();
    $('body').on('click', '.timeago', function () {
        $(this).timeago();
        $('.ui.dropdown').dropdown();
    });
    var week_day_assoc = {
            'شنبه': 0,
            'یکشنبه': 1,
            'دوشنبه': 2,
            'سه شنبه': 3,
            'چهارشنبه': 4,
            'پنج شنبه': 5,
            'جمعه': 6
        },
        week_day = new Array(
            'شنبه',
            'یکشنبه',
            'دوشنبه',
            'سه شنبه',
            'چهارشنبه',
            'پنج شنبه',
            'جمعه'
        );
    persianDate().format();
    var modalScrPos,
        lockScrollLoad = false,
        teacher_clicked_image,
        teacher_clicked_image_src,
        loading = $('.loading').attr('src'),
        gallery_images;
    /************************************** all pages ***************************************/
    $('.ui.dropdown').dropdown();
    $('.special.cards .card .image').dimmer({
        on: 'hover'
    });
    $('.modal-show').on('click', function (e) {
        e.preventDefault();
        var th = $(this),
            modal_target = th.data('modal-target');
        console.log(modal_target)
        if (!modal_target) {
            return;
        }
        $(modal_target).modal('show');

    })
    /*********** read more ***********/
    $('.rdmb').rdm('.rdmp');
    /*********** read more end ***********/
    /********** masonary **********/
    var $container = $('.masonry_container').masonry();
// layout Masonry again after all images have loaded
    $container.imagesLoaded(function () {
        $container.masonry({"itemSelector": ".masonry-item", "gutter": 12});
    });
    /******** end of masonary *********/
    $('.header .nav a, .subMenu a').pathHighlight(null, 'breadcrumb');
    $('.subMenu a').pathHighlight(null, 'active');
    $('[data-placeholder]').placeholder('[data-placeholder]');
    /*if the href attribute of <a> tag is equal to current page set href attribute to 'javascript:;' */
    //TODO:: change this to a plugin
    /**************** put asterisk after all required fields ****************/
    $('.required, .asterisk').required_asterisk();
    /**************** end of put asterisk after all required fields ****************/
    /************** message *************/
    $('.ui.message .close.icon').on('click', function(){
        $(this).closest('.message').fadeOut('slow');
    })

    $('.ui.radio.checkbox')
        .checkbox()
    ;
    /************** message end *************/
    /********************* qtip2 **********************/
    /*$.fn.qtip.defaults = {
     style: {
     classes: 'qtip-light'
     }
     }*/
    $('.help').qtip({
        content: {
            attr: 'data-help'
        }
        ,
        hide: {
            event: 'unfocus'
        },
        style: {
            classes: 'qtip-light'
        }
    })
    $('[data-help]:input, label[data-help]').qtip({
        content: {
            attr: 'data-help'
        }
        ,
        show: {
            event: 'hover'
            /*,effect: function(offset) {
             $(this).slideDown(100)
             }*/
            //target: $('li')
            //,target: $('h1, h2, h3, h4')
        }
        ,
        hide: {
            event: 'mouseleave',
            effect: function (offset) {
                $(this).slideUp(100); // "this" refers to the tooltip
            }
        }
        ,
        position: {
            my: 'top right',
            at: 'top left'
        },
        style: {
            classes: 'qtip-light'
        }
    });
    $('input[data-help]').on('focus', function () {
        //alert($(this).data('help'))
    })
    $('body').on('mouseover', '[data-rest-url]', function (event) {
        console.log(event)
        console.log(window)
        var pos_array = set_qtip_my_and_at(event, $(window));
        //console.log(pos_array);
        function set_qtip_my_and_at(event, window) {

            var my_array = {
                    'left bottom': 'top right',
                    'left top': 'right bottom',
                    'right bottom': 'left top',
                    'right top': 'bottom left',
                    'center top': 'bottom center',
                    'center bottom': 'top center'
                },
                positions = {
                    at_x: 'center',
                    at_y: 'top',
                    my_y: 'bottom',
                    my_x: 'center'
                },
            //window = $(document),
                view_port_width = window.width(),
                view_port_height = window.height(),
                x = event.clientX,
                y = event.clientY;
            //console.log(view_port_height);
            if (x < view_port_width * 20 / 100) {
                positions['at_x'] = 'right'
            }
            else if (x > view_port_width * 80 / 100) {
                positions['at_x'] = 'left'
            }
            if (y < view_port_height * 20 / 100) {
                positions['at_y'] = 'bottom'
            }
            else if (y > view_port_height * 80 / 100) {
                positions['at_y'] = 'top'
            }

            return {
                at: positions['at_x'] + ' ' + positions['at_y'],
                my: my_array[positions['at_x'] + ' ' + positions['at_y']]
            };
        }

        $(this).qtip({
            style: {
                classes: 'qtip-light'
                //width: 400
            },
            overwrite: false, // Make sure the tooltip won't be overridden once created
            content: {
                text: function (event, api) {
                    //console.log($(this).attr('href'))
                    //console.log($(this).data('rest-url'))
                    $.ajax({
                        url: basePath + $(this).data('rest-url') // Use data-url attribute for the URL
                    })
                        .then(function (content) {
                            // Set the tooltip content upon successful retrieval
                            api.set('content.text', content);
                            $('.qtip').css({'max-width': '400px'})
                        }, function (xhr, status, error) {
                            // Upon failure... set the tooltip content to the status and error value
                            api.set('content.text', status + ': ' + error);
                            console.log(error)
                        }).complete(function (response) {
                            console.log(response)
                        });

                    return '<img class="img_middle" src="/_css/img/loading.gif">'; // Set some initial text
                }
            }
            ,
            show: {
                event: event.type, // Use the same show event as the one that triggered the event handler
                ready: true // Show the tooltip as soon as it's bound, vital so it shows up the first time you hover!
                //delay: 500
                //modal: {
                //on : true
                //}
            }
            , hide: {
                event: 'mouseout',
                //inactive: 500,
                fixed: true
                //distance: 50
            }
            , position: {
                my: pos_array.my,
                at: pos_array.at
                //at: at
                //target: 'mouse',
                //adjust: {
                //    mouse : false
                //    y: -30
                //}
            }
        }, event); // Pass through our original event to qTip
    })
    /******************* end of qtip2 ********************/
    /************ help tooltip *************/
    /*$('[data-help]').on('click', function (e) {
     e.stopImmediatePropagation();
     //        console.log(this.getBoundingClientRect());return;
     var th = $(this),
     help_data = th.data('help');

     show_tooltip(true, e, help_data);
     });*/
    /************ help toolipt end ********/
    /******************* textarea grow ************/
    //$('textarea, .grow').autoGrow();
    $('textarea, .grow').autoGrow();
    /******************* textarea grow end ************/
    /****************** collapse ********************/
    $('.collapse').collapse();
    /**************** datapicker set ****************/
    $('.datepicker').pDatepicker();
    /**************** commaSeparate ****************/
    $('.comma_separate').commaSeparate();
    /**************** chosen select ****************/
        ///*
    $('.chosen-select').chosen({
        no_results_text: "گزینه ای یافت نشد",
        placeholder_text_multiple: 'برای انتخاب کلیک کنید',
        disable_search_threshold: 5,
        placeholder_text_single: 'یک مورد را انتخاب کنید',
        max_selected_options: 5
    }).on('change', function (evt, params) {
        $(this).trigger('chosen:updated');
    });
    //*/
    //$('.chosen-select').dropdown();
    //$('.chosen_evt_type').chosen({
    //    no_results_text:           "گزینه ای یافت نشد",
    //    placeholder_text_multiple: 'برای انتخاب کلیک کنید',
    //    disable_search_threshold:  5
    //disable_search:            true
    //});
    /**************** search ****************/
    $('.header input[name=searchtext]').on('input', function () {

        var searchText = $.trim($(this).val()),
//            url         = $(this).attr('data-ajax-dest'),
            sentData = {searchtext: searchText};
        if (searchText.length < 3) return;
        $.ajax({
            url: basePath + 'search/doSearch.php',
            data: sentData,
            type: 'POST',
            dataType: 'json'
        }).done(function (data) {
            console.log(data);
            if (!data.done) {
                //$('#searchResBox').empty().hide();
                //$('#searchResBox').empty().append(data).slideDown(500);
                $('.morphsearch-content').empty().append(data);
            }
        }).error(function (data) {
            console.log(data);
        }).complete(function (data) {
            console.log(data.responseText);
//            show_message({text:data.responseText, time: 1000000})
        })
    }).attr('autocomplete', 'off');
    /************ end of search ****************/
    //$('.goToTop').scrollToTop();
    /**************** user options  ****************/
    $('.prof_menu_open').on('click', function (e) {
        e.stopImmediatePropagation();
        $('#profMenu').toggle();
    });
    $('body').on('click', function () {
        $('#profMenu').hide();
        show_tooltip(false); // if tooltip is not hidden then hide it
    })
    /**************** end of user options ****************/
    /************************************** end of all pages ***************************************/
    /************* photo cover **********/
    $('#cover-photo-file').on('change', function (e) {
        var th = $(this),
            form = th.parent('#cover-photo-form');
        if ($(this).val() != '') {
            form.trigger('submit');
        }
    });
    $('#cover-photo-form').on('submit', function (e) {
        var th = $(this),
            data = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: basePath + $(this).attr('action'),
            type: 'POST',
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('.cover-photo-img').attr('style', "background-image: url(/_css/img/loading.gif)").css({
                    'background-position': 'center center',
                    'background-size': '32px'
                });
            }
        }).done(function (response) {
            if (response.done) {
                console.log(response)
                //$('.cover-photo-img').style({'background-image' : 'url('+response.data.url+')'})
                $('.cover-photo-img').attr('style', "background-image: url(" + response.data.url.thumb + ")")
                $('.cover-photo-img').parent('.venobox').attr('href', response.data.url.large);
            }
            //console.log('done',response)
        }).complete(function (response) {
            console.log('complete', response);
        });
    })
    /*********** photo cover end **********/
    /************** match table create ************/
    function table_save(data, userData) {
        $.ajax({
            url: basePath + userData.url,
            data: {event_id: userData.event_id, mwid: userData.mwid, table_data: JSON.stringify(data)},
            dataType: 'json',
            type: 'POST'
        }).done(function (respond_data) {
            console.log('done', respond_data)
        }).complete(function (respond_data) {
            console.log('comp', respond_data)
        })
    }

    if ((typeof table_data).toString() != 'undefined') {
        var parameter = {
            init: table_data,
        }
        if ((typeof access_table).toString() != 'undefined') {
            //console.log(basePath+'events/score_save.php');
            //alert()
            if (access_table == true) {
                parameter.save = table_save;
//                parameter.url =      '/events/score_save.php';
                parameter.userData = {
                    url: 'events/score_save.php',
                    mwid: mwid,
                    event_id: event_id
                }
            }
        }
        $(function () {
            var container = $('.content .table-show');
            //console.log(parameter)
            container.bracket(parameter)
        });

    }
    /************** end of match table create ************/
    /********************** masters/masters_map.php ***************/
    if (typeof map != 'undefined') {
        $('path').each(function (i, v) {
            var id = $(this).attr('id');
            if (map.hasOwnProperty(id)) {
                v.setAttribute("class", "path-city path-city-g2");
            }
        });
//        console.log(map);
    }
    $('path').on('click', function (e) {
        e.stopImmediatePropagation();
        var id = $(this).attr('id') ? $(this).attr('id') : null;
        if (!map.hasOwnProperty(id)) {
            show_tooltip(false); // if click on outside an path that has not any master in it, tooltip will disappear
            return;
        }
        var masters_container = $('<ul></ul>');
        $.each(map[id], function (i, master) {
            var pic = master['picurl'] ? '/profilePics/' + master['picurl'] : '/_css/img/user-64.png',
                li = $('<li></li>'),
                tooltip_container = $('<div class="tooltip_container"></div>'),
                img = $('<img>').attr('src', pic),
                link = $('<a></a>').attr('href', '/' + master['userID']),
                text = master['first_name'] + ' ' + master['last_name'],
                names = $('<div></div>').text(master['statename'] + ", " + master['countyname']).addClass('blue');
//            console.log(link.append(img));
            masters_container.append(li.append(img, link.text(text), names)).css({'min-width': '200px'});
        })
        show_tooltip(true, e, masters_container);
    })
    /********************** masters/masters_map.php end ***************/
    /*********************** news pages ******************************/
    $('form#news_sport_select_form').on('submit', function (e) {
        e.preventDefault();
        document.location.href = basePath + 'news/' + $(this).find('select#sport_id').val();
    });

    $('form#news_sport_select_form select').on('change', function () {
        $(this).parent('form').trigger('submit');
    })
    /*********************** news pages end ******************************/
    /************************************** index.php ***************************************/
//    console.log(window.location);
        // TODO : Delete image session before exit if it's set

    window.onbeforeunload = function (e) {
//        console.log(e);
        var e = e || window.event, url;
        url = window.location.pathname.match(/register.php/g) ? 'register/image.php' : 'post/imageInsert.php';
        $.ajax({
            url: basePath + url,
            type: "POST",
            data: {end: 'end'}
        });
        // For IE and Firefox prior to version 4
        /*if (e) {
         e.returnValue = url;
         }

         // For Safari
         return url;*/

    };
    /* in ghesmat baraye sabte poste jadid mibashad k tavasote Ajax anjam shode va az safe kharej nmishavad */

    /*sabte khabar ba click bar roye dokmeye #submitNews anjam mishavad */
    $('#wall_post').on('submit', function (e) {
        //console.log(e)
        var th = $(this);
        //console.log(th.serialize());
        //console.log(JSON.stringify(th.serializeArray().serialize()));
        //return false;
        //title = $('#title').val(),
        mainText = $('#mainText').val(),
            catID = $('#catID').val();
        e.preventDefault();
        setTimeout(function () {
            th.removeAttr('disabled')
        }, 1500);//$(this),5000);

        /* in ghesmat b ehtemale ziad hazf mishe. karesh ine k age har kodam az se field khali bod nazare post sabt beshe */
        if (/*title == '' || */mainText == '' || catID == '') return false;
        th.attr('disabled', 'disabled');
        /* maghadiri ra k az se field gerefte b safeye postInsert.php ersal mikonad */
        $.ajax({
            type: "POST",
            url: basePath + "post/postInsert.php",
            dataType: "json",
            data: th.serializeArray(),
            //data:     new FormData(this),
            //contentType: false,
            //cache:       false,
            //processData: false,
            success: function (data) {
                //console.log('success', data);
                if (data['done']) {
                    if ($('.news_part:first').attr('class')) {
                        $('.news_part:first').before(data.data);
                        $('.news_part:first').children().hide().show(1000);
                    } else {
                        $('.news_new').after(data.data);//.append(data);
                        $('.news_part:first').children().hide().show(1000);
                    }
                    /* dokmeye submitNews k disable shode bod ba movafaghiat amiz bodane sabt aknon enable mishavad */
                    th.removeAttr('disabled').css('background-color', '#159594');

                    /* agar sabt movafaghiat amiz bashad in do field khali mishavand */
                    $('#title,#mainText').val('');
                    $('.chosen-select').val('').trigger('chosen:updated');
                    $("#imgBack").html('انتخاب تصویر').hide();
                }
                if (data.message != '') {
                    show_message({text: data.message});
                }
            },
            complete: function (data) {
                /* agar Ajax ba moshkel ro be ro shavad in ghesmat etefagh mioftad */
                console.log('comp', data)
                th.removeAttr('disabled').css('background-color', '#159594');
                $('body').trigger('element_created');
                $('#inputImg').val('');
            },
            error: function (data) {
                //console.log('error',data)
            }

        });
    });
    $('#uploadImgForm').change(function (e) {
        var th = $(this);
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $("#imgBack").html('در حال بارگذاری').show();
                    }
                }, false);
                //Download progress
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with download progress
                        //console.log(percentComplete);
                    }
                }, false);
                return xhr;
            },
            url: basePath + "post/imageInsert.php",
            type: "POST",
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $("#imgBack").html('در حال بارگذاری').show();
            }
        }).done(function (data) {
            if (!data['error']) {
                $("#imgBack").html($('<img>').attr('src', data['success']));
                //$('#progress').css({width: '100%'}).slideUp(700);
            }
            else {
                $("#imgBack").html(data['error']);
                //$('#progress').css({width: '0%'});
            }
        }).complete(function (data) {

        }).error(function () {
            $("#imgBack").html('عملیات ناموفق').hide();
            //$('#progress').css({width: '0%'});
        });
    });
    /* in khat kod makani k dar an hastim ra namayesh midahad */
    /* ba scoll kardane safe post haye jadid namayesh dade mishavad.*/
    $(window).on('scroll', function () {
        /* load new content */
        if (typeof scroll_load_permitted != 'undefined') {
            if ($(this).scrollTop() + $(this).height() == $(document).height() && $(this).scrollTop() != 0) {
                load_new_posts();
            }
        }
    });
    /* if we want to load posts when we click on #more button we must uncomment this line
     $('#more').on('click', function(){
     load_new_posts(false);
     });
     */
    function load_new_posts(lock_scroll) {
        if (lock_scroll) lockScrollLoad = lock_scroll;
        /* in ghesmat az inke ba yek bar scroll chand post load shavad jologiri mikonad va t darkhaste badi 2sanie bayad sabr kard */
        if (lockScrollLoad) return;
        /* agar khabare digari vojod nadashte bashad bdone anjame har kari khoroj mikonad. */
        if ($('#more').text() != '') return;
        var start = $('#pageStart').html(),
            sentData = {start: start},
            url;
//        release = setTimeout(function(){
//            lockScrollLoad = false;
//        }, 2500);
        if ($('.content').attr('data-user') != undefined) sentData.user_id = $('.content').attr('data-user');
        url = $('.content').attr('data-destination') != undefined ? $('.content').attr('data-destination') : '';
        //console.log('send', sentData);
        //console.log('content', basePath+url);
        /* dar shoroe amaliat #waitImg nabayesh dade mishavad */
        $.ajax({
            type: 'POST',
            data: sentData,
            url: basePath + url,
            dataType: "json",
            beforeSend: function () {
                loading_img($('#waitImg'));
                lockScrollLoad = true;
            },
            success: function (data) {
                console.log('succ', data);
                if (!data.noRow) {
                    $('.news_part:last').after($(data['data']));
                    $('.timeago').trigger('click');
                    $('.news_part:last').children().hide().show(1000, function () {
                        lockScrollLoad = false;
                    });
                    $('#pageStart').text(data['start']);
                } else {
                    $('#more').html(data['noRow']).css('cursor', 'auto');
                    lockScrollLoad = true;
                }
            },
            complete: function (data) {
                $('body').trigger('element_created');
                console.log('comp', data);
                loading_img($('#waitImg'));
            },
            error: function (data) {
//            console.log(data);
            }
        });
    }

    $('input.inpImg').fileInputVal({relBtn: '.set_img, #imgBack'});
    $('input.cover-photo-file').fileInputVal({relBtn: '.cover_image_btn'});
    //$('.profile-img-change').on('click', function(e){
    //    e.stopPropagation();
    //    return false;
    //})
    $('input.profile-photo-file').fileInputVal({relBtn: '.profile-img-change'});
    $('input.teacher').fileInputVal({relBtn: '.teacher_pic'});
    $('input#profImgUpload').fileInputVal({relBtn: '.profImg, img.SignUpProfPic'});
    $('input#profImage').fileInputVal({relBtn: '.set_img'});
    $('body').on('click', 'ul.editable li:first-child', function () {
        $(this).siblings('li').toggle();
    })
    $('body').on('click', 'ul.editable li', function () {
        var targetPage = $(this).attr('class'),
            thPar = $(this).parents('.news_part'),
            id = thPar.attr('id');
        if (targetPage == 'postEdit') {
            $(this).parent().siblings('article').attr('contentEditable', 'true').css({'background-color' : 'red'});
            $(this).parent().siblings('article').focus();//.after($('<button>').attr('class','button').html('button'));

        } else if (targetPage == 'postDelete') {
            if (!confirm('مطمئن هستید؟')) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: basePath + 'post/postDelete.php',
                data: {id: id},
                dataType: "json",
                success: function (data) {
                    console.log('done', data);
                    if (data['success']) {
//                        console.log(data);
//                        $('.errors').append($('<li>').html(data['success']));
                        thPar.slideUp("slow");
                        setTimeout(function () {
                            thPar.remove();
                        }, 700);
                    } else {
                        console.log('sss'); return;
                        $('.errors').append($('<li>').attr('id', 'postDel').html(data['error']));
                    }
                },
                error: function (data) {
                    console.log('error',data);
                    if (data['success']) $('.errors').append($('<li>').html(data['success']));
                    else $('.errors').append($('<li>').html(data['error']));
                }
            })
                .complete(function(data){
                    console.log('compelete', data);
                });
        }
        else if (targetPage == 'postDone') {
            var mainText = $(this).parents('.news_part').find('>article');
            if (mainText.attr('contentEditable') == 'true') {
                var /*header = $(this).parents('.news_part').find('h2'),*/
                    date = $(this).parents('.news_part').find('.news_timeline_date a'),
                    context = mainText.text();
                //title = header.text();
//                console.log(context);return;
                $.ajax({
                    type: 'POST',
                    url: basePath + 'post/postEdit.php',
                    data: {context: context, id: id/*, title: title*/},
                    dataType: 'json'
                }).done(function (data) {
                    console.log('success', data);
                    if (data['success']) {
                        mainText.hide().fadeIn(1000).removeAttr('contentEditable');
                        //header.hide().fadeIn(1000).removeAttr('contentEditable');
                        date.html(data['updated']);
                    }
                }).complete(function (data) {
                        console.log('error',data);
                })
                $(this).parent().find('li').not(':first-child').toggle();
            } else return false;
        }
    });
    $('.sentWclick').on('keyup', function (e) {
//        console.log(e);
        if (e.keyCode == 13) {
            $(this).parent().attr('id');
        }
    });
    $('.news_part').on('keyup', 'article[contenteditable=true]', function (e) {
        if (e.keyCode == 13 && e.shiftKey == true) {
            $(this).siblings('ul.editable').find('li.postDone').trigger('click');
        }
    });
    /**************************************  End of index.php ***************************************/
    /**************************************  events/index.php ***************************************/
    $('#events').on('submit', function (e) {
        e.preventDefault();
        document.location.href = '/events/' + $(this).find('select#sportID').val();
    });
    $('#events select#sportID').on('change', function () {
        $(this).parent('form').trigger('submit');
    })
    /**************************************  End of events/index.php ***************************************/
    /**************************************  register.php ***************************************/
    /* classe '.datefield' baraye fieldhaii k az se fielde input tashkil shode and va b manzor gereftane
     * tarikh az karbar morede estefade gharar migirad (in tarikh ha shamele tarikh haii k ba estefade az taghvim
     * daryaft mishavand nmishavad */
    $('.datefield').bind('mouseenter keyup', function (e) {
        /*
         * taabe datefieldInput mitavand yek parametre digar niz bpazirad b in sorat:
         * dateFiledInput($(this),{showDateTag:'ID ya Classe yek tag k peyghamhaye bargashti ra namayesh dahad',
         *           in felan nist //pDate: tarikhe darkhasti ast k ziad estefade nmishavad chon b sorate khodkar b dast miaayad
         *                         format: k formate khoroji ra k showDateTag namayesh midahad na moshakhas mikonad(por karbord)
         *                         calType: moshakhas mikonad k meghdare bargashte shamsi bashad ya miladi. perDate = shamsi, greDate = miladi
         *                         });
         * mesal: dateFieldInput($(this),{showDateTag:'.datefieldShow',format:'YYYY/MM/DD',calType:'greDate'});
         * agar hich optioni ferestade nashavad b tore pishfarz tarikhe shamsi b formate khordad/12 dar tagi ba class .datefieldShow
         * namayesh dade mishavad
         */
        dateFieldInput($(this), e);
    });

    ////////////////////////// form validations
    // sign up form validation
    $('.validate').wnPlugin({button: '.btnForce', errorShow: '.errors'});
    $('.validate2').wnPlugin({button: '.step-2', errorShow: '.errors'});

    // form
    $('.formValidator').wnPlugin();

    /* register Ajaxes */

    /* found countyNames, fieldsName AND branchesName (stateID, sportID) */
    $('#stateResult, #countyResult, #fieldResult, #branchResult').on('change', function () {
        removeFirstChild($(this));
        var url = $(this).attr('data-ajax-dest'),
            name = $(this).attr('name'),
            value = $(this).val(),
            AjaxDef = {
                url: basePath + url,
                type: 'POST'
            };
        console.log(url);
        console.log(AjaxDef);
        if (value == 0 || url == undefined) return;
        if (name == 'stateResult') {
            var options = {data: {stateName: value}}
            $.extend(AjaxDef, options);
            $.ajax(AjaxDef).done(function (data) {
//                addAllToSelect($('#countyResult'), url);
                $('#countyResult').html(data).trigger('change');
//                console.log('stateResult,done',data);
            }).complete(function (data) {
//                console.log('stateResult,Complete',data);
            });
        }
        else if (name == 'countyResult') {
            var options = {data: {stateID: value, stateName: $('#stateResult').val()}}
            $.extend(AjaxDef, options);
            $.ajax(AjaxDef).done(function (data) {
//                addAllToSelect($('#fieldResult'), url);
//                console.log('countyResult,done',data);
                $('#fieldResult').html(data).trigger('change');
            }).complete(function (data) {
//                console.log('countyResult,Complete',data);
            });
        }
        else if (name == 'fieldResult') {
            var options = {
                data: {
                    sportName: value,
                    stateID: $('#countyResult').val(),
                    stateName: $('#stateResult').val()
                }
            }
            $.extend(AjaxDef, options);
            $.ajax(AjaxDef).done(function (data) {
//                addAllToSelect($('#branchResult'), url);
                $('#branchResult').html(data);
                $('#superior').trigger('change');
            })
        }
        else {
            $('#superior').trigger('change');
        }
    });
    /*
     function addAllToSelect(th, url){
     if(url == 'adminSCFB.php')
     th.empty().prepend($('<option>').attr('val','همه').text('همه'));
     }
     */
    /* found name of all superiors of an sport in a special state */
    $('#superior').on('change', function () {
        removeFirstChild($(this));
        var stateID = $('#countyResult').val(),
            sportID = $('#branchResult').val(),
            jobID = $(this).val();
        if (stateID == 0 || sportID == 0 || jobID == 0) return;
//        console.log('miiiii');
        $.ajax({
            url: basePath + 'register/registerSCFB.php',
            type: 'POST',
//            dataType: 'json',
            data: {stateID: stateID, sportID: sportID, jobID: jobID},
            success: function (data) {
                $('#teName').empty();
                if (data) {
//                    console.log(data);
                    $('#teName').append(data);
//                    $.each(data, function(index, value){
//                        console.log(i, v);
//                        $('$teName').append
//                        $('#teName').append($('<option>').val(value.userID).text(value.first_name + ' '+ value.last_name));
//                    })
                }

//                console.log($('#teName').find(':first-child').val());
                var firstVal = $('#teName').find(':first-child').val();
//                console.log(parseInt(firstVal))
                if (firstVal == 0) $('#teName').attr('disabled', true);
                else $('#teName').removeAttr('disabled');
//                if(data == '<option>بدون استاد</option>') alert('hi') // $('#teName').attr('disable', 'true');
            },
            complete: function (data) {
//                console.log(data);
//                console.log('hi');
            }
        })
        /*.done(function(data){
         alert('hi');
         console.log('hi');
         console.log(data);
         }).complete(function(data){
         console.log(data)
         })*/
    });
    $('#teName').on('change', function () {
        removeFirstChild($(this));
    });

    /* image immediately upload to temp */
    $('#profImgUpload').on('change', function () {
        var file_data = new FormData();
        file_data.append('profilePic', $('input[name=profImgUpload]')[0].files[0]);
        $.ajax({
            url: basePath + 'register/image.php',
            data: file_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('.profImg').css('width', '80px').text('در حال بارگذاری');
            },
            success: function (data) {
                if (data.success)
                    $('img.SignUpProfPic').attr('src', data.success);
                else
                    $('body').append(data.error);
                console.log(data);
            },
            complete: function (data) {
                $('.profImg').css('width', '80px').text('افزودن عکس');
                console.log(data);
            }
        });
    });

    /* sync name,last_name And national_code FROM first part of register page to it's 3rd page
     * And check email, username, and national_code with ajax */
    /* put first_name and last_name to #SignUpCheckName */
    $('#first_name, #last_name').on('change', function () {
        var fullName = $('#first_name').val() + ' ' + $('#last_name').val();
        $('#SignUpCheckName').text(fullName);
    });
    /* put national_code to #SignUpCheckId */
    $('#national_code').on('change', function () {
        $('#SignUpCheckId').text($(this).val());
    })
    $('#Email').on('change', function () {
        $('#SignUpCheckEmail').text($(this).val());
    });
    $('#national_code, #Email, #username').on('change', function () {
        var th = $(this),
            val = th.val(),
            name = th.attr('name'),
            id = th.attr('id'),
            data = new Object();
        if ($.trim(val) == '') return
        data[name] = val;
//        console.log(data);
        $.ajax({
            url: basePath + 'register/registerSCFB.php',
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
//                console.log('change');
//                console.log(data);
                if (data == 'exist') {
//                    console.log('label[for='+id+']');
                    var label = th.attr('placeholder');
//                    console.log(label);
//                    $('.errors').children('li#Error'+id).remove();
                    var child = $('<li></li>').text(label + ' وارد شده قبلا گرفته شده').attr('id', 'Error' + id);
                    $('.errors').append(child);
                } else if (data == 'notExist') {
//                    $('.errors').find('li#Error'+id).remove();
                    $('.errors li#Error' + id).remove();
//                .children('li#Error'+id)
//                    console.log('remove');
                }
//
            },
            complete: function () {
//                console.log('done');
            }
        });

    });
    /* register steps */
    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches
    $(".form_next").click(function () {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale(' + scale + ')'});
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 300,
            complete: function () {
                current_fs.hide();
                animating = false;
            }
            //this comes from the custom easing plugin
            // easing: 'easeInOutBack'
        });
    });
    $(".form_previous").click(function () {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
            },
            duration: 300,
            complete: function () {
                current_fs.hide();
                animating = false;
            }
            //this comes from the custom easing plugin
            // easing: 'easeInOutBack'
        });
    });
    /**************************************  End of register.php ***************************************/
    /************************************* honors.php ****************************************************/
    $('.content').on('click', '.honor', function (e) {
        e.preventDefault();
        var form = $(this).parents('.new_row').find('form'),
            data = form.serialize(),
            action = $(this).hasClass('edit') ? '&action=edit' : '&action=remove',
            th = $(this), text;
        data += action;
        console.log(form.attr('action'))
        // check to see if any field is empty return and do not submit
        $.ajax({
            url: basePath + form.attr('action'),
            data: data,
            dataType: 'JSON',
            type: 'POST'
        }).done(function (data) {
            console.log(data);
            if (data != false) {
                text = 'درخواست شما با موفقیت اعمال شد';
                if (action == '&action=remove') {
                    th.parents('.new_row').animate({
                        height: 0
                    }, 1500, function () {
                        th.parents('.new_row').remove();
                    });
                } else {
                    form.find('[name=awd_id]').val(data.id);
                }
            } else {
                text = 'عملیات ناموفق بود لطفا دوباره سعی کنید';
            }
            show_message({text: text});
        }).error(function (data) {
//            console.log(data);
        })
    });
    $('.new_honor').on('click', function () {
        //create a new form for new_honor
        $(this).parent('.new_row').after(new_honor_form_create());
    });
    function new_honor_form_create() {
        var sport_select = $('[name=sportID]').clone().css({display: 'inline-block'}),// sports select
//            sport_chosen = $('[name=sportID]').next('.chosen-container'), //sport div for chosen
            medal_select = $('[name=medal]').clone().css({display: 'inline-block'});// medal select
//            medal_chosen = $('[name=medal]').next('.chosen-container');// medal div for chosen
        var main_row = $('<div class="new_row"></div>'), //create new_row
            form = $('<form action="setting/honors.php"></form>'), // create form
            inner_row = $('<div></div>').addClass('new_inner_row box_shadow padding'), //create new_inner_row
            award_id = $('<input>').attr({type: 'hidden', name: 'awd_id', value: 0}), // awd_id input field
            title_label = $('<label> عنوان: </label>'), // title label
            award_title = $('<input>').attr({
                type: 'text',
                name: 'title',
                placeholder: 'عنوان افتخار',
                class: 'new_input new_medium_input'
            }), // awd_title
            sport_label = $('<label> رشته: </label>'), // sport label

            medal_label = $('<label> مدال: </label>'), // medal label

            desc_row = $('<div class="new_row"></div>'), // new row for description
            desc_label = $('<label> توضیحات: </label><br>'), // label for description
            desc_text = $('<textarea></textarea>').attr({
                class: 'new_long_input',
                name: 'description',
                id: 'description',
                cols: 30,
                rows: 10,
                placeholder: 'توضیحات'
            }),
            edit_btn = $('<button>اعمال تغییرات</button>').addClass('btn btn_blue edit honor'),
            remove_btn = $('<button>حذف </button>').addClass('btn btn_orange remove honor'),
            description = desc_row.append(desc_label, desc_text),
        // append all items into main_row
            new_honor = main_row.append(form.append(inner_row.append(award_id, title_label, award_title, sport_label, sport_select[0], medal_label, medal_select[0], description, edit_btn, remove_btn)));
        // insert a form for new_honor insert to the page, after new_honor
        return new_honor;
    }

    /************************************* end of honors.php ****************************************************/
    /************************************* add_new_class.php for teachers ****************************************************/
    $('.new_class').on('click', function () {
        var select = create_select_element_from_array(week_day, {name: 'week_day[]'}),
            edit = $('<div></div>').addClass('edit-box').append('<div class="icon remove_icon new_time_remover"></div>'),
            day = $('<span> روز </span>'),
            hour = $('<span> از ساعت </span>'),
            to = $('<span> تا </span>'),
            start_time = $('<input type="text" name="start_time[]">').addClass('new_input four_digit_input').attr({
                placeHolder: 'شروع',
                required: 'required'
            }),
            end_time = $('<input type="text" name="end_time[]">').addClass('new_input four_digit_input').attr({
                placeholder: 'اتمام',
                required: 'required'
            });
        var new_inner_row = $('<div></div>').addClass('new_inner_row new_time_adder').append(edit, day, select, hour, start_time, to, end_time);
        $(this).parent('.new_inner_row').before(new_inner_row);
    });
    $('[name="new_class_form"]').on('submit', function (e) {
        e.preventDefault();
        var th = this,
            thi = $(this),
            method = $(this).attr('method'),
            url = $(this).attr('action'),
            data = new FormData(th);
        $.ajax({
            url: basePath + url,
            data: data,
            type: method,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false
        }).done(function (data) {
            console.log('done', data);
            if (data != false) {
                th.reset();
                thi.parents('.box:first').after(data).next('.box').hide().show('slow');
                show_message({text: 'ثبت باشگاه و کلاس با موفقیت انجام شد'});
                $('.new_time_adder').slideUp('fast', function () {
                    $(this).remove();
                });
//                console.log('my')
            } else {
                show_message({
                    text: 'ثبت باشگاه ناموفق بود لطفا دوباره تلاش کنید',
                    message_type: 'error_message'
                });
            }
        }).complete(function (data) {
            console.log('complete',data);
        })
    });
    $('.box').on('click', '.new_time_remover', function () {
        /* remove a set of inputs that add new time on class, this inputs => (day, start_time, end_time) */
        $(this).parents('.new_inner_row.new_time_adder').remove();
    });
    $('.profile_field').on('click', '.remove_class', function () {
        var par = $(this).parents('table'),
            id = par.attr('id'),
            th = $(this);
        //console.log(par);
        if (!confirm('آیا از حذف کلاس اطمینان دارید؟')) return;
        /*
         swal({
         title: "آیا از حذف کلاس اطمینان دارید؟",
         text: "با حذف این کلاس دیگر قادر به بازگرداندن اطلاعات آن نخواهید بود",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         confirmButtonText: "حذف!",
         closeOnConfirm: true
         }, function (isConfirm) {
         return isConfirm;
         });
         */
        $.ajax({
            url: basePath + 'teacherPhpFiles/remove_class.php',
            data: {action: 'delete', class_id: id},
            type: 'post',
            dataType: 'json'
        }).done(function (responseData) {
            if (responseData != false) {
                show_message({text: 'حذف مورد با موفقیت انجام شد', time: 4000})
//                swal({type:'حذف مورد با موفقیت انجام شد', time:4000})
                par.slideUp(function () {
                    $(this).remove();
                })
            } else {
                show_message({type: 'مورد حذف نشد لطفا دوباره تلاش کنید', time: 4000, message_type: 'error_message'})
            }
//            console.log('done',responseData)
        }).complete(function (responseData) {
            console.log('complete', responseData)
        })
    });
    /************************************* end of add_new_class.php for teachers ****************************************************/
    /************************************* teacher-classes-edit.php ****************************************************/
    /************************************* end of teacher-classes-edit.php ****************************************************/
    /************************************* user_sport_setting.php ****************************************************/
    $('.std_spt_setting').on('click', function (e) {
        e.preventDefault();
        var form = $(this).parent('form'),
            data = form.serialize() + '&submit=true',
            url = form.attr('action'),
            method = form.attr('method'),
            options,
            callback,
            time = 3000;
        //console.log(data);
        var ajax = $.ajax({
            url: basePath + url,
            type: method,
            dataType: 'json',
            data: data
        }).done(function (data) {
            console.log(data);
            if (data.error) {
                options = {text: data.error, message_type: 'error_message', time: time};
            } else {
                // console.log(options);
                options = {time: time}
//                console.log(options);
//                callback = function () {
//                    window.location = "/" + form.attr('id');
//                };
            }
        }).error(function (data) {
        }).complete(function (data) {
            console.log(data);
            show_message(options, callback);
        })
    });
    /************************************* end of user_sport_setting.php ****************************************************/
    /************************************* manager/sport_setting ******************************************/
    var last_description;
    $('.sport_description').on('click', function () {
        var th = $(this),
            sp_description_holder = $('div#sport_description'),
            sport_description = sp_description_holder.text();
        //content_editable = $('div#sport_description').prop('contentEditable');
        //sp_description_holder.attr('contenteditable',true) ;
        /*if(content_editable) {
         sp_description_holder.attr('contenteditable',true) ;
         th.html($('<span></span>').addClass('glyphicon glyphicon-check').after(' اعمال تغییرات'));
         last_description = sp_description_holder.text();
         return;
         } else {
         if(last_description == sport_description) {
         console.log('contenteditable == true');
         show_message('تغییرات با موفقیت اعمال شد');
         th.html($('<span></span>').addClass('glyphicon glyphicon-edit').after('ویرایش '));
         sp_description_holder.attr('contenteditable',false);
         console.log('yes equal');
         return;
         }
         }*/
        //return;
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: {sport_description: sport_description}
        }).done(function (response_data) {
            if (response_data.done != false) show_message({text: 'تغییرات با موفقیت اعمال شد'})
            else show_message({text: response_data.message, message_type: 'error_message'})
//            console.log('done',response_data)
        }).complete(function (response_data) {
//            console.log('comp',response_data)
        })
    });

    // event types
    $('.content').on('click', '.edit_evt_type, .edit-evt-job', function () {
        //:input,form:radio,form:checkbox
        var parent = $(this).parents('.evt_type_holder');
        parent.find(':input,:radio,:checkbox').removeAttr('disabled');
        //parent.find(':input[type=submit]').toggleClass('btn_black btn_blue');
        //$(this).parents('.evt_type_holder')
    });

    $('form#new_evt_type, form#new_event_job').on('submit', function (e) {
        e.preventDefault();
        //return true;
        var th = $(this),
            thi = this,
            url = th.attr('action'),
            method = th.attr('method'),
            data = th.serialize();
        //console.log(data);return;
        $.ajax({
            url: basePath + url,
            data: data,
            type: method,
            dataType: 'json'
        }).done(function (data) {
            //console.log('done',data);
            show_message({text: data.message.text, message_type: data.message.class});
            if (data.done) {
                thi.reset();
                th.parents('fieldset').after(data.data);
            }
        }).complete(function (data) {
            console.log(data);
        });
    });

    $('.content').on('submit', 'form.evt_type, form.evt-job', function (e) {
        e.preventDefault();
        var th = $(this),
            url = th.attr('action'),
            method = th.attr('method'),
            data = th.serialize() + '&id=' + th.attr('id');
        var event_type_ajax = $.ajax({
            url: basePath + url,
            data: data,
            type: method,
            dataType: 'json'
        }).done(function (data) {
            console.log('done', data);
            if (data.done) {
                th.children().attr('disabled', 'disabled');
            }
            show_message({text: data.message.text, message_type: data.message.class});
        }).complete(function (data) {
            console.log(data);
        });
    });
    $('body').on('click', '.remove_evt_type, .remove_evt_job', function () {
        var th = $(this),
            grand_parent = th.parents('.evt_type_holder'),
            parent = grand_parent.find('form'),
            url = parent.attr('action'),
            method = parent.attr('method');
        if (!confirm('آیا از حذف اطمینان دارید؟')) return;
        //return;
        $.ajax({
            url: basePath + url,
            data: {id: parent.attr('id')},
            type: method,
            dataType: 'json'
        }).done(function (data) {
            //console.log('done', data);
            if (data.done) {
                grand_parent.slideUp('normal', function () {
                    $(this).remove();
                })
            }
            show_message({text: data.message.text, message_type: data.message.class});
        }).complete(function (data) {
            //console.log(data);
        });

    })
    // belt
    $('.content').on('click', '.edit_belt', function () {
        //:input,form:radio,form:checkbox
        var parent = $(this).parents('.belt_holder');
        parent.find(':input,:radio,:checkbox').removeAttr('disabled');
        //parent.find(':input[type=submit]').toggleClass('btn_black btn_blue');
        //$(this).parents('.evt_type_holder')
    });

    $('form#new_belt').on('submit', function (e) {
        e.preventDefault();
        var th = $(this),
            thi = this,
            url = th.attr('action'),
            method = th.attr('method'),
            data = th.serialize();
        $.ajax({
            url: basePath + url,
            data: data,
            type: method,
            dataType: 'json'
        }).done(function (data) {
//            console.log('done',data);
            show_message({text: data.message.text, message_type: data.message.class});
            if (data.done) {
                thi.reset();
                th.parents('fieldset').after(data.data);
            }
        }).complete(function (data) {
//            console.log('comp', data);
        });
    });
    $('.content').on('submit', 'form.belt', function (e) {
        e.preventDefault();
        var th = $(this),
            url = th.attr('action'),
            method = th.attr('method'),
            data = th.serialize() + '&id=' + th.attr('id');
        var event_type_ajax = $.ajax({
            url: basePath + url,
            data: data,
            type: method,
            dataType: 'json'
        }).done(function (data) {
            console.log('done', data);
            if (data.done) {
                th.children().attr('disabled', 'disabled');
            }
            show_message({text: data.message.text, message_type: data.message.class});
        }).complete(function (data) {
            console.log(data);
        });
    });
    $('body').on('click', '.remove_belt', function () {
        var th = $(this),
            grand_parent = th.parents('.belt_holder'),
            parent = grand_parent.find('form'),
            url = parent.attr('action'),
            method = parent.attr('method');
        if (!confirm('آیا از حذف اطمینان دارید؟')) return;
        //return;
        $.ajax({
            url: basePath + url,
            data: {id: parent.attr('id')},
            type: method,
            dataType: 'json'
        }).done(function (data) {
            //console.log('done', data);
            if (data.done) {
                grand_parent.slideUp('normal', function () {
                    $(this).remove();
                })
            }
            show_message({text: data.message.text, message_type: data.message.class});
        }).complete(function (data) {
            console.log('comp', data);
        });

    });

    $('#sport_logo').on('submit', function (e) {
        var th = $(this),
            $this = this,
            data = new FormData(this);
        //console.log('s');
        //return false;
        e.preventDefault();
        show_message({text: 'عملیات بارگذاری آغاز شد لطفا شکیبا باشید', message_type: 'success_message', time: 1500});
        $.ajax({
            url: basePath + $(this).attr('action'),
            type: 'POST',
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                th.find('[type=submit]').attr('disabled', 'disabled');
                //$('.cover-photo-img').attr('style',"background-image: url(/_css/img/loading.gif)").css({'background-position':'center center','background-size': '32px'});
            }
        }).done(function (response) {
            console.log(response);
            if (response.done) {
                console.log(response)
                show_message({text: 'تصویر مورد نظر با موفقیت قرار داده شد', message_type: 'success_message'},function(){
                    $('.sport-logo-image').attr('src', response.data.url.large);
                });
                //$('.cover-photo-img').style({'background-image' : 'url('+response.data.url+')'})
                //$('.cover-photo-img').attr('style',"background-image: url("+response.data.url.thumb+")")
                //$('.cover-photo-img').parent('.venobox').attr('href',response.data.url.large);
            }
            else {
                show_message({text: 'عملیات ناموفق بود لطفا دوباره امتحان نمایید', message_type: 'error_message'});
            }
            $this.reset();
            //console.log('done',response)
        }).complete(function (response) {
            th.find('[type=submit]').removeAttr('disabled');
            //console.log('complete',response)
        });
    });
    /************************************* end of manager/sport_setting ******************************************/
    /************************************* homePage.php ****************************************************/
//    $('#signInHeaderBtn').click(singInHeader);
    $('#signInHeaderBtn').on('click', function (e) {
        singInHeader(e);
    });
    /* all pages menu dropdown list */
    /* IDe '#profMenu' baraye haman menuii k dar balaye safahate karbarane Log in karde gharar
     * darad ast. haman k dar kenare akse kochake karbar gharar darad */
    $('#profMenu span:first-child').click(function () {
        $(this).next('ul').toggle();
        var profMenuTimeout;
        $(this).next('ul').mouseleave(function () {
            var ul = $(this);

            function hideProfMenu(ul) {
                profMenuTimeout = setTimeout(function () {
                    ul.hide();
                }, 1000);
            }

            hideProfMenu(ul);
        }).mouseover(function () {
            clearTimeout(profMenuTimeout);
        });
    });
    /**************************************  End of homepage.php ***************************************/
    /**************************************  control/index.php   ***************************************/
    $('.conf_detail').on('click', function () {
        $(this).parents('.Cnoti').find('.CnotiDetails').slideToggle(400);
    });
    $('.conf').on('click', function () {
        //console.log($(this).attr('class'));
        var th = $(this),
            parent = th.parents('.Cnoti'),
            sentData = {
                action: function () { //set a function in object that cat return true or false
                    if (th.hasClass('conf_accept')) return 1; //if user clicked on a button with conf_accept class return true
                    else if (th.hasClass('conf_deny')) return 0; //if user clicked on a button with conf_deny class return false
                },
                userID: parent.attr('id')
            };
        //console.log(sentData.action());
        sentData.action = sentData.action(); //set action to true or false
        //sentData.action = true; //set action to true or false

        console.log(sentData);
//        console.log(parent.attr('data-dest'));
//        return;
        $.ajax({
            url: basePath + 'control/' + parent.attr('data-dest'),
            data: sentData,
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function () {
                th.attr('disabled', 'disabled');
            }
        }).done(function (data) {
            console.log('DONE', data);
            //return;
            // if process is not done
            if (data.done) {
                parent.slideUp("slow");
                setTimeout(function () {
                    parent.remove();
                }, 700);
            }
            show_message({text: data.message.text, message_type: data.message.class});
//                 parent.slideUp(300).fadeOut(400).remove();
//                else console.log(data);
        }).complete(function (data) {
            console.log(data);
        })
    })
    /**************************************  end of control/index.php   ***************************************/
    /**************************************  teprofile.php ***************************************/
    $('.cover_image_btn').on('click', function (e) {

    })
    $('.modal img').on('click', function (e) {
        e.stopImmediatePropagation();
    })
    $('.modal_close, .modal').on('click', function () {
        $('.modal, .modal_description, .modal_image,.slidshow_btn').hide();
        $('body').removeClass('modal-show');
    });
    $('.show_modal_description').on('click', function (e) {
        //e.stopImmediatePropagation();
        e.stopPropagation();
        $('.modal').show().find('.modal_description').slideDown(500);
        $('body').addClass('modal-show');
        return false;
    });
    $('body').on('click', '.rqtbtn', function () {
        follow_unfollow($(this));
        return false;
        //$('#rqtbtn').trigger('click');
    })
    $('#rqtbtn').on('click', function (e) {
        //e.stopImmediatePropagation();
        follow_unfollow($(this));
        return false;
    });
    function follow_unfollow(th) {
        var SendData = new Object();
        SendData.foingID = th.data('user');
        //console.log(SendData);
//        return;
        $.ajax({
            url: basePath + 'follow/follow.php',
            data: SendData,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
//                th.attr('disabled','disabled');
            },
            success: function (data) {
                //console.log(data);
                if (data.done) {
                    th.find('span.glyphicon').toggleClass('glyphicon-check glyphicon-remove');
                    th.find('span.glyphicon').hasClass('glyphicon-remove') ? th.find('span.text_holder').text('لغو دنبال') : th.find('span.text_holder').text('دنبال');
                    $('follower_num').text(data.follower);
                }
            },
            error: function (data) {
                //console.log(data);
            },
            complete: function () {
                //console.log(data);
                setTimeout(function () {
                    th.removeAttr('disabled');
                }, 20000);
            }
        });
    }

    /**************************************  End of teacher-profile.php ***************************************/
    /**************************************  admin/index.php ***************************************/
//$('.management .paginate a').on('click', function(e){
//    e.preventDefault();
//    $('input[name=page]').val($(this).attr('href'));
////    alert($('input[name=page]').val());
////    return;
//    $('#searchPeople').trigger('click');
//})
///*
    $('body').on('click', '.management .paginate a', function (e) {
        e.preventDefault();
        advanceSearchAdminPeople($(this));
    });
//    */
//    /*
//     $('form[action^=management]').on('submit', function(e){
     $('#management_form').on('submit', function(e){
         e.preventDefault();
         advanceSearchAdminPeople($(this));
     })
//*/
    function advanceSearchAdminPeople(th) {
//        console.log(th.attr('class'));
//        console.log(th.text());
//        $('.content').children('.paginate,.person_management').remove();//.html(data);
//        return;
//        var page = th.attr('href') ? th.attr('href') : 1,
        var must_remove_element = $('.content').children(':not(.panel,.subMenu)'),
            url = th.attr('action'),
            prepareData = {
                stateResult: $('#stateResult').val(),
                countyResult: $('#countyResult').val(),
                fieldResult: $('#fieldResult').val(),
                branchResult: $('#branchResult').val(),
                page: th.attr('href'),
                limit: $('#limit').val(),
                level: $('#superior').val(),
                name: $('#name').val(),
                submit: true
            },
            ajax = $.ajax({
                type: 'POST',
                dataType: 'json',
                url: url,
                data: prepareData,
                beforeSend: function () {
//                    $('.loading').prependTo('.content');
                    must_remove_element.slideUp();
                    $('.panel').after().append($(".loading").addClass('loading_middle'));
                    $('.loading').show();
                }
            });
//        ajax.beforeSend();
        ajax.done(function (data) {
            console.log('success', data);
            must_remove_element.remove();//.html(data);
            if (data) {
                $('.content').append(data);
            } else {
//                    $('.peopleBox').append(data['message']);
            }
        }).error(function () {
//                $('.lastPeople').append($('div').addClass('peopleBox').text('خطایی رخ داده لطفا دوباره امتحان کنید.').css('text-align','center'));
        }).complete(function (data) {
            console.log('complete', data);
            $('.loading').hide();
        })
    }

//*/
    /**************************************  End of index.php ***************************************/
    /**************************************  setting.php ***************************************/
    $('.teacher_pic').on('click', function () {
        teacher_clicked_image = $(this);
        teacher_clicked_image_src = $(this).attr('src');
    });
    $('#teacher_images').change(function (e) {
//        console.log(e);
        var th = $(this),
            data = new FormData(this);
        data.append('current_img', teacher_clicked_image_src);
        $.ajax({
            url: basePath + "teacherPhpFiles/teImageUpload.php",
            type: "POST",
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                teacher_clicked_image.attr('src', loading);
            },
            success: function (data) {
//                console.log(data);
                if (data['success']) {
                    var d = new Date();
                    teacher_clicked_image.attr('src', data['success']);//+'?'+d.getTime());
                    $(".warnning center").html('');
                    show_message({text: 'تصویر با موفقیت تغییر یافت', time: 4000});
                } else {
                    teacher_clicked_image.attr('src', teacher_clicked_image_src);
                    show_message({text: 'عملیات ناموفق. لطفا دوباره امتحان کنید', message_type: 'error_message'});
//                    $(".warnning center").html(data);
                }
            },
            complete: function (data) {
                $(".loading").hide();
//                console.log('complete');
//                console.log(data)
            },
            error: function (data) {
                teacher_clicked_image.attr('scr', teacher_clicked_image_src);
                show_message({text: 'عملیات ناموفق. لطفا دوباره امتحان کنید', message_type: 'error_message'});
//                console.log('error');
//                console.log(data)
//                $(".warnning center").html('عملیات ناموفق. لطفا دوباره امتحان کنید');
            }
        });
    });
    $('.teacher_pic .delete').on('click', function () {
        confirm('آیا مطمئن هستید؟');
    });
    /*
    (function ($) {
        $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

        $('.tab ul.tabs li a').click(function (g) {
            var tab = $(this).closest('.tab'),
                index = $(this).parent('li').index();

            tab.find('ul.tabs > li').removeClass('current');
            $(this).parent('li').addClass('current');

            tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
            tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

            g.preventDefault();
        });
    })(jQuery);
    //*/
    /**************************************  End of setting.php ***************************************/
    /**************************************  End of events/create/index ***************************************/
    $('[name=requirements]').on('click', function () {
        var show = false;
//        console.log(show);
        $(this).each(function (i, v) {
            if (v.checked && v.value == 1) show = true;
            console.log('checked', v.checked, v.id)
        })
        if (show) $('.requirements').slideDown(600);
        else $('.requirements').slideUp(600);
    });
    $('[name=allow_sports]').on('click', function () {
        /*if($(this).val() == 1) $('.allowsSports').slideDown(600);
         else $('.allowsSports').slideUp(600);*/
        var show = false;
//        console.log(show);
        $(this).each(function (i, v) {
            if (v.checked && v.value == 1) show = true;
//            console.log('checked', v.checked, v.id)
        })
        if (show) $('.allowsSports').slideDown(600);
        else $('.allowsSports').slideUp(600);
//        console.log(show);
//        console.log($(this).find(':checked').attr('value'));
    })
    $('[name=allow_sports]:checked,[name=requirements]:checked').trigger('click');

    /**************************************  End of events/create/index.php ***************************************/
    /**************************************  events table management ***************************************/
    $('.new_weight').on('click', function () {
        $(this).parent().before('<div class="new_inner_row">عنوان وزن <input class="new_input" name="weight_class_name[]" placeholder="مثال: وزن 1 یا سبک وزن">از   <input type="text" class="new_input four_digit_input" name="min_weight[]" required="required" placeholder="کیلوگرم"> تا <input type="text" class="new_input four_digit_input" name="max_weight[]" required="required" placeholder="کیلوگرم"></div>');
    });
    $('[name="evt_weight_age"]').on('submit', function (e) {
        e.preventDefault();
        var th = $(this),
            method = $(this).attr('method'),
            url = $(this).attr('action'),
            data = th.serialize();
        $.ajax({
            url: basePath + url,
            data: data,
            type: method,
            dataType: 'JSON'
            //contentType: false,
            //cache:       false,
            //processData: false
        }).done(function (data) {
            //console.log('done', data);
            var message_class = data.success ? 'success_message' : 'new_errors';
            show_message({text: data.message, message_type: message_class});
            if (data.success) {
                //refresh page
                location.reload();
            }
        }).complete(function (data) {
            //console.log('comp',data);
        })
    })
    $('.evt_table_close').on('click', function () {
        confirm('پس از تایید شما دیگر قادر به ویرایش جدول ها نخواهید بود، آیا از بسته شدن جدول ها اطمینان دارید؟');
        var data = {event_id: $(this).attr('id')};
        $.ajax({
            url: basePath + $(this).data('ajax-dest'),
            data: data,
            type: 'post',
            dataType: 'json'
        }).done(function (response) {
            console.log(response)
            if (response.done) {
                location.reload();
            }
            show_message({text: data.message.text, message_type: data.message.class});
        }).complete(function (response) {
            console.log(response)
        });
    })
    /**************************************  End of events table management ***************************************/
    /**************************************  event card management ***************************************/
    $('form#person-card-setup').on('submit', function (e) {
        e.preventDefault();
        //return true;
        var th = $(this),
            thi = this,
            url = th.attr('action'),
            method = th.attr('method'),
            data = th.serialize();
        //console.log(data);return;
        $.ajax({
            url: basePath + url,
            data: data,
            type: method,
            dataType: 'json'
        }).done(function (data) {
            console.log('done', data);
            show_message({text: data.message.text, message_type: data.message.class});
            if (data.done) {
                thi.reset();
                $('.ui.cards').prepend(data.data);
            }
        }).complete(function (data) {
            console.log(data);
        });
    });

    $('.person-card-remove').on('click', function (e) {
        e.preventDefault();
        //return true;
        var th = $(this),
            thi = this,
            url = th.attr('href'),
            data = {id: th.data('id')};
        //console.log(data);return;
        $.ajax({
            url: basePath + url,
            data: data,
            type: 'post',
            dataType: 'json'
        }).done(function (data) {
            console.log('done', data);
            show_message({text: data.message.text, message_type: data.message.class});
            if (data.done) {
                th.parents('.ui.card').remove();
            }
        }).complete(function (data) {
            console.log(data);
        });
    })

    $('.print').on('click', function(e) {
        e.preventDefault();
        window.print();
        //print($('.ui.card'));
    });

    function print($element) {
        //var printContents = document.getElementById(divName).innerHTML;
        var head = $('head').html();
        //console.log(head);
        var printContents = $element.html();
        //console.log(printContents);
        //return false;
        w = window.open();
        w.document.innerHTML = head + printContents;
        w.print();
        w.close();
    }
    /**************************************  End of event card management ***************************************/

    /**************************************  posts, likes and comments ***************************************/
    $('body').on('keypress', '.cm_insert', function (e) {
        console.log(e)
        var th = $(this),
            body = th.text(),
            post_id = th.parents('.news_part').attr('id'),
            char_code = e.keyCode || e.charCode;
        if (char_code != 13 || body == '' || post_id == undefined || e.shiftKey) {
            return;
        }
        //console.log('sss')
        $.ajax({
            data: {body: body, entity_id: post_id, bundle: 'post'},
            dataType: 'json',
            type: 'POST',
            url: basePath + 'post/comments.php'
        }).done(function (data) {
            console.log('done', data);
            /*
             if data.done = true
             contentEditable = false;
             some icons must be added to this entry
             a little flash must be happen in this entry
             -------------
             new entry must be open
             */
            if (data.done) {
                //th.before($('<div class="new_comment"></div>').html(data.data));
                th.text('').trigger('blur');
            }
            if (data.message) {
                show_message({text: data.message.text, message_type: data.message.class})
            }
        }).complete(function (data) {
            console.log('comp', data);
        });
    })
    $('body').on('click', '.like_insert', function (e) {
        e.preventDefault();
        var th = $(this),
            entity_id = th.parents('.news_part').attr('id'),
            url = th.attr('href'),
            bundle = th.data('bundle');
        $.ajax({
            url: basePath + url,
            type: 'post',
            data: {entity_id: entity_id, bundle: bundle},
            dataType: 'JSON'
        }).done(function (data) {
            console.log('success', data);
            if (data.done) {
                /*th.text(data.data.text);
                $('.likes_number').text(data.data.likes);*/
                //$('.like-box').replaceWith(data.data);
                th.parent('.like-box').replaceWith(data.data);
            }
        }).complete(function (data) {
            console.log('complete', data);
        });
    })

    //$('body').on('click', '.ui.dropdown i', function() {
    //   $(this).trigger('click');
    //});
    $('body').on('click', '.cm_action', function (e) {
        var th = $(this),
            action = th.data('action'),
            cm_id = th.data('cm-id'),
            data = {cm_id: cm_id, action: action};
        //th.parents('.comment').slideUp(800);
        //th.parents('.comment').show(250);
        //console.log(data);return;
        if (action == 'remove' && !confirm('آیا از حذف نظر اطمینان دارید؟')) {
            return;
        }
        $.ajax({
            data: data,
            dataType: 'json',
            type: 'POST',
            url: basePath + 'post/comments.php'
        }).done(function (data) {
            /*
             if data.done = true
             contentEditable = false;
             some icons must be added to this entry
             a little flash must be happen in this entry
             -------------
             new entry must be open
             */
            if (data.done) {
                //console.log('done', data);
                //th.before($('<div class="new_comment"></div>').html(data.data));
                //th.text('').trigger('blur');
                if (action == 'remove') {
                    th.parents('.comment').slideUp('slow', function () {
                        $(this).remove();
                        console.log($(this));
                    });
                }
                else if (action == 'confirm') {
                    th.parents('.comment').hide().fadeIn(300);
                    th.remove();
                }
                //else if(action)
            }
            if (data.message) {
                show_message({text: data.message.text, message_type: data.message.class})
            }
        }).complete(function (data) {
            console.log('comp', data);
        });
    })
    /*
     $('body').on('click', '.cm_confirm', function(e){
     alert('confirm');
     })
     $('body').on('click', '.cm_remove', function(e){
     alert('remove');
     })
     */
    /**************************************  end of likes and comments ***************************************/

});

/**************************************  Plugins ***************************************/
/*****************  put asterisk at the end of required fields Plugin *****************/
(function ($) {
    $.fn.required_asterisk = function (options) {
        $(this)
            .after('<span class="required_asterisk">&#42;</span>')
            .on('input', function () {
                var asterisk = $(this).next('.required_asterisk');
                if ($(this).val().length != 0) asterisk.hide();
                else asterisk.show();
            });
    }
})(jQuery);
/*****************  end of put asterisk at the end of required fields Plugin*****************/
/*****************  inputFile Validation Plugin *****************/
(function ($) {
    $.fn.fileInputVal = function (options) {
        var defaultSetting = {
            'showingError': '.errors',
            'errorMessage': 'فایل وارد شده صحیح نمی باشد',
            'showErrFlag': true,
            'acceptExt': 'image/*',
            'relBtn': '.inFileMirror',
            'relBtnAct': 'click',
            'fileAdrShow': '.inFileMirror',
            'fileAdrShowFlag': false
        }
        if (options) $.extend(defaultSetting, options);

        var showingError = defaultSetting['showingError'],
            errorMessage = defaultSetting['errorMessage'],
            showErrFlag = defaultSetting['showErrFlag'],
            acceptExt = defaultSetting['acceptExt'],
            relBtn = defaultSetting['relBtn'],
            relBtnAct = defaultSetting['relBtnAct'],
            fileAdrShow = defaultSetting['fileAdrShow'],
            fileAdrShowFlag = defaultSetting['fileAdrShowFlag'],
            th = $(this),
            id = th.attr('id');
        $(this).attr('accept', acceptExt);
        $(relBtn).on(relBtnAct, function (e) {
            th.trigger('click');
            e.preventDefault();
        }).css('cursor', 'pointer');
        th.change(function () {
            var val = th.val();
            if (val.length > 0) {
                switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
                    case 'jpg':
                    case 'png':
                        $('#ext' + id).remove();
//                    alert("an image");
                        break;
                    default:
                        $(this).val('');
                        if (showErrFlag) $(showingError).append($('<li>').html(errorMessage).attr('id', 'ext' + id));
                        // error message here
//                        alert("not an image");
                        return false;
                        break;
                }
            }
        });
    }
})(jQuery);
/*****************  end of inputFile validation plugin *****************/
/*****************  validation Plugin *****************/
(function ($) {
    $.fn.InputValidation = function (options) {
        var showingError, button, error, disable, inputForceClass, validationClass;

        function mainFunction(th) {
            error = '';
            var txt;
            $(button).removeAttr('disabled');
            var fieldDataType = findValidation(th);
            var id = th.attr('id');
            /*id sheye feli ra migirad*/
            var fieldName = $('[for=' + id + ']').text();
            /*har input 100% bayad yek label dashte bashad in khat matne an label ra migirad.*/
            if (fieldName == '') fieldName = ' مقدار فیلد';
            /* check mikone k fieldMaxLength hatman tarif shode bashad va agar ham tarif shode bod barabare '' nabashad */
            var value = th.val();
            var defaultSetting = {
                'fieldMaxLength': 50,
                'fieldMinLength': 0,
                'fieldDataType': fieldDataType,
                'showingError': '.errors',
                'button': '.btnForce',
                'remainChars': false,
                'remainCharsShow': '',
                'allChars': false
            }
            if (options) {
                $.extend(defaultSetting, options);
            }
            var fieldMaxLength = defaultSetting['fieldMaxLength'],
                fieldMinLength = defaultSetting['fieldMinLength'],
                remainChars = defaultSetting['remainChars'],
                allChars = defaultSetting['allChars'];
            fieldDataType = defaultSetting['fieldDataType'];
            showingError = defaultSetting['showingError'];
            button = defaultSetting['button'];
            if (th.attr('maxlength') != undefined && th.attr('maxlength') != '') fieldMaxLength = th.attr('maxlength');
            if (value.length > fieldMaxLength) {
                value = value.substr(0, fieldMaxLength);
                th.val(value);
                error = fieldName + ' نمیتواند بزرگتر از ' + fieldMaxLength + ' حرف باشد';
                $('li#lengthError' + id).remove();
                $(showingError).append('<li id="lengthError' + id + '">' + error + '</li>');
            } else {
                $('li#lengthError' + id).remove();
            }
            if (allChars || fieldDataType == 'password') {
                /* agara karakter haye khasi ra baraye hazf kardan morede nazar dashte bashad in if ejra mishavad */
                /*if(fieldVarRefuse != '') {
                 var pattEnd = fieldVarRefuse;
                 } else {*/
//                var pattEnd = '';
//                }
            } else {
                var pattMain = /([\\\|,\$\^\?\+\(\)\[\]\{\}\*<>&~`!;:#%=+'"\/])|(^[\s])([\s&]{2})/gi;
                var mainTxt = th.val();
                txt = mainTxt.replace(pattMain, '');
                switch (fieldDataType) {
                    case'number':
                        var pattNumber = /\D/g;
                        txt = txt.replace(pattNumber, '');
                        error = 'لطفا فقط از اعداد استفاده کنید';
                        break;
                    case'email':
                        var spaceRemove = /\s/gi;
                        txt = txt.replace(spaceRemove, '');
                        error = 'استفاده از این علامت در ایمیل مجاز نمی باشد';
                        var pattEmail = /^[\w-]+(\.[\w-]+)*@([\w-]{3,}\.)+[a-zA-Z]{2,7}$/;
                        var mch = txt.match(pattEmail);
                        $('li#emailError' + id).remove();
                        $('li#emailAccept' + id).remove();
                        if (mch) {
                            $('.show').text(mch[0]);
                            var emailNote = fieldName + ' وارد شده صحیح می باشد';
                            $(showingError).append('<li style="color: #00ae00" id="emailAccept' + id + '">' + emailNote + '</li>');
                        } else {
//                            $(button).attr({disabled:'disabled'});
                            error = fieldName + ' وارد شده صحیح نمی باشد';
                            $(showingError).append('<li id="emailError' + id + '">' + error + '</li>');
                            $(button).attr({disabled: 'disabled'});
                        }
                        break;
                    case'name':
                        var pattName = /\w|\d|[-\.@]/g;
                        txt = txt.replace(pattName, '');
                        if (mainTxt !== txt) error = 'لطفا فقط از حروف پارسی استفاده نمایید';
                        break;
                    case'username':
                        var pattUsername = /[^(a-z|A-Z|.|_|\d)]/gi;
                        txt = txt.replace(pattUsername, '');
                        error = 'فقط حروف انگلیسی و علامت های . و _ مجاز می باشد';
                        break;
                }

                /* agar mainTxt != txt bashad yanni hatman y chizi taghir karde k dar in sorat varede in if mishavim
                 * va peyghame monaseb ra namayesh midahim */
                if (mainTxt !== txt) {
                    th.val(txt);
                    $('li#InvalidCharsError' + id).remove();
                    if (error != '') $(showingError).append('<li id="InvalidCharsError' + id + '">' + error + '</li>');
                } else $('li#InvalidCharsError' + id).remove();
            }
            /**** matched Email or Password ****/
            if (fieldDataType == 'email' || fieldDataType == 'password') {
                var matchedClass = th.attr('class');
                var pattEmailMatched = /\bmatched\w+/gi;
//                var pattEmailMatched = /\bmatched(\S*(\s?|\'))/gi; OK
                var emailMatched = matchedClass.match(pattEmailMatched);
                if (emailMatched) {
                    th.removeClass(emailMatched[0]);
                    $('.' + emailMatched[0]).each(function (index, val) {
                        if (val.value != value && value != '' && val.value != '') {
                            if (fieldDataType == 'email') {
                                error = 'آدرس های ایمیل متفاوت هستند';
                            } else {
                                error = 'رمزهای عبور متفاوت هستند';
                            }
                            $('li.MatchError' + emailMatched[0]).remove();
                            $(showingError).append('<li class="MatchError' + emailMatched[0] + '">' + error + '</li>');
                            $(button).attr({disabled: 'disabled'});
                        } else {
                            $('li.MatchError' + emailMatched[0]).remove();
                        }
                    });
                    th.addClass(emailMatched[0]);
                }
            }

            /**** in ghesmat check mikonad k chand character baghi mande ast ****/

            /* in if check mikonad k agar 'remainChars=true' digar baraye hameye inputha tedad characterha ra mishmorad */
            if (!remainChars) {
                /* in ghesmat check mikonad k aya inpute feli classi ba name 'remainCharsActive' darad ya kheyr? */
                var checkChars = th.attr('class');
                var pattRemainChars = /\bremainCharsActive\b/;
                var remainCharsActive = checkChars.match(pattRemainChars);
                /* agar inpute feli classi ba name */
            } else {
                remainCharsActive = true;
            }
            if (remainCharsActive) {
                value = th.val();
                var remainCharsShow = defaultSetting['remainCharsShow'];
                var remainCharsLength = fieldMaxLength - value.length;
                if (remainCharsShow == '') {
                    remainCharsShow = showingError;
                    var note = remainCharsLength + ' حرف باقی مانده';
                    if (remainCharsLength < fieldMaxLength - 1) {
                        $('li#remainError' + id).remove();
                        $(remainCharsShow).append('<li id="remainError' + id + '">' + note + '</li>');
                    }
                } else {
                    if ($(remainCharsShow).attr('type'))  $(remainCharsShow).val(remainCharsLength);
                    $(remainCharsShow).text(remainCharsLength);
                }
            }
            var minError = minLength(th, error, showingError, button, fieldMinLength);
//            alert(error + minError);
            return error + minError;
        }

        $(this).bind('keyup blur', function () {
            var dis = mainFunction($(this));
            if (dis == '') {
                disable = false;
                var defaultInputForce = {
                    inputForceClass: 'blonError',
                    validationClass: 'validate'
                };
                if (options) {
                    $.extend(defaultInputForce, options);
                }
                inputForceClass = defaultInputForce['inputForceClass'];
                validationClass = defaultInputForce['validationClass'];
                $(validationClass).each(function (ind, val) {
                    if ($(this).attr('required') == 'required' && $(this).css('display') != 'none') {
                        var reqValue = $(this).val();
                        if (reqValue.length == 0) disable = true;
                    }
                });
            } else {
                disable = true;
            }
            if (disable) {
                var error = 'لطفا تمامی موارد را تکمیل نمایید';
                $('li#btnDisabledError').remove();
                $(showingError).append('<li id="btnDisabledError">' + error + '</li>');
                $(button).attr({disabled: 'disabled'});
//                console.log(button);
            }
            else {
                $('li#btnDisabledError').remove();
                $(button).removeAttr('disabled');
            }
        });
        $(this).focus(function () {
            mainFunction($(this));
        })

        $('[required=required]').blur(function () {
            if (inputForceClass == undefined) return;
            if ($(this).attr('required') && $(this).val().length == 0) {
                $(this).addClass(inputForceClass);
            } else {
                console.log($(this));
                console.log(inputForceClass);
                $(this).removeClass(inputForceClass);
            }
        });
        $(this).blur(function () {
            var id = $(this).attr('id');
            /*var fieldName = $('[for='+id+']').text();
             if (fieldName == '') fieldName = 'فیلد';*/
            $('li#lengthError' + id).remove();
            $('li#remainError' + id).remove();
            $('li#InvalidCharsError' + id).remove();
            $('li#minLengthError' + id).remove();
            $('li#emptyError' + id).remove();
            $('li#emailAccept' + id).remove();
        });
    }
    function findValidation(input) {
        // validatione bar roye classhaii ba in num ha anjam mishavad
        var thisClass = input.attr('class');
        /* agar attribute class baraye input tarif nashode bashad, va ya khali bashad varede in if mishavad
         * k dar in if ba estefade az type check mishavad k che noe validation bayad sorat begirad */
        if (thisClass == undefined || thisClass == '') {
            return matched = findValidationFromType(input);
        } else {
            var classPatt = /(usernameValidation|emailValidation|numberValidation|textValidation|nameValidation)/gi;
            var matched = thisClass.match(classPatt);
            if (matched) {
                return matched[0].replace('Validation', '');
            } else return matched = findValidationFromType(input);
        }
    }

    function findValidationFromType(input) {
        var matched;
        var classPatt = /(email|number|text|password)/gi;
        var thisType = input.attr('type');
        /* agar attribute type tarif shode bashad va khali ham nabashad in if anjam mishavad
         * k dar an chon type vojod darad, ba estefade az type noe validation moshakhas mishavad */
        if (thisType != undefined && thisType != '') {
            var matched = thisType.match(classPatt);
            if (matched != null) return matched = matched[0];
            /* agar typee mojod ba hich yek az mavarede classPatt motabeghad nadashte bashad
             * validation username sorat migirad */
            else return matched = 'username';
        } else {
            /* agar type khali bashad (type='') digar hich rahi baghi nmimanad bjoz inke validatione text
             * sorat bgirad k hamin else mibashad */
            return matched = 'text';
        }
    }

    function minLength(th, error, showingError, button, fieldMinLength) {
        if (th.attr('minlength') != undefined && th.attr('minlength') != '') fieldMinLength = th.attr('minlength');
        var value = th.val();
        var id = th.attr('id');
        var fieldName = $('[for=' + id + ']').text();
        if (fieldName == '') fieldName = 'فیلد';
        if (value != '' && fieldMinLength) {
            if (value.length < fieldMinLength) {
                error = fieldName + ' باید حداقل  ' + fieldMinLength + ' حرف باشد';
                $('li#minLengthError' + id).remove();
                $(showingError).append('<li id="minLengthError' + id + '">' + error + '</li>');
                $(button).attr({disabled: 'disabled'});
            } else {
                error = '';
                $('li#minLengthError' + id).remove();
            }
        }
        return error;
    }
})(jQuery);
/*****************  end of validation Plugin *****************/
/*****************  scrollToTop Plugin *****************/
(function ($, window) {
    $.fn.scrollToTop = function (options, css) {
        var defaultCss = {
                position: 'fixed',
                right: '50px',
                display: 'none',
                cursor: 'pointer',
                bottom: '60px',
                width: '22px',
                height: '23px',
                //'border-radius': '100%',
                //background:      '#f54f36 url("/_css/icon/up_arrow_white.png") no-repeat center center'
                'background-color': '#f54f36',
                'color': '#fff'
//            'background-image': 'url("/_css/icon/up_arrow.png")',
//            'background-position' : 'center ',
//            'background-repeat' : 'no-repeat'
            },
            defaultOptions = {
                scrollPos: 0,
                showScrollBtnPos: 200,
                text: ''
            },
            scrollToTopObject = $(this);
        if (css) {
            $.extend(defaultCss, css);
        }
        if (options) {
            $.extend(defaultOptions, options);
        }
        scrollToTopObject.css(defaultCss).html($('<span class="glyphicon glyphicon-chevron-up"></span>'));
        $(window).scroll(function () {
            var current_sct = $(this).scrollTop();
            (current_sct > defaultOptions['showScrollBtnPos']) ? scrollToTopObject.fadeIn('slow') : scrollToTopObject.hide();
        });
        scrollToTopObject.on('click', function (e) {
            e.stopImmediatePropagation();
            $('body').animate({
                scrollTop: defaultOptions['scrollPos']    /* scroll */
            }, 1000);
        });
        $('body').on('click wheel', function () {
            $(this).stop();
        });
        scrollToTopObject.on('mouseover', function () {
            $(this).css({'background-color': '#06BF8E'})
        }).on('mouseout', function () {
            $(this).css({'background-color': defaultCss['background-color']});
        })
    }
})(jQuery, window);
/*****************  end of scrollToTop Plugin *****************/
/*****************  put comma on cost Plugin *****************/
(function ($) {
    /* a plugin for separate comma from an input that is for cost input
     * just add this line to out code to active plugin
     * ('selector').commaSeparate();
     * */
    $.fn.commaSeparate = function () {
        var th = $(this), value;
        th.on('input', function () { //on input event execute next line code
            value = $(this).val();
            value = value.replace(/\D/g, ""); // remove everything except numbers
            th.val(value.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")); // put , on every 3digit on input
        }).on('keyup', function () {
            // this next two lines is for handling placeholder text in rtl languages
            if (value != '') th.css('direction', 'ltr'); // change input field direction to ltr if has any number
            else th.css('direction', 'rtl'); // change input field direction to rtl if it's empty
        });
    }
})(jQuery);
/*****************  end of put comma on cost Plugin *****************/
/***************** collapse **********************/
(function ($) {
    $.fn.collapse = function (options) {
        var th = $(this),
            collapse = th.find('[data-toggle=collapse]'),
            open = collapse.data('open-collapse'),
            defaultSetting = {
                first_classes: 'icon arrow_up_white_icon',
                toggle_classes: 'arrow_up_white_icon arrow_down_white_icon'
            };
        if (options) $.extend(defaultSetting, options);
//        if(open) collapse.addClass(defaultSetting['first_classes']).css('cursor', 'pointer');
//        else collapse.addClass('icon arrow_down_white_icon').css('cursor', 'pointer');
        collapse.on('click', function () {
            $(this).toggleClass(defaultSetting['toggle_classes']).parent().siblings().slideToggle(500);
            /*var open = $(this).data('open-collapse') ? 0 : 1;
             if (open)
             $(this).parent().siblings().slideDown(500);
             else
             $(this).parent().siblings().slideUp(500);
             $(this).data('open-collapse', open);*/
        });
    }
})(jQuery);
/***************** end of collapse **********************/
/***************** placeholder **********************/
(function ($) {
    $.fn.placeholder = function (selector) {
        var th = $(this),
            placeholder_text = th.attr('data-placeholder');
        th.text(placeholder_text);
        $('body').on('blur', selector, function () {
            if ($(this).text() == '') {
                $(this).text(placeholder_text)
            }
        });
        $('body').on('focus', selector, function () {
            if ($(this).text() == placeholder_text) {
                $(this).text('');
            }
        })
    }
})(jQuery);
/***************** end of placeholder **********************/
/***************** read more btn **********************/
(function ($) {
    $.fn.rdm = function ($read_more_parent) {
        var rdm_btn = $(this),
            rdm_parent;
        $('body').on('click', '.rdmb', function (e) {
            rdm_parent = $(this).parents($read_more_parent);
            //console.log(rdm_parent)
            //e.stopImmediatePropagation();
            e.preventDefault();
            //rdm_parent.css({color: 'red'})
            rdm_parent.animate({
                height: '100%'
            }, 'slow');
            // /rdm_parent.animate({
            //    height: 'auto'
            //},2005);
            $(this).hide();
        })
    }
})(jQuery);
/***************** read more btn end **********************/
/******************** highlight links in path ***************/
(function ($) {
    $.fn.pathHighlight = function (selector, classes) {
        var th = (this),
            loc = window.location.pathname,
            breadcrumb,
            href,
            parent,
            breadcrumb_array = [],
            breadcrumb_temp_link = '';
        breadcrumb = loc.split('/');
        breadcrumb.shift();
        $(this).each(function (index, value) {
            href = $(this).attr('href');
            if (href == undefined) return;
            href = href.replace(/\//gi, '');
            parent = $(this).parent();
            //console.log(breadcrumb)
            for (var i = 0; i < breadcrumb.length; i++) {
                breadcrumb_temp_link = '';
                for (var j = 0; j <= i; j++) {
                    breadcrumb_temp_link += breadcrumb[j];
                }
                breadcrumb_array[i] = breadcrumb_temp_link;
            }
            //console.log(breadcrumb_array);
            if ($.inArray(href, breadcrumb_array) != -1) {
                $(this).addClass(classes); // add current class to this <a> parent
            }
        });
    }
})(jQuery);
/****************** highlight links in path end **************/
/**************************************  end of Plugins ***************************************/
/**************************************  FUNCTIONS DEFINITION ***************************************/
var calcDaysUntilEventInterval;

function show_tooltip(show, e, tooltip_content) {
    /* if at list one tooltip in body element exists do nothing,
     * else create one of them */
    var tooltip = $('.tooltip');
    if ($('body > .tooltip').attr('class') == undefined) {
        tooltip = $('<div></div>').toggleClass('tooltip');
        $('body').prepend(tooltip);
        $('.tooltip').not('body > .tooltip').remove();
    }
//    console.log(tooltip.css('display'));
    if (!show) {
        tooltip.hide();
        return;
    }
    tooltip.empty().append(tooltip_content);
    var
        tooltip_height = tooltip.height(),
        tooltip_width = tooltip.width(),
        first_top = (e.pageY - tooltip_height - 60),
        top = first_top < 0 ? e.pageY + 20 : first_top,
        left = (e.pageX - (tooltip_width / 1.6)) > $(window).width() ? e.pageX - (tooltip_width) : e.pageX - (tooltip_width / 1.6),
        css_options = {
            top: top,
            left: left
        };
    /* add ^ or v to tooltips */
    if (first_top < 0)  tooltip.prepend($('<span></span>').toggleClass('tooltip_up')); // if tooltip is on the top of page prepend ^
    else  tooltip.append($('<span></span>').toggleClass('tooltip_down')); // if tooltip is not on the top of page append v
    tooltip.css(css_options).show();
}

function loading_img(element, src, options) {
    src = src ? src : '/_css/img/loading.gif';
    var default_setting = {
            display: 'none'
        },
        display = element.css('display');
    default_setting['display'] = display != 'none' ? 'none' : 'block';
    if (options) $.extend(default_setting, options);
    element.attr('src', src).css(default_setting);
}

/*function similar(str1, str2) {
 var lengthA = str1.length,
 lengthB = str2.length,
 equivalency = 0,
 minLength = (lengthA > lengthB) ? lengthB : lengthA,
 maxLength = (lengthA < lengthB) ? lengthB : lengthA;
 for(var i = 0; i < minLength; i++) {
 if(str1[i] == str2[i]) {
 equivalency++;
 }
 }
 var weight = equivalency / maxLength;
 return (weight * 100) *//*+ "%"*//*;
 }*/

function removeFirstChild(th) {
    /* removing first child in select elements if it's value is 0
     * @th : select element object */
    if (th.find(':first-child').val() == 0 && th.val() != 0) th.find(':first-child').remove();
}

/**
 * show a message in the top of the page
 * @param options {
 *   text: showing text as message content <br/>
 *   message_type: determine that which type message is, like success_message,error_message,notification,warning or ... <br/>
 *   time: determine that how much the message shows on the top and then disappear <br/>
 * @param callback_function
 */
function show_message(options, callback_function) {
    /*var notification = new NotificationFx({
     message : '<span class="icon icon-settings"></span><p>Your preferences have been saved successfully. See all your settings in your <a href="#">profile overview</a>.</p>',
     layout : 'bar',
     effect : 'exploader',
     ttl : 9000000,
     type : 'notice', // notice, warning or error
     onClose : function() {
     bttn.disabled = false;
     }
     });
     notification.show();
     return;*/
    var default_opt = {text: 'تغییرات با موفقیت اعمال شد', message_type: 'success_message', time: 4000},
        exec_callback_function = function () {
            $('.sticky_message').slideUp();
            if ($.isFunction(callback_function)) callback_function();
        };
    if (options) $.extend(default_opt, options);
    $('.sticky_message').remove();
    $('body').append($('<div></div>')
        .prepend(default_opt['text'])
        .addClass('sticky_message ' + default_opt['message_type'])
        .append($('<div></div>').addClass('progress')/*,$('<div class="icon close_icon"></div>')*/))
    $('.sticky_message .progress').animate({
            width: 0
        }, default_opt['time'], exec_callback_function
    );
}

function create_select_element_from_array(my_array, attrs, custom_options) {
    var result;
    result = $('<select></select>').attr(attrs);
    $.each(my_array, function (index, value) {
        result.append($('<option></option>').val(index).text(value));
//            console.log(index, value);
    });
//        console.log(result)
    return result;
}
/************* Date functions *************/
/* dateFieldInput vaghti farakhani mishavad field ha ra modiriat mikonad */
function dateFieldInput(th, e, options) {

    /* in ghesmat baraye in ezafe shod k har bar k in tabe ejra shod check konad bbinad kodam field ha khali hastand
     * k focus ra bar roye an field gharar dahad */
    if (e.type == 'mouseenter') {
        th = th.find('input:first-child');
        th.focus();
        console.log(th);
        var length = th.val().length,
            maxLength = th.attr('maxlength');
        if (length == maxLength) th.next().focus();

    }
    th.find('input').bind('keyup blur focus click', function (e) {
        var type = e.type;

        /* agar harkodam az in se field khali bod bar roye an focus mikonad */
        if (type == 'keyup') {
            var length = $(this).val().length;
            var maxLength = $(this).attr('maxlength');
            var id = $(this).attr('id');
            if (length == maxLength) $(this).next().focus();
        }
        /*if(type == 'focus') {
         if($(this).attr('id') == 'year'){
         if($(this).val().length == 0) {
         $(this).val('13');
         }
         }
         }*/
        var defaultSetting = {
            'showDateTag': '.datefieldShow'
        };
        if (options) $.extend(defaultSetting, options);
        var perDate = checkDateFull($(this), defaultSetting['showDateTag'], type);
        if (perDate) {
            defaultSetting = {
                'pDate': perDate,
                'format': 'DD / MMMM',
                'showDateTag': '.datefieldShow',
                'calType': 'perDate'
            };
            if (options) {
                $.extend(defaultSetting, options);
            }
            var t = persianToGregorian(defaultSetting);
            if (t['error']) {
                $(defaultSetting['showDateTag']).text(t['error']);
            } else {
                $(defaultSetting['showDateTag']).text(t[defaultSetting['calType']]);
            }
        }
    });


    /* in tabe check mikonad k se fielde marbot be zaman por hastand ya na??
     * va agar fieldi khali nabod meghdare majmoe in se field ra bar migardanad */
    function checkDateFull(th, showDateTag, type) {

        var inputPerDate, dateper = new Array();


        /* agar har kodam az fieldha blur shode bashand in if ejra mishavad*/
        if (type == 'blur') {
            var inputID = th.attr('id');
            var inputMax = th.attr('maxlength');
            var val = th.val();
            if (inputID != 'year') {
                /* agar harkodam az fieldhaye day ya month blur shode bashand dar hali k
                 * 1 addad dar anha gharar darad khodash yek 0 dar avale anha ghadar midahad */
                if (val.length == inputMax - 1) th.val('0' + val);
            } else if (inputID == 'year') {
                /* agar fielde year blur shode bashad dar hali k 2 ta addad dar an gharar darad
                 * khodash yek 13 dar avale an migozarad */
                if (val.length == inputMax - 2) {
                    th.val('13' + val);
                    if (checkFull(th)) return checkFull(th);
                }
            }
        }

        /* check mikonad bbinad field ha khali hastand ya kheyr*/
        function checkFull(th) {
            var count = 0;
            for (var i = 3; i >= 1; i--) {
                var child = th.parent().find('input:nth-child(' + i + ')');
                var childMax = child.attr('maxlength');
                var childVal = child.val();
                if (childVal == '') return false;
                else if (childVal != '' && childVal.length >= childMax) {
                    /* agar addad haye 00 ya 000 ya 0000 dar har kodam az field ha gharar dashte bashad in ejra mishavad. */
                    if (childVal != 00 || childVal != 000 || childVal != 0000) {
                        if (childVal != 0) {
                            dateper[i] = childVal;
                            count++;
                            if (count == 3) return dateper[3] + '/' + dateper[2] + '/' + dateper[1];
                        }
                    } else {
                        $(showDateTag).text('مقدار وارد شده صحیح نمی باشد');
                    }
                }
            }
        }

        var ret = checkFull(th);
        if (ret) return ret;
    }
}

function persianToGregorian(options) {
    /* ************in tabe tarikhe jalali(persian) ra be Miladi va niz b shamsi ba formate darkhasti tabdil mikonad *******************
     ****** parametri k mipazirad ingone ast
     ***
     {'pDate: yek tarikh b shamsi 1393/05/21 ya 1393-05-21 k agar in ghesmat khali bashad tarikhe felie system bargasht dade mishadav,
     'format: tarkibi k mishavad baraye namayeshe tarikhe shamse estefade kard masalan YYYY/MM/DD ya YYYY/MMMM/DD ya ...
     }
     ***
     ****** khoroji haye in tabe ingone ast
     ***
     * persianToGregorian()['greDate']  // bazgardandane tarikh b miladi
     persianToGregorian()['perDate'] // bargardandane tarikhe shamsi ba formate delkhah pishfarz ingone ast روز/ماه(ب حروف)/سال
     */
    var mod, pDate, format, error;
    var arra = new Array();
    /* yek araye baraye mod tarif mikonim, k dar vaghe mod bayad yki az ina bashad
     * yani meghdari k karbar vard mikonad bayad ya ba estefade az / va ya - az ham joda shode bashand */
    var modArray = new Array('/', '-');
    var d = new persianDate();
    d.formatPersian = false;
    var e = d.format();
    var setting = {
        'pDate': e,
        'format': 'DD/MMMM/YYYY',
        'mod': '/'
    };
    if (options) {
        $.extend(setting, options);
    }
    pDate = setting['pDate'];
//    mod = setting['mod'];
    format = setting['format'];
    function modSet(pDate, modArray) {
        $.each(modArray, function (index, value) {
            var patt1;
            patt1 = new RegExp(value);
            if (patt1.test(pDate)) {
                mod = value;
            }
        });
        return mod;
    }

    mod = modSet(pDate, modArray);
    arra = pDate.split(mod);
    $.each(arra, function (index, value) {
        arra[index] = parseInt(value);
    });
    if (arra) {
        // check mikone bbine tarikhe vard shode sahihe ya na.
        if (arra[0] > persianDate().year() - 4) {
            error = 'امکان ثبت نام کودکان کمتر از چهار سال وجود ندارد.';
        } else if (arra[0] < 1310) {
            error = 'سال وارد شده مورد قبول نمی باشد.';
        } else if (arra[1] > 12) {
            error = 'ماه وارد شده صحیح نمیباشد';
        } else if (arra[2] > persianDate([arra[0], arra[1]]).daysInMonth()) {
            error = 'روز وارد شده صحیح نمی باشد';
        } else {
            error = false;
        }
    }
    var persianUnix = persianDate(arra).unix();
    persianUnix *= 1000; //change second to milisecond
    var gDate = new Date(persianUnix); // get date in
    gDate = gDate.toLocaleDateString();
    perDate = persianDate(arra);
    perDate.formatPersian = true;
    var perDate = perDate.format(format);
    var ret = {
        greDate: gDate,
        perDate: perDate,
        error: error
    };
    return ret;
}
/************* end of Date functions *************/
/* sign in */
function singInHeader(e) {
    e.stopPropagation();
    var main = $('.mainHeader'),
        height = main.css('height');
    if (height != '135px') {
        e.preventDefault();
        main.css({height: '135'});
        $('.signInArea').slideDown(700);
        $(".signIn_btn").css({'margin-left': "20px"});
        $('.btn').addClass('btn_medium').css('bottom', '23%');
        $('.logo').css({'margin-top': '40px'})
    }
}
/**************************************  End of FUNCTIONS DEFINITION ***************************************/
/******** search morph *********/
(function () {
    var morphSearch = $('#morphsearch'),
        input = morphSearch.find('input.morphsearch-input'),
        ctrlClose = morphSearch.find('span.morphsearch-close'),
        isOpen = isAnimating = false,
    // show/hide search area
        toggleSearch = function (evt) {
            // return if open and the input gets focused
            if (evt.type.toLowerCase() === 'focus' && isOpen) return false;

            var offsets = morphsearch.getBoundingClientRect();
            if (isOpen) {
                morphSearch.removeClass('open');
                $('body').removeClass('modal-show');
                input.attr('placeholder', 'جستجو ...')
                // trick to hide input text once the search overlay closes
                // Todo: hardcoded times, should be done after transition ends
                if (input.value !== '') {
                    setTimeout(function () {
                        morphSearch.addClass('hideInput');
                        setTimeout(function () {
                            morphSearch.removeClass('hideInput');
                            //input.value = '';
                        }, 300);
                    }, 500);
                }

                input.blur();
            }
            else {
                morphSearch.addClass('open');
                $('body').addClass('modal-show');
                input.attr('placeholder', 'نام فرد و یا کلمه از متنی که میخواهید جستجو کنید را وارد نمایید')
            }
            isOpen = !isOpen;
        };

    // events
    input.on('focus', toggleSearch);
    ctrlClose.on('click', toggleSearch);
    // esc key closes search overlay
    // keyboard navigation events
    $(document).on('keydown', function (ev) {
        var keyCode = ev.keyCode || ev.which;
        if (keyCode === 27 && isOpen) {
            toggleSearch(ev);
        }
    });


    /***** for demo purposes only: don't allow to submit the form *****/
    morphSearch.find('button[type="submit"]').on('click', function (ev) {
        ev.preventDefault();
    });
})();
/******** end search morph *********/

/********** remove title on hover *************/
(function ($) {
    $.fn.title_remover = function (options) {
        $(this).on('mouseenter', function(){
            $(this).attr('data-title', $(this).attr('title'));
            $(this).attr('title', '');
        })
            .on('mouseout', function(){
                $(this).attr('title', $(this).attr('data-title'));
            });
    }
})(jQuery);

/********** end of remove title on hover *************/