/**
 * Created by Raminli on 9/14/2015.
 */
//loadScript('wnJqueryFormValidate/languages/wnValidate_plugin_lang.js');
(function ($) {
    $.fn.wnPlugin = function (options) {

        // if noting is selected
        if (!this.length) {
            // TODO:select a form element in page if exists
            //var form = $('form');
            if (options && options.debug && window.console) {
                console.warn("Nothing selected, can't validate, returning nothing.");
            }
            return;
        }

        var form,
            elements,
            defaultSetting = {
                fieldMaxLength: 35,
                fieldMinLength: 0,
                errorShow: '',
                button: '',
                remainChars: false,
                remainCharsShow: '',
                noValidate: false,
                allowedChars: false,
                disallowedChars: false,
                language_letters: 'fa-en',
                language_num: 'fa-en'
            },
            settings = defaultSetting,
            errors = {},
            button,
            requirements,
            invalid_chars = /([\\\|\$\^\?\+\(\)\[\]\{}\*<>&~`!;#%='"])|(^[\s])|([\s]{2,})/gi;

        if (options) {
            settings = $.extend(defaultSetting, options);
        }

        form = !$(this).is('form') ? $(this).parents('form') : $(this);

        if(form.attr_existence_check('noValidate', true)) {
            return;
        }
        // if selected element is form or is a child of a form.
        if (form.length) {

            elements = $(this).is('form') ? $(this).find(':input') : $(this);

            form.on('submit', function (e) {
                var enable_button = check_requirements(requirements);
                if (!$.isEmptyObject(errors) || !enable_button) {
                    e.preventDefault();
                    button.attr('disabled', 'disabled');
                }
            });

            // Add novalidate attribute if HTML5.
            form.attr("novalidate", "novalidate");

            // find related button to the form (if a form exist )
            button = !settings.button ? form.find(':submit') : !settings.button;
        }

        //select related button at the end
        button = button.length ? button : $(settings.button);

        // disable related button at the beginning
        button.attr('disabled', 'disabled');

        requirements = elements.map(function () {
            if ($(this).is('.required, [required=required], [required]')) {

                return $(this);
            }
        });

        elements.each(function () {
            var name = $(this).attr_existence_check('name'),
                elem;

            elem = name ? $('[name="' + name + '"]') : $(this);

            elem.off();
            if (elem.is('input:not([type=radio], [type=checkbox], select), textarea')) {
                elem.on('input focus', function () {
                    // find all validations which set for an element
                    validations = $(this).findValidation();
                    // if element has no validation then exit from this event handler
                    if (!validations) return true;
                    var element = $(this),
                        value = element.val(),
                        input_lang = element.findLanguage(settings.language_letters),
                        element_id = element.attr_existence_check('id') ? element.attr_existence_check('id') : null,
                        field_name = element.get_field_name(),
                        no_lang = element.attr_existence_check('data-no-lang', true),
                    // a unique id for each input which we want to work with
                        id = element.attr_existence_check('data-validationId'),
                        validations,
                        changeable_value = value,
                    // errorShow variable: find element which
                        errorShow = settings.errorShow,
                        errors_addresses = {},
                        remainNum,
                        remainCharsShow = element.attr_existence_check('data-remainCharShow') ? element.attr_existence_check('data-remainCharShow') : settings.remainCharsShow,
                        beep,
                        special_lang = input_lang,
                        special_regexp = invalid_chars,
                        accept_chars = element.acceptable_chars(settings.allowedChars),
                        disallowed_chars = element.disallow_chars(settings.disallowedChars),
                        max_length = element.maxlength(settings.fieldMaxLength),
                        min_length = element.minlength(settings.fieldMinLength),
                        range = element.get_wnrange(),
                    // noValidate variable: when this variable is true no validation perform on this element
                        noValidate = element.noValidateElement() || settings.noValidate,

                    // remainChars variable: when this variable is true, count remain character on user input
                        remainChars = settings.remainChars;
                    // set id when user focus on an element for the first time.
                    if (!id) {
                        var date = new Date();
                        id = date.getTime();
                        element.attr('data-validationId', id);
                    }

                    // set remain chars
                    var remainChars_index = $.inArray('remainChars', validations);
                    if (remainChars_index > -1) {
                        remainChars = true;
                        // remove persian element from validation array
                        validations.splice(remainChars_index, 1);
                    }

                    $.each(validations, function (index, validate_name) {
                        if (noValidate) return false;
                        switch (validate_name) {
                            case 'number':
                                special_regexp = /,/gi;
                                // if it has no input class persian set default to english for number
                                if (!element.findLanguage('', true)) {
                                    special_lang = settings.language_num;
                                }
                                special_lang += '-num';

                                var num_validate = number_validate(changeable_value);

                                if ($.inArray('comma', validations) > -1) {
                                    special_regexp = /[]/gi;
                                    // remove everything except numbers then put , on every 3digit on input
                                    changeable_value = changeable_value.replace(/\D/g, "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                                }


                                // if number validated successfully.
                                if (num_validate !== true) {
                                    changeable_value = num_validate;
                                }

                                break;

                            case 'phone':
                                var phone_regexp = /(^00989|^\+?989|^9)/gi;
                                changeable_value = changeable_value.replace(phone_regexp, '09');

                                break;
                            case 'email':
                                special_regexp = /[\s]/gi;
                                // if it has no in input class persian set default to english for number
                                if (!element.findLanguage('', true)) {
                                    special_lang = settings.language_letters;
                                }

                                // email address must be lowercase
                                changeable_value = changeable_value.toLowerCase();

                                var em_validate = email_validate(changeable_value);

                                // if email validated successfully.
                                if (em_validate) {
                                    changeable_value = em_validate;
                                    errors_addresses[id + '_email'] = false;
                                }
                                else {
                                    // error about email validation of this element
                                    errors_addresses[id + '_email'] = {};
                                    errors_addresses[id + '_email']['field_name'] = field_name;
                                }

                                break;


                            case 'username':
                                // it has no error to show

                                special_regexp = /[@\s]/;

                                if (!element.findLanguage('', true)) {
                                    special_lang = settings.language_letters;
                                }

                                break;

                            case 'name':
                            case 'address':
                                if (validate_name == 'name') {
                                    special_regexp = /[،\d@_-]/g;
                                }
                                else if (validate_name == 'address') {
                                    special_regexp = /[@_]/g;
                                }

                                break;

                            //case 'password':
                            // do something
                            //break;
                            case 'url':
                                special_regexp = /[\s]/gi;
                                // if it has no in input class persian set default to english for number
                                if (!element.findLanguage('', true)) {
                                    special_lang = settings.language_letters;
                                }

                                // email address must be lowercase
                                changeable_value = changeable_value.toLowerCase();

                                var validated_url = url_validate(changeable_value);
                                // if email validated successfully.
                                if (!validated_url) {
                                    // error about email validation of this element
                                    errors_addresses[id + '_url'] = {};
                                    errors_addresses[id + '_url']['field_name'] = field_name;
                                }
                                else {
                                    errors_addresses[id + '_url'] = false;
                                }
                                break;
                            case 'match':
                                // do something
                                break;
                        }


                        if(!no_lang) {
                            changeable_value = convertLang(changeable_value, special_lang);
                            changeable_value = changeable_value.replace('vXhg', 'R');
                        }
                        // if it has any invalid chars remove them and play beep
                        beep = changeable_value;
                        changeable_value = changeable_value.replace(invalid_chars, '');
                        changeable_value = changeable_value.replace(special_regexp, '');

                        // Handling allowed or disallowed chars
                        if (accept_chars) {
                            // remove other chars
                            changeable_value = changeable_value.replace(accept_chars, '');
                        }

                        if (disallowed_chars) {
                            // remove this chars
                            changeable_value = changeable_value.replace(disallowed_chars, '');
                        }

                        beep != changeable_value ? playSound() : null;
                    });

                    if (range) {
                        var rang_error = false,
                            range_value = changeable_value.replace(/\D/gi, '');
                        if (range.min && parseFloat(range_value) < range.min) {
//                            changeable_value = range.min;
                            rang_error = true;
                        }
                        else if (range.max && parseFloat(range_value) > range.max) {
//                            changeable_value = range.max;
                            rang_error = true;
                        }

                        if (rang_error) {
                            // error about email validation of this element
                            if (range.max && range.min) {
                                errors_addresses[id + '_range'] = {};
                                errors_addresses[id + '_range']['field_name'] = field_name;
                                errors_addresses[id + '_range']['min'] = range.min;
                                errors_addresses[id + '_range']['max'] = range.max;
                            }
                            else if (range.max) {
                                errors_addresses[id + '_range_max'] = {};
                                errors_addresses[id + '_range_max']['field_name'] = field_name;
                                errors_addresses[id + '_range_max']['max'] = range.max;
                            }
                            else if (range.min) {
                                errors_addresses[id + '_range_min'] = {};
                                errors_addresses[id + '_range_min']['field_name'] = field_name;
                                errors_addresses[id + '_range_min']['min'] = range.min;
                            }
                        }
                        else {
                            errors_addresses[id + '_range'] = false;
                            errors_addresses[id + '_range_min'] = false;
                            errors_addresses[id + '_range_max'] = false;
                        }

                    }

                    // length section.
                    /*
                     * if we have max length and out min length is bigger that minlength then, user never can rich to minlength and always see an error
                     * here we check that this not happen to user. and if it's going to happen, we set minlength equal to maxlenght
                     */
                    min_length = parseFloat(max_length) < parseFloat(min_length) ? parseFloat(max_length) : parseFloat(min_length);
                    if (!noValidate) {
                        // check min length limit.
                        if (changeable_value.length < min_length) {
                            // error about minlength validation of this element
                            // if minlength == maxlength we must have exact length
                            var length_error = parseFloat(max_length) == parseFloat(min_length) ? '_exactlength' : '_minlength';
                            errors_addresses[id + length_error] = {};
                            errors_addresses[id + length_error]['field_name'] = field_name;
                            errors_addresses[id + length_error]['length'] = min_length;
                        }
                        else {
                            errors_addresses[id + '_minlength'] = false;
                            errors_addresses[id + '_exactlength'] = false;
                        }
                    }

                    if (remainChars) {
                        remainNum = max_length - changeable_value.length;
                        element.remain_chars_show(remainCharsShow, remainNum);
                    }
                    // length section end.
                    element.val_cursor(changeable_value);

                    // handling all errors: showing and hiding errors
                    if (!noValidate) {
                        errors = error_handler(element, errorShow, errors, errors_addresses);
                    }
                    $.isEmptyObject(errors) && check_requirements(requirements) ? button.removeAttr('disabled') : button.attr('disabled', 'disabled');
                })
                ///*
                elem.on('blur', function () {
                    // find all validations which set for an element
                    validations = $(this).findValidation();

                    // if element has no validation then exit from this event handler
                    if (!validations) return true;
                    var element = $(this),
                        value = element.val(),
                        input_lang = element.findLanguage(settings.language_letters),
                        element_id = element.attr_existence_check('id') ? element.attr_existence_check('id') : null,
                        field_name = element.get_field_name(),
                    // this id is: a unique id for each input which we want to work with
                        id = element.attr_existence_check('data-validationId'),
                        validations,
                        changeable_value = value,
                    // errorShow variable: find element which
                        errorShow = settings.errorShow,
                        errors_addresses = {},
                        remainNum,
                        remainCharsShow = element.attr_existence_check('data-remainCharShow') ? element.attr_existence_check('data-remainCharShow') : settings.remainCharsShow,
                        beep,
                        special_lang = input_lang,
                        special_regexp = invalid_chars,

                        max_length = element.maxlength(settings.fieldMaxLength),
                        min_length = element.minlength(settings.fieldMinLength),

                    // noValidate variable: when this variable is true no validation perform on this element
                        noValidate = element.noValidateElement() || settings.noValidate,

                    // remainChars variable: when this variable is true, count remain character on user input
                        remainChars = settings.remainChars;


                    // set remain chars
                    var comma_index = $.inArray('comma', validations);
                    if (comma_index > -1) {
                        changeable_value = changeable_value.replace(/,/gi, '');
                    }


                    // length section end.
                    element.val(changeable_value);

                    // handling all errors: showing and hiding errors
                    if (!noValidate) {
                        errors = error_handler(element, errorShow, errors, errors_addresses);
                    }

                    $.isEmptyObject(errors) && check_requirements(requirements) ? button.removeAttr('disabled') : button.attr('disabled', 'disabled');
                });
                //*/
                ///*
                // integrate with mlKeyboard
                if ($(this).attr_existence_check('data-wnkeyboard') && !elem.next('img').hasClass('wnKeyboard') && $.fn.mlKeyboard) {
                    var first_validation = elem.findValidation(),
                        number_index = $.inArray('number', first_validation),
                        lang = number_index > -1 ? 'language_num' : 'language_letters',
                        language = elem.findLanguage(settings[lang]).replace(/.*-/gi, ''),
                        default_disallowed = settings.disallowedChars ? settings.disallowedChars : [],
                        default_allowed = settings.allowedChars ? settings.allowedChars : [],
                        accept_chars = elem.attr_existence_check('data-allowed-chars') ? elem.attr_existence_check('data-allowed-chars').replace(/,|/gi).split('') : default_allowed,
                        disallowed_chars = elem.attr_existence_check('data-allowed-chars') ? elem.attr_existence_check('data-allowed-chars').replace(/,|/gi).split('') : default_disallowed,
                        id = $(this).attr('id');
                    $(this).mlKeyboard({
                        layout: language
                        //num: number_index > -1,
                    });
                    $(this).after($('<img>').addClass('wnKeyboard').attr('src', 'data:image/png;base64,iVBOnw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABUUlEQVRIS7WVgW3CMBBFPxOUDdoNygbQDbpBGaGdoLABI8AG3aBlA9iAEcoErR66jw4piVESR7LkOOf/v//dORNVfiaV8VUimEn6lXTqK6REsAjgn1oEK0nbWicAfCrpva969rVZtAzvv4aAdxFgCySDn7YToPx1MHpYhM/PY4A1YBw5waEiwQGCv5HU0zM0JoMcbphngr0kgijPz7CP929JL5JoNsTkeVFbJsAqap7qYVgRSljnOyR5DkEnoQnOAfAUzQUY80dJnMyFcIz+mMc6hO70RqtNkO1B+T1WrcNO2wQBeLb1sm4CbkuU8BHlnpuINWyzWvLEnnzLWhhJxtYbAr9/hFUkdpeqActcGcQ6V2+xMccSRzwY1xOYICfMFuAzA9W+trOF7M2xjrvkZMw+aCxZCPhjPRQLul/AGQIuNRJIKY75IHxT+mUOJqxO8A+Kw14fVG3NggAAAABJRU5ErkJggg=='));
                }
                //*/
            }
            else if ($(this).is(':radio, :checkbox, select')) {
                elem.on('change', function () {
                    var enable_button;

                    enable_button = check_requirements(requirements);

                    $.isEmptyObject(errors) && enable_button ? button.removeAttr('disabled') : button.attr('disabled', 'disabled');
                });
            }
        });
    }

    $.extend($.fn, {
        /**
         *
         * @returns {Array|{index: number, input: string}|*}
         *  return an array which contains all possible validations on this element
         */
        findValidation: function () {
            var element = $(this),
                validations,
                classes,
                extra_validation;

            // if data-wnValidations is exists that means this element was validated before.
            if (validations = element.attr_existence_check('data-wnValidations')) {

                return validations.split('|');
            }

            if (!validations) {
                validations = element.findValidationInputClass();
            }

            if (!validations) {
                validations = element.findValidationInputName();
            }

            if (!validations) {
                validations = element.findValidationInputType();
            }

            if (classes = element.attr_existence_check('class')) {
                extra_validation = classes.match(/remain_?Chars/gi);
            }

            var has_phone_validation = $.inArray('phone', validations);
            var has_number_validation = $.inArray('number', validations);
            if (has_phone_validation > -1 && has_number_validation <= -1) {
                validations.push('number');
            }


            // if we found validations put it on element as a data attribute so we can access theme later faster.
            if (validations || extra_validation) {
                validations = $.isArray(validations) ? validations : [];
                extra_validation = $.isArray(extra_validation) ? extra_validation : [];

                validations = $.merge(validations, extra_validation);

                // change all regex items to their equal words
                var validations_string = validations.join('|');
                validations_string = validations_string.replace(/first_name|last_name|fname|lname|firstname|lastname|surname|family/gi, 'name');
                validations_string = validations_string.replace(/national_code|number/gi, 'number');
                validations_string = validations_string.replace(/user_name|userName/gi, 'username');
                validations_string = validations_string.replace(/pass|pass_word|password/gi, 'password');
                validations_string = validations_string.replace(/mobile|tel/gi, 'password');
                // turn string to array and remove similar array elements
                validations = validations_string.split('|').getUnique();

                element.attr('data-wnValidations', validations.join('|'));
            }

            return validations;
        },
        findValidationInputType: function () {
            var element = $(this),
                default_value = ['text'],
                inputType,
                matched,
                classPattern = /(email|number|text|password|url|tel)/gi;

            // if name attr exists, then try to find a match
            if (inputType = element.attr_existence_check('type')) {
                matched = inputType.match(classPattern);
            }

            return matched ? matched : default_value;
        },
        findValidationInputName: function () {
            var element = $(this),
                inputName,
                matched = false,
                classPattern = /(username|user_name|email|number|cost|price|text|address|name|fname|lname|first_name|last_name|surname|family|password|pass_word|pass|url|phone|mobile|tel)/gi;

            // if name attr exists,
            if (inputName = element.attr_existence_check('name')) {
                matched = inputName.match(classPattern);
            }

            return matched ? matched : false;
        },
        /**
         *
         * @returns {*|Array|{index: number, input: string}}
         *   if element has novalidate attribute or noValidation class return true elese return false.
         */
        noValidateElement: function () {
            var element = $(this),
                classes;

            // if element has novalidate attribute or noValidation class return true elese return false
            return element.attr_existence_check('novalidate', true) || ((classes = element.attr_existence_check('class')) && classes.match(/noValidate/gi));
        },
        findValidationInputClass: function () {
            var element = $(this),
                classes,
                matched = false,
                classPattern = /(user_?name|url|email|national_?code|number|text|((first|last)_?)?name|address|comma|phone|mobile|tel)Validation/gi;

            // if there is a class attr, then find matched classes
            if ((classes = element.attr_existence_check('class')) && (matched = classes.match(classPattern))) {

                // remove 'Validation' from end of strings
                matched = $.map(matched, function (value) {
                    return value.replace('Validation', '');
                });
            }

            return matched;
        },
        findLanguage: function (default_lang, real) {
            real = real ? real : false;
            var element = $(this),
                classes,
                language = element.attr_existence_check('data-wnlanguage');

            // if developer wants real value of input language
            if (real) {
                classes = element.attr_existence_check('class');
                if (!language && classes) {
                    if (language = classes.match(/(\b\w{2,3}-\w{2,3}\b)/)) {
                        language = language.shift();
                    }
                    else {
                        language = false;
                    }
                }

                return language;
            }
            if (language && !real) {

                return language;
            }

            if (!language && (classes = element.attr_existence_check('class'))) {
                if (language = classes.match(/(\b\w{2,3}-\w{2,3}\b)/gi)) {
                    language = language.shift();
                }
                else {
                    language = default_lang;
                }
            }

            return language;

        },
        acceptable_chars: function (default_regexp) {
            var result = default_regexp,
                acceptable_chars;
            if (acceptable_chars = $(this).attr_existence_check('data-allowed-chars')) {
                result = new RegExp('[^' + acceptable_chars.replace(/\|/g, '') + ']');
            }

            return result;
        },
        disallow_chars: function (default_regexp) {
            var result = default_regexp,
                disallowed_chars;
            if (disallowed_chars = $(this).attr_existence_check('data-disallowed-chars')) {
                result = new RegExp('[' + disallowed_chars.replace(/\|,/g, '') + ']');
            }

            return result;
        },
        maxlength: function (maxlength) {
            var element = $(this),
                max_length;
            if (max_length = element.attr_existence_check('maxlength')) {
                return max_length;
            }
            else {
                element.attr('maxlength', maxlength);
                return maxlength;
            }
        },
        minlength: function (minlength) {
            var element = $(this),
                min_length;
            if (min_length = element.attr_existence_check('minlength')) {
                return min_length;
            }
            else {
                element.attr('minlength', minlength);
                return minlength;
            }
        },
        val_cursor: function (changeable_value) {
            //$(this).val(changeable_value);
            //return false;
            // check to see this element is not radio or checkbox.
            if (!$(this).is('input, textarea') || $(this).is(':radio, :checkbox, :button')) {
                return false;
            }

            // for email number and ... type getCursor function does not work we turn their type to text.
            if ($(this).is('[type=email], [type=number]')) {
                $(this).attr('type', 'text');
            }

            var element = $(this),
                value = element.val(),
                cursor_position = element.getCursor();
            if (changeable_value != value) {
                cursor_position = cursor_position - (value.length - changeable_value.length);

                element.val(changeable_value);

                element.setCaretPosition(cursor_position);
            }
        },
        getCursor: function () {
            var input = $(this).get(0);
            if (!input) return; // No (input) element found
            if ('selectionStart' in input) {
                // Standard-compliant browsers
                return input.selectionStart;
            } else if (document.selection) {
                // IE
                input.focus();
                var sel = document.selection.createRange();
                var selLen = document.selection.createRange().text.length;
                sel.moveStart('character', -input.value.length);
                return sel.text.length - selLen;
            }
        },
        setCaretPosition: function (caretPos) {
            var elem = $(this).get(0)

            if (elem != null) {
                if (elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.move('character', caretPos);
                    range.select();
                }
                else {
                    if (elem.selectionStart) {
                        elem.focus();
                        elem.setSelectionRange(caretPos, caretPos);
                    }
                    else
                        elem.focus();
                }
            }
        },
        attr_existence_check: function (attribute, empty_allowed) {
            var attr = $(this).attr(attribute);

            empty_allowed = empty_allowed ? empty_allowed : false;

            if (typeof attr !== typeof undefined) {
                if (attr.length > 0) {
                    return attr;
                }
                else if (empty_allowed) {
                    return attribute;
                }
            }

            return false;
        },
        get_field_name: function () {
            var id = $(this).attr_existence_check('id'),
                elem_name = null;
            // if id isset look for label
            // if label exists return filtered label
            // else look for placeholder
            // else return "field" string
            if (id && $('[for=' + id + ']').length > 0) {
                // look for label
                elem_name = $('[for=' + id + ']').text().replace(/[^ا-ی\s]/gi, '');
            }
            else if (elem_name = $(this).attr_existence_check('placeholder')) {

            }
            else {
                elem_name = 'فیلد';
            }

            return elem_name;
        },
        get_wnrange: function () {
            var range = $(this).attr_existence_check('data-wnrange'),
                result = {};
            if (range) {
                range = range.split('-');
                if (range[0]) {
                    var min_range = range.filter(function (value) {
                        if (value != '') {
                            return value;
                        }
                    });
                    result.min = range != '' ? Math.min.apply(null, min_range) : false;
                }

                if (range[1]) {
                    range = range.filter(function (value) {
                        if (value != '') {
                            return value;
                        }
                    })
                    result.max = Math.max.apply(null, range);
                }
            }

            return !$.isEmptyObject(result) ? result : false;
        }
    });
})(jQuery);

/**
 *
 * @param email
 * @returns {*}
 *  return false if email is not valid and return email if it's a valid email
 */
function email_validate(email) {
    var regexp = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    //test regexp
    var email_validate = regexp.test(email);

    return !email_validate ? false : email.replace(/^(w{3}\.)/gi, '');
}

function url_validate(value, validate_regexp) {
    // Copyright (c) 2010-2013 Diego Perini, MIT licensed
    // https://gist.github.com/dperini/729294
    // see also https://mathiasbynens.be/demo/url-regex
    // modified to allow protocol-relative URLs
    var default_regexp = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
    validate_regexp = validate_regexp ? validate_regexp : default_regexp;
    return validate_regexp.test(value);
}

/**
 * Validate a string to see if it is a number.
 * @param number
 * @returns {boolean}
 */
function number_validate(number) {
    var regexp = /([^0-9٠-٩,])/gi,
        result = true,
        regexp_test = regexp.test(number);

    if (regexp_test) {
        playSound();
        result = number.replace(regexp, '')
    }
    return result;
}

/**
 *
 * @param requirements: An object which contains jquery object elements.
 * @returns {boolean}
 */
function check_requirements(requirements) {
    var enable_button = true;

    requirements.each(function () {
        //if ($(this).is('select')) {
        //}

        if ($(this).is('input, textarea') && $(this).val() == '' || ($(this).is('select') && $(this).val() == '' || $(this).val == '_none')) {
            enable_button = false;
            return false;
        }
        else if ($(this).is(':radio, :checkbox')) {
            //!$(this).is(':checked');
            var name = $(this).attr('name');
            if (!$('[name=' + name + ']').is(':checked')) {
                enable_button = false;
                return false;
            }
        }
    });

    return enable_button;
};

function error_handler(element, error_showing, errors, errors_addresses) {
    var $messages = error_showing,
        messages_tag = 'div',
        element_exists,
        message_tag = 'div',
    // the last part which hold error message text
        message = element_creator(message_tag);

    // if error_showing == default we must create an element_element for that
    if (!error_showing) {
        // create an element near this element and showing errors on that element
        if (!(element.next(messages_tag).length > 0)) {
            $messages = element_creator(messages_tag);
            element.after($messages);
        }
        else {
            $messages = element.next(messages_tag);
        }
    }
    else {
        $messages = $(error_showing);
    }

    // check to see if an element exists which we can show our errors into that element
    element_exists = ($messages.length > 0);
    $.each(errors_addresses, function (index, has_error) {
        // It is and variable that show us is this error shows before or not? <b>(is this showing error element exists or not?)</b>
        var error_type = index.match(/\B([a-z|A-Z]*)\b/gi).shift(),
            error_message;

        if (has_error) {

            // fill error variable
            error_message = error_text_creator(has_error, error_type);
            errors[index] = error_message;

            // work with DOM
            if (element_exists) {
                if (!($messages.find('#' + index).length > 0)) {
                    $messages.append(message.text(error_message).attr('id', index));
                }
                else {
                    $messages.find('#' + index).fadeIn();
                }
            }

        }
        else {
            // remove this index from email variable
            delete errors[index];
            // remove related DOM element
            $messages.find('#' + index).fadeOut();
        }
    });

    return errors;
}

function error_text_creator(values, error_type) {
    var error_message = error_messages(),
        signs = [],
        signs_values = [];

    $.each(values, function (index, value) {
        signs.push(index);
        signs_values.push(value);
    });

    if (typeof error_message[error_type] != typeof undefined) {
        return error_message[error_type].replaceArray(signs, signs_values);
    }
}

function error_messages() {
    var error_messages = {
        email: 'field_name وارد شده صحیح نمی باشد',
        maxlength: 'field_name نباید بیشتر از length حرف باشد',
        minlength: 'field_name نباید کمتر از length حرف باشد',
        exactlength: 'field_name باید length حرف باشد',
        remain: 'remain حرف باقی مانده',
        url: 'field_name وارد شده صحیح نمی باشد',
        range: 'field_name باید بین min و max باشد.',
        min: 'field_name باید بزرگتر از min باشد.',
        max: 'field_name باید کوچکتر از max باشد.'
    }

    return error_messages;
}

function convertLang(string, lang) {
    var languages = wn_languages(),
        changed_string;

    //console.log('lang', lang);
    lang = lang.replace(/-/gi, '_');
    //console.log(languages[lang]);


    changed_string = string.split('')
        .map(function (char, index) {
            //if($.inArray(char.charCodeAt(0), ))
            if (typeof languages[lang][char.charCodeAt(0)] != typeof undefined) {
                char = String.fromCharCode(languages[lang][char.charCodeAt(0)]);
            }
            else if (typeof languages[lang][char.toLowerCase().charCodeAt(0)] != typeof undefined) {
                char = String.fromCharCode(languages[lang][char.toLowerCase().charCodeAt(0)]);
            }

            return char;
        })
        .join("");

    return changed_string;
}

// give a tag name and return an element
function element_creator(tagName) {

    return $('<' + tagName + '></' + tagName + '>');
}

/**
 * play a beep sound
 */
function playSound() {
    var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
    snd.play();
}

function wn_languages() {
    var default_languages = {
            en_fa: {
                84: 1548,        //  T  =>  ،
                104: 1575,        //  h  =>  "ا"
                102: 1576,        //  f  =>  "ب"
                92: 1662,        //  \  =>  "پ"
                106: 1578,        //  j  =>  "ت"
                101: 1579,        //  e  =>  "ث"
                91: 1580,        //  [  =>  "ج"
                93: 1670,        //  ]  =>  "چ"
                112: 1581,        //  p  =>  "ح"
                111: 1582,        //  o  =>  "خ"
                110: 1583,        //  n  =>  "د"
                98: 1584,        //  b  =>  "ذ"
                118: 1585,        //  v  =>  "ر"
                99: 1586,        //  c  =>  "ز"
                96: 1688,        //  `  =>  "ژ"
                115: 1587,        //  s  =>  "س"
                97: 1588,        //  a  =>  "ش"
                119: 1589,        //  w  =>  "ص"
                113: 1590,        //  q  =>  "ض"
                120: 1591,        //  x  =>  "ط"
                122: 1592,        //  z  =>  "ظ"
                117: 1593,        //  u  =>  "ع"
                121: 1594,        //  y  =>  "غ"
                116: 1601,        //  t  =>  "ف"
                114: 1602,        //  r  =>  "ق"
                59: 1705,        //  ;  =>  "ک"
                39: 1711,        //  '  =>  "گ"
                103: 1604,        //  g  =>  "ل"
                108: 1605,        //  l  =>  "م"
                107: 1606,        //  k  =>  "ن"
                44: 1608,        //  ,  =>  "و"
                105: 1607,        //  i  =>  "ه"
                100: 1740,        //  d  =>  "ی"
                109: 1574,        //  m  =>  "ئ"
                72: 1570,       //  H  =>  "آ"
                67: 1688,        //  C  =>  "ژ"
            },
            fa_en: {
                1575: 104,        //  "ا"  =>  h
                1576: 102,        //  "ب"  =>  f
                1578: 106,        //  "ت"  =>  j
                1579: 101,        //  "ث"  =>  e
                1581: 112,        //  "ح"  =>  p
                1582: 111,        //  "خ"  =>  o
                1583: 110,        //  "د"  =>  n
                1584: 98,        //  "ذ"  =>  b
                1585: 118,        //  "ر"  =>  v
                1586: 99,        //  "ز"  =>  c
                1587: 115,        //  "س"  =>  s
                1588: 97,        //  "ش"  =>  a
                1589: 119,        //  "ص"  =>  w
                1590: 113,        //  "ض"  =>  q
                1591: 120,        //  "ط"  =>  x
                1592: 122,        //  "ظ"  =>  z
                1593: 117,        //  "ع"  =>  u
                1594: 121,        //  "غ"  =>  y
                1601: 116,        //  "ف"  =>  t
                1602: 114,        //  "ق"  =>  r
                1604: 103,        //  "ل"  =>  g
                1605: 108,        //  "م"  =>  l
                1606: 107,        //  "ن"  =>  k
                1607: 105,        //  "ه"  =>  i
                1740: 100,        //  "ی"  =>  d
                1574: 109,        //  "ئ"  =>  m

                // Capital english letters
                1617: 70,        //  ّّ   =>  F
                1600: 74,        //  ـ  =>  J
                1613: 69,        //  ٍ   =>  E
                92: 80,        //  \  =>  P
                91: 79,        //  [  =>  O
                1571: 78,        //  أ  =>  N
                1573: 66,        //  إ  =>  B
                1572: 86,        //  ؤ  =>  V
                1615: 83,        //  ُ   =>  S
                1614: 65,        //  َ   =>  A
                1612: 87,        //  ٌ   =>  W
                1611: 81,        //  ً   =>  Q
                1610: 88,        //  ي  =>  X
                1577: 90,        //  ة  =>  Z
                //44: 85,        //  ,  =>  U
                1563: 89,        //  ؛  =>  Y
                1548: 84,        //  ،  =>  T
                1728: 71,        //  ۀ  =>  G
                187: 76,        //  »  =>  L
                171: 75,        //  «  =>  K
                93: 73,        //  ]  =>  I
                1616: 68,        //  ِ   =>  D
                1569: 77,        //  ء  =>  M
                //1585: 82,        //  ريال  =>  R
                1688: 67,        //  ژ  =>  C
                1570: 72,        // "آ" =>  H
                // signs
                1662: 92,        //  "پ"  =>  \
                1580: 91,        //  "ج"  =>  [
                1670: 93,        //  "چ"  =>  ]
                1705: 59,        //  "ک"  =>  ;
                1711: 39,        //  "گ"  =>  '
                1608: 44,        //  "و"  =>  ,
            },
            en_fa_num: {
                48: 1632,        //  0  =>  "٠"
                49: 1633,        //  1  =>  "١"
                50: 1634,        //  2  =>  "٢"
                51: 1635,        //  3  =>  "٣"
                52: 1636,        //  4  =>  "٤"
                53: 1637,        //  5  =>  "٥"
                54: 1638,        //  6  =>  "٦"
                55: 1639,        //  7  =>  "٧"
                56: 1640,        //  8  =>  "٨"
                57: 1641,        //  9  =>  "٩"
            },
            fa_en_num: {
                1632: 48,       //  "٠"  =>  0
                1633: 49,        //  "١"  =>  1
                1634: 50,        //  "٢"  =>  2
                1635: 51,        //  "٣"  =>  3
                1636: 52,        //  "٤"  =>  4
                1637: 53,        //  "٥"  =>  5
                1638: 54,        //  "٦"  =>  6
                1639: 55,        //  "٧"  =>  7
                1640: 56,        //  "٨"  =>  8
                1641: 57,        //  "٩"  =>  9
            },
        },
        languages;

    wn_extra_language = (typeof wn_extra_language != typeof undefined && !$.isEmptyObject(wn_extra_language)) ? wn_extra_language : {};

    languages = $.extend(default_languages, wn_extra_language);

    return languages;
}

function loadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    var body = $('body');
    var attributes = {
        type: 'text/javascript',
        src: url
    };
    var script = $('<script></script>').attr(attributes);

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    body.append(script);
}

function isset(variable) {
    return typeof variable != typeof undefined;
}

Array.prototype.getUnique = function () {
    var u = {}, a = [];
    for (var i = 0, l = this.length; i < l; ++i) {
        if (u.hasOwnProperty(this[i])) {
            continue;
        }
        a.push(this[i]);
        u[this[i]] = 1;
    }
    return a;
}

String.prototype.replaceArray = function (find, replace) {
    var replaceString = this;
    for (var i = 0; i < find.length; i++) {
        replaceString = replaceString.replace(find[i], replace[i]);
    }
    return replaceString;
};

$.fn.remain_chars_show = function (remainChar_showing, remainCharsNum) {
    var element = $(this),
        $messages = remainChar_showing,
        id = element.attr('data-validationid'),
        messages_tag = 'div',
        element_exists,
        message_tag = 'div',
    // the last part which hold error message text
        message = element_creator(message_tag),
        remain_message;

    // if developer doesn't change the default value of remainCharsShow
    if (!remainChar_showing) {
        // create an element near this element and showing errors on that element
        // if next to this element no div element exists
        if (!(element.next(messages_tag).length > 0)) {
            $messages = element_creator(messages_tag);
            element.after($messages);
        }

        // if next to this element an div element exists`
        else {
            $messages = element.next(messages_tag);
        }
    }

    // if developer change the default value of
    else {
        $messages = $(remainChar_showing);
    }

    // check to see if an element exists which we can show our errors into that element
    element_exists = ($messages.length > 0);

    // filling error variable
    remain_message = error_text_creator({remain: remainCharsNum}, 'remain');

    // work with DOM
    if (element_exists) {
        if (!($messages.find('#' + id).length > 0)) {
            $messages.append(message.text(remain_message).attr('id', id));
        }
        else {
            $messages.find('#' + id).text(remain_message).fadeIn();
        }
    }
}

/****************
 Ideas:
 TODO: We can create rules object which can be extensible by users.
 TODO: We can let users add some exampleValidation class to our rules dynamically.
 TODO: (done) Url check, date time check, (done) only accept special characters, (done) do not accept some characters.
 ****************/