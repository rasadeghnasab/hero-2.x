<?php
    /*<!-- <meta charset="utf-8">-->
     * Admin Advance search
     */
    require_once(__DIR__ . '/../includes/initial.php');
///*

    $required_fields = array(
        'stateResult'   ,
        'countyResult'  ,
        'fieldResult'   ,
        'branchResult'  ,
        'level'         ,
    );
    $fields_with_max_lengths = array(
        'stateResult'   => 30,
        'fieldResult'   => 30
    );
    validate_presences($required_fields);
    validate_input_lengths($fields_with_max_lengths);

    if(!empty($errors)) {
        $errors['errors'] = true;
        echo json_encode($errors);
        die();
    }
//*/
    pagination_variables();

    $stateName = $_POST['stateResult'];
    $stateID   = $_POST['countyResult'];
    $field     = $_POST['fieldResult'];
    $sportID   = $_POST['branchResult'];
    $level     = $_POST['level'];


    $result = select_all_people($page, $limit, $stateName, $stateID, $field, $sportID, $level);
    echo json_encode($result);
