<?php
/**
 * adminSCFB.php
 * select State, County, Field AND Branch for admin
 * Date: 10/15/14
 * Time: 8:42 PM
 */
    require_once(__DIR__ . '/../includes/initial.php');
    if (isset($_POST['stateName']) && !isset($_POST['stateID'])){
        $stateName = $_POST['stateName'];
        if(!empty($stateName)){
            if($stateName == 'همه'){
                echo '<option value ="همه">همه</option>';
            } else {
                echo '<option value ="همه">همه</option>';
                echo stateID_county_by_stateName($stateName);
            }
        }
        // agar parametr haye state va county ersal shode bashand
    }
    else if (isset($_POST['stateName'],$_POST['stateID']) && !isset($_POST['sportName']) && !isset
        ($_POST['sportID']) && !isset ($_POST['jobID'])){
        $stateID = $_POST['stateID'];
        $stateName = $_POST['stateName'];

        if(!empty($stateID) && !empty($stateName)){
            if($stateID == 'همه' || $stateName == 'همه') {
                if($stateName == 'همه' ){
                    echo '<option value ="همه">همه</option>';
                    echo sport_distinct_select();
                } else if($stateID == 'همه' ) {
                    echo '<option value ="همه">همه</option>';
                    echo sport_distinct_by_stateName($stateName);
                }
            } else {
                echo '<option value ="همه">همه</option>';
                echo sport_distinct_by_stateID($stateID);
            }
        }
        // agar parametr haye state,county va field ersal shode bashand
    }
    else if ( isset($_POST['stateID'],$_POST['stateName'],$_POST['sportName']) && !isset($_POST['sportID']) ) {
        $stateID    = $_POST['stateID'];
        $sportName  = $_POST['sportName'];
        $stateName  = $_POST['stateName'];
        if(!empty($stateID) && !empty($sportName) && !empty($stateName)) {
            if($sportName == 'همه' || $stateID == 'همه' || $stateName == 'همه') {
                if($sportName == 'همه') {
                    echo '<option value ="همه">همه</option>';
                } else if ($stateID == 'همه' || $stateName == 'همه'){
                    echo '<option value ="همه">همه</option>';
                    echo branch_by_sportName($sportName);
                }
            } else {
                echo '<option value ="همه">همه</option>';
                echo branch_by_sportName_stateID($sportName, $stateID);
            }
        }
    }