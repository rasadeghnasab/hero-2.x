<?php require_once(__DIR__ . '/../includes/initial.php'); ?>
<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> تنظیمات </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/_css/style.css"/>
    <style>

    </style>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>
<body>
<?php layout_template('header');?>

<div class="main_frame">

	<div class="frame">
        <?php layout_template('admin_navigation');?>
        <div class="lastPeople" data-select-offset = "0">
        <!--                 -->
            <?php
                $url = 'index.php';
                pagination_variables('get');
                echo select_all_people($page, $limit,$url);
            ?>
        </div>
        
        
	</div>

</div>
<!-- scripts -->
<?php require_once(__DIR__ . '/../includes/javascripts.php'); ?>
<!-- End scripts -->
</body>
</html>