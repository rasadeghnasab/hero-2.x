<?php
    require_once(__DIR__ . '/../../includes/initial.php');
    // Page Variables
    pagination_variables();
    if(isset($_POST['submit'])){
        $required_fields = array(
            'stateResult'   ,
            'countyResult'  ,
            'fieldResult'   ,
            'branchResult'  ,
            'level'         ,
        );
        $fields_with_max_lengths = array(
            'stateResult'   => 30,
            'fieldResult'   => 30,
            'name'          => 30
        );

        validate_presences($required_fields);
        validate_input_lengths($fields_with_max_lengths);

        if(!empty($errors)) {
            $errors['errors'] = true;
            echo $errors;
            die();
        }
//*/
        $stateName = $_POST['stateResult'];
        $stateID   = $_POST['countyResult'];
        $sportName = $_POST['fieldResult'];
        $sportID   = $_POST['branchResult'];
        $jobID     = $_POST['level'];
        $name      = trim($_POST['name']);

        $result = select_all_people($page, $limit, null,$name, $sportID, $jobID, $stateName, $stateID, $sportName);
    } else {
        $result = select_all_people();
    }
  ?>
<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> تنظیمات </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/_css/style.css"/>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>
<body>
<?php layout_template('header');?>
<div class="main_frame">
	<div class="frame">
        <?php layout_template('admin_navigation');?>
        <!--<div class="subMenu">
        	<ul>
            	<li><a href="javascript:;"> افراد</a></li>
<!--                <li><a href="/register"> + افراد جدید</a></li>
            </ul>
        </div>-->
        <div class="lastPeople">
        	<div class="controlPeople">
            	<form action="" method="POST">
                	<div class="row">
                        <span>
                            <select id="stateResult" name="stateResult" data-ajax-dest = "admin/adminSCFB.php"
                                    class="balloondd validate2">
<!--                                <option value="همه">همه</option>-->
                                <?php echo state_distinct_select(); ?>
                            </select><label for="stateResult">استان</label>
                           </span>
                           <span>
                                <select id="countyResult" name="countyResult" data-ajax-dest="adminSCFB.php" class="balloondd"
                                        required="required">
                                    <option  value="همه"> همه </option>
                                    <?php echo stateID_county_by_stateName($stateName); ?>
                                </select><label for="county">شهر </label>
                           </span>
                        <span>
                                <select id="fieldResult" name="fieldResult" data-ajax-dest = "admin/adminSCFB.php"  class="balloondd"
                                        required="required">
                                        <option  value="همه"> همه </option>
                                    <?php
                                        if(isset($stateID) && $stateID !== 'همه') {
                                            echo sport_distinct_by_stateID($stateID);
                                        } elseif(isset($stateName) && $stateName !== 'همه') {
                                            echo sport_distinct_by_stateName($stateName);
                                        } else {
                                            echo sport_distinct_select();
                                        }
                                    ?>
                                </select><label for="fieldResult">رشته </label>
                            </span>
                            <span>
                                <select id="branchResult" name="branchResult" data-ajax-dest = "admin/adminSCFB.php" class="balloondd"
                                        required="required">
                                        <option  value="همه"> همه </option>
                                    <?php echo branch_by_sportName($sportName); ?>
                                </select><label for="branch1">سبک </label>
                           </span>
                        <span>
                               <select  id="superior" name="level" class="balloondd  validate2" >
                                   <option value="همه" /> همه </option>
                                   <?php echo $jobResult = job_select(false); ?>
                               </select><label for="layer">سمت</label>
			  	        </span>
                        <span>
                            <input id="name" name="name" class="balloon validate2 numberValidation" type="text"
                                   minlenght="5" maxlength="30" value=" <?php if(isset($name)) echo $name;?>">
                            <label for="name">نام - نام خانوادگی</label>
                        </span>
                        <span>
                           <select  id="limit" name="limit" class="balloondd  validate2" >
                               <?php limit_query($limit); ?>
                           </select>
                            <label for="limit">تعداد  </label>
			  	        </span>
                        <input type="hidden" name="page" value="1">
                        <span>
                            <input type="submit" name="submit" id="searchPeople" value="نمایش" style="width:100px;
                            height:40px;
                            " >
                        </span>
                       </div>
            	</form>
            </div>
            <?php echo $result; ?>
        </div>
	</div>
</div>
<!-- scripts -->
<?php require_once(LIB_PATH.DS.'javascripts.php'); ?>
<!-- End scripts -->
</body>
</html>