<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 4/30/2015
 * Time: 4:14 PM
 */
$permission_name = 'student_control_conf_deny';
$return          = TRUE;
require_once(__DIR__ . '/../includes/initial.php');
$message         = $result = array();
$done            = FALSE;
$required_fields = array(
  'userID',
  'action',
);
validate_presences($required_fields);
if (!errors_to_ul($errors)) {
  $teacherID    = $cUser->user_id;
//  echo json_encode($_POST);die;
  $action       = $_POST['action'] ? 1 : 0;
  // $_POST['userID'] is $_POST['event_reg_id'];
  $event_reg_id = $_POST['userID'];
  // check to see is there any registration with this ID exists ?
  if ($evt_reg = eventRegister::find_by_id($event_reg_id)) {
    // check to see is this user teacher of the registered user ?
    if ($done = is_teacher_student_by_infoid($evt_reg->info_id, $teacherID)) {
      $evt_reg->confirm      = $action;
      $evt_reg->confirmer_id = $teacherID;
      $done = $evt_reg->save();
    }
  }
}
$message['text']  = $done ? 'عملیات با موفقیت صورت گرفت' : 'عملیات ناموفق بود لطفا دوباره امتحان نمایید';
$message['class'] = $done ? 'success_message' : 'error_message';
$result           = array(
  'message' => $message,
  'done'    => $done,
);
echo json_encode($result);