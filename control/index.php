<?php
// permission validation
$permission_name = 'student_control_view';
// include necessary fiels
require_once(__DIR__ . '/../includes/initial.php');
//set $limit and $page
pagination_variables('GET');
//variables initializing
$pagination = $result = '';
$control_name = isset($_GET['control_name']) ? $_GET['control_name'] : 'students';
//set url for pagination
$url        = '/control/' . $control_name;
if ($control_name == 'new-students') {
  $sub_menu_child = array();
  $result     = string_teacher_students(find_teacher_students($cUser->user_id, NULL, TRUE));
  $pagination = pagination_function($url);
  //set student_register = 0 in notification table;
  $result            = $result != '' ? $result : '<ul class="errors"><li > شاگرد جدیدی وجود ندارد</li></ul>'; // if we have no result
  $columns_to_update = array('student_register');
  $columns_to_insert = array(
    'userID',
    'student_register'
  );
  $values_to_insert  = array(
    ':userID'           => $cUser->user_id,
    ':student_register' => 0
  );
  insert_update_notificatoins($columns_to_insert, $values_to_insert, $columns_to_update, TRUE);
  check_notifications(TRUE);
}
elseif ($control_name == 'event' || $control_name == 'confirm-event' || $control_name == 'deny-event') {
  $event_confirm = array(
    'deny-event' => 0,
    'confirm-event' => 1,
    'event' => NULL,
  );
  $sub_menu_child = array(
    '/control/confirm-event' => 'تایید شده ها',
    '/control/deny-event' => 'تایید نشده ها',
  );
  $confirm = isset($event_confirm[$control_name]) ? $event_confirm[$control_name] : NULL;
  $result     = students_event_string(student_event_register($cUser->user_id, $confirm));
  $pagination = pagination_function($url);
  //set student_register = 0 in notification table;
  $result            = $result != '' ? $result : '<ul class="errors"><li > شاگردی در این قسمت وجود ندارد</li></ul>'; // if we have no result
  $columns_to_update = array('event_register');
  $columns_to_insert = array(
    'userID',
    'event_register'
  );
  $values_to_insert  = array(
    ':userID'           => $cUser->user_id,
    ':event_register' => 0
  );
  insert_update_notificatoins($columns_to_insert, $values_to_insert, $columns_to_update, TRUE);
  check_notifications(TRUE);
}
else {
  $students_confirm = array(
    'not-confirm' => 0,
    'confirm' => 1
  );
  $sub_menu_child = array(
    '/control/not-confirm' => 'تایید نشده ها',
    '/control/confirmed' => 'تایید شده ها',
  );
  $confirm = isset($students_confirm[$control_name]) ? $students_confirm[$control_name] : NULL;
//  var_dump($confirm);
  $confirm    = $control_name == 'not-confirm' ? 0 : NULL;
  if($control_name == 'not-confirm') {
    $confirm = 0;
  }
  elseif($control_name == 'confirmed') {
    $confirm = 1;
  }
  else {
    $confirm = NULL;
  }
  $result     = string_teacher_students(find_teacher_students($cUser->user_id, $confirm));
  $result = $result != '' ? $result : errors_to_ul('شاگردی در این قسمت یافت نشد', 'errors');
  $pagination = pagination_function($url);
}
$result .= $pagination;
?>

<?php
$title = 'مدیریت شاگردان';
layout_template('head_section');
layout_template('header');?>
  <div class = "content">
    <div class = "subMenu">
      <ul>
        <li>
          <a href = "/control/new-students/"> شاگرد جدید</a>
          <?php echo ($_SESSION['student_reg'] > 0) ? '<span class="badge" >'. $_SESSION['student_reg'] . '</span>' : NULL; ?>
        </li>
        <li><a href = "/control/event" >رویداد ها</a>
          <?php echo ($_SESSION['event_reg'] > 0) ? '<span class="badge" >'. $_SESSION['event_reg'] . '</span>' : NULL; ?>
        </li>
        <li><a href = "/control/all">تمامی شاگردان</a></li>
<!--        <li><a href = "/control/not-confirm/">تایید نشده ها</a></li>-->
      </ul>
    </div>
    <?php
    if(!empty($sub_menu_child)) echo sub_menu_creator($sub_menu_child,'child'); ?>
    <?php echo $result ?>
  </div>
<?php layout_template('foot/foot_section'); ?>