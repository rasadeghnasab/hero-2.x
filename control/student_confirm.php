<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 11/24/14
 * Time: 7:15 PM
 */
//if confirm_student
// execute : student_confirm => is_confirm = 1 and updateDate = date("Y-m-d H:i:s");
// else deny_student
// execute : student_confirm => is_confirm = 0 and updateDate = date("Y-m-d H:i:s");
// permission validation
//echo json_encode($_POST);
//die;
$permission_name = 'student_control_conf_deny';
$return = TRUE;
require_once(__DIR__ . '/../includes/initial.php');
$done = FALSE;
$message = $result = array();
$required_fields = array(
  'userID',
  'action',
);
validate_presences($required_fields);
if (!$message = errors_to_ul($errors)) {
  $studentID = $_POST['userID'];
  $teacherID = $cUser->user_id;
  $action = $_POST['action'];
//  echo json_encode($action);
  $done = student_conf_deny($studentID, $teacherID, $action);
  if($done && $action) {
    $message = 'شاگرد مورد نظر تایید شد';
  }
  elseif ($done && !$action) {
    $message = 'فرد مورد نظر دیگر شاگرد شما نمی باشد، می توانید با مراجعه به قسمت شاگردان تایید نشده دوباره وی را تایید نمایید';
  }
}
$class = $done ? 'success_message' : 'error_message';
$result = array(
  'message' => array(
    'text'  => $message,
    'class' => $class
  ),
  'done'    => $done,
);

echo json_encode($result);