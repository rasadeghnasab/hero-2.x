<?php require_once(__DIR__ . '/../includes/initial.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>هیروز | شما مجوز دسترسی به این صفحه را ندارید.</title>
    <link rel="stylesheet" href="/_css/errors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/_css/errors/font-awesome.css">
    <link rel="stylesheet" href="/_css/errors/notFoundStyle2.css">
</head>
<body>
<div class="logo">
    <div class="content">
        <h1 class="_403"> 403 </h1>
        <h1 class="title"> عــــــدم دسترســـی</h1>
        <div class="desc">
            <p>
                شما مجوز دسترسی به این صفحه را ندارید.
            </p>
        </div>

        <span>
<!--        <a href="/"><i class="fa fa-home"></i> خانه</a>-->
            <?php print $back_btn; ?>
    </span>
    </span>
    <span>
        <a href="/contact-us"><i class="fa fa-phone"></i>  تماس با ما</a>
    </span>

    <div class="search">
        <!--place for search-->
    </div>
    </div>
</div>


</body>
</html>