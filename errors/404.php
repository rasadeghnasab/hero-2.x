<?php require_once(__DIR__ . '/../includes/initial.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>صفحه مورد نظر یافت نشد | وب سایت قهرمانان</title>
  <link rel="stylesheet" href="/_css/errors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/_css/errors/font-awesome.css">
  <link rel="stylesheet" href="/_css/errors/notFoundStyle2.css">
</head>
<body>
<div class="content">
  <h1 class="_404"> 404 </h1>
  <h1 class="title">
    صفحه مورد نظر یافت نشد!
  </h1>
  <div class="desc">
<!--    <p>
      یک متن کوتاه
    </p>
-->  </div>
    <span>
<!--        <a href="/"><i class="fa fa-home"></i> خانه</a>-->
      <?php print $back_btn; ?>
</span>
    <span>
        <a href="/contact-us"><i class="fa fa-phone"></i>  تماس با ما</a>
    </span>
  <div class="search">
    <!--place for search-->
  </div>
</div>
</body>
</html>