<?php
$title = 'شما اجازه دسترسی به این صفحه را ندارید';
if (!isset($permission)) require_once(__DIR__ . '/../includes/initial.php');
$error_message = isset($error_message) ? $error_message : 'شما اجازه دسترسی به این بخش را ندارید';?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
  <div class = "content">
    <?= $back_btn; ?>
    <div class = "profile_field">
      <div class = "box error_message">
        <?php echo $error_message; ?>
      </div>
    </div>
  </div>
<?php layout_template('foot/foot_section'); ?>