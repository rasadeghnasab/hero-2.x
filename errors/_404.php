<?php require_once(__DIR__ . '/../includes/initial.php'); ?>
<?php
$title = 'فایل مورد نظر یافت نشد';
layout_template('head_section');
layout_template('header');
?>
  <div class = "content">
    <?= $back_btn; ?>
    <div class = "new_errors">
      صفحه مورد نظر وجود ندارد لطفا دوباره امتحان کنید
    </div>
    <div class = "new_row">
    </div>
  </div>
<?php layout_template('foot/foot_section'); ?>