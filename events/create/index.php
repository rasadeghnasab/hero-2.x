<?php
/* permission validation */
$permission_name = 'event_create';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
$sport      = sportName_by_sportID($_SESSION['sportID']);
$sport_name = $sport['field'] . ' ' . $sport['branch']; //used in allows sport (only from sport $sport_name)
//must retrieve entered valued if user has any problems
/*
 * ☻- //create html (teacher approval needed must be added)
 * ☻- //event type add some events type
 * ☻- //form submission
 * ☻- //requirements CRUD
 * ☻- //Allow sports CRUD
 * ☻- //events CRUD
 * */
$continue_process = $event_message = FALSE;
if (isset($_POST['submit'])) {
    $required_fields = array(
            'event_title',
            'evt_type',
            'address',
            //            'description',
            'deadline',
            'openingDay',
            'requirements',
            'allow_sports',
    );
    // if allow_sports === 1 (that means we must select specific sports (allow_sports_name))
    if ((int) $_POST['allow_sports'] === 1) {
        if (empty($_POST['allow_sports_name'])) {
            $errors[] = ' باید یک رشته خاص انتخاب شود';
        }
    }
    $fields_with_max_lengths = array(
            'event_title' => 80,
            'address'     => 300,
            'description' => 300,
    );
    $fields_with_min_lengths = array(
            'event_title' => 5,
    );

    validate_presences($required_fields);
    validate_input_lengths($fields_with_max_lengths, $fields_with_min_lengths);

    check_date($_POST['deadline']);
    check_date($_POST['openingDay']);
    if (!$event_message = errors_to_ul($errors, 'new_message new_errors')) {
        $event_message = '<ul class="new_errors"></ul>';
        //submit values into tables
        $qualified_users_for_notifications             = array();
        $qualified_users_for_notifications[':sportID'] = array($_SESSION['sportID']);
        $continue_process                              = TRUE;
        $db->beginTransaction();
        //if event has requirements
        if ((int) $_POST['requirements'] === 1) {
            // check requirements fields for values
            $requirement_required_fields = array(
                    'min_age',
                    'max_age',
                    'min_job',
                    'must_referee',
                    'must_teacher',
            );
            $requirement_vars            = get_specified_superGlobals_from_array($requirement_required_fields);
            if (isset($requirement_vars['must_referee'])) {
                $qualified_users_for_notifications[':is_referee'] = $requirement_vars['must_referee'];
            }
            if (isset($requirement_vars['must_teacher'])) {
                $qualified_users_for_notifications[':is_teacher'] = $requirement_vars['must_teacher'];
            }
            //if all values are empty the $errors array will has 5 element and count($errors) must be equal to 5
            if (!empty($requirement_vars)) { // if $errors array has less than 5 element that means on of its elements has value
                //insert into eventRequirement table
                //function to insert into eventRequirement table
                if (evt_requirement_insert($requirement_vars)) {
                    $_POST['requirements'] = TRUE;
                }
                else {
                    $db->rollBack();
                    $continue_process = FALSE;
                };
            }
        }
        else {
            $_POST['requirements'] = FALSE;
        } // if it has no requirements
        //insert into event table
        if ($continue_process) {
            if (!event_insert()) {
                $db->rollBack();
                $continue_process = FALSE;
            }
        }
        $last_event_id = $db->lastInsertId();
        if ($continue_process) {
            if ((int) $_POST['allow_sports'] === 1) {
                //if allows sports == 1 then insert into evtallowsports.
                //                    if(!in_array($_SESSION['sportID'],$_POST['allow_sports_name'])) $_POST['allow_sports_name'][] = $_SESSION['sportID'];
                foreach ($_POST['allow_sports_name'] as $allow_sport_id) {
                    //insert into evtallowsports
                    $qualified_users_for_notifications[':sportID'][] = $allow_sport_id;
                    if (!evtallowsports_insert($allow_sport_id, $last_event_id)) {
                        $db->rollBack();
                        $continue_process = FALSE;
                        break;
                    }
                }
            }
        }
        if ($continue_process) {
            // if $continue_process still equal to true that means all tables that related to event is inserted correctly
            $db->commit();
            // check event to see if it requires weight table or not ? ? ?
            $event_array = event_select($last_event_id, NULL, 1, 1)->fetch(PDO::FETCH_ASSOC);
            // set this massage for user
            $event_message = '<div class="content">';
            $event_message .= '<ul class="new_message new_success_message">
                            <li>رویداد با موفقیت ثبت شد</li>';
            $event_message .= '</ul>';
            $event_message .= '<div class="new_inner_row">';
            if ($event_array['has_weight']) {
                $event_message .= '<a class="btn btn_green btn_medium" href="/events/events-table-management/' . $last_event_id . '">' . 'ویرایش جدول وزن ها' . '</a>';
            }
            $event_message .= '<a class="btn btn_blue btn_medium" href="/events/create/">ثبت رویداد جدید</a>';
            $event_message .= '</div>';
            $event_message .= $back_btn;
            $event_message .= '</div>';
            // we must send a notification to all qualified persons
            // first select all qualified persons
            $persons = user_select_for_event_notification($qualified_users_for_notifications);
            // then set notification for all of them
            while ($person = $persons->fetch(PDO::FETCH_ASSOC)) { // for each qualified person insert notification, or update their notifications
                //                    var_dump($person);
                insert_update_notificatoins(array(
                        'userID',
                        'new_event',
                ), array(
                        'userID'     => $person['userID'],
                        ':new_event' => 1,
                ), array('new_event'));
            }
        }
    }
}
?>
<?php
$title = 'رویداد جدید';
layout_template('head_section');
layout_template('header'); ?>
<?php print($event_message);
if (!$continue_process):?>
    <div class="content">
        <?php layout_template('sport_master_navigation'); ?>
        <?php
        echo $back_btn;
        $sub_menu_array = array(
                '/management/events/' => 'رویدادها',
                '/events/create/'     => 'رویداد جدید +',
        );
        echo sub_menu_creator($sub_menu_array, 'child');
        ?>
        <!--    <div class = "new_row">-->
        <h1> ثبت رویداد جدید</h1>
        <!--    </div>-->
        <form action="" method="post" novalidate="novalidate"
              class="form-grouper">
            <div class="split_bottom">
                <div class="input-group">
                    <label for="event_title" class="">عنوان رویداد</label>
                    <input id="event_title"
                           name="event_title"
                           class="new_input new_medium_input required"
                           required
                           type="text"
                           placeholder="عنوان رویداد "/>
                </div>
                <!--        <label for="event_type" class="icon label_icon title_icon"></label>-->
                <div class="input-group">
                    <label for="evt_type">نوع رویداد</label>
                    <?php $evt_type = array(
                            'name'  => 'evt_type',
                            'id'    => 'evt_type',
                            'class' => 'chosen-select chosen-rtl new_normal_input asterisk',
                    );
                    echo select_element(select_evt_type_by_sportID($_SESSION['sportID']), 'id', 'evt_type_name', $evt_type); ?>
                </div>
                <div class="input-group">
                    <label for="gender">جنسیت</label>
                    <select id="gender" name="gender"
                            class="new_input required">
                        <option value="1" <?php echo form_values('gender', 'select', 1); ?>>
                            مرد
                        </option>
                        <option value="0" <?php echo form_values('gender', 'select', 0); ?>>
                            زن
                        </option>
                    </select>
                </div>
                <div class="input-group">
                    <label for="event_cost">هزینه ثبت نام</label>
                    <input type="text"
                           id="event_cost"
                           name="event_cost"
                           data-help="در صورت خالی گذاشتن این فیلد شرکت در رویداد رایگان خواهد بود"
                           class="new_input new_input_num comma_separate digit_input"
                           placeholder="ریال"
                    />
                </div>
                <div class="input-group">
                    <label for="max_reg">حداکثر تعداد شرکت کننده</label>
                    <input type="text"
                           name="max_reg"
                           id="max_reg"
                           data-help="اگر در این فیلد مقداری وارد نکنید، محدودیتی در تعداد افراد ثبت نام نموده در رویداد وجود نخواهد داشت"
                           class="new_input new_input_num digit_input"
                           placeholder="نفر">
                </div>
                <div class="input-group">
                    <label for="deadline">آخرین مهلت ثبت نام</label>
                    <input id="deadline"
                           name="deadline"
                           type="text"
                           class="new_input new_input_num datepicker required digit_input"
                           placeholder="تاریخ آخرین مهلت ثبت نام">
                </div>
                <div class="input-group">
                    <label for="openingDay">تاریخ برگذاری</label>
                    <input id="openingDay"
                           name="openingDay"
                           type="text"
                           class="new_input new_input_num datepicker required  digit_input"
                           placeholder="تاریخ برگذاری رویداد"/>
                </div>
                <div class="input-group">
                    <label for="address">آدرس</label>
                    <input type="text"
                           id="address"
                           name="address"
                           class="new_input new_semi_medium_input required"
                           required="required"
                           placeholder="آدرس محل برگذاری رویداد">
                </div>
                <div class="input-group">
                    <div class="input-container">
          <textarea class="required"
                    name="description"
                    id="desc"
                    cols="30"
                    rows="5"
                    placeholder="توضیحات در مورد رویداد"></textarea>
                    </div>
                </div>
            </div>
            <div class="new_row">
                <input type="radio" id="no_req" name="requirements" value="0"
                       checked/><label for="no_req">بدون
                    محدودیت</label>
                <input id="req" type="radio" name="requirements"
                       value="1"/><label for="req">محدود ساختن شرکت
                    کنندگان</label>

                <div class="requirements  split_bottom">
                    <input type="text"
                           name="min_age"
                           class="new_input new_input_num digit_input"
                           placeholder="کمترین سن مجاز (سال)"/>
                    <input type="text"
                           name="max_age"
                           class="new_input new_input_num digit_input"
                           placeholder="بیشترین سن مجاز (سال)"/>

                    <div class="new_row">
                        <input type="checkbox"
                               name="must_teacher"
                               id="just_teachers"
                               value="1"/><label for="just_teachers">فقط از
                            اساتید</label>
                        <input type="checkbox"
                               name="must_referee"
                               id="just_referees"
                               value="1"/><label for="just_referees">فقط از
                            داوران</label>
                    </div>
                </div>
            </div>
            <div class="new_row">
                <input type="radio" id="my_sport" name="allow_sports" value="0"
                       checked/>
                <label for="my_sport">فقط از
                    رشته <?php echo $sport_name ?></label>
                <input type="radio" id="spec_sports" name="allow_sports"
                       value="2"/>
                <label for="spec_sports"
                       data-help="با انتخاب این گزینه به ورزشکاران تمامی رشته ها مجوز حضور در این رویداد را خواهید داد">تمامی
                    رشته ها</label>
                <input type="radio" id="all_sports" name="allow_sports"
                       value="1"/>
                <label for="all_sports"
                       data-help="با انتخاب این گزینه میتوانید رشته هایی که مجاز به شرکت در این رویداد هستند را مشخص نمایید">رشته
                    های خاص</label>

                <div class="allowsSports split_bottom"
                     style="padding-bottom: 200px">
                    <!--            <label for = "sports">نام رشته ها</label>-->
                    <?php $all_sport_name = array(
                            'name'  => 'allow_sports_name[]',
                            'multiple',
                            'class' => 'chosen-select chosen-rtl new_input new_long_input',
                            'id'    => 'sports',
                    );
                    echo select_element(all_sports_name(), 'sport_id', 'sportName', $all_sport_name); ?>
                </div>
            </div>
            <input class="btn btn_blue btn_medium" type="submit" name="submit"
                   value="ثبت"/>
        </form>
    </div>
<?php endif; ?>
<?php layout_template('foot/foot_section');