<?php
require_once(__DIR__ . '/../../includes/initial.php');
if (isset($_GET['eventID'])) {
  $eventID = $_GET['eventID'];
}
else {
  redirect_to(BASEPATH . 'events');
}
//first retrieve the event information
// check something then show event and event register button
// first event deadline date
//if now > deadlinedate -> echo 'مهلت ثبت نام تمام شده';
// then no needed to check anything die();
//else check something
// first check to see if person is logged in
// then retrieve person information
// then if (it has requirements) check the requirements
// check the age
//if($event['min_age'] if min_age < ageCalc($person['birth_date']) ->else do not display register button;
//if($event['max_age'] if max_age > ageCalc($person['birth_date']) ->else do not display register button;
//check the teacher or referee
//if($event['must_referee'] if($person['is_referee']) -> else do not display register button
//if($event['must_teacher'] if($person['is_teacher']) -> else do not display register button
// if $event['allSports'] --> else do not display register button
// elseif $_SESSION['sportID'] == $event['sportID'] --> else do not display register button
// elseif $_SESSION['otherSports']
// retrieve allowsSports WHERE eventID = $event['eventID'];
// else show register button;
//-----------------------------------------------------------------------------------
$event_object = event_select($eventID);
$event        = $event_object->fetch(PDO::FETCH_ASSOC);
if ($event) {
  $register_button = FALSE;

  if ($Acl->check_permission('page_view', $permission['group_id'], $permission['user_id'])) {
    $register_button = register_button_for_events(event_allowed_person($event, $cUser->user_id, $_SESSION['sportID']), $eventID);
  }

  ?>

  <?php
  $title = $event['evtName'];
  layout_template('head_section');
  layout_template('header');
  ?>
  <div class="content">
    <?php
    echo_full_events($event, $register_button);
    echo $back_btn;
    ?>
  </div>
  <?php layout_template('foot/foot_section'); ?>
<?php
  // event if;
} else {
  message(array('چنین رویدادی موجود نمیباشد'), 'error', TRUE);
//  redirect_to(BASEPATH . 'events');
}
?>