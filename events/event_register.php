<?php
// check permission // you have no permission
// check access ?!?
/* permission validation */
$permission_name = 'page_view';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
if(isset($_GET['event_id']) && !empty($_GET['event_id'])) {
  $event_id = $_GET['event_id'];
}
else {
  message('شناسه رویداد به درستی وارد نشده است');
  redirect_to($referer);
}
$event = event_select($event_id)->fetch(PDO::FETCH_ASSOC);
if(!$event) {
  message('چنین رویدادی با شناسه مشخص شده موجود نمی باشد');
  redirect_to($referer);
}

$title = 'ثبت نام در  ' . $event['evtName'];
/**
 * check to see is he/she an allowed person for registration  // you can not register in this event {
 * // * 1 - check to see if person information is filled out
 * // * 2 - check deadend time
 * // * 3 - max, min (age)
 * // * 4 - must teacher, must referee
 * // * 5 - check sports // if all sport return true; :) else load allowed_sports
 * }
 * capacity // check from event register table count * where event_id = event_id
 * if cost != 0 user must pay some money (go to payment page)
 * TODO: sport confirmation must be check in future :)
 * TODO: problem for registration payment (if while a person locate in payment page the other one come and register faster, and the slower person payed event cost :| and it's the last place for registration
 */
$user = find_person_by_id($cUser->user_id);
$match_weight_columns_value = array(
  'min_age < :min_age '           => ageCalc($user['birth_date']),
  'AND max_age > :max_age '       => ageCalc($user['birth_date']),
  'AND min_weight < :min_weight ' => $user['weight'],
  'AND max_weight > :max_weight ' => $user['weight'],
  'AND evtID = :evtID '           => $event_id,
);

//var_dump($user);die();
$registration = FALSE;
if (!$user) {
  $errors[] = 'شما در سایت ثبت نام نکرده اید، وارد شوید و یا ثبت نام نمایید.';
}
elseif($user['sex'] != $event['sex']) {
  $sex = $event['sex'] ? ' آقایان ' : ' بانوان';
  $errors[] = "فقط {$sex} مجاز به شرکت در رویداد مشخص شده می باشند.";
}
elseif (!event_allowed_person($event, $cUser->user_id, $_SESSION['sportID'])) {
  $errors[] = 'طبق محدودیت های قرار داده شده، شما نمی توانید در این رویداد ثبت نام نمایید';
}
elseif (!($event['maxReg'] > event_capacity($event_id)) && ($event['maxReg'])) {
  $errors[] = 'متاسفانه ظرفیت ثبت نام این رویداد پر شده است.';
}
elseif (!$event['cost'] && !event_payment($event_id, $cUser->user_id)) {
  // must go to payment page
}
elseif ($event['has_weight'] && incomplete_atprofile($user['weight'])) {
  $errors[] = 'وزن شما وارد نشده است، لطفا به صفحه تنظیمات رفته، وزن خود را تعیین کرده و سپس به ثبت نام در رویداد بپردازید';
}
elseif ($event['has_weight'] && !$event['evt_table_close']) {
  $errors[] = 'جدول های این رویداد تاکنون بسته نشده است لطفا منتظر بمانید...';

}
elseif ($event['has_weight'] && !$match_weight = match_weight_select($match_weight_columns_value, NULL, array('limit' => 1))->fetch(PDO::FETCH_ASSOC)) {
  $errors[] = 'متاسفانه افرادی با وزن و سن شما نمیتوانند در این رویداد شرکت نمایند.';
}
elseif(!$you_already_registered = TRUE) {
  $errors[] = 'شما در حال حاضر ثبت نام شده اید، منتظر تایید استاد خود بمانید';
}
else {
  $registration = TRUE;
}

if ($registration) {
  $evt_register_columns_values = array(
    'info_id'  => $user['infoID'],
    'event_id' => $_GET['event_id'],
    'created'  => time(),
  );
  // register person into this event
  $db->beginTransaction();
  $transaction_continue = $transaction_complete = event_register_insert($evt_register_columns_values);
  if ($event['has_weight'] && $transaction_continue) {
    $match_group_columns_values = array(
      'evtID'    => $_GET['event_id'],
      'matchWID' => $match_weight['matchWID'],
      'user_id'  => $user['userID'],
    );
    $transaction_complete = matches_group_insert($match_group_columns_values);
  }
  if ($transaction_complete) {
    $confirmation = $event['confirm_needed'] ? ' منتظر تایید استاد خود بمانید' : NULL;
    $errors[] = 'ثبت نام در رویداد مورد نظر با موفقیت انجام شد' . $confirmation;
    $db->commit();
  }
  else {
    $errors[] = 'ثبت نام ناموفق بود لطفا دوباره امتحان نمایید';
    $registration = FALSE;
    $db->rollBack();
  }
}/* else {
  $errors[] = 'شما به دلیلی نامشخص نمیتوانید در این رویداد ثبت نام نمایید لطفا دوباره تلاش نمایید';
}*/
$message_class = $registration ? 'success_message' : 'errors';
layout_template('head_section');
layout_template('header');
?>
  <div class="content">
    <?php echo errors_to_ul($errors, 'box_shadow padding ' . $message_class); ?>
  </div>
<?php layout_template('foot/foot_section'); ?>