<?php
/**
 * User: Ramin
 * Date: 4/10/2015
 * Time: 11:08 AM
 */
// Ramin:fixMe in future we must do table sorting in a better and faster way than foreach 1394.01.21
// this page gives a value and sort registered users in
/*
 * retrieve all match_weights sort
 * retrieve all person who registered in this event from event_register table
 * loop in persons -> check in match_weights
 */
/* permission validation */
$permission_name = 'event_table_edit';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$event_id = $_GET['event_id'];
//$event_id = 76;
$event = Event::find_by_id($event_id);
//var_dump($event);
//var_dump(!compare_two_date($event->deadendDate));
//die;
if (!$event) {
  $errors[] = 'چنین رویدادی موجود نمیباشد';
  $class = 'errors';
}
elseif ($event->evt_table_sorted) {
  $errors[] = 'جدول های مربوط به این رویداد قبلا ساخته شده اند و اکنون شما میتوانید آنها را در ای صفحه مشاهده نمایید';
  $errors[] = '<div><a class="btn btn_blue" href="/events/events-table-show/' . $event_id . '"> مشاهده جداول</a></div>';
  $class = 'errors';
}
elseif (compare_two_date($event->deadendDate)) {
  $errors[] = 'مهلت ثبت نام هنوز پایان نیافته، هنوز نمیتوانید جدول مسابقات را بچینید';
  $errors[] = '<div><a class="btn btn_blue" href="/management/events/"> صفحه مدیریت</a></div>';
  $class = 'errors';
}
elseif ($event->inserterID != $cUser->user_id) {
  $errors[] = 'شما اجازه دسترسی به این صفحه را ندارید';
  $errors[] = '<div><a class="btn btn_blue" href="/events/' . $event_id . '">صفحه این رویداد</a></div>';
  $class = 'errors';
}
elseif ($event->event_register_num(1) < 2) {
  $errors[] = 'تعداد افراد ثبت نام کرده کمتر از 2 نفر می باشد و نمیتوان جدولی برای این تعداد در نظر گرفت';
  $class = 'errors';
}
else {
  $match_group_columns_values = array(
    'evtID = :evtID' => $event_id
  );
  $query = db_select()
    ->from('user u')
    ->innerJoin('`matches_group` m ON `m`.`user_id` = `u`.`userID`')
    ->select(NULL)
    ->select('*')
    ->innerJoin('`state` s ON `s`.`stateID` = `u`.`stateID`')
    ->leftJoin('`userpic` p ON `p`.`userID` = `u`.`userID`')
    ->where('evtID', $event_id)
  ;
//    echo $query->getQuery();
//    die();
  $match_groups = $query->execute();
  $groups = array();
  //  $match_groups               = match_group($match_group_columns_values);
  while ($match_group = $match_groups->fetch(PDO::FETCH_ASSOC)) {

    $groups[$match_group['matchWID']][$match_group['user_id']]['name'] = $match_group['first_name'] . ' ' . $match_group['last_name'];
    $groups[$match_group['matchWID']][$match_group['user_id']]['user_id'] = $match_group['user_id'];
    $groups[$match_group['matchWID']][$match_group['user_id']]['state'] = $match_group['stateName'] . ' ' . $match_group['countyName'];
    $groups[$match_group['matchWID']][$match_group['user_id']]['pic'] = $match_group['picUrl'];
  }
//  var_dump($groups);die;
  $table_creator = array();
  $base = $power = 0;
  //  $participant_number = count($groups[4]);
  //  while ($base < $participant_number) {
  //    $base = pow(2, $power);
  //    $power++;
  //  }
  foreach ($groups as $mwid => $group) {
    $table_column = $power = 0;
    $participant_number = count($groups[$mwid]);
    while ($table_column < $participant_number) {
      $table_column = pow(2, $power);
      $power++;
    }
    $rests = $table_column - $participant_number;
    for ($i = 0; $i < $rests; $i++) {
      if ($i % 2 == 0) {
        $table_creator[$mwid][$i] = 'rest';
      }
      else {
        $table_creator[$mwid][$table_column - $i] = 'rest';
      }
    }
    foreach ($group as $person) {
      $insertion = FALSE;
      do {
        $position = rand(0, $table_column - 1);
        if (empty($table_creator[$mwid][$position])) {
          $table_creator[$mwid][$position] = $person;
          $insertion = TRUE;
        }
      } while (!$insertion);
    }
    // sort rests base on standards
    foreach ($table_creator as $key => $table) {
      ksort($table);
      $table_creator[$key] = $table;
    }
    //    $all_array[$mwid] = $main_array;
  }
  //  var_dump($table_creator);
  //  die();
  $main_table_creator = $table_creator;
  $all_array = array();
  $transaction_complete = FALSE;
  foreach ($main_table_creator as $mwid => $table_creator) {
    $item = '';
    $sub_array = $main_array['teams'] = $main_array['results'] = $result_temp = $result_main = array();
    foreach ($table_creator as $key => $table) {
      $item = $table == 'rest' ? "استراحت" : $table['name'];
      array_push($sub_array, $item);
      if ($key % 2 == 1) {
        array_push($main_array['teams'], $sub_array);
        if (in_array('استراحت', $sub_array)) {
          foreach ($sub_array as $temp) {
            if ($temp == 'استراحت') {
              $result_temp[] = 0;
            }
            else {
              $result_temp[] = 1;
            }
          }
        }
        array_push($main_array['results'], $result_temp);
        $sub_array = $result_temp = array();
      }
    }
    $all_array[$mwid] = $main_array;
  }
  //  var_dump($all_array);
  $db->beginTransaction();
  foreach ($all_array as $key => $data) {
    $columns_values = array(
      'event_id'     => $event_id,
      'mwid'         => $key,
      'uid'          => $cUser->user_id,
      'matches_data' => serialize($data),
    );
    $transaction_complete = matches_data_insert($columns_values);
    //    var_dump($transaction_complete);
    if (!$transaction_complete) {
      $db->rollBack();
      break;
    }
  }
  //  /*
  $columns_value_event_update = array(
    'evt_table_sorted' => 1,
  );
  $event_update_where = array(
    'eventID' => $event_id,
  );
  if ($transaction_complete && event_update($columns_value_event_update, $event_update_where)) {
    $errors['success'] = 'جدول های رویداد مورد نظر ساخته شد، میتواند آنها را در صفحه زیر مشاهده نمایید';
    $errors['button'] = '<div><a class="btn btn_blue" href="/events/events-table-show/' . $event_id . '"> مشاهده جداول</a></div>';
    $class = 'success_message';
    message($errors, $class);
    $db->commit();
    redirect_to(BASEPATH . 'events/event-table/' . $event_id . '/' . $mwid);
  }
  else {
    $errors[] = 'متاسفاه عملیات با مشکل روبرو شده است';
    $errors[] = 'جدول ها ساخته نشده است';
    $errors[] = 'لطفا دوباره امتحان کنید.';
    $class = 'errors';
    //    $db->rollBack();
  }
  //  */
}
$access_table = FALSE;
?>
<?php
layout_template('head_section');
layout_template('header');
?>
<div class="content ltr">
  <div class="table-show clearfix"></div>
  <?php echo errors_to_ul($errors, 'box_shadow padding ' . $class); ?>
  <!--  --><?php //echo $html_table; ?>
  <?php echo $back_btn; ?>
</div>
<?php layout_template('foot/foot_section'); ?>



