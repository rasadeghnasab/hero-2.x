<?php
//die('index');
require_once(__DIR__ . '/../includes/initial.php');
pagination_variables('GET');
$url = '/events';
$sportID = NULL;
if (isset($_GET['sportID']) && ($_GET['sportID'] != '' && $_GET['sportID'] != 'all')) {
  $sportID = $_GET['sportID'];
  $url .= '/' . $sportID;
}
else {
  if (isset($cUser->user_id)) {
    insert_update_notificatoins(array(
        'userID',
        'new_event'
    ), array(
        ':userID' => $cUser->user_id,
        ':new_event' => 0
    ), array('new_event'), TRUE);
    check_notifications(TRUE);
  }
  $url .= '/all';
}
?>
<?php
$title = 'رویدادها';
layout_template('head_section');
?>
<?php layout_template('header'); ?>
  <div class = "content">
    <div class = "new_row split_bottom">
      <form id = "events" action = "" method = "get">
        <label for = "sportID">نمایش رویدادها در ورزش: </label>
        <?php
        $sport_attrs = array(
            'id' => 'sportID',
            'name' => 'sportID',
            'class' => 'chosen-select chosen-rtl new_input',
        );
        $sports_custom_options = array('همه' => 'all');
        $selected_value = isset($_GET['sportID']) ? $_GET['sportID'] : NULL;
        ?>
        <?php echo select_element(all_sports_name(), 'sport_id', 'sportName', $sport_attrs, $selected_value, $sports_custom_options); ?>
<!--        <input class = "btn btn_orange" type = "submit" value = "جستجو" />-->
      </form>
    </div>
    <?php
    $events = event_select(NULL, $sportID);
    $pagination = pagination_function($url);
    $fetched_events = event_fetch_pdo($events);
    node_view_multiple('event', $fetched_events);
    ?>
    <div class = "paginate"><?php echo $pagination;?></div>
    <?php echo $back_btn; ?>
  </div>
<?php layout_template('foot/foot_section'); ?>