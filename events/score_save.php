<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/14/15
 * Time: 7:24 PM
 */
//echo json_encode($_POST);
//echo json_decode($_POST['table_data']);
//die();
// if user has table_edit permission
// if user has permission to edit this event particularly
/* permission validation */
$permission_name = 'event_table_edit';
$return          = TRUE;
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$result          = array(
    'done'    => FALSE,
    'message' => array(
        'text',
        'class'
    ),
    'data'    => NULL,
);
$done            = FALSE;
$required_fields = array(
    'event_id',
    'mwid',
    'table_data',
);
if (validate_presences($required_fields)) {
  foreach ($required_fields as $field) {
    $$field = $_POST[$field];
  }
  $table_data = json_decode($table_data);
  // load event and if inserterID == $cUser->user_id user can insert a row
  $event = event_select($event_id)->fetch(PDO::FETCH_ASSOC);
  if ($event != FALSE) {
    if ($event['inserterID'] == $cUser->user_id) {
      $columns_values = array(
          'event_id'     => $event_id,
          'mwid'         => $mwid,
          'uid'          => $cUser->user_id,
          'matches_data' => serialize($table_data),
      );
      // insert into matches_data table
      $done           = matches_data_insert($columns_values);
      $message        = 'با موفقیت ثبت شد';
    }
  }
  else {
    $message = errors_to_ul($errors, 'errors');
  }
}
$class                      = $done ? 'success_message' : 'errors_message';
$result['done']             = $done;
$result['message']['text']  = $message;
$result['message']['class'] = $class;
echo json_encode($result);
