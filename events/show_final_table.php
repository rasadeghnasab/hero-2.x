<?php
/**
 * Date: 3/28/15
 * Time: 6:29 PM
 */
/* permission validation */
$permission_name = 'page_view';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$event_id = $_GET['event_id'];
$mw_id = $_GET['mw_id']; // mw stands for match weight
// load event
$event = Event::find_by_id($event_id);
if ($event) {
  $event_type = eventType::find_by_id($event->evtTypeID);
}
if (!$event) {
  $errors[] = 'چنین رویدادی موجود نمی باشد';
}
elseif (!$event->evt_table_close) {
  $errors[] = 'جدول رویداد هنوز بسته نشده است';
}
elseif (!$event_type->has_weight) {
  $errors[] = 'منتظر بمانید تا مسئول مربوطه جدول وزن ها را تکمیل نماید';
}
elseif (!$event->evt_table_sorted) {
  $errors[] = 'جدول وزن های ' . $event_type->evt_type_name . ' هنوز آماده نشده لطفا منتظر بمانید';
}
if (is_array($errors) && !empty($errors)) {
  $error_message = errors_to_ul($errors, 'padding');
  require_once(__DIR__ . '/../errors/403.php');
  die();
}
//select MWID from match_weight and return age_class_name;
// return on match weight for check match_weight existence
$conditions = array(
  ' matchWID = :matchWID' => $mw_id,
  ' AND evtID = :evt_id'  => $event_id
);
$evt_one_match = match_weight_select($conditions)->fetch(PDO::FETCH_ASSOC);

// return all match weight with this age_class = ... for eventID = ... (for submenu)
$menu_submenu = mch_w_menu_creator($event_id, $evt_one_match['age_class_name']);
/*
 * 1 - check permissions {
 *  //first access permission
 * }
 * 2 - check to see if this event has weight and MWID and ...
 * 3 - check this event MWID and return age_class_name {
 * // SELECT DISTINCT WHERE event_id = ...; to create menu
 * // SELECT WHERE event_id = ... AND age_class_name = ... ; to create sub menu
 * }
 * // 4 - check to see if table_close ??
 * // 5 - if table_close { show table } else { show error_message };
 */
$menu = sub_menu_creator($menu_submenu['main_menu']) . sub_menu_creator($menu_submenu['sub_menu'], 'child');
$columns_values_conditions = array(
  'mwid = :mwid '             => $mw_id,
  'AND event_id = :event_id ' => $event_id,
);
$table = event_matches_data($columns_values_conditions);
//var_dump($table);die();
$access_table = FALSE;
if (isset($cUser->user_id) && $cUser->user_id == $event->inserterID) {
  $access_table = TRUE;
}
layout_template('head_section');
layout_template('header');
?>
<div class="content">
  <?php echo $menu; ?>
  <div class="table-show clearfix  ltr"></div>
  <!--  --><?php //echo $table['sorted_table_html']; ?>
  <?php echo $back_btn; ?>
</div>
<script> var table_data = <?php echo json_encode(unserialize($table['matches_data'])); ?></script>
<?php if ($access_table): ?>
  <script>
    var access_table = true,
      mwid = <?php echo $mw_id;?>,
      event_id =
    <?php echo $event_id; ?>
    ;
  </script>
<?php endif; ?>
<?php
layout_template('foot/foot_section');
?>
