<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/29/2015
 * Time: 3:24 AM
 */
/* permission validation */
$permission_name = 'event_table_edit';
$return = TRUE;
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$done = FALSE;
//echo '<meta charset="utf-8" />';
$message = '';
$event_id = $_POST['event_id'];
$event = Event::find_by_id($event_id);
//var_dump($event);
if ($event->get_table_close()) {
  $errors['evt_err'] = 'جدول این رویداد قبلا بسته شده است';
}
elseif ($cUser->user_id != $event->inserterID) {
  $errors['evt_err'] = 'این رویداد توسط فرد دیگری قرار داده شده و شما اجازه دسترسی به آن را ندارید.';
}
if (!$message = errors_to_ul($errors)) {
  // if ok close tables
  $event->set_table_close();
  $done = $event->save();
  $message = $done ? 'عملیات با موفقیت انجام شد' : 'درخواست ناموفق بود، لطفا دوباره امتحان نمایید';
}
$class = $done ? 'success_message' : 'errors';
$data = array(
  'message' => array(
    'text'  => $message,
    'class' => $class
  ),
  'done'    => $done,
);
echo json_encode($data);
//var_dump($data);