<?php
/*
 * Date: 3/25/15
 * Time: 1:36 AM
 */
/* steps
 * load event
 * check to see if event_table_close == 0
 * check to see if this event type needs table has_weight == 1
 * check to see if inserterID = $cUser->user_id;
 * if today date > deadend date => first: close event_table then:show create tables button. else show wait to deadend date
 * load event detail form
 */
/*
 * view
 * one new form
 * remove weight age if exist
 * -- we have not edit
 */
/* permission validation */
$permission_name = 'event_table_edit';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$event_id = $_GET['event_id'];
$event    = event_select($event_id)->fetch(PDO::FETCH_ASSOC);
if (!$event['has_weight']) {
  $errors['evt_err'] = 'رویداد مورد نظر نیازی به جدول بندی ندارد';
}
elseif (!compare_two_date($event['openingDay'])) {
  $errors['evt_err'] = 'مهلت ویرایش جدول این رویداد تمام شده است.';
}
elseif ($event['evt_table_close']) {
  $errors['evt_err'] = 'جدول این رویداد بسته شده است و دیگر قادر به ویرایش آن نیستید';
  if (compare_two_date($event['openingDay']) && !$event['evt_table_sorted']) {
    $errors[] = '<a href="/events/final-table-sort/'. $event_id .'" class="btn btn_black btn_larg img_middle table_sort">چیدن جدول های وزن های مختلف</a>';
  } else {
    $errors[] = '<a class="btn btn_blue" href="/events/events-table-show/'. $event_id .'"> جدول ها </a>';
  }
}
elseif ($cUser->user_id != $event['inserterID']) {
  $errors['evt_err'] = 'این رویداد توسط فرد دیگری قرار داده شده و شما اجازه دسترسی به آن را ندارید.';
}
if (isset($errors['evt_err'])) {
  message($errors, 'error', TRUE);
  require_once(__DIR__ . '/../errors/403.php');
  die;
}
layout_template('head_section');
layout_template('header');
?>
  <div class = "content">
    <?php
    $help_text = 'رویداد مورد نظر شما برای برگذاری و شروع ثبت نام به جدول نیاز دارد<br/>.
     جدول مورد نظر را بر اساس نیازهای خود تکمیل کرده و سپس بر روی دکمه اتمام کلیک کنید.</br>
     دقت داشته باشید که برای شروع عملیات ثبت نام حتما باید بر روی دکمه اتمام کلیک نمایید
     و نیز اگر بر روی این دکمه کلیک نمایید دیگر قادر به ادامه ویرایش نخواهید بود. </br>
     <strong>لطفا اطلاعات را با دقت پر نمایید!</strong>';
    help_tooltip($help_text, FALSE); ?>
    <div class = "profile_field">
      <h2>تنظیمات جدول بندی</h2>

      <div class = "box padding">
        <?php evt_table_add_form($event_id); ?>
      </div>
      <div class = "new_row">
        <!--        --><?php //help_tooltip($help_text, FALSE); ?>
        <?php echo evt_table_show(evt_table_select($event_id)); ?>
        <button id = "<?= $event_id ?>" data-ajax-dest="events/table_close.php" class = "btn btn_warning btn_medium evt_table_close">
          اتمام ویرایش <span class = "glyphicon glyphicon-warning-sign"></span>
        </button>
      </div>
    </div>
    <?php echo $back_btn ?>
  </div>
<?php layout_template('foot/foot_section'); ?>