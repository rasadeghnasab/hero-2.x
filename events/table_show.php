<?php
/*
 * Date: 3/25/15
 * Time: 12:24 PM
 */
/* permission validation */
$permission_name = 'page_view';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$event_id = $_GET['event_id'] ? $_GET['event_id'] : 0;
$event = Event::find_by_id($event_id);
if(!$event) {
  $errors[] = 'چنین رویدادی موجود نمی باشد';
}
elseif(!$event->evt_table_sorted) {
  $evt_type = eventType::find_by_id($event->evtTypeID);
  $errors[] = 'جدوهای رویداد ' . $evt_type->evt_type_name. ' هنوز چیده نشده اند، لطفا منتظر بمانید';
}
else {
  $event_tables = evt_table_select($event_id);
  if ($event_tables->rowCount()) {
    $tables = evt_table_show($event_tables);
  }
  else {
    $errors[] = 'جدول وزن ها تاکنون ایجاد نشده است';
  }
}
if($message = errors_to_ul($errors, 'box_shadow errors')) {
  $tables = $message;
}
?>
<?php
layout_template('head_section');
layout_template('header');
?>
  <div class = "content">
    <div class = "profile_field">
      <?php echo $tables; ?>
    </div>
    <?php echo $back_btn; ?>
  </div>
<?php layout_template('foot/foot_section'); ?>