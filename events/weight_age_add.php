<?php
/*$return = array(
    'success' => TRUE,
    'message' => 'امتحان به منظور تست',
);
echo json_encode($return);exit;*/
//echo '<meta charset="utf-8">';
/**
 * Date: 3/25/15
 * Time: 11:16 PM
 */
//var_dump($_GET);die();
/* permission validation */
$permission_name = 'event_table_edit';
$return          = TRUE;
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
/*
 * validate inputs
 *
 */
$required_fields         = array(
    'age_class_name',
    'min_age',
    'max_age',
);
$fields_with_max_lengths = array(
    'age_class_name' => 80,
    'min_age' => 2,
    'max_age' => 2,
);
$fields_with_min_length  = array(
    'age_class_name' => 5,
    'min_age' => 1,
    'max_age' => 1,
);

validate_presences($required_fields);
validate_input_lengths($fields_with_max_lengths, $fields_with_min_length);

$message = errors_to_ul($errors);
if ($message != FALSE) {
  $result = array(
      'success' => FALSE,
      'message' => $message,
  );
  json_encode($result);
  die;
}
// do match weight variable insert here
$skip_name = array('submit');
$add_in = array('age_class_name', 'min_age', 'max_age', 'evtID');
$array = reArrayFiles($_POST, $add_in, $skip_name);
$db_insert = FALSE;
$db->beginTransaction();
foreach($array as $columns_values) {
  $db_insert = event_table_insert($columns_values);
  if(!$db_insert){
    $db->rollBack();
    break;
  }
}
// if all db insertion is done, commit the insertions
if($db_insert) $db->commit();
// select and return
$result['success'] = $db_insert;
$result['message'] = $db_insert ? 'با موفقیت ثبت شد' : 'ثبت اطلاعات ناموفق بود لطفا دوباره تلاش نمایید';
echo json_encode($result);
