<?php
/**
 * foer  = follower
 * foing = following
 */
/* permission validation */
$permission_name = 'follow';
$return = TRUE;
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
if (isset($_POST['foingID'], $cUser->user_id)) {
  $foerID      = $cUser->user_id;
  $foingID     = $_POST['foingID'];
//  echo json_encode($foerID);
//  echo json_encode($foingID);
//  die();
  $is_follower = is_follower($foingID, $foerID);
  $result      = array();
  if (!empty($foingID)) {
    $done = $is_follower ? unfollow($foingID, $foerID) : follow($foingID, $foerID);
    if ($done) {
      $followers          = count_followers_following($foingID);
      $result['done']     = TRUE;
      $result['follower'] = $followers;
    }
    else {
      $result['done'] = FALSE;
    }
  }
  else {
    $result['done'] = FALSE;
  }
}
else {
  $result['done'] = FALSE;
}
echo json_encode($result);
