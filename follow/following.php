<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/8/15
 * Time: 8:01 PM
 */
require_once(__DIR__ . '/../includes/initial.php');

if(!isset($_GET['user_id'])) {
  $destination = $referer ? $referer : BASEPATH;
  redirect_to($destination);
}
$user_id = $_GET['user_id'];
require_once(__DIR__ . '/../profile/profile_init.php');
$uids = Follow::following($user_id);
$persons_string = '';
if(count($uids) > 0) {
  $users = find_persons_by_ids($uids);
  $persons_string = echo_person_for_management($users);
}

$persons_string = $persons_string != '' ? $persons_string : 'هنوز هیچ دوستی انتخاب نکرده است';
?>
<?php //$title = $first_name . " " . $last_name; ?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php layout_template('profile_head', $profile_variables); ?>
<div class="content">
  <?php echo $persons_string; ?>
</div>

<?php layout_template('foot/foot_section'); ?>
