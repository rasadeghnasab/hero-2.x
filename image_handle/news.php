<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 6/3/2015
 * Time: 3:28 PM
 */
require_once(__DIR__ . '/../includes/initial.php');
require_once(__DIR__ . '/../includes/classes/class.upload.php');
/*
 * A function for load all user info with user_id
 * TODO:: user id, job id, sport(s) id, is_teacher, is_referee, and ...
 */
$location = SITE_ROOT . DS . 'files' . DS . 'picture';
// check image
// upload image on user specified folder
// we create an instance of the class, giving as argument the PHP object
// corresponding to the file field from the form
// This is the fallback, using the standard way
$handle = new Upload($_FILES['files']);
$image_bundles_properties = array(
  'cover_photo' => array(
    'validate' => array(
      array('property' => 'image_src_x', 'value' => 400, 'operator' => '>'),
      array('property' => 'image_src_y', 'value' => 150, 'operator' => '>'),
      array(
        'property' => 'file_is_image',
        'value'    => TRUE,
        'operator' => '='
      ),
    ),
    'upload_properties' => array(
      'thumb_path' =>
      array(
        'path' => 'thumb_path',
        'property' => array(
          array('property' => 'image_src_x', 'value' => 640, 'operator' => '>'),
          array('property' => 'image_src_y', 'value' => 480, 'operator' => '>'),
          array('property' => 'file_new_name', 'value_is_var' => TRUE, 'value' => 'name', 'operator' => '='),
          array('property' => 'image_resize', 'value' => TRUE, 'operator' => '='),
        ),
      ),
      'large_path' =>
      array(
        'path' => 'large_path',
        'property' => array(
          array('property' => 'image_src_x', 'value' => 850, 'operator' => '<', 'new_value' => 850),
          array('property' => 'image_src_x', 'value' => 900, 'operator' => '>', 'new_value' => 850),
          array('property' => 'image_src_y', 'value' => 480, 'operator' => '>', 'new_value' => 300),
          array('property' => 'file_new_name', 'value_is_var' => TRUE, 'value' => 'name', 'operator' => '=', 'new_value' => ''),
          array('property' => 'image_resize', 'value' => TRUE, 'operator' => 'boolean', 'new_value' => TRUE),
        ),
      )
    ),
  ),
);
$validate_image = $handle->image_src_x > 400 && $handle->image_src_y > 150 && $handle->file_is_image;
if ($validate_image) {
  $name = time() . '_' . $cUser->user_id;
  $handle->file_new_name_body = $name;
  $handle->image_resize = TRUE;
  $handle->image_x = $handle->image_src_x > 640 ? 640 : $handle->image_src_x;
  $handle->image_y = $handle->image_src_y > 480 ? 480 : $handle->image_src_y;
  // upload in actual size
  $handle->process($location . DS . 'large' . DS . $cUser->user_id);
  $large = $handle->processed;
  // initialization for thumb upload
  $handle->file_new_name_body = $name;
  $handle->image_resize = TRUE;
  $handle->image_x = $handle->image_src_x < 850 || $handle->image_src_x > 900 ? 850 : $handle->image_src_x;
  //  if($handle->image_src_y < 150) {
  $handle->image_y = 300;
  //  }
  //$thumb = $handle->process($location.DS.'thumb'.DS.$cUser->user_id)->processed;
  $handle->process($location . DS . 'thumb' . DS . $cUser->user_id);
  $thumb = $handle->processed;
}
else {
  $data = array(
    'message' => 'تصویر دریافتی باید دارای حداقل عرض 400 و حداقل طول 150 پیکسل باشد.',
    'class' => 'error_message',
  );
  echo json_encode(array('done' => $validate_image, 'data' => $data));
  die();
}
//echo json_encode($handle->file_is_image);die;
if ($thumb) {
  $attributes = array(
    'image_name' => $name.'.'.$handle->file_src_name_ext,
    'uid' => $cUser->user_id,
    'bundle' => 'cover_photo',
  );
  $save = Image::make($attributes)->save();
  $data = array(
    'url' => array(
      'thumb' => "/files/picture/thumb/{$cUser->user_id}/{$name}.{$handle->file_src_name_ext}",
      'large' => "/files/picture/large/{$cUser->user_id}/{$name}.{$handle->file_src_name_ext}",
    )
  );
  echo json_encode(array('done' => (bool) $save, 'data' => $data));
}
