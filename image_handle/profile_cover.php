<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/18/2015
 * Time: 12:00 AM
 */
/* permission validation */
//$permission_name = 'image_upload';
//$return = TRUE;
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
require_once(__DIR__ . '/../includes/classes/class.upload.php');
$handle = new Upload($_FILES['cover_photo']);

/*
 * TODO:: Image validate base on image bundle
 * TODO:: Image path base on image bundle
 * TODO:: Image resize base on image bundle
 * TODO:: Image upload base on path and bundle.
 */
 /*
 * A function for load all user info with user_id
 * TODO:: user id, job id, sport(s) id, is_teacher, is_referee, and ...
 */
$location = SITE_ROOT . DS . 'files' . DS . 'picture';
// check image
// upload image on user specified folder
// we create an instance of the class, giving as argument the PHP object
// corresponding to the file field from the form
// This is the fallback, using the standard way
$bundle = key($_FILES);
//$files = array();
//var_dump($_FILES);die;
//foreach ($_FILES[$bundle] as $k => $l) {
//  foreach ($l as $i => $v) {
//    if (!array_key_exists($i, $files))
//      $files[$i] = array();
//    $files[$i][$k] = $v;
//  }
//}
//$files[$bundle] = $files;

function image_validate($image_name, $handled_file) {
  $image_bundles_properties = array(
    'cover_photo' => array(
      'validate' => array(
        array('property' => 'image_src_x', 'value' => 400, 'operator' => '>'),
        array('property' => 'image_src_y', 'value' => 150, 'operator' => '>'),
        array(
          'property' => 'file_is_image',
          'value'    => TRUE,
          'operator' => '='
        ),
      ),
    ),
  );
  $validation = $image_bundles_properties[$image_name]['validate'];
  // if validation is equal to true or false return theme, e.g. if we want to pass the validation we can set validate => true;
  if (is_bool($validation)) {
    return $validation;
  }
  foreach ($validation as $property_validate) {
    if ($property_validate['operator'] == '>') {
      if ($handled_file->$property_validate['property'] < $property_validate['value']) {
        return FALSE;
      }
    }
    elseif ($property_validate['operator'] == '<') {
      if ($handled_file->$property_validate['property'] > $property_validate['value']) {
        return FALSE;
      }
    }
    elseif ($property_validate['operator'] == '=') {
      if ($handled_file->$property_validate['property'] != $property_validate['value']) {
        return FALSE;
      }
    }
  }
  return TRUE;
}

$handle = new Upload($_FILES['cover_photo']);
//$validate_image = $handle->image_src_x > 400 && $handle->image_src_y > 150 && $handle->file_is_image;
$validate_image = image_validate($bundle, $handle);
//echo json_encode($validate_image);
//die;
if ($validate_image) {
  $name = time() . '_' . $cUser->user_id;
  $handle->file_new_name_body = $name;
  $handle->image_resize = TRUE;
  $handle->image_ratio_y         = true;
  $handle->image_x = $handle->image_src_x > 640 ? 640 : $handle->image_src_x;
//  $handle->image_y = $handle->image_src_y > 480 ? 480 : $handle->image_src_y;
  // upload in actual size
  $handle->process($location . DS . 'large' . DS . $cUser->user_id);
  $large = $handle->processed;
  // initialization for thumb upload
  $handle->file_new_name_body = $name;
  $handle->image_resize = TRUE;
  $handle->image_ratio_y         = true;
  $handle->image_x = $handle->image_src_x < 850 || $handle->image_src_x > 900 ? 850 : $handle->image_src_x;
  //  if($handle->image_src_y < 150) {
//  $handle->image_y = 300;
  //  }
  //$thumb = $handle->process($location.DS.'thumb'.DS.$cUser->user_id)->processed;
  $handle->process($location . DS . 'thumb' . DS . $cUser->user_id);
  $thumb = $handle->processed;
}
else {
  $data = array(
    'message' => 'تصویر دریافتی باید دارای حداقل عرض 400 و حداقل طول 150 پیکسل باشد.',
    'class'   => 'error_message',
  );
  echo json_encode(array('done' => $validate_image, 'data' => $data));
  die();
}
//echo json_encode($handle->file_is_image);die;
if ($thumb) {
  $attributes = array(
    'image_name' => $name . '.' . $handle->file_src_name_ext,
    'uid'        => $cUser->user_id,
    'bundle'     => 'cover_photo',
  );
  $image = Image::make($attributes);
  $save = $image->save();
  $data = array(
    'url' => array(
      // its better to use create_source() method
      // $image->create_source('thumb');
      'thumb' => '/files/picture/thumb/' . $cUser->user_id . '/' . $name . '.' . $handle->file_src_name_ext,
      // $image->create_source('large');
      'large' => '/files/picture/large/' . $cUser->user_id . '/' . $name . '.' . $handle->file_src_name_ext,
    )
  );
  echo json_encode(array('done' => (bool) $save, 'data' => $data));
}
