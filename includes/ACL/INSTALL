Tackle - Tiny ACL Authentication for PHP

	Updates and information for this project can always 
	be found at http://tackle.sourceforge.net.

Installation

Uncompress the package in a directory that is in your PHP include path. Optionally, 
you can add the location of the tackle.inc file to the "include_path" entry in your 
php.ini. Even more optionally, you can simply provide a full path to the tackle.inc 
file when you include it in your own application.

Edit the tackle config.inc file for your environment. First, make sure ADODB_PATH 
points to the directory that contains the adodb.inc.php file provided by ADOdb (see 
ADOdb Database Library). Next, populate the database connection information. The 
DB_PLATFORM value should be a driver name from the ADOdb database drivers list. 
("mysql" or "postgres7" are good bets.)

If your application uses its own database, run the appropriate create script to 
create the tackle database structure. If your application doesn't require its own 
database, create a database for tackle and run the appropriate database create script.

   1. For mysql: mysqladmin create tackle
   2. Edit the tackle.ini file for your database connection information and environment.
   3. Browse to tackle/setup/install_tackle.php

Include tackle.inc somewhere near the top of your PHP application.

Installation Summary

   1. Make sure the ADOdb Database Library is installed. (There are RPMs available 
   		for several Linux distributions available at rpmfind.net.
   2. Uncompress the tackle package somewhere browsable.
   3. Make sure PHP or your application knows where to include tackle.inc.
   4. Edit tackle.ini to point to ADOdb. If necessary, configure your database 
   		connection as well.
   5. Browse to tackle/setup/install_tackle.php
   6. Include tackle.inc in your application.

Using Tackle
Usage Summary

   1. Create a tackle object.
   2. Create some requestors, resources, and actions.
   3. Create permissions.
   4. Authenticate users.

Example
<?PHP
require_once( "../tackle/tackle.inc" );
// Tackle will automatically include ADOdb.

// your application here...

// Let's create some resources and actions
$t = new tackle();

// You can over-ride the config.inc connection information by using
// $t = new tackle( FALSE, 'postgres7', 'localhost', 'tackleapp', 'tackleuser', 'tacklepw' );

// Turn on ADOdb's database-level debugging by using
// $t = new tackle( TRUE );

if ( $t->createResource( "article" ) ) {
$t->createAction( "article", "view" );
$t->createAction( "article", "add" );
}
if ( $t->createResource( "user" ) ) {
$t->createAction( "user", "add" );
$t->createAction( "user", "change" );
$t->createAction( "user", "remove" );
}

// Now for some requestors: groups and members
$t->createGroup( "anonymous", "Anonymous Users" );
$t->createGroup( "registered", "Registered Users" );
$t->createGroup( "admin", "Administrators" );

$t->createMember( "mjones" );
$t->createMember( "msmith" );

// Add the members to groups.
$t->addToGroup( "anonymous", "mjones" );
$t->addToGroup( "registered", "msmith" );

// Finally, let's create some permissions
$t->addPermission( "group", "anonymous", "article", "view", "ALLOW" );
$t->addPermission( "group", "registered", "article", "add", "ALLOW" );
$t->addPermission( "group", "admin", "user", "add", "ALLOW" );
$t->addPermission( "group", "admin", "user", "change", "ALLOW" );
$t->addPermission( "group", "admin", "user", "remove", "ALLOW" );

// We'll give msmith permission to remove users, even though he's not an admin.
$t->addPermission( "member", "msmith", "user", "remove", "ALLOW" );

// Finally let's pretend we're in your code where some user (Mr. Jones) is trying to
// add a new article:
$user = "mjones";
if ( ! tackle_authorizedMember( $user, "add", "article" ) {
die( "We're very sorry $user; you don't have permission to add new articles." );
}
// Your article-adding code goes here.
?>

$Id: INSTALL,v 1.4 2003/10/21 21:40:57 richtl Exp $