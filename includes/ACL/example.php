<?PHP
require_once( "./tackle.inc" );
// Tackle will automatically include ADOdb.

// your application here...

// Let's create some resources and actions
$t = new tackle();

// You can over-ride the config.inc connection information by using
//$t = new tackle( FALSE, 'postgres7', 'localhost', 'tackleapp', 'tackleuser', 'tacklepw' );

// Turn on ADOdb's database-level debugging by using
$t = new tackle( TRUE );

if ( $t->createResource( "article" ) ) {
$t->createAction( "article", "view" );
$t->createAction( "article", "add" );
}
if ( $t->createResource( "user" ) ) {
$t->createAction( "user", "add" );
$t->createAction( "user", "change" );
$t->createAction( "user", "remove" );
}

// Now for some requestors: groups and members
$t->createGroup( "anonymous", "Anonymous Users" );
$t->createGroup( "registered", "Registered Users" );
$t->createGroup( "admin", "Administrators" );

$t->createMember( "mjones" );
$t->createMember( "msmith" );

// Add the members to groups.
$t->addToGroup( "anonymous", "mjones" );
$t->addToGroup( "registered", "msmith" );

// Finally, let's create some permissions
$t->addPermission( "group", "anonymous", "article", "view", "ALLOW" );
$t->addPermission( "group", "registered", "article", "add", "ALLOW" );
$t->addPermission( "group", "admin", "user", "add", "ALLOW" );
$t->addPermission( "group", "admin", "user", "change", "ALLOW" );
$t->addPermission( "group", "admin", "user", "remove", "ALLOW" );

// We'll give msmith permission to remove users, even though he's not an admin.
$t->addPermission( "member", "msmith", "user", "remove", "ALLOW" );

// Finally let's pretend we're in your code where some user (Mr. Jones) is trying to
// add a new article:
$user = "mjones";
if ( ! tackle_authorizedMember( $user, "add", "article" ) ) {
	die( "We're very sorry $user; you don't have permission to add new articles." );
}
// Your article-adding code goes here.
?>