<?PHP
// Copyright (c) 2002-2003 ars Cognita, Inc., all rights reserved
/********************************************************************************
	
     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; version 2 of the License.
   
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
   
     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
   
*******************************************************************************/
/**
* tackle - Tiny ACL Authentication Library for PHP
*
* Installation script
*  
* @author		$Author: richtl $
* @version		$Revision: 1.4 $
*
* @package tackle
* @subpackage setup
*/
 
define( 'TACKLE_SCHEMA', 'tackle_schema.xml' );

// Process the installation configuration file
$config = parse_ini_file( '../tackle.ini' );

/**
* Require the ADODB xml-schema library
*/
require( "{$config['adodb']}/adodb-xmlschema.inc.php" );

print "<H1>Tackle</H1>";
print "<P>Installing <EM>Tackle</EM> on {$config['dbName']}...</P>";

// Connect to the database.
$db = ADONewConnection( $config['dbType'] );
if( !$db->Connect( $config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName'] ) ) {
	die( "Unable to connect to database. Please verify database configuration in tackle.ini." );
}

// Instantiate the schema object, parse the schema file, and execute
// the resulting SQL.
$schema = new adoSchema( $db );
$sql = $schema->ParseSchema( TACKLE_SCHEMA );
$result = $schema->ExecuteSchema( $sql );
$schema->Destroy();

print "<P>Finished!</P>";
print "<H3>Tackle has been installed on the {$config['dbName']} database.</H3>";
?>