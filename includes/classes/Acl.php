<?php

/**
 * Created by PhpStorm.
 * User: Raminli
 * Date: 3/24/15
 * Time: 1:51 AM
 */
class Acl {

  //  private $db;
  public $role_message = '';

  /*
   * class constructor
   */
  function __construct( /*$database*/) {
    //    $this->db = $database;
  }

  /**
   * check user or group permissions
   *
   * @param $permission_name string
   * @param $group_id string | int
   * @param $user_id string | int
   *
   * @return boolean
   */
  public function check_permission($permission_name, $group_id = NULL, $user_id = NULL) {
    global $cUser;
    // if there is no permission name set we return true by default
    $job_id      = isset($cUser->jid) ? $cUser->jid : FALSE;
    $bool_job_id = $job_id == FALSE ? NULL : $job_id;
    $uid         = isset($cUser->user_id) ? $cUser->user_id : NULL;
    $group_id    = $group_id != NULL ? $group_id : $bool_job_id;
    $user_id     = $user_id != NULL ? $user_id : $uid;
    if ($permission_name == NULL) {
      return TRUE;
    }
    if ($user_id != NULL && ($allow = $this->user_permission($permission_name, $user_id)) !== NULL) {
      return $allow;
    }
    if (($allow = $this->group_permission($permission_name, $group_id)) !== NULL) {
//      var_dump($job_id);die();
      return $allow;
    }
//    var_dump($this->group_permission($permission_name, $group_id));die();
    // if no record in user_permission or group_permission founded
    // search for role name to find role_default_allow
    return (boolean) $this->role_default_allow($permission_name);
  }

  /**
   * check user or group permissions on things that can be done by multi users
   * e.g. event editing, post editing, or ...
   *
   * @param $permission_name string
   * @param $group_id string | int
   * @param $user_id string | int
   *
   * @return boolean
   */
  public function check_co_work_permission($permission_name, $group_id, $user_id) {
    // if there is no permission name set we return true by default
    if ($permission_name == NULL) {
      return TRUE;
    }
    if ($user_id != NULL && ($allow = $this->co_work_user_permission($permission_name, $user_id)) !== NULL) {
      return $allow;
    }
    if (($allow = $this->co_work_group_permission($permission_name, $group_id)) !== NULL) {
      return $allow;
    }
    // if no record in user_permission or group_permission founded
    // search for role name to find role_default_allow
    return $this->role_default_allow($permission_name);
  }

  /**
   * @param $permission_name string
   * @param $user_id string | int
   *
   * @return boolean
   */
  private function user_permission($permission_name, $user_id) {
    $query = 'SELECT * ';
    $query .= 'FROM `user_permission` ';
    $query .= 'JOIN `role` ON `user_permission`.`role_id` = `role`.`role_id` ';
    $query .= 'WHERE (`user_id` = :user_id AND `role_name` = :role_name ) ';
    $query .= 'LIMIT 1 ';
    $columns_values = array(
      ':user_id'   => $user_id,
      ':role_name' => $permission_name,
    );
    $result         = sql_query_executor($query, $columns_values)->fetch(PDO::FETCH_ASSOC);
//    $query              = db_select()
//      ->from('user_permission')
//      ->select(NULL)
//      ->select('*')
//      ->innerJoin('`role` ON `user_permission`.`role_id` = `role`.`role_id`')
//      ->where('`user_id`', $user_id)
//      ->where('role_name', $permission_name)
//    ;
//    $result = $query->execute()->fetchObject('Acl');
    $this->role_message = $result['role_message'];
    // if no rows found return { NULL }
    // if role is not active OR user allow to use this role { !$result['role_active'] || $result['allow'] }
    return !is_array($result) ? NULL : (!$result['role_active'] || $result['allow']);
  }

  /*
   * @param $permission_name string
   * @param $group_id string | int
   * @return boolean
   */
  private function group_permission($permission_name, $group_id) {
//    $group_query = $group_id == NULL ? '`group_id` IS :group_id' : '`group_id` = :group_id';
//    $query       = 'SELECT * ';
//    $query .= 'FROM `group_permission` ';
//    $query .= 'JOIN `role` ON `group_permission`.`role_id` = `role`.`role_id` ';
//    $query .= 'WHERE ( ' . $group_query . ' AND `role_name` = :role_name ) ';
//    $query .= 'LIMIT 1 ';
//    $columns_values     = array(
//      ':group_id'  => $group_id,
//      ':role_name' => $permission_name,
//    );

    $query = db_select()
      ->from('group_permission')
      ->select(NULL)
      ->select('*')
      ->innerJoin('`role` ON `group_permission`.`role_id` = `role`.`role_id`')
      ->where('`group_id`', $group_id)
      ->where('role_name', $permission_name)
    ;

    $result = $query->execute()->fetch(PDO::FETCH_ASSOC);

//    $result             = sql_query_executor($query, $columns_values);
//    $result             = $result->fetch(PDO::FETCH_ASSOC);

    $this->role_message = $result['role_message'];
    // if no rows found return null
    // if role is not active OR user allow to use this role
    return !is_array($result) ? NULL : (!$result['role_active'] || $result['allow']);
  }

  /*
   * @param $permission_name string
   * @param $user_id string | int
   * @return boolean
   */
  private function co_work_user_permission($permission_name, $user_id) {
    /*
     * Ramin:FixMe
     * must change the query
     * query copied from user_permission function
     */
    $query = 'SELECT * ';
    $query .= 'FROM `user_permission` ';
    $query .= 'JOIN `role` ON `user_permission`.`role_id` = `role`.`role_id` ';
    $query .= 'WHERE (`user_id` = :user_id AND `role_name` = :role_name ) ';
    $query .= 'LIMIT 1 ';
    $columns_values = array(
      ':user_id'   => $user_id,
      ':role_name' => $permission_name,
    );
    $result         = sql_query_executor($query, $columns_values)->fetch(PDO::FETCH_ASSOC);
    // if no rows found return { NULL }
    // if role is not active OR user allow to use this role { !$result['role_active'] || $result['allow'] }
    return !is_array($result) ? NULL : (!$result['role_active'] || $result['allow']);
  }

  /*
   * @param $permission_name string
   * @param $group_id string | int
   * @return bool
   */
  private function co_work_group_permission($permission_name, $group_id) {
    /*
     * Ramin:FixMe
     * must change the query
     * query copied from group_permission function
     */
    $group_query = $group_id == NULL ? '`group_id` IS :group_id' : '`group_id` = :group_id';
    $query       = 'SELECT * ';
    $query .= 'FROM `group_permission` ';
    $query .= 'JOIN `role` ON `group_permission`.`role_id` = `role`.`role_id` ';
    $query .= 'WHERE ( ' . $group_query . ' AND `role_name` = :role_name ) ';
    $query .= 'LIMIT 1 ';
    $columns_values = array(
      ':group_id'  => $group_id,
      ':role_name' => $permission_name,
    );
    $result         = sql_query_executor($query, $columns_values);
    $result         = $result->fetch(PDO::FETCH_ASSOC);
    // if no rows found return null
    // if role is not active OR user allow to use this role
    return !is_array($result) ? NULL : (!$result['role_active'] || $result['allow']);
  }

  /*
   * check default habit of a role, is this role allowed by default or not ?
   * @param $permission_name string
   * @return boolean
   */
  private function role_default_allow($permission_name) {
    $columns_values = array(
      'role_name' => $permission_name,
    );
    $query          = 'SELECT `role_default_allow`, `role_message` FROM `role` ';
    $query .= 'WHERE ';
    foreach ($columns_values as $key => $value) {
      $query .= " {$key} = :{$key} ";
      $new_columns_values[":$key"] = $value;
    }
    $query .= ' LIMIT 1';
    unset($columns_values);
    // $result possible values ->
    // if no record found => 'FALSE'
    // if a record found => value of role_default_allow
    $result             = sql_query_executor($query, $new_columns_values)->fetch(PDO::FETCH_ASSOC);
    $this->role_message = $result['role_message'];
    return $result['role_default_allow'];
  }
}

$Acl = new Acl();