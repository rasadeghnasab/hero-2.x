<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 6/1/2015
 * Time: 12:16 AM
 */
class Address extends DatabaseObject {

  protected static $table_name = 'address';
//  protected static $primary_key = 'userID';

  public $userID;
  public $mobile;
  public $phone;
  public $address;

  protected static $db_fields = array(
    'userID',
    'mobile',
    'phone',
    'address',
  );

}