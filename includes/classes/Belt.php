<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 5/7/15
 * Time: 12:10 AM
 */
class Belt extends DatabaseObject
{
  protected static $table_name = 'belt';
  protected static $primary_key = 'beltID';

  public $beltID;
  public $sportID;
  public $beltName;
  public $beltValue;
  public $reqTime;
  public $created;

  protected static $db_fields = array(
      'beltID',
      'sportID',
      'beltName',
      'beltValue',
      'reqTime',
      'created',
      'updated',
  );

  public function edit_box() {
    $output = '';
    $output .= '<div class="split_bottom new_row belt_holder">';
    $output .= '<div class="edit-box">';
    $output .= '<ul>';
    $output .= '<li><span class="glyphicon glyphicon-edit edit_belt blue" title="ویرایش"></span></li>';
    $output .= '<li><span class="glyphicon glyphicon-remove  blue remove_belt" title="حذف"></span></li>';
    $output .= '</ul>';
    $output .= '</div>';
    $output .= '<form method="post" action="management/sport-setting/sport_belts.php" class="belt" id="' . $this->beltID . '">';
    $output .= "<input type= 'text' name ='belt_name' disabled class='new_input new_medium_input' value = '{$this->beltName}' />";
    $output .= '<input type = "submit" name="submit" disabled class="btn btn_blue" value = "ثبت"/></input>';
    $output .= '</form>';
    $output .= '</div>';

    return $output;

  }

  /*public static function make($sport_id, $belt_name, $belt_value = 1, $req_time = 1, $id = NULL) {
    $belt            = new Belt();
    $belt->sportID   = (int) $sport_id;
    $belt->beltName  = $belt_name;
    $belt->beltValue = $belt_value;
    $belt->reqTime   = $req_time;
    $belt->created   = strftime("%Y-%m-%d %H:%M:%S", time());
    if ($id != NULL) {
      $belt->beltID = $id;
    }

    return $belt;
  }*/

  public static function belts_by_sport_id($sport_id) {
    $query = 'SELECT `beltID`, `beltName` ';
    $query .= 'FROM `belt` ';
    $query .= 'WHERE `sportID` = :sportID ';
    $query .= 'ORDER BY `beltValue` ';
    $parameters = array(':sportID' => $sport_id);

    return static::find_by_sql($query, $parameters);
  }
}