<?php
/**
 * User: Ramin
 * Date: 4/17/15
 * Time: 3:50 PM
 */

/**
 * This class let post to add, edit, view and ... comments
 *
 * Class Comment
 */
class Comment extends DatabaseObject {

  protected static $table_name = 'comment';

  public $id;
  public $entity_id;
  public $bundle = 'post';
  public $author_id;
  public $body;
  public $confirm;
  public $created;
  public $updated;

  protected static $db_fields = array(
    'id',
    'entity_id',
    'bundle',
    'author_id',
    'body',
    'confirm',
    'created',
    'updated',
  );

  /*public static function make($entity_id, $author_id, $bundle, $body, $id = NULL) {
    $comment            = new Comment();
    $comment->entity_id = (int) $entity_id;
    $comment->bundle    = $bundle;
    $comment->created   = strftime("%Y-%m-%d %H:%M:%S", time());
    $comment->author_id = $author_id;
    $comment->body      = $body;
    if ($id) {
      $comment->id = $id;
    }

    return $comment;
  }*/

  public function comment_confirm() {
    $this->confirm = 1;
    return $this->save();
  }

  public static function find_comments_on($entity_id, $confirm = FALSE) {
    $columns_values = array(':entity_id' => $entity_id);
    $table_name     = static::table_name();
    $query          = 'SELECT SQL_CALC_FOUND_ROWS * ';
    $query .= 'FROM ' . $table_name;
    $query .= ' WHERE ' . $table_name . '.`entity_id` = :entity_id ';
    if ($confirm !== FALSE) {
      $query .= ' AND ' . $table_name . '.`confirm` = :confirm ';
      $columns_values[':confirm'] = $confirm;
    }
    $query .= 'ORDER BY created DESC ';

    return self::find_by_sql($query, $columns_values);
  }

  public static function comment_to_string($comment, $post_owner) {
    global $cUser;

    $user = find_person_by_id($comment->author_id);
    $user = (object) $user;
    $user_id = isset($cUser->user_id) ? $cUser->user_id : FALSE;
    $comment_owner = $user->userID == $user_id ? TRUE : FALSE;
    $post_owner = $post_owner == $user_id ? TRUE : FALSE;
    $comment_editor = $post_owner || $comment_owner;
    if (!$comment) {
      return FALSE;
    }
    $user_image = new Image('profile');
    $user_image->image_name = $user->picurl;
    $user->picurl = $user_image->create_src();
    $output  = '<article class="comment">';
    if($comment_editor):
      $output .= '<div class="edit-box">';
      $output .= '<div class="ui dropdown">';
      $output .= ' <i class="glyphicon glyphicon-chevron-down"></i>';
      $output .= '<div class="menu">';
      if($comment_owner) {
//                $output .= '<span class="glyphicon glyphicon-edit cm_action" data-cm-id="'.$comment->id.'" data-action="edit" title="ویرایش نظر"></span>';
//        $output .= '<div class="item cm_action" data-cm-id="'.$comment->id.'" data-action="edit" >ویرایش</div>';
      }
      if($post_owner && !$comment->confirm) {
//                $output .= '<span class="glyphicon glyphicon-check cm_action" data-cm-id="'.$comment->id.'" data-action="confirm" title="تایید نظر"></span>';
        $output .= '<div class="item cm_action" data-cm-id="'.$comment->id.'" data-action="confirm" >تایید</div>';
      }
//      $output .= '<span class="glyphicon glyphicon-remove cm_action" data-cm-id="'.$comment->id.'" data-action="remove" title="حذف نظر"></span>';
      $output .= '<div class="item cm_action" data-cm-id="'.$comment->id.'" data-action="remove" >حذف</div>';
      $output .= '</div>'; // menu
      $output .= '</div>'; // ui icon top left pointing dropdown button
      $output .= '</div>'; // edit-box
    endif;
    $output .= '<span class="bottom-half-border"></span>';
    $output .= "<a class='comment-img' href='/$comment->author_id' >";
    $output .= "<img src='$user->picurl' />";
    $output .= '</a>';
    $output .= "<div class='comment-body'>";
    $output .= '<p class="attribution">';
//    $output .= '<span> نویسنده </span>';
    $output .= "<span><a data-rest-url='min-prof/$comment->author_id' href='/$comment->author_id' class='lightblue'> ".htmlentities($user->first_name). ' ' . htmlentities($user->last_name) ." </a>";
//    $output .= "<span> در تاریخ </span>";
    $output .= "<abbr class='timeago fl' title='$comment->created'></abbr> ";
    $output .= "</span>";
    $output .= '</p>'; // attribution
    $output .= "<div class='text'>";
    $output .= "<p>$comment->body</p>";
    $output .= '</div>'; // text

    $output .= "</div>"; //comment-body
    $output .= '</article>'; //comment
    return $output;
  }

  public static function comments_to_string($comments, $post_owner) {
    // we cant use bundle to create different comment view, for every bundle.
    $output = '';
    foreach ($comments as $comment) {
      if($comment->bundle == 'post') $output .= self::comment_to_string($comment, $post_owner);
//      elseif($comment->bundle == 'x') $output .= '';
    }
    return $output;
  }

} 