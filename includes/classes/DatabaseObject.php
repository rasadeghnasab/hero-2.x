<?php

interface iTemplate {
  function hero_print();
}

/**
 * User: Ramin Sadegh nasab
 * Date: 4/25/2015
 * Time: 10:39 PM
 */
abstract class DatabaseObject implements iTemplate {
  protected static $table_name;
  protected static $db_fields;
  // If a table primary key name is not 'id' it can be set here to work with.
  protected static $primary_key = 'id';
  // if an object has a column for sportID we can specify it's name here
  protected static $sport_key = 'sportID';
  // if an object has a column for userID we can specify it's name here
  protected static $user_key = 'user_id';

  protected $link_creator_key = 'id';
  protected $link_creator_address = BASEPATH;

  protected static $entity_field = 'entity_id';

  /**
   * @$foreign_key_tables array has structure like below <br/>
   * array('table2_name' => array( <br/>
   * 'join_type' => 'type', (default is left_join) <br/>
   * 'join_statement' => 'table2_name ON table1.field_name = table2_name.field_name'  (default is table2_name) <br/>
   * 'columns' => 'tables columns' (default is table2_name.*) <br/>
   * ));
   */
  protected static $foreign_key_tables = array();

  // determine node_type
  protected static $node_type = 'node';

  protected static $basePath = BASEPATH;

  public $id;
  public static $pdo;

  public static function make($attributes) {
    $object = new static();
    foreach (static::$db_fields as $field) {
      if (isset($attributes[$field])) {
        $object->$field = $attributes[$field];
      }
    }
    global $cUser;

    // set uid by default for a user
    $user_name = static::$user_key;
    if (!isset($object->$user_name) && in_array($user_name, static::$db_fields) && static::$primary_key != $user_name) {
      $object->$user_name = $cUser->user_id;
    }

    // set default sport_id for a user
    $sport_key = static::$sport_key;
    if (!isset($object->$sport_key) && in_array($sport_key, static::$db_fields) && !$cUser->multi_sport) {
      $sport_id           = current($cUser->sid);
      $object->$sport_key = $sport_id;
    }

    // set created column for a inserted record
    if (in_array('created', static::$db_fields)) {
      $object->created = strftime("%Y-%m-%d %H:%M:%S", time());
    }

    return $object;
  }

  public static function make_insert($attributes) {
    $this_object = static::make($attributes);
    return $this_object->save();
  }

  /**
   * @author Ramin Sadegh nasab
   *
   * @param int $id
   *
   * @return bool|mixed
   */
  public static function find_by_id($id = 0) {
    $query = db_select()
      ->from(static::$table_name)
      ->where(static::$primary_key, $id)
    ;
    //    echo $query->getQuery();die;
    $result_array = static::find_by_fQuery($query);
    if (is_array($result_array) && count($result_array) == 1) {
      $result_array = array_shift($result_array);
    }

    return $result_array;
  }

  public static function find_by_sql($sql = "", $parameters = array(), $questionMark = FALSE) {
    $result_set = sql_query_executor($sql, $parameters, $questionMark);
    if ($result_set == FALSE) {
      return $result_set;
    }
    $object_array = array();
    $result_set->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, get_called_class());
    while ($object = $result_set->fetch()) {
      /*
      because we using setFetchMode function we do not need instantiate method any more. (the line bellow)
      $object_array[] = static::instantiate($row);
      */
      $object_array[] = $object;
    }

    return $object_array;
  }

  public static function find_by_fQuery($query) {
    $class_name   = get_called_class();
    $object_array = array();

    // pagination variables
    if (isset($_SERVER['REQUEST_METHOD'])) {
      $method               = strtolower($_SERVER['REQUEST_METHOD']) == 'get' ? 'get' : 'post';
      $pagination_variables = pagination_variables($method, array(
        'page'  => 1,
        'limit' => NULL,
      ));
      if ($pagination_variables['offset']) {
        if (strtolower($class_name) == 'image') {
          $query->offset(0);
        }
        else {
          $query->offset($pagination_variables['offset']);
        }
      }
      if ($pagination_variables['limit']) {
        $query->limit($pagination_variables['limit']);
      }
    }

    $order = global_order();
    $query->orderBy(static::$table_name . ".created {$order}");
    //    echo json_encode($query->getQuery());
    //    die;
        /*
        echo $query->getQuery();
        var_dump($query->getParameters());
        die;
    //*/
    $result_set = $query->execute();
    $result_set->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $class_name);
    while ($object = $result_set->fetch()) {
      if(method_exists($object, 'link') && property_exists($object, 'link_creator_address')) {
        $address = $object->link('post');
        $object->link = $address;
      }
      /*
      because we using setFetchMode function we do not need instantiate method any more. (the line bellow)
      $object_array[] = static::instantiate($row);
      */
      $object_array[] = $object;
    }
    return $object_array;
  }

  /**
   * @param null $entity_id
   * @param null $bundle
   *
   * @return array
   * @author Ramin Sadegh nasab
   * @author Ramin Sadegh nasab
   */
  public static function find_by_entity_id($entity_id = NULL, $bundle = NULL) {
    $query = db_select()->from(static::$table_name);
    if ($entity_id) {
      $query->where(static::$entity_field, $entity_id);
    }

    if ($bundle) {
      $query->where('bundle', $bundle);
    }

    if (strtolower(get_called_class()) == 'image') {
//            echo $query->getQuery();
//            var_dump($query->getParameters());
      //      die;
    }

    return static::find_by_fQuery($query);
  }

  /**
   * @author Ramin Sadegh nasab
   *
   * @param int $id
   * @param int $sid
   *
   * @return bool|mixed
   */
  public static function find_by_id_sid($id = NULL, $sid = NULL) {
    $query = db_select()->from(static::$table_name);
    if ($id) {
      $query->where(static::$primary_key, $id)->limit(1);
    }
    if ($sid) {
      $query->where(static::$sport_key, $sid);
    }

//        echo $query->getQuery();die;
    $result = static::find_by_fQuery($query);
    if (!empty($result) && $id) {
      if (count($result) == 1) {
        $result = array_shift($result);
      }
    }

    return $result;
  }

  public static function find_by_uid($uids, $bundle = NULL) {
    $query     = db_select()->from(static::$table_name);
    $user_name = static::$user_key;
    $query->where($user_name, $uids)
//          ->orderBy(static::$table_name . '.created DESC')
    ;
    if ($bundle && in_array('bundle', static::$db_fields)) {
      $query->where('bundle', $bundle);
    }

    $result = static::find_by_fQuery($query);
    if (!empty($result) && count($result) == 1) {
      $result = array_shift($result);
    }

    return $result;
  }

  // Some methods which use for most of tables
  //  public static function dynamic_select($order_by = array(), $group_by = array(), $additional_table = array()) {
  /**
   * @param array $options               has three main key <br/>
   *                                     1- $options['conditions'] = array('field' => 'value', ...) <br/>
   *                                     2- $options['order_by'] = array('field1 ACS', 'field2 DESC',...); <br/>
   *                                     3- $options['group_by'] = array('field1', 'field2',...); <br/>
   * @param array $additional_table_join has structure like below <br/>
   *                                     array('table2_name' => array( <br/>
   *                                     'join_type' => 'type', (default is left_join) <br/>
   *                                     'join_statement => 'table2_name ON table1.field_name = table2_name.field_name'  (default is table2_name) <br/>
   *                                     'columns' => 'tables columns' (default is table2_name.*) <br/>
   *                                     ));
   *
   * @return array
   * @author Ramin Sadegh nasab
   */
  public static function dynamic_select($options = array(), $additional_table_join = array()) {
    // it returns us page and limit
    //    $pagination_variables['offset'] = $pagination_variables['limit'] = NULL;
    //    $pagination_variables = pagination_variables('get');
    //    foreach ($pagination_variables as $var_name => $var_value) {
    //      $$var_name = $var_value;
    //    }
    $query = db_select()->from(static::$table_name);
//    echo $query->getQuery();
//    var_dump($query->getParameters());
//    die;
    // create join query
    if (isset(static::$foreign_key_tables) || !empty($additional_table_join)) {
      $joins = isset(static::$foreign_key_tables) ? static::$foreign_key_tables : array();
      // if additional_tables join is not empty it add to foreign key tables join
      $joins = array_merge_recursive($joins, $additional_table_join);
      //      var_dump($joins);die;
      //      var_dump($joins);die;
      foreach ($joins as $join_table_name => $join_options) {
        if (is_int($join_table_name)) {
          $join_table_name = $join_options;
        }
        $join_type      = isset($join_options['join_type']) ? $join_options['join_type'] : 'left_join';
        $join_statement = isset($join_options['join_statement']) ? $join_options['join_statement'] : $join_table_name;
        $columns        = isset($join_options['columns']) ? $join_options['columns'] : "$join_table_name.*";

        if ($join_type == 'left_join') {
          $query->leftJoin($join_statement)->select($columns);
        }
        elseif ($join_type == 'join') {
          $query->innerJoin($join_statement)->select($columns);
        }
      }
    }
//    echo $query->getQuery();
//    var_dump($query->getParameters());
//    die;
    //conditions
    if (isset($options['conditions']) /*|| isset(static::$options['conditions'])*/) {
      $conditions = array();
      $conditions = isset($options['conditions']) ? array_merge($conditions, $options['conditions']) : array();
      //      $conditions = isset(static::$options['conditions']) ? array_merge($conditions, static::$options['conditions']) : array();

      foreach ($conditions as $field => $value) {
        //        var_dump($field);
        //        var_dump($value);
        //        echo '<hr/>';
        $query->where($field, $value);
      }
    }

    // order by string
    if (!isset($options['order_by'])) {
      $options['order_by'][] = static::$table_name . '.created DESC';
    }

    $order_by = array();
    $order_by = isset($options['order_by']) ? array_merge($order_by, $options['order_by']) : array();
    //    $order_by = isset(static::$options['order_by']) ? array_merge($order_by, static::$options['order_by']) : $order_by;

    $order_by = implode(',', $order_by);
    $query->orderBy($order_by);

    // group by string
    if (isset($options['group_by'])) {
      $group_by = array();
      $group_by = isset($options['group_by']) ? array_merge($group_by, $options['group_by']) : array();
      //      $group_by = isset(static::$options['group_by']) ? array_merge($group_by, static::$options['group_by']) : array();

      $group_by = implode(',', $group_by);
      $query->groupBy($group_by);
    }

//    echo $query->getQuery();
//    var_dump($query->getParameters());
//    die;
    //    return $query->getQuery();
    // offset and limit is done in find_by_fQuery
    //    $query->offset($pagination_variables['offset'])->limit($pagination_variables['limit']);
    $count = $query->count();
    if (!$count) {
      return FALSE;
    }

    $result = static::find_by_fQuery($query);

    array_push($result, $count);

    return $result;
  }

  // get || set methods
  public function get_node_type() {
    return static::$node_type;
  }

  // CRUD methods
  /**
   * This method works on instances, if id is set update the instance else
   * create new record
   *
   * @return boolean
   *
   * @see update()
   * @see create()
   */
  public function save() {
    $primary_key = static::$primary_key;
    return isset($this->$primary_key) ? $this->update() : $this->create();
  }

  /**
   * If an instance created with @make method, with create method we can create
   * new record in database table
   *
   * @return boolean
   *
   * @see make
   */
  protected function create() {
    $db              = db_connection();
    $primary_key     = static::$primary_key;
    $attributes      = $this->attributes();
    $attribute_pairs = array();
    foreach ($attributes as $key => $value) {
      if ($value == NULL) {
        continue;
      }
      $attribute_pairs[$key] = "{$value}";
    }
    $result = sql_none_select_query_executor(insert_query_creator_new($attribute_pairs, static::$table_name));
    if ($result) {
      $id                 = $db->lastInsertId();
      $this->$primary_key = $result = $id;
    }

    return $result;
  }

  /**
   * update an database record
   *
   * @return boolean
   *
   * @see save()
   */
  protected function update() {
    $primary_key     = static::$primary_key;
    $attributes      = $this->attributes();
    $attribute_pairs = array();
    //    $this->updated   = strftime("%Y-%m-%d %H:%M:%S", time());
    foreach ($attributes as $key => $value) {
      if ($this->has_attribute($key)) {
        $attribute_pairs[$key] = "{$value}";
      }
    }
    unset($attribute_pairs[$primary_key]);

    //        echo json_encode(new_update_query_creator($attribute_pairs, static::$table_name, array($primary_key => $this->$primary_key)));die();
    return sql_none_select_query_executor(new_update_query_creator($attribute_pairs, static::$table_name, array($primary_key => $this->$primary_key)));
  }

  /**
   * @public method, delete an database record.
   *
   * @return boolean
   */
  public function delete() {
    // Don't forget your SQL syntax and good habits:
    // - DELETE FROM table WHERE condition LIMIT 1
    // - escape all values to prevent SQL injection
    // - use LIMIT 1
    $primary_key = static::$primary_key;
    return sql_none_select_query_executor(delete_query_creator_new(array($primary_key => $this->$primary_key), static::$table_name));
    // NB: After deleting, the instance of User still
    // exists, even though the database entry does not.
    // This can be useful, as in:
    //   echo $user->first_name . " was deleted";
    // but, for example, we can't call $user->update()
    // after calling $user->delete().

  }

  public static function delete_by_id($id) {
    $primary_key = static::$primary_key;
    return sql_none_select_query_executor(delete_query_creator_new(array($primary_key => $id), static::$table_name));
  }

  public static function delete_by_entity_id($entity_id, $bundle = NULL) {
    //    var_dump(get_called_class());
    $query = db_delete()
      ->deleteFrom(static::$table_name)
      ->where(static::$entity_field, $entity_id)
    ;

    if ($bundle) {
      $query->where('bundle', $bundle);
    }

    //    echo $query->getQuery();
    //    var_dump($query->getParameters());

    return $query->execute();
  }


  /// fQuery
  public static function executable_query($fQuery) {
    return $fQuery->execute();
  }


  // attributes methods
  protected function attributes() {
    // return an array of attribute names and their values
    $attributes = array();
    foreach (static::$db_fields as $field) {
      if (property_exists($this, $field)) { //property exists is a function for PHP >= 5.1.0
        $attributes[$field] = $this->$field;
      }
    }

    return $attributes;
  }

  protected function has_attribute($attribute) {
    // We don't care about the value, we just want to know if the key exists
    // Will return true or false
    return array_key_exists($attribute, $this->attributes());
  }

  protected static function table_name() {
    return ' `' . static::$table_name . '`';
  }

  public function link($url_selector = 'main') {
    $link_creator_key = $this->link_creator_key;
    $object_path      = '';
    if (is_array($link_creator_key)) {
      $counter = 1;
      $total   = count($link_creator_key);
      foreach ($link_creator_key as $key) {
        if (!isset($this->$key)) {
          continue;
        }
        $object_path .= $this->$key;
        if ($counter != $total) {
          $object_path .= DS;
        }
        $counter++;
      }
    }
    else {
      $object_path = $this->$link_creator_key;
    }

    if ($url_selector && isset($this->link_creator_address[$url_selector])) {
      $address_path = $this->link_creator_address[$url_selector];
    }
    else {
      $address_path = $this->link_creator_address;
    }

    return static::$basePath . $address_path . $object_path;
  }

  /************************* Template interface functions *******************/
  function hero_print() {
    $variables = static::attributes();
    foreach ($variables as $var_name => $value) {
      $variables[$var_name] = htmlentities($value);
    }
    return $variables;
  }
}

