<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 5/29/2015
 * Time: 3:27 AM
 */
class Event extends DatabaseObject {
  protected static $table_name = 'event';
  protected static $primary_key = 'eventID';
  protected $link_creator_key = 'eventID';
  protected $link_creator_address = 'events/event/';

  public $eventID;
  public $evtReqID;
  public $sportID;
  public $subBranchID;
  public $inserterID;
  public $evtTypeID;
  public $evtName;
  public $deadendDate;
  public $openingDay;
  public $evtAddress;
  public $maxReg;
  public $cost;
  public $description;
  public $otherSports;
  public $allSports;
  public $created;
  public $confirm_needed;
  public $evt_table_close;
  public $evt_table_sorted;

  protected static $db_fields = array(
    'eventID',
    'evtReqID',
    'sportID',
    'subBranchID',
    'inserterID',
    'evtTypeID',
    'evtName',
    'deadendDate',
    'openingDay',
    'evtAddress',
    'maxReg',
    'cost',
    'description',
    'otherSports',
    'allSports',
    'created',
    'confirm_needed',
    'evt_table_close',
    'evt_table_sorted',
  );

  public static function is_event_type_used($evt_type_id) {
    $query = db_select()->from(self::$table_name)->where('evtTypeID', $evt_type_id);
    return static::find_by_fQuery($query);
    echo $query->getQuery();
    die;
  }

  public function set_table_close($close = 1) {
    $this->evt_table_close = $close;
  }

  public function get_table_close() {
    return $this->evt_table_close;
  }

  /**
   * Return number of registered users in a particular event
   *
   * @param null $confirm
   *
   * @return int|string
   * @author Ramin Sadegh nasab
   */
  public function event_register_num($confirm = NULL) {
    $query = db_select()
      ->from('evt_register')
      ->where('event_id = ?', $this->eventID);
    if ($confirm !== NULL) {
      $confirm = $confirm ? 1 : 0;
      $query->where('confirm = ?', $confirm);
    }
    $count = $query->count();

    return $count;
  }

  public function event_users($confirm = 1, $conditions = array()) {
    $query = db_select()
      ->from('evt_register')
      ->select(NULL)
      ->select('infotable.userID')
      ->where('event_id', $this->eventID)
      ->innerJoin('infotable ON infotable.infoid = evt_register.info_id')
    ;

    // $confirm: 1 -> is confirm, 0 -> is not confirm, NULL -> not seen, False -> do not goes to if statement
    if($confirm !== FALSE) {
      $query->where('confirm', $confirm);
    }

    $evt_registers = eventRegister::find_by_fQuery($query);

    $uids = array();
    foreach($evt_registers as $evt_register) {
      $uids[] = $evt_register->userID;
    }

    $users = user_load_multiple($uids, $conditions);

    return $users;
  }

  public function event_has_capacity() {
    $count = $this->event_register_num();
    return $count < $this->maxReg || $this->maxReg == NULL;
  }

}