<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 5/7/15
 * Time: 9:31 PM
 */
class Follow extends DatabaseObject
{
  protected static $table_name = 'follow';
  protected static $primary_key = 'foID';

  public $foID;
  public $foerID;
  public $foingID;
  public $created;

  protected static $db_fields = array(
      'foID',
      'foerID',
      'foingID',
      'created',
  );

  public static function followers($user_id) {
    $query = 'SELECT `foerID` as user_id ';
    $query .= 'FROM ' . self::table_name();
    $query .= ' WHERE `foingID` = :foingID ';
    $parameters = array(
        ':foingID' => $user_id,
    );

    return sql_query_executor($query, $parameters)->fetchAll(PDO::FETCH_COLUMN);
  }

  public static function following($user_id) {
    $query = 'SELECT `foingID` as user_id ';
    $query .= 'FROM ' . self::table_name();
    $query .= ' WHERE `foerID` = :foerID ';
    $parameters = array(
        ':foerID' => $user_id,
    );

    return sql_query_executor($query, $parameters)->fetchAll(PDO::FETCH_COLUMN);
  }

}