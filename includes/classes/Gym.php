<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/10/2015
 * Time: 2:22 AM
 */
class Gym extends DatabaseObject {
  protected static $table_name = 'gym';

  public $id, $state_id, $gym_name, $gym_address, $gym_phone;

  protected static $db_fields = array(
    'id',
    'state_id',
    'gym_name',
    'gym_address',
    'gym_phone',
  );

}