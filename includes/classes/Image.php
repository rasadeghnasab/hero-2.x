<?php

/**
 * User: Ramin
 * Date: 5/1/15
 * Time: 9:34 PM
 */
class Image extends DatabaseObject {
  protected static $table_name = 'image';
  /////// keys
  protected static $primary_key = 'fid';
  protected static $user_key = 'uid';
  protected static $sport_key = 'sid';

  protected static $source = 'files/picture';
  protected static $node_type = 'node/image';

  protected static $db_fields = array(
    'fid',
    'uid',
    'sid',
    'entity_id',
    'bundle',
    'image_name',
    'width',
    'height',
    'src_from_root',
    'updated',
  );
  private static $default_pic_src = array(
    'cover_photo' => '_css/img/cover-photo.jpg',
    'profile'     => '_css/img/user-64.png',
    'sport_logo'  => '_css/img/cover-photo.jpg',
    'post'        => '_css/img/cover-photo.jpg',
  );
  /**
   * @var array
   * source = where files of this type must be stored or read from
   * size   = is this bundle has variety of size ? (folders of different sizes)
   * user   = is uid of image publishe important for this bundle ?
   */
  private static $pic_src = array(
    'cover_photo' => array(
      'source' => 'files/picture',
      'size'   => 1,
      'user'   => 1,
    ),
    'profile'     => array(
      'source' => 'profilePics',
      // it has just one size
      'size'   => 0,
      // no matter which user upload this pic
      'user'   => 0,
    ),
    'sport_logo'  => array(
      'source' => 'files/picture',
      'size'   => 1,
      'user'   => 1,
    ),
    'post'        => array(
      'source' => 'files/picture',
      'size'   => 1,
      'user'   => 1,
    ),
  );

  public $fid;
  public $uid;
  public $sid;
  public $entity_id;
  public $bundle;
  public $image_name;
  public $width;
  public $height;
  public $src_from_root;
  public $updated;

  function __construct($bundle = NULL, $image_name = NULL) {
    $this->bundle     = $bundle;
    $this->image_name = $image_name;
  }

  /*public static function make($image_name, $uid, $bundle = 'wall', $entity_id = NULL, $width = NULL, $height = NULL, $source_from_root = NULL, $fid = NULL) {
    $image = new Image();
    $image->uid = (int) $uid;
    $image->image_name = $image_name;
    $image->bundle = $bundle;
    $image->entity_id = $entity_id;
    $image->width = $width;
    $image->height = $height;
    $image->src_from_root = $source_from_root;
    if ($fid != NULL) {
      $image->fid = $fid;
    }

    return $image;
  }*/

  public static function user_active_cover_photo($uid) {
    $where = array('uid' => $uid, 'bundle' => 'cover_photo');
    $tbn   = static::$table_name;
    $query = db_select()
      ->from($tbn)
      ->where($where)
      ->orderBy('created DESC')
    ;

    return static::find_by_fQuery($query);
  }

  public static function news_images_to_html($images) {
    $output = '';
    foreach ($images as $image) {
      if (!is_object($images)) {
        continue;
      }
      $output .= $image->news_image_to_html();
    }

    return $output;
  }

  /**
   * return all sports lasts logo
   *
   * @param $sid
   *
   * @return array
   * @author Ramin Sadegh nasab
   */
  public static function sport_logo_load($sid) {
    $query = db_select()
      ->from(static::$table_name)
      ->where('sid', $sid)
      ->where('bundle', 'sport_logo')
      ->orderBy('created DESC')
      ->limit(1)
    ;
    $image  = static::find_by_fQuery($query);

    return !empty($image) ? array_shift($image) : new static('sport_logo');
  }

  /**
   * return a sport logo
   *
   * @param array $sids
   *
   * @return array
   * @author Ramin Sadegh nasab
   */
  public static function sport_logo_load_multiple($sids = array()) {
    $fquery = db_select()
      ->from(static::$table_name . ' img')
      ->where('bundle', 'sport_logo')
      ->groupBy('sid')
    ;
    if (!empty($sids)) {
      $fquery->where('sid', $sids);
    }
    return static::find_by_fQuery($fquery);
  }

  /**
   * @author Ramin Sadegh nasab
   *
   * @param string $size // TODO:: in future I must create a table which holds name of the default pics for each
   *     bundle, in each size
   *
   * @return string
   */
  public function cover_photo_html($size = 'thumb') {
    $output = '';
    if ($size == 'thumb') {
      $large_src = $this->create_src('large');
      $output .= "<a href='{$large_src}' class='venobox'>";
    }
    $source = $this->create_src();
    $output .= "<div class = 'cover-photo-img' style = 'background-image: url($source)'></div>";
    if ($size == 'large') {
      $output .= '</a>';
    }

    return $output;
  }

  public static function delete_by_fid($fid) {
    $result = FALSE;
    $image = static::find_by_id($fid);
    if($image) {
      $image['images']->delete_image();
      $result = $image['images']->delete();
    }

    return $result;
  }

  public function delete_image() {
    // remove thumb image
    $sources = array();
    $options = array(
      'absolute',
    );

    $sources['thumb']  = $this->create_src('thumb', $options);
    $sources['large']  = $this->create_src('large', $options);
    $sources['actual'] = $this->create_src('actual', $options);
    foreach($sources as $source) {
      if(file_exists($source)) {
        unlink($source);
      }
    }
  }

  public function create_src($size = 'thumb', $options = array()) {
    $path = static::$pic_src[$this->bundle]['source'] . DS;
    if(static::$pic_src[$this->bundle]['size']) {
      $path .= $size . DS;
    }
    if(static::$pic_src[$this->bundle]['user']) {
      $path .= $this->uid . DS;
    }
    $path .= $this->image_name;
    $absolute_path = SITE_ROOT . DS . $path;
    $path          = preg_replace('/[\\\\\/]/i', '/', $path);
    if(in_array('absolute', $options)) {
      if(file_exists($absolute_path)) {
        return $absolute_path;
      }
      else {
        return FALSE;
      }
    }
    $file = !is_dir($absolute_path) && file_exists($absolute_path) ? static::$basePath . $path : static::$basePath . static::$default_pic_src[$this->bundle];
    // echo 'file exist'.is_dir(SITE_ROOT . DS . static::$source . DS . $size . DS . $this->uid . DS . $this->image_name).'<hr/>';
    // echo SITE_ROOT . DS . static::$source . DS . $size . DS . $this->uid . DS . $this->image_name.'<hr/>';
    // echo $file.'<hr/>';
    return $file;
  }

  public function news_image_to_html() {
    $output = '';
    $output .= "<img src={$this->create_src()} />";

    return $output;
  }
}
