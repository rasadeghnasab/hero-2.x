<?php

/**
 * User: Ramin
 * Date: 4/18/15
 * Time: 12:02 AM
 */
class Like extends DatabaseObject {
  protected static $table_name = 'like';

  public $id;
  public $entity_id;
  public $bundle = 'posts';
  public $author_id;
  public $created;
  public $user_name;
  protected static $db_fields = array(
    'id',
    'entity_id',
    'bundle',
    'author_id',
    'created',
  );

  /*function __constructor($bundle = 'post') {
    $bundle = strtolower($bundle);
    $this->bundle = $bundle;
  }*/

  /**
   * Return name of persons who link entity.
   *
   * @param int    $entity_id
   * @param array  $pagination
   * @param string $bundle
   *
   * @return array of objects
   */
  public static function find_liker_names($entity_id, $bundle = 'post', $pagination = array()) {
    global $cUser;
    $columns_values = array(
      ':entity_id' => $entity_id,
      ':bundle'    => $bundle,
    );

    $alias = ' l';
    $query = 'SELECT SQL_CALC_FOUND_ROWS * ';
    $query .= 'FROM `user` u ';
    $query .= 'JOIN ' . static::table_name() . " $alias ON `u`.`userID` = $alias.`author_id` ";
    $query .= " WHERE {$alias}.`entity_id` = :entity_id ";
    $query .= " AND {$alias}.`bundle` = :bundle ";
    $query .= "ORDER BY ";
    if(isset($cUser->user_id)) {
      $query .= "FIELD({$alias}.author_id,{$cUser->user_id}) DESC, ";
    }
    $query .= "$alias.created DESC ";
    if (!empty($pagination)) {
      $query .= ' LIMIT ';
      if (isset($pagination['offset'])) {
        $query .= ":offset ";
        $columns_values[':offset'] = $pagination['offset'];
      }
      if (isset($pagination['offset'], $pagination['limit'])) {
        $query .= ",";
      }
      if (isset($pagination['limit'])) {
        $query .= ":limit ";
        $columns_values[':limit'] = $pagination['limit'];
      }
    }
    //echo '<hr/>';
//                echo $query;die();
    return static::find_by_sql($query, $columns_values);
  }

  /**
   * Number of likes on an entity
   *
   * @param int $entity_id
   *
   * @return int
   */
  public static function find_likes_on($entity_id) {
    $columns_values = array(':entity_id' => $entity_id);

    $query = 'SELECT SQL_CALC_FOUND_ROWS * ';
    $query .= 'FROM ' . static::table_name() . ' l';
    $query .= ' WHERE l.`entity_id` = :entity_id ';
    $query .= 'ORDER BY l.created DESC ';
    sql_query_executor($query, $columns_values);
    $likes_num = found_rows_calc();
    //    $count = db_select()->from(static::$table_name . ' l')
    //      ->where('l.entity_id')
    //      ->orderBy('l created DESC')
    //      ->count()
    ;

    return (int) $likes_num;
  }

  /**
   * Determine does a person like an entity or not?
   *
   * @param int    $entity_id
   * @param int    $uid
   *
   * @param string $bundle
   *
   * @return \Like object
   */
  public static function is_liked_by_person($entity_id, $uid, $bundle = 'post') {
    $columns_values = array(
      ':entity_id' => $entity_id,
      ':author_id' => $uid,
      ':bundle'    => $bundle,
    );
    //    $query  = 'SELECT  EXISTS ( ';
    $query = 'SELECT * ';
    $query .= ' FROM ' . static::table_name() . ' l';
    $query .= ' WHERE l.`entity_id` = :entity_id ';
    $query .= ' AND l.author_id = :author_id ';
    $query .= ' AND l.bundle = :bundle ';
    $query .= ' LIMIT 1 ';
    //    $query .= ')';
    $liked = static::find_by_sql($query, $columns_values);
    $liked = $liked ? array_shift($liked) : FALSE;

    return $liked;
  }

}