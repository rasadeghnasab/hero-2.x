<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 5/22/2015
 * Time: 6:46 PM
 */
class News extends DatabaseObject {

  protected static $table_name = 'news';
  protected static $primary_key = 'id';

  public $id, $user_id, $body, $created;

  public static $db_fields = array(
    'id',
    'user_id',
    'body',
    'created',
  );

  public static $foreign_key_tables = array(
    'user' => 'join',
  );

  public static function newses_html($newses) {
    $output = '';
    foreach ($newses as $news) {
      if (!is_object($news)) {
        continue;
      }
      $output .= $news->news_html();
    }

    return $output;
  }

  public function news_html() {
    global $cUser;
    $user_id = isset($cUser->user_id) ? $cUser->user_id : NULL;
     $images = Image::find_by_entity_id($this->id);
    $user_key = static::$user_key;
    $image = FALSE;
    if($images) {
      $image = Image::news_images_to_html($images);
    }


    $owner = $this->user_id == $user_id ? TRUE : FALSE;

    $edit_box  = "";
    if($owner):
      $edit_box .= '<div class="edit-box">';
      $edit_box .= '<div class="ui dropdown">';
      $edit_box .= '<i class="glyphicon glyphicon-chevron-down"></i>';
      $edit_box .= '<div class="menu">';
      $edit_box .= '<a class="item" href="/news/edit/'.$this->id.'">ویرایش</a>';
      $edit_box .= '<a class="item" href="/news/delete/'.$this->id.'">حذف</a>';
      $edit_box .= '</div>'; // menu
      $edit_box .= '</div>'; // ui icon top left pointing dropdown button
      $edit_box .= '</div>'; // edit-box
    endif;

    $output = '';
//    $output .= '<div class="news_part">';
//    $output .= '</div>';
    $output .= '<div class="news_part box_shadow" id="' . $this->id . '" >';
    $output .= '<div class="news_infos">';
    $output .= $edit_box;
//  $output .= '<img class="img_profile news _circle" src="' . $user_pic . '">';
    $output .= '<div class="news_timeline_name">';
    $output .= '<a data-rest-url = "min-prof/' . $this->$user_key . '" class="profile_load" href="/' . $this->$user_key . '">' . full_name($this->first_name, $this->last_name) . '</a>';
    $output .= '</div>';
    $output .= '<div class="news_timeline_date">';
    $output .= '<a href="/post/' . $this->id . '">';
    if ($this->updated == NULL) {
      $output .= 'تاریخ ثبت ' . '<span class="date_time">' . r_gregorian_to_jalali($this->created) . '</span>';
    }
    else {
      $output .= 'ویرایش در' . '<span class="date_time">' . r_gregorian_to_jalali($this->updated) . '</span>';
    }
    $output .= '</a>';
    $output .= '</div>';
    $output .= '<div class="news_timeline_cat">';
    $output .= '</div>';
    $output .= '<div class="split_bottom"></div>';
    $output .= '</div>'; // news_infos
    $output .= '<div class="news_content_no_style">';
    $context = $this->body;
    $class = $rdmb = NULL;
    $output .= "<p class='{$class}'> " . $context. " {$rdmb} </p>";
    if ($image) {
      $output .= '<div class="news_imgHolder" ><a class="venobox" href="' . $image . '"><img class="box_shadow" src="' . $image . '" /></a></div>';
    }
    $output .= '</div>';

    $output .= '</div>'; //
    $output = $output != '' ? '<div class="news_posts">' . $output . '</div>' : '';
    

    return $output;
  }

  public static function news_select(array $sport_id = array(), $user_id = NULL, $has_image = FALSE) {
    // it returns us page and limit
    $offset = $limit = NULL;
    $pagination_variables = pagination_variables('get');
    foreach ($pagination_variables as $var_name => $var_value) {
      $$var_name = $var_value;
    }

    $query = db_select();
    $query = $query->from(static::$table_name)
      ->select('u.*')
      ->select('inf.*')
      ->select('s.*')
      ->innerJoin('user u ON u.userID = ' . static::$table_name . '.' .static::$user_key)
      ->innerJoin('infotable inf ON u.userid = inf.userid')
      ->innerJoin('sport s ON s.id = inf.sportid');
    if ($has_image) {
      $query = $query->innerJoin("image ON image.entity_id = " . static::table_name() . '.id');
    }
    if (!empty($sport_id)) {
      $query = $query->where('s.id', $sport_id);
    }
    if ($user_id) {
      $query = $query->where(static::$table_name.'.'.static::$user_key, $user_id);
    }

    $query->orderBy(static::$table_name. '.created DESC');
    /*else {
      $query = $query->leftJoin("image ON image.entity_id = ".static::table_name().'.id');
    }*/
    $count = $query->count();
    $query->offset($offset)->limit($limit);
//    echo $query->getQuery();die;
//    die('s');
    $result = static::find_by_fQuery($query);
    array_push($result, $count);

    return $result;
  }

}