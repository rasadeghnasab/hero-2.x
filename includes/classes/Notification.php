<?php

/**
 * User: Ramin
 * Date: 4/24/2015
 * Time: 4:37 PM
 */
class Notification {

  protected static $table_name = 'notification';

  public $id;
  public $actor_id;
  public $subject_id;
  public $object_id;
  public $type_id;
  public $seen;
  public $created;
  public $updated;

  protected static $db_fields = array(
    'id',
    'actor_id',
    'subject_id',
    'object_id',
    'type_id',
    'seen',
    'created',
    'updated',
  );


  public function save() {
    return isset($this->id) ? $this->update() : $this->create();
  }

  public function create() {
    $attributes      = $this->attributes();
    $attribute_pairs = array();
    foreach ($attributes as $key => $value) {
      $attribute_pairs[$key] = "{$value}";
    }

    return sql_none_select_query_executor(insert_query_creator_new($attribute_pairs, self::$table_name));
  }

  public function update() {
    $attributes      = $this->attributes();
    $attribute_pairs = array();
    $this->updated   = strftime("%Y-%m-%d %H:%M:%S", time());
    foreach ($attributes as $key => $value) {
      $attribute_pairs[$key] = "{$value}";
    }
    unset($attribute_pairs['id']);

    return sql_none_select_query_executor(new_update_query_creator($attribute_pairs, self::$table_name, array('id' => $this->id)));
  }

  public function delete() {
    // Don't forget your SQL syntax and good habits:
    // - DELETE FROM table WHERE condition LIMIT 1
    // - escape all values to prevent SQL injection
    // - use LIMIT 1
    return sql_none_select_query_executor(delete_query_creator_new(array('id' => $this->id), self::$table_name));
    // NB: After deleting, the instance of User still
    // exists, even though the database entry does not.
    // This can be useful, as in:
    //   echo $user->first_name . " was deleted";
    // but, for example, we can't call $user->update()
    // after calling $user->delete().
  }

  public static function make($actor_id, $subject_id, $object_id, $type_name = 'comment on post', $seen = 0) {
    if ($notificationTypeId = NotificationType::notification_type_id_by_name(strtolower($type_name))) {
      $notificationTypeId = $notificationTypeId[0]->id;
    }
    else return FALSE;

    $notification             = new Notification();
    $notification->actor_id   = $actor_id;
    $notification->subject_id = $subject_id;
    $notification->object_id  = $object_id;
    $notification->type_id    = $notificationTypeId;
    $notification->seen       = $seen;
    $notification->created    = strftime("%Y-%m-%d %H:%M:%S", time());

    return $notification;
  }

  public static function find_by_id($id = 0) {
    $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");

    return !empty($result_array) ? array_shift($result_array) : FALSE;
  }

  public static function find_by_sql($sql = "", $parameters = array(), $questionMark = FALSE) {
    $result_set = sql_query_executor($sql, $parameters, $questionMark);
    if ($result_set == FALSE) {
      return $result_set;
    }
    $object_array = array();
    $result_set->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, get_class());
    while ($object = $result_set->fetch()) {
      /*
      because we using setFetchMode function we do not need instantiate method any more. (the line bellow)
      $object_array[] = self::instantiate($row);
      */
      $object_array[] = $object;
    }

    return $object_array;
  }

  protected function attributes() {
    // return an array of attribute names and their values
    $attributes = array();
    foreach (self::$db_fields as $field) {
      if (property_exists($this, $field)) { //property exists is a function for PHP >= 5.1.0
        $attributes[$field] = $this->$field;
      }
    }

    return $attributes;
  }

  private function has_attribute($attribute) {
    // We don't care about the value, we just want to know if the key exists
    // Will return true or false
    return array_key_exists($attribute, $this->attributes());
  }

  private static function table_name() {
    return ' `' . self::$table_name . '`';
  }

  public function get() {

  }

  public static function find_notifications_by_user_id($id, $seen = FALSE) {
    $columns_values = array(':id' => $id);
    $query          = 'SELECT SQL_CALC_FOUND_ROWS * ';
    $query .= 'FROM ' . self::table_name() . ' tb';
    $query .= ' WHERE `tb`.`id` = :id ';
    if ($seen !== FALSE) {
      $query .= ' AND `tb`.`seen` = :seen ';
      $columns_values[':seen'] = $seen;
    }
    $query .= 'ORDER BY created DESC ';

    return self::find_by_sql($query, $columns_values);
  }

}

class NotificationType {

  protected static $table_name = 'notification_type';

  public $type_name;
  public $id;
  public $description;

  public static function make($type_name, $description) {
    $notificationType              = new NotificationType();
    $notificationType->type_name   = $type_name;
    $notificationType->description = $description;

    return $notificationType;
  }

  public static function find_by_id($id = 0) {
    $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");

    return !empty($result_array) ? array_shift($result_array) : FALSE;
  }

  public static function find_by_sql($sql = "", $parameters = array(), $questionMark = FALSE) {
    $result_set = sql_query_executor($sql, $parameters, $questionMark);
    if ($result_set == FALSE) {
      return $result_set;
    }
    $object_array = array();
    $result_set->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, get_class());
    while ($object = $result_set->fetch()) {
      /*
      because we using setFetchMode function we do not need instantiate method any more. (the line bellow)
      $object_array[] = self::instantiate($row);
      */
      $object_array[] = $object;
    }

    return $object_array;
  }

  protected function attributes() {
    // return an array of attribute names and their values
    $attributes = array();
    foreach (self::$db_fields as $field) {
      if (property_exists($this, $field)) { //property exists is a function for PHP >= 5.1.0
        $attributes[$field] = $this->$field;
      }
    }

    return $attributes;
  }

  private function has_attribute($attribute) {
    // We don't care about the value, we just want to know if the key exists
    // Will return true or false
    return array_key_exists($attribute, $this->attributes());
  }

  //  /*
  public static function notification_type_id_by_name($name) {
    $columns_values = array(':type_name' => $name);
    $query          = 'SELECT * ';
    $query .= 'FROM ' . self::table_name() . ' nt ';
    $query .= 'WHERE `nt`.`type_name` = :type_name ';
    $query .= 'LIMIT 1';

    return self::find_by_sql($query, $columns_values);
  }
  //  */

  private static function table_name() {
    return ' `' . self::$table_name . '`';
  }

}