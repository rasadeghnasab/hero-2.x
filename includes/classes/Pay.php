<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/11/2015
 * Time: 6:56 PM
 */
class Pay extends DatabaseObject {
  // constants
  const INITIAL = 0;
  const PAID = 1;
  const SETTLEMENT_CALC = 2;

  protected static $table_name = 'pay';
  protected static $db_fields = array(
    'id',
    'plan_id',
    'user_id',
    'sport_id',
    'entity_id',
    'bundle',
    'mine',
    'their',
    'status',
    'created',
  );

  protected static $node_type = 'node/financial';

  public static function pays_types($options = NULL) {
    $join_tables = array(
      'plan' => array(
        'columns' => array(
          'SUM(pay.mine) AS mine_total',
          'SUM(pay.their) AS their_total',
          'COUNT(*) as count',
        ),
      ),
    );

    return static::dynamic_select($options, $join_tables);
  }

}