<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/11/2015
 * Time: 6:55 PM
 */
class Plan extends DatabaseObject {
  // constants
  const ACTIVE_PLAN = 1;
  const INACTIVE_PLAN = 1;

  protected static $table_name = 'plan';
  protected static $sport_key = 'sport_id';
  protected static $db_fields = array(
    'id',
    'sport_id',
    'user_id',
    'bundle',
    'mine',
    'their',
    'status',
    'created',
  );




}