<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/5/2015
 * Time: 1:35 AM
 */
class Post extends DatabaseObject {
  protected static $table_name = 'post';
  protected static $sport_key = 'sport_id';
  protected static $node_type = 'node/post';

  protected $link_creator_address = 'post/' ;

  protected static $foreign_key_tables = array(
    'image' => array(
      'join_statement' => 'image ON image.entity_id = post.id',
    ),
    'tag' => array(
      'join_type' => 'join',
      'join_statement' => 'tag ON tag.entity_id = post.id'
    ),
  );
  // object properties
  public $id;
  public $user_id;
  public $sport_id;
  public $title;
  public $context;
  public $abstraction;

  protected static $db_fields = array(
    'id',
    'user_id',
    'sport_id',
    'bundle',
    'title',
    'context',
    'abstraction',
    'created',
  );

  public static function find_by_cat_id($cat_id) {
    $tags = Tag::nid_by_cat_ids($cat_id);
    $nid  = array_map(function ($tag) {return $tag->entity_id;}, $tags);

    return static::find_by_id($nid);
  }

  public static function find_has_pic($options) {
//    $query = db_select()->from(static::$table_name)->
    return static::find_by_fQuery($query);
  }
}
