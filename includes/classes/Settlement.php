<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/11/2015
 * Time: 10:06 PM
 */
class Settlement extends DatabaseObject {
  const REQUEST = 'request';
  const PAID = 'paid';

  protected static $foreign_key_tables = array(
    'user' => array(
      'join_type'      => 'join',
      'join_statement' => "user ON user.userid = financial_settlement.user_id",
      'columns'        => array(
        'user.*',
        'financial_settlement.created as created',
      ),
    ),
  );

  protected static $table_name = 'financial_settlement';
  protected static $node_type = 'node/financial';
  protected static $sport_key = 'sport_id';

  protected static $db_fields = array(
    'id',
    'user_id',
    'sport_id',
    'bundle',
    'amount',
    'settlement_till',
    'created',
  );

  /**
   * check to see
   *
   * @return array|int|mixed
   * @author Ramin Sadegh nasab
   */
  public static function request_not_allowed() {
    global $cUser;
    $table_name = static::$table_name;
    $method     = method();
    $method == 'post' ? $_POST['limit'] = 1 : $_GET['limit'] = 1;
    $options = array(
      'conditions' => array(
        'user_id'                    => $cUser->user_id,
        'bundle'                     => 'request',
        $table_name . '.created > ?' => date('Y-m-d H-i-s', strtotime('-3 days')),
      ),
    );

    $additional_table = array(
      $table_name => array(
        'columns' => array(
          $table_name . '.*',
          $table_name . ".created as settlement_created",
        ),
      ),
    );

    $result = static::dynamic_select($options, $additional_table);
    if ($method == 'post') {
      unset($_POST['limit']);
    }
    else {
      unset($_GET['limit']);
    }

    if ($result) {
      $result = array_shift($result);
      // count days left
      $result = 3 - newDateDiff($result->settlement_created)[2];
    }

    return $result;
  }

}