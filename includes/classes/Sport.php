<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/13/2015
 * Time: 3:29 PM
 */
class Sport extends DatabaseObject {
  protected static $table_name = 'sport';

  public $id, $field, $branch, $sportConfirm, $sport_description;

  protected static $db_fields = array(
    'id',
    'field',
    'branch',
    'sportConfirm',
    'sport_description',
  );

  protected static $primary_key = 'id';
  protected static $sport_key = 'id';
  protected static $user_key = NULL;
  protected $link_creator_key = array('field', 'branch');
  protected $link_creator_address = array(
    'main' => 'masters/',
  );

  protected static $entity_field = 'entity_id';

  public function full_name() {
    return full_name($this->field, $this->branch);
  }
}