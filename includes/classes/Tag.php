<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/16/2015
 * Time: 4:48 AM
 */

class Tag extends  DatabaseObject {
  protected static $table_name = 'tag';

  public $id;
  public $entity_id;
  public $bundle;
  public $cat_id;
  public $created;

  protected static $db_fields = array(
    'id',
    'entity_id',
    'bundle',
    'cat_id',
    'created',
  );

  /**
   * Makes an instance of Tag class
   * @author Ramin Sadegh nasab
   *
   * @param        $entity_id
   * @param        $cat_id
   * @param string $bundle
   * @param null   $id
   *
   * @return \Tag
   */
  /*public static function make($entity_id, $cat_id, $bundle = 'post', $id = NULL) {
    $tag            = new Tag();
    $tag->entity_id = (int) $entity_id;
    $tag->bundle    = $bundle;
    $tag->created   = strftime("%Y-%m-%d %H:%M:%S", time());
    $tag->cat_id = $cat_id;
    if ($id) {
      $tag->id = $id;
    }

    return $tag;
  }*/

  /**
   * @param null $post_id
   * @param null $bundle
   *
   * @return array|bool|\PDOStatement
   * @author Ramin Sadegh nasab
   */
  public static function find_by_entity_id($post_id = NULL, $bundle = NULL) {
    $query  = 'SELECT *';
    $query .= 'FROM  '.static::table_name().' t ';
    $query .= 'JOIN `category` c ON `c`.`id` = `t`.`cat_id` ';
    $query .= 'WHERE `t`.`entity_id` = :entity_id ';
    $parameters = array(
      ':entity_id' => $post_id
    );
//    echo $query;die();
//    var_dump(static::find_by_sql($query,$parameters));die;
    return static::find_by_sql($query,$parameters);
  }

  public static function nid_by_cat_ids($cat_ids) {
    $query = db_select()->from(static::$table_name)
      ->where('cat_id', $cat_ids);

    return static::find_by_fQuery($query);
  }

  public static function all_categories() {
    $query = db_select()->from('category cat')->select(null)->select('id');
//    echo $query->getQuery();die();
    return static::find_by_sql($query->getQuery());
  }

  public function tag_link(array $classes = array()) {
    $class_string = '';
    foreach($classes as $class) {
      $class_string .= $class.' ';
    }
    $class_string .= $class_string != '' ? ' class="'.$class_string.'"' : NULL;

//    var_dump($this->cat_id);
    return "<a href='/cat/" . htmlentities($this->cat_id) . "' $class_string >". htmlentities($this->name)."</a>";
//    return $this->cat_id;
      //    $output .= '<a href="/cat/' . htmlentities($post['catID']) . '">' . htmlentities($post['catName']) . '</a>';
  }

  public static function tags_link($tags, array $classes = array()) {
    $count = count($tags);
    $output = '';
    $counter = 1;
    foreach($tags as $tag) {
      $output .= $tag->tag_link($classes);
      $output .= $counter != $count ? ', ' : NULL;
      $counter++;
    }

    return $output;
  }
}