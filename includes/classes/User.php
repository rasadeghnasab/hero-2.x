<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 5/29/2015
 * Time: 1:59 PM
 */
class User extends DatabaseObject {
  protected static $table_name = 'user';
  protected static $primary_key = 'userID';
  protected $link_creator_key = 'user_id';
  protected $link_creator_address = '';

  public $userID;
  public $stateID;
  public $degreeID;
  public $first_name;
  public $last_name;
  public $sex;
  public $national_code;
  public $birth_date;
  public $foot;
  public $weight;
  public $length;
  public $description;
  public $created;

  protected static $db_fields = array(
    'userID',
    'stateID',
    'degreeID',
    'first_name',
    'last_name',
    'sex',
    'national_code',
    'birth_date',
    'foot',
    'weight',
    'length',
    'description',
    'created',
  );

  // read user information from database on each page request.
  // if isset user_cache load from cache table else load from database join tables and ...
  // when user changes his/her basic information then delete cache
  // for now just select user from database normally

  public function full_name() {
    return full_name($this->first_name, $this->last_name);
  }
}