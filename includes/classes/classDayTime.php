<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/10/2015
 * Time: 2:33 AM
 */
class classDayTime extends DatabaseObject {
  protected static $table_name = 'class_day_time';

  public $id, $class_id, $day, $start_time, $end_time;

  protected static $entity_field = 'class_id';

  protected static $db_fields = array(
    'id',
    'class_id',
    'day',
    'start_time',
    'end_time',
  );

}