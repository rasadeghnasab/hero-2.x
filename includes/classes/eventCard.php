<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 8/29/2015
 * Time: 7:14 PM
 */
class eventCard extends DatabaseObject {
  protected static $table_name = 'event_card';

  public $uid;
  public $eid;
  public $ejid;

  protected static $db_fields = array(
    'uid',
    'eid',
    'ejid',
    'created',
  );
}