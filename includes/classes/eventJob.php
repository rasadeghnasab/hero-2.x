<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 8/28/2015
 * Time: 7:09 PM
 */
class eventJob extends DatabaseObject {
  protected static $table_name = 'event_job';
  protected static $sport_key = 'sid';

  public $id;
  public $sid;
  public $name;
  public $weight;
  public $created;

  protected static $db_fields = array(
    'id',
    'sid',
    'name',
    'weight',
    'created',
  );


}