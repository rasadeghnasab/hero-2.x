<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 4/30/2015
 * Time: 8:16 PM
 */
class eventRegister extends DatabaseObject {
  protected static $table_name = 'evt_register';
  protected static $db_fields = array(
    'id',
    'info_id',
    'event_id',
    'user_id',
    'confirm',
    'confirmer_id',
    'created',
    'updated',
  );

  public $id;
  public $info_id;
  public $event_id;
  public $user_id;
  public $confirm;
  public $confirmer_id;
  public $created;
  public $updated;


  /**
   * A 'make' function is dedicated to eventRegister
   *  $attributes['event_id'], $attributes['info_id'] is necessary
  //  public static function make($event_id, $info_id, $user_id = NULL, $confirm = 0) {
   *
   * @param array $attributes
   *
   * @return \eventRegister
   * @author Ramin Sadegh nasab
   */
  public static function make($attributes = array()) {
    if (!isset($attributes['user_id'])) { $attributes['user_id'] = NULL; }
    if (!isset($attributes['confirm'])) { $attributes['confirm'] = 0; }

    $eventRegister = new eventRegister();
    $eventRegister->event_id = $attributes['event_id'];
    $eventRegister->info_id = $attributes['info_id'];
    $eventRegister->user_id = $attributes['user_id'];
    $eventRegister->created = strftime("%Y-%m-%d %H:%M:%S", time());
    $eventRegister->confirm = $attributes['confirm'];

    return $eventRegister;
  }
}