<?php

/**
 * Created by PhpStorm.
 * User: Raminli
 * Date: 4/25/2015
 * Time: 10:16 PM
 */
class eventType extends DatabaseObject {
  protected static $table_name = 'evt_type';

  public $id;
  public $sport_id;
  public $evt_type_name;
  public $has_weight;
  public $created;

  protected static $db_fields = array(
    'id',
    'sport_id',
    'evt_type_name',
    'has_weight',
    'created',
    'updated',
  );

  public function edit_box() {
    $output  = '';
    $checked = $this->has_weight ? 'checked' : NULL;
    $output .= '<div class="split_bottom new_row evt_type_holder">';
      $output .= '<div class="edit-box">';
        $output .= '<ul>';
          $output .= '<li><span class="glyphicon glyphicon-edit edit_evt_type blue" title="ویرایش"></span></li>';
          $output .= '<li><span class="glyphicon glyphicon-remove  blue remove_evt_type" title="حذف"></span></li>';
        $output .= '</ul>';
      $output .= '</div>';
    $output .= '<form method="post" action="management/sport-setting/event_type.php" class="evt_type" id="' . $this->id . '">';
    $output .= "<input type= 'text' name ='evt_type_name' disabled class='new_input new_medium_input' value = '{$this->evt_type_name}' />";
    $output .= '<input type="checkbox" name="weight_req" disabled id="weight_req_' . $this->id . '" ' . $checked . '/>';
    $output .= '<label for="weight_req_' . $this->id . '">نیاز به جدول بندی دارد</label>';
    $output .= '<input type = "submit" name="submit" disabled class="btn btn_blue" value = "ثبت"/></input>';
    $output .= '</form>';
    $output .= '</div>';

    return $output;
  }

  /*public static function make($sport_id, $evt_type_name, $has_weight, $id = NULL) {
    var_dump($id);
    $event_type                = new static();
    $event_type->sport_id      = (int) $sport_id;
    $event_type->evt_type_name = $evt_type_name;
    $event_type->created       = strftime("%Y-%m-%d %H:%M:%S", time());
    $event_type->has_weight    = $has_weight;
    if ($id != NULL) {
      $event_type->id = $id;
    }

    return $event_type;
  }*/

  public static function find_by_sport_id($sport_id) {
    $columns_values = array(':sport_id' => $sport_id);
    $query          = 'SELECT SQL_CALC_FOUND_ROWS * ';
    $query .= 'FROM ' . self::table_name(). ' tb';
    $query .= ' WHERE `tb`.`sport_id` = :sport_id ';

    $query .= 'ORDER BY created DESC ';

//    echo $query;die();
    return static::find_by_sql($query, $columns_values);
  }

}