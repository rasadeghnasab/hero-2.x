<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 9/8/2015
 * Time: 12:26 PM
 */
class gymClass extends DatabaseObject {
  protected static $table_name = 'class';
  protected static $sport_key = 'sport_id';
  protected static $node_type = 'node/class';

  public $id;
  public $user_id;
  public $sport_id;
  public $gym_id;

  protected static $db_fields = array(
    'id',
    'user_id',
    'sport_id',
    'gym_id',
    'created'
  );

  protected static $foreign_key_tables = array(
    'gym'            => array(
      'join_type' => 'join',
    ),
    'class_day_time' => array(
      'join_type'      => 'join',
      'join_statement' => 'class_day_time ON class_day_time.class_id = class.id',
      'columns'        => array('class_day_time.*', 'class_day_time.id as class_day_time_id', 'class.id as class_id'),
    ),
    'sport'          => array(
      'join_type' => 'left_join'
    )
  );

  /**
   * Sort classes for used in views (show them up)
   *
   * @param $classes
   *
   * @return mixed
   * @author Ramin Sadegh nasab
   */
  public static function classes_sort($classes) {
    $sorted_class = array();
    if(!is_array($classes) && !is_object($classes)) return FALSE;
    foreach ($classes as $class) {
      if (!is_object($class)) {
        continue;
      }
//      var_dump($class);
//      continue;
      $sorted_class[$class->class_id]['user_id']     = $class->user_id;
      $sorted_class[$class->class_id]['gym_id']      = $class->gym_id;
      $sorted_class[$class->class_id]['gym_name']    = $class->gym_name;
      $sorted_class[$class->class_id]['gym_address'] = $class->gym_address;
      $sorted_class[$class->class_id]['gym_phone']   = $class->gym_phone;
      if (isset($class->field)) {
        $sorted_class[$class->class_id]['field'] = full_name($class->field, $class->branch);
      }
      $sorted_class[$class->class_id]['class_id'] = $class->class_id;
      //        if(is_string($sorted_class[$class['class_id']]['class_day_time_id'])) unset($sorted_class[$class['class_id']]['class_day_time_id']);
      $sorted_class[$class->class_id]['class_day_time_id'][$class->class_day_time_id]['class_day_time_id'] = $class->class_day_time_id;
      $sorted_class[$class->class_id]['class_day_time_id'][$class->class_day_time_id]['day']               = $class->day;
      $sorted_class[$class->class_id]['class_day_time_id'][$class->class_day_time_id]['start_time']        = $class->start_time;
      $sorted_class[$class->class_id]['class_day_time_id'][$class->class_day_time_id]['end_time']          = $class->end_time;
    }

    return $sorted_class;
  }

}