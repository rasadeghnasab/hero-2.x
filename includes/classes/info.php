<?php

/**
 * User: Ramin Sadegh nasab
 * Date: 8/31/2015
 * Time: 1:11 AM
 */
class info extends DatabaseObject {
  protected static $table_name = 'infotable';
  protected static $primary_key = 'infoID';

  public $infoID;
  public $userID;
  public $sportID;
  public $superiorID;
  public $jobID;
  public $beltID;
  public $is_referee;
  public $is_teacher;
  public $has_class;
  public $is_confirm;
  public $insertDate;
  public $updateDate;

  protected static $db_fields = array(
    "infoID",
    "userID",
    "sportID",
    "superiorID",
    "jobID",
    "beltID",
    "is_referee",
    "is_teacher",
    "has_class",
    "is_confirm",
    "insertDate",
    "updateDate",
  );

  public function is_confirm() {
    return $this->is_confirm;
  }


}