<?php
///*
//*/

//error_reporting(0);
//@ini_set('display_errors', 0);

// Database Constants
defined('DB_SERVER') ? NULL : define("DB_SERVER", "localhost");
defined('DB_USER') ? NULL : define("DB_USER", "root");
defined('DB_PASS') ? NULL : define("DB_PASS", "");
defined('DB_NAME') ? NULL : define("DB_NAME", "wall");
defined('CHARSET') ? NULL : define("CHARSET", "utf8");

// set default timezone for my php
date_default_timezone_set('Asia/Tehran');

// jquery base path
defined('BASEPATH') ? NULL : define('BASEPATH','/');

