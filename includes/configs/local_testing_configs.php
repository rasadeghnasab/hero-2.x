<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/8/2015
 * Time: 10:49 PM
 */
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

// for more depth var_dump show
ini_set('xdebug.var_display_max_depth', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);
