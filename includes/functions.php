<?php

//require_once(__DIR__ . '/db_connection.php');
//require_once(__DIR__ . '/calendar.php');
//$error = array();
/* an array for week days */
$week_day       = array(
  'شنبه',
  'یکشنبه',
  'دوشنبه',
  'سه شنبه',
  'چهار شنبه',
  'پنج شنبه',
  'جمعه',
);
$week_day_assoc = array(
  'شنبه'      => 0,
  'یکشنبه'    => 1,
  'دوشنبه'    => 2,
  'سه شنبه'   => 3,
  'چهار شنبه' => 4,
  'پنج شنبه'  => 5,
  'جمعه'      => 6,
);

function getPDOConstantType($var) {
  /* get PDO constant for prepared statement */
  if (is_int($var)) {
    return PDO::PARAM_INT;
  }
  elseif (is_bool($var)) {
    return PDO::PARAM_BOOL;
  }
  elseif (is_null($var)) {
    return PDO::PARAM_NULL;
  }
  
  //Default
  return PDO::PARAM_STR;
}

function ageCalc($birth_date) {
  /* @birth_date
   * this function give $birth_date from user and return age in year
   */
  $time_array = newDateDiff($birth_date);
  
  return array_shift($time_array);
}

/* @$first_date //entered date that we want know difference with now
 * TODO: its better to have $second date
 *
 * @out_in_array // if this variable set to true return date as an array else return as string
 *               date_create fucntion return now as a date.
 */
function newDateDiff($first_date = '2020-06-09 10:30:00', $out_in_array = TRUE) {
  $intervalo = date_diff(date_create(), date_create($first_date));
  $diff_outs = $intervalo->format("%Y,%M,%d,%H,%i,%s");
  if (!$out_in_array) {
    return $diff_outs;
  }
  $diff_outs = explode(',', $diff_outs);
  
  return $diff_outs;
}

/**
 * compare date1 with date2 and <br/>
 * if date1 < date2 then return true else return false
 *
 * @param $first_date  : the first date that we want to check out with the
 *                     second one
 * @param $second_date : the second date that we want to check out with the
 *                     first one if $second_date is false check the first_date with now
 *
 * @return boolean
 */
function compare_two_date($first_date, $second_date = FALSE) {
  $first_date = new DateTime($first_date);
  if ($second_date) {
    $second_date = new DateTime($second_date);
  }
  else {
    $second_date = new DateTime();
  }
  if ($first_date < $second_date) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

function two_entry_equality($first_entry, $second_entry) {
  return $first_entry === $second_entry;
}

function change_thumb_to_large($path, $revers = FALSE) {
  $path_parts = pathinfo($path);
  foreach ($path_parts as $key => $value) {
    if (!$revers) {
      $path_parts[$key] = str_replace('thumb', 'large', $value);
    }
    else {
      $path_parts[$key] = str_replace('large', 'thumb', $value);
    }
  }
  
  return $path_parts;
}

function incomplete_atprofile($weight, $length = '100', $foot = '100') {
  return empty($weight) || empty($length) || empty($foot);
}

function redirect_to($location = NULL) {
  if ($location != NULL) {
    header("Location: {$location}");
    exit;
  }
}

function IpHash() {
  $iip = $_SERVER['REMOTE_ADDR']; // to get user IP
  $iip = hash('crc32b', $iip); // hash to crc32b out put is 8digit
  $iip = $iip >> 3; // shift
  $iip = hash('crc32b', $iip); // hash to crc32b out put is 8digit
  return $iip;
}

/**
 * az in tabe dar mavagheii k mikhahim dar safhe va ya ghesmate az safe yek
 * amoozesh, peygham, va ya har matne digari gharar dahim estefade khahim kard
 *
 * @param string $help_statement matalebi ast k mikhahim dar tooltip khod
 *                               namayesh dahim
 * @param bool   $tooltip        agar meghdare in moteghayer barabare true bashad yek
 *                               tooltip khali ijad mikonad va dar gheyre in sorat kehyr. void help html
 *                               tag
 *
 * @return string
 */
function help_tooltip($help_statement, $tooltip = TRUE) {
  $output = '<a class="help" data-help="' . $help_statement . '"><span class="glyphicon glyphicon-question-sign"></span></a>';
  //  $output .= $tooltip ? '<div class="tooltip"></div>' : NULL;
  echo $output;
}

/**
 * @author Ramin Sadegh nasab
 *
 * @param array $links
 * @param bool  $class
 *
 * @return string
 */
function sub_menu_creator($links, $class = FALSE) {
  $constant_class = 'class ="subMenu hidden-print';
  $constant_class .= $class ? ' ' . $class . '"' : '"';
  $output = '';
  $output .= '<div ' . $constant_class . ' >';
  $output .= '<ul>';
  foreach ($links as $href => $text) {
    $output .= '<li><a href="' . $href . '">' . $text . '</a></li>';
  }
  $output .= '</ul>';
  $output .= '</div>';
  
  return $output;
}

/**
 * @param      $value_post        the array which we want to change
 * @param      $add_in_each_array the keys in this array are keys in
 *                                $value_post that has one value not multiple
 * @param null $skip_names        a key in the $value_post array that we want the
 *                                lenght
 *
 * @return array
 */
function reArrayFiles(&$value_post, $add_in_each_array, $skip_names = NULL) {
  $file_ary   = array();
  $file_keys  = array_values(array_diff(array_keys($value_post), $add_in_each_array));
  $file_count = count($value_post[$file_keys[0]]);
  for ($i = 0; $i < $file_count; $i++) {
    foreach ($file_keys as $key) {
      if (array_search($key, $skip_names) !== FALSE) {
        continue;
      }
      $file_ary[$i][$key] = $value_post[$key][$i];
    }
    foreach ($add_in_each_array as $key) {
      if (array_search($key, $skip_names) !== FALSE) {
        continue;
      }
      $file_ary[$i][$key] = $value_post[$key];
    }
  }
  
  return $file_ary;
}

///////////////////////////////// classes functions
/**
 * @param $class_name
 */
function __autoload($class_name) {
  $class_name = ucfirst($class_name);
  $path       = CLASS_PATH . $class_name . '.php';
  if (file_exists($path)) {
    require_once($path);
  }
  else {
    die('فایل ' . $class_name . ' یافت نشد');
  }
}

/**
 * @param $class_name
 *
 * @author Ramin Sadegh nasab
 */
function class_include($class_name) {
  $class_name = ucfirst($class_name);
  $path       = CLASS_PATH . $class_name . '.php';
  if (file_exists($path)) {
    require_once($path);
  }
  else {
    die('فایل ' . $class_name . ' یافت نشد');
  }
}

///////////////////////////////// form
/**
 * Get and set default values for forms inputs
 *
 * @param string $name
 * @param string $type
 * @param string $selected_value
 * @param string $method
 *
 * @return string
 */
function form_values($name, $type = 'text', $selected_value = '', $method = 'post') {
  $method = strtolower($method);
  $result = '';
  if (($method == 'get' && !isset($_GET[$name])) || ($method == 'post' && !isset($_POST[$name]))) {
    return $result;
  }
  if ($method != 'get' && $method != 'post') {
    return $result;
  }
  $value = $method == 'get' ? $_GET[$name] : $_POST[$name];
  switch ($type) {
    case 'text' :
      $result = ' value = "' . htmlentities($value) . '" ';
      break;
    case 'textarea' :
      $result = htmlentities($value);
      break;
    case 'checkbox' :
      $result = ' checked = "checked" ';
      break;
    case 'radio' :
      if ($value == $selected_value) {
        $result = ' checked = "checked" ';
      }
      break;
    case 'select':
      $result = ' selected ';
      break;
    case 'value':
      $result = $value;
      break;
    default :
      $result = '';
  }

  return $result;
}

///////////////////////////// select
/**
 * Gives an object or array ,loop in that array or object and return a html
 * select element which <br/> if it is an PDO object it's options values are
 * given from $value and it's options texts are given from $text
 *
 * @param        PDO object | array $object_array
 * @param string $value
 * @param string $text
 * @param array  $attributes
 * @param string $selected_value
 * @param array  $custom_option
 *
 * @return string
 */
function select_element($object_array, $value, $text, $attributes, $selected_value = '', $custom_option = array()) {
  $attrs = '';
  if (!empty($attributes) && is_array($attributes)) {
    foreach ($attributes as $key => $values) {
      if (!is_int($key)) {
        $attrs .= ' ' . $key . '="' . $values . '"';
      }
      else {
        $attrs .= ' ' . $values;
      }
    }
  }
  $output = "<select {$attrs} >";
  //        if($all)
  //            $output .= '<option value="all">همه</option>';
  if (is_array($custom_option) && !empty($custom_option)) {
    foreach ($custom_option as $opt_text => $opt_value) {
      $output .= auto_select_value_in_select($opt_value, $opt_text, $selected_value);
    }
  }
  if (is_object($object_array)) {
    if (get_class($object_array) == 'PDOStatement') {
      while ($row = $object_array->fetch(PDO::FETCH_ASSOC)) {
        //        var_dump($row);
        $output .= auto_select_value_in_select($row[$value], $row[$text], $selected_value);
      }
      //      die;
    }
    else {
      foreach ($object_array as $row) {
        $output .= auto_select_value_in_select($row->$value, $row->$text, $selected_value);
      }
    }

    $output .= '</select>';
  }
  elseif (is_array($object_array)) {
    foreach ($object_array as $key => $element) {
      if (is_object($element)) {
        $output .= auto_select_value_in_select($element->$value, $element->$text, $selected_value);
      }
      else {
        $output .= auto_select_value_in_select($key, $element, $selected_value);
      }
    }
  }
  else {
    $output = FALSE;
  }
  if ($output) {
    $output .= '</select>';
  }

  return $output;
}

function auto_select_value_in_select($opt_value, $opt_text, $selected_value) {
  //  var_dump($opt_text);
  //  var_dump($opt_value);
  $selected = $opt_value != $selected_value ? NULL : 'selected';
  $output   = "<option value='{$opt_value}' {$selected}>{$opt_text}</option>";
  
  return $output;
}

//////////////////////////// superGlobals
/**
 * Returns superGlobal variables as an array
 *
 * @param string $name (i.g. GET, POST, and other super global variables)
 *
 * @return mixed
 */
function &getSuperGlobal($name) {
  $name = strtoupper($name);
  
  return $GLOBALS["_$name"];
}

/**
 * Return some of the super globals variables which their name is in array and
 * return an array
 *
 * @author Ramin Sadegh nasab
 *
 * @param array  $variableArray
 * @param string $method
 *
 * @return array
 */
function get_specified_superGlobals_from_array($variableArray, $method = 'post') {
  $method                = strtolower($method);
  $output                = array();
  $fullSuperGlobalsArray = getSuperGlobal($method);
  foreach ($variableArray as $wanted_variable) {
    //if the form element is multi select element this might be occurred
    if (isset($fullSuperGlobalsArray[$wanted_variable]) && is_array($fullSuperGlobalsArray[$wanted_variable]) && empty($fullSuperGlobalsArray[$wanted_variable])) {
      continue;
    }
    elseif (!validate_presences(array($wanted_variable), $method, FALSE, TRUE)) {
      continue;
    }
    $output[$wanted_variable] = $fullSuperGlobalsArray[$wanted_variable];
  }
  
  return $output;
}

///////////////////////////// pagination
/**
 * Set pagination variables as global variables for easier use.
 *
 * @param string $method Determine that which method do we have to set
 *                       pagination variables?
 * @param array  $pagination_vars
 *
 * @return array
 * @author Ramin Sadegh nasab
 */
function pagination_variables($method = 'POST', $pagination_vars = array(
  'page'  => 1,
  'limit' => 15,
)) {
  // TODO: Variable type must be set
  $method = strtolower($method);
  $vars   = get_specified_superGlobals_from_array(array_keys($pagination_vars), $method);
  foreach (array_keys($pagination_vars) as $var) {
    global $$var;
    $value                 = isset($vars[$var]) ? $vars[$var] : $pagination_vars[$var];
    $pagination_vars[$var] = $value;
    $$var                  = (int) $value;
  }
  //  var_dump($pagination_vars);die;
  $pagination_vars['offset'] = (((int) $pagination_vars['page'] - 1) * (int) $pagination_vars['limit']) > 0 ? ((int) $pagination_vars['page'] - 1) * (int) $pagination_vars['limit'] : 0;
  //  unset($pagination_vars['page']);
  //    var_dump($pagination_vars);die;
  
  return $pagination_vars;
}

function pagination_function($url = NULL, $showed_button = 3, $num_rows = NULL) {
  $pagination_variables = pagination_variables(method(), $pagination_vars = array(
    'page'  => 1,
    'limit' => 15,
  ));
  $limit                = $pagination_variables['limit'];
  $current_page         = $pagination_variables['page'];
  $numRows              = $num_rows ? $num_rows : found_rows_calc();
  $pre_page             = (int) $limit;
  $total_count          = (int) $numRows;
  $total_page           = ceil($total_count / $pre_page);
  $previous_page        = $current_page - 1;
  $next_page            = $current_page + 1;
  $has_next_page        = $previous_page >= 1 ? TRUE : FALSE;
  $has_prev_page        = $next_page <= $total_page ? TRUE : FALSE;
  $href                 = $url != NULL ? $url . '/' : '';
  if (preg_match('/page=/i', $url)) {
    $href = $url;
  }
  if ($total_page > 1) {
    $output = '<ul class="paginate pag5 clearfix" >';
    $output .= '<span> ' . $numRows . ' مورد یافت شد</span>';
    $output .= '<li class="single">' . $current_page . ' از ' . $total_page . '</li>';
    if ($has_next_page) {
      $output .= '<li class="navpage"><a href="' . $href . '1"><span class="glyphicon glyphicon-backward"></span></a></li>';
      $output .= '<li class="navpage"><a href="' . $href . $previous_page . '"><span class="glyphicon glyphicon-chevron-left"></span></a></li>';
    }
    else {
      $output .= '<li class="navpage"><a href="' . $href . $total_page . '"><span class="glyphicon glyphicon-chevron-left"></span></a></li>';
    }
    for ($i = 1; $i <= $total_page; $i++) {
      if ($total_page > ($showed_button * 2)) {
        if (($i <= $current_page && $i > $current_page - $showed_button) || ($i > $total_page - $showed_button)) {
          if ($i == $current_page) {
            $output .= '<li class="current">' . $i . '</li>';
          }
          else {
            $output .= '<li><a href="' . $href . $i . '">' . $i . '</a></li>';
          }
        }
        else {
          if ($i == ceil($total_page / 2)) {
            $output .= '<li><a href="' . $href . $i . '">...</a></li>';
          }
        }
      }
      else {
        if ($i == $current_page) {
          $output .= '<li class="current">' . $i . '</li>';
        }
        else {
          $output .= '<li><a href="' . $href . $i . '">' . $i . '</a></li>';
        }
      }
      
    }
    if ($has_prev_page) {
      $output .= ' <li class="navpage"><a href="' . $href . $next_page . '"><span class="glyphicon glyphicon-chevron-right"></span></a></li> ';
      $output .= '<li class="navpage"><a href="' . $href . $total_page . '"><span class="glyphicon glyphicon-forward"></span></a></li>';
    }
    else {
      $output .= ' <li class="navpage"><a href="' . $href . '1"><span class="glyphicon glyphicon-chevron-right"></span></a></li> ';
    }
    $output .= '</ul>';
    
    return $output;
  }
  else {
    return NULL;
  }
  
}

function global_order($order = NULL) {
  $order = $order == NULL ? 'DESC' : $order;
  $order = isset($_POST['order']) ? $_POST['order'] : $order;
  $order = isset($_GET['order']) ? $_GET['order'] : $order;

  $_POST['order'] = $order;
  return $order;
}
/**
 * Return method of this Request on the Server
 *
 * @param null $method
 *
 * @return null|string
 * @author Ramin Sadegh nasab
 */
function method($method = NULL) {
  if ($method) {
    return $method;
  }
  $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';

  return $method;
}

function found_rows_calc() {
  global $db;
  $rowCountQuery = "SELECT FOUND_ROWS();";
  $rowsCount     = $db->query($rowCountQuery)->fetch(PDO::FETCH_NUM);
  
  return $rowsCount[0];
}

/**
 * @param bool $limit           if our limit = $limit that line is selected
 * @param int  $offset          gap between two serial limit number
 * @param int  $number_of_limit determine that how many choices we want to have?
 */
function limit_query($limit = FALSE, $offset = 5, $number_of_limit = 3) {
  for ($i = 1; $i <= $number_of_limit; $i++) {
    $j = $i * $offset;
    if ($j == $limit) {
      echo '<option value="' . $j . '" selected>' . $j . '</option>';
    }
    else {
      echo '<option value="' . $j . '" >' . $j . '</option>';
    }
  }
}

///////////////////////////////////////////////////// password functions
function password_check($password, $existing_hash) {
  $hash = crypt($password, $existing_hash);
  
  return $hash === $existing_hash ? TRUE : FALSE;
}

function password_encrypt($password) {
  $hash_format     = "$2y$10$"; // Tells PHP to use Blowfish with a "cost" of 10
  $salt_length     = 22; // Blowfish salts should be 22-characters or more
  $salt            = generate_salt($salt_length);
  $format_and_salt = $hash_format . $salt;
  $hash            = crypt($password, $format_and_salt);
  
  return $hash;
}

function generate_salt($length) {
  // Not 100% unique, not 100% random, but good enough for a salt
  // MD5 returns 32 characters
  $unique_random_string = md5(uniqid(mt_rand(), TRUE));
  // Valid characters for a salt are [a-zA-Z0-9./]
  $base64_string = base64_encode($unique_random_string);
  // But not '+' which is valid in base64 encoding
  $modified_base64_string = str_replace('+', '.', $base64_string);
  // Truncate string to the correct length
  $salt = substr($modified_base64_string, 0, $length);
  
  return $salt;
}

///////////////////////////////////////////////////// database functions
function sql_query_executor($query, $parameters = array(), $qusetionMark = FALSE) {
  global $db;
  $result = TRUE;
  if (empty($query)) {
    return FALSE;
  }
  //        global $log_error_message;
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $preparedQuery = $db->prepare($query);
  if (!empty($parameters)) {
    foreach ($parameters as $key => $value) {
      if ($qusetionMark) {
        $bind = $key + 1;
      }
      else {
        $bind = $key;
      }
      $preparedQuery->bindValue($bind, $value, getPDOConstantType($value));
    }
    $match_count = array();
    if (!$qusetionMark) {
      preg_match_all('/\B:\w*/', $preparedQuery->queryString, $matches);
      foreach ($matches[0] as $match) {
        if (!in_array($match, $match_count)) {
          $match_count[] = $match;
        }
      }
    }
    else {
      preg_match_all('/\?/', $preparedQuery->queryString, $matches);
      $match_count = $matches[0];
    }
    if (count($parameters) === count($match_count)) {
      try {
        $result = $preparedQuery->execute();
      } catch(Exception $e) {
        $log_error_message = $e->getMessage();
        echo('270' . $log_error_message);
        
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
    /*$error = $db->errorInfo();
            var_dump($error);
            if(isset($error[2])) {
                $log_error_message = $error[2];
            }*/
    if ($result == 0) {
      return FALSE;
    }
  }
  else {
    try {
      $preparedQuery = $db->query($query);
    } catch(Exception $e) {
      $log_error_message = $e->getMessage();
      
      //                var_dump('270'.$log_error_message);
      return FALSE;
    }
  }
  
  return $preparedQuery;
}

function sql_none_select_query_executor($query) {
  global $db;
  $result = FALSE;
  $query  = trim($query);
  if (empty($query)) {
    return FALSE;
  }
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  try {
    $result = $db->exec($query);
  } catch(Exception $e) {
    $log_error_message = $e->getMessage();
    //            var_dump('270'.$log_error_message);
    //            echo json_encode('270'.$log_error_message);
    //            echo json_encode($query);die();
  }
  
  return $result;
}

function update_query_creator($columns, $table_name, $where, $limit = TRUE, $questoinMark = FALSE) {
  $update = 'UPDATE ' . $table_name;
  $update .= ' SET ';
  $count = 0;
  foreach ($columns as $column) {
    if ($count > 0) {
      if ($questoinMark) {
        $update .= ',' . $column . ' = ?';
      }
      else {
        $update .= ',' . $column . ' = :' . $column;
      }
    }
    else {
      if ($questoinMark) {
        $update .= $column . ' = ?';
      }
      else {
        $update .= $column . ' = :' . $column;
      }
    }
    $count++;
  }
  $update .= ' WHERE ';
  if (is_array($where)) {
    foreach ($where as $key => $col) {
      if ($key == 0) {
        if ($questoinMark) {
          $update .= $col . ' = ?';
        }
        else {
          $update .= $col . ' = :' . $col;
        }
      }
      else {
        if ($questoinMark) {
          $update .= ' AND ' . $col . ' = ?';
        }
        else {
          $update .= ' AND ' . $col . ' = :' . $col;
        }
      }
      
    }
  }
  else {
    if ($questoinMark) {
      $update .= $where . ' = ?';
    }
    else {
      $update .= $where . ' = :' . $where;
    }
  }
  if ($limit === TRUE) {
    $update .= ' LIMIT 1';
  }
  else {
    if ($limit != FALSE) {
      $update .= ' LIMIT ' . $limit;
    }
  }
  
  return $update;
}

/**
 * @author Ramin Sadegh nasab
 *
 * @param array  $columns_values
 * @param string $table_name
 * @param array  $where
 * @param bool   $limit
 *
 * @return string
 */
function new_update_query_creator($columns_values, $table_name, $where, $limit = TRUE) {
  global $db;
  $query = 'UPDATE `' . $table_name . '`';
  $query .= ' SET ';
  $count = 0;
  foreach ($columns_values as $column => $value) {
    if ($count > 0) {
      $query .= ', ';
    }
    $query .= '`' . $column . '` = ' . $db->quote($value);
    $count++;
  }
  $query .= ' WHERE ';
  if (is_array($where) && !empty($where)) {
    $count = 0;
    //            $query .= implode(' AND ',$where);
    foreach ($where as $cond => $val) {
      if ($count > 0) {
        $query .= ' AND ';
      }
      $query .= '`' . $cond . '` = ' . $db->quote($val);
      $count++;
    }
  }
  if ($limit === TRUE) {
    $query .= ' LIMIT 1';
  }
  elseif ($limit != FALSE) {
    $query .= ' LIMIT ' . $limit;
  }
  
  return $query;
}

function insert_query_creator($columns, $table_name) {
  if (empty($columns) || !is_array($columns)) {
    return FALSE;
  }
  $update = 'INSERT INTO ' . $table_name;
  $update .= ' (';
  $update .= implode(', ', $columns);
  $update .= ') VALUES (';
  $count = 0;
  foreach ($columns as $column) {
    if ($count > 0) {
      $update .= ', :' . $column;
    }
    else {
      $update .= ' :' . $column;
    }
    $count++;
  }
  $update .= ')';
  
  return $update;
}

function insert_query_creator_new($columns_values, $table_name) {
  global $db;
  if (empty($columns_values) || !is_array($columns_values)) {
    return FALSE;
  }
  $columns = array_keys($columns_values);
  $query   = 'INSERT INTO `' . $table_name . '`';
  $query .= ' (';
  $query .= implode(', ', $columns);
  $query .= ') VALUES (';
  $count = 0;
  foreach ($columns_values as $value) {
    if ($count > 0) {
      $query .= ', ' . $db->quote($value);
    }
    else {
      $query .= ' ' . $db->quote($value);
    }
    $count++;
  }
  $query .= ')';
  
  return $query;
}

function delete_query_creator($where, $table_name) {
  $delete = FALSE;
  if (is_array($where)) {
    $delete = 'DELETE FROM ' . $table_name;
    $delete .= ' WHERE ';
    foreach ($where as $key => $value) {
      if ($key == 0) {
        $delete .= $value . ' = :' . $value;
      }
      else {
        $delete .= ' AND ' . $value . ' = :' . $value;
      }
    }
  }

  return $delete;
}

/**
 * @param array  $where   array { <br/>
 *                        keys : include where clause , <br/>
 *                        values : include values for where clause <br/>
 *                        }
 * @param string $table_name
 *
 * @return bool|string
 */
function delete_query_creator_new($where, $table_name) {
  global $db;
  if (!is_array($where) || empty($where)) {
    return FALSE;
  }
  $query = "DELETE FROM `{$table_name}`";
  $query .= ' WHERE ';
  $count = 0;
  foreach ($where as $key => $value) {
    $query .= $count > 0 ? ' AND ' : NULL;
    $query .= $key . ' = ' . $db->quote($value) . ' ';
    $count++;
  }
  
  return $query;
}

/** new data base functions */
function db_select() {
  global $db;

  return new FluentPDO($db);
}

function db_insert() {

  return db_select();
}

function db_update() {

  return db_select();
}

function db_delete() {

  return db_select();
}

function db_connection() {
  global ${DATABASE_CONNECTION_NAME};

  return ${DATABASE_CONNECTION_NAME};
}

function last_insert_id() {
  return db_connection()->lastInsertId();
}

/*function select_query_creator(){}*/
/////////////////////////////
function new_post_form() {
  $cat_attrs = array(
    'name'  => "catID[]",
    'id'    => "catID",
    'class' => "chosen-select chosen-rtl",
    'multiple',
    'style' => "width: 60%",
  );
  $output    = '<div class="news_new">';
  $output .= '<form id="uploadImgForm" class="hidden" action="post/imageInsert.php" enctype="multipart/form-data" method="post">';
  $output .= '<input type="hidden" name="' . ini_get("session.upload_progress.name") . '" value="123" >';
  $output .= '<input type="file" id="inputImg" name="userImage" class="setimg inpImg hidden" >';
  $output .= '</form>';
  //        $output .= '<form action="" method="post">';
  //  $output .= '<div class="progress_outer"><div id="progress" class="progress"></div></div>';
  /*
   $output .= '<input type="text" id="title" name="title" class="new_input new_medium_input validate nameValidation"
                   placeholder="عنوان"
                   required />';
  */
  $output .= '<form id="wall_post" action="post/postInsert.php" data-ajax = "true" method="post" novalidate="novalidate">';
  $output .= '<div class="input-container">';
  $output .= '<textarea rows="5" class="main_text" id="mainText" name="mainText"
placeholder="اطلاعات ورزشی خود را در اینجا با دیگران به اشتراک بگذارید..." required="required"></textarea>';
  $output .= '<div id="imgBack"></div>';
  $output .= '<div class="clearfix"></div>';
  $output .= '</div>'; // input-container end
  $output .= '<div class="news_footer">';
  $output .= '<span class="fl glyphicon glyphicon-plus set_img" style="font-size: .4em;"></span>';
  $output .= '<span class="fl glyphicon glyphicon-camera set_img"></span>';
  $output .= '<input class="btn btn_white btnForce" id="submitNews" type="submit" name="submit" value="ثبت">';
  $output .= '<span class="subject">';
  $output .= '<label for="catID">موضوع</label>';
  $output .= select_element(all_categories(), 'id', 'name', $cat_attrs);
  $output .= '</span>';
  $output .= '</div>'; // footer end
  $output .= '</form>'; // form end
  //  $output .= '<div class="clearfix" ></div>';
  $output .= '</div>'; // news_new
  
  return $output;
}

function teacher_submenu($user_id, $info_id, $honors = TRUE, $classes = TRUE, $is_teacher = FALSE) {
  global $Acl;
  global $cUser;

  if (isset($cUser->user_id) && $cUser->user_id == $user_id) {
    $self_teacher = ($Acl->check_permission('self_teacher', $cUser->sid, $cUser->user_id) && is_teacher_student_by_infoid($info_id, $user_id)) ? TRUE : FALSE;
  }
  else {
    return FALSE;
  }

  $output = '<div class="subMenu">';
  $output .= '<ul>';
  if ($honors) {
    $output .= "<li><a href=\"/setting/sports/{$user_id}/{$info_id}/honors\"> افتخارات من</a></li>";
  }
  if ($classes && $is_teacher) {
    $output .= "<li><a href=\"/setting/sports/{$user_id}/{$info_id}/classes\"> باشگاه های من</a></li>";
  }
  if ($self_teacher) {
    $output .= "<li><a href=\"/setting/sports/{$user_id}/{$info_id}/students\"> مدیریت ورزشی</a></li>";
  }
  $output .= '</ul>';
  $output .= '</div>';

  echo $output;
}

///////////////////////////// post
function image_post_insert($columns_values) {
  $table_name = 'wallpic';
  $query      = insert_query_creator_new($columns_values, $table_name);
  
  return sql_none_select_query_executor($query);
}

function post_insert($columns_values) {
  $table_name = 'wall';
  $query      = insert_query_creator_new($columns_values, $table_name);
  
  return sql_none_select_query_executor($query);
}

////////////////////////////
function sport_description_select($sportID) {
  $query = 'SELECT `sport`.`sport_description` ';
  $query .= 'FROM `sport` ';
  $query .= 'WHERE `sport`.`id` = :sportID ';
  $result = sql_query_executor($query, array(':sportID' => $sportID))->fetch(PDO::FETCH_NUM);
  
  return array_shift($result);
}

/* select states which have at least one sport in it
         * */
function state_distinct_select($sportID = NULL, $outputType = 'PDOObject') {
  global $db;
  global $stateName;
  $stateQ = 'SELECT DISTINCT `stateName` ';
  $stateQ .= 'FROM `state` ';
  if ($sportID != NULL) {
    $stateQ .= 'JOIN `place` ON `state`.`stateID` = `place`.`stateID` ';
    if (!is_bool($sportID)) {
      $stateQ .= $sportID != FALSE ? 'WHERE `place`.`sportID` = ' . $sportID : NULL;
    }
  }
  $stateResult = $db->query($stateQ);
  if ($outputType === 'string') {
    $output = '';
    while ($row = $stateResult->fetch(PDO::FETCH_NUM)) {
      $output .= auto_select_value_in_select($row[0], $row[0], $stateName);
    }
    
    return $output;
  }
  
  //          echo $stateQ;die();
  return $stateResult;
}

function stateID_county_by_stateName($stateName) {
  $query = 'SELECT DISTINCT `state`.`stateID`, `countyName` ';
  $query .= 'FROM `state` ';
  //        $query .= 'JOIN `place` ON `state`.`stateID` = `place`.`stateID` ';
  $query .= 'WHERE stateName = :stateName';
  $columns_values = array(':stateName' => $stateName);
  
  return sql_query_executor($query, $columns_values);
}

function state_and_persons_by_sportID($sportID) {
  $states     = array();
  $all_states = state_distinct_select();
  while ($state = $all_states->fetch(PDO::FETCH_NUM)) {
    $states[] = $state[0];
  }
  $states = "'" . implode("','", $states) . "'";
  $query  = "SELECT SQL_CALC_FOUND_ROWS `infotable`.`infoID`, `user`.`userID`, `user`.`first_name`, `user`.`last_name`, ";
  $query .= "`user`.`created`, `sport`.`field`, `sport`.`branch`, `state`.`stateID`, `sport`.`id` as `sport_id`";
  $query .= ",`state`.`statename`, `state`.`countyname`, `infotable`.`insertdate`, `userpic`.`picurl`, ";
  $query .= "`job`.`jobname`, `infotable`.`is_confirm`, `infotable`.`superiorID` ";
  $query .= "FROM `infotable` ";
  $query .= "JOIN `user` ON `user`.`userID`   = `infotable`.`userID` ";
  $query .= "JOIN `sport` ON `sport`.`id` = `infotable`.`sportID` ";
  $query .= "JOIN `state` ON `state`.`stateID` = `user`.`stateID` ";
  $query .= "LEFT JOIN `userpic` ON `userpic`.`userID`= `user`.`userID` ";
  $query .= "JOIN `job` ON `job`.`jobID` = `infotable`.`jobID` ";
  $query .= ' WHERE ';
  $query .= '`infotable`.`jobID` > 2 AND `infotable`.`jobID` < 5 AND `infotable`.`sportID` = :sportID ';
  $query .= " AND `state`.`statename` IN ({$states}) ";
  $query .= ' ORDER BY ';
  $query .= '`infotable`.`jobID` DESC, `user`.`stateID` DESC ';
  $values = array(':sportID' => $sportID);

  //  echo $query;die;
  return sql_query_executor($query, $values);
}

function state_person_sort($Object) {
  $new_obj = array();
  while ($row = $Object->fetch(PDO::FETCH_ASSOC)) {
    //            var_dump($row);
    $new_obj[$row['statename']][] = $row;
  }
  //        die();
  //        var_dump($new_obj);die();
  return json_encode($new_obj);
}

function job_select($jobID = 3) {
  $acl              = new Acl();
  $bundle           = $acl->check_permission('admin') ? 'admin' : 'sport';
  $access           = $acl->check_permission('self_teacher');
  $operator         = $access ? ' <= ' : ' < ';
  $values[':jobID'] = $jobID;
  $result           = db_select()
    ->from('`job` j')
    ->select(NULL)
    ->select('j.`jobID`, j.`jobName`')
    ->where("j.jobID {$operator} ?", $jobID)
    ->where('j.bundle = ?', $bundle)
    ->execute()
  ;
  //  var_dump($values);
  //  var_dump($jobID);
  //  var_dump($bundle);
  //  echo $query->getQuery();die;
  return $result;
}

function degree_select_all() {
  global $db;
  $degreeQ   = 'SELECT `degID`, `degName` FROM `degree`';
  $degResult = $db->query($degreeQ);
  
  return $degResult;
}

function echo_degreeID($PDOObject, $degreeID = NULL) {
  $output = '';
  while ($row = $PDOObject->fetch(PDO::FETCH_NUM)) {
    if ($row[0] != $degreeID) {
      $output .= "<option  value=\"{$row[0]}\">{$row[1]}</option>";
    }
    else {
      $output .= "<option  value=\"{$row[0]}\" selected>{$row[1]}</option>";
    }
  }
  
  return $output;
}

function sport_distinct_select() {
  $query = 'SELECT DISTINCT `field`, `branch`, `sport_description`, `sportID` ';
  $query .= 'FROM `sport` JOIN `place` ';
  $query .= 'ON `sport`.`id` = `place`.`sportID` ';
  
  return sql_query_executor($query);
}

/*function sports_to_string($object) {
  $output = '';
  $open = 1;
  $close_class = ' class ="icon arrow_up_white_icon"';
  $class = $open_class = ' class ="icon arrow_down_white_icon"';
  while ($sport = $object->fetch(PDO::FETCH_ASSOC)) {
    $output .= '<div class="collapse">';
    $output .= '<div class="collapse_header">';
    $output .= '<span class="collapse_icon"></span>';
    $output .= '<h3 ' . $class . ' data-toggle="collapse" data-open-collapse = "' . $open . '" >' . htmlentities($sport['sportName']) . "<span class='fl'><span class='date_time' > ({$sport['students_number']}) </span> ورزشکار </span>" . '</h3>';
    $output .= '</div>';
    $output .= '<div class="collapse_body">';
    if ($sport['sport_description'] != NULL) {
      $output .= '<p>' . htmlentities($sport['sport_description']) . '</p>';
    }
    else {
      $output .= '<p> مطلبی در مورد این ورزش قرار داده نشده است.</p>';
    }
    $output .= '</div>';
    $output .= '<div class="collapse_footer">';
    $output .= '<h5><a href="/masters/' . $sport['field'] . '/' . $sport['branch'] . '">مشاهده استاتید این رشته</a></h5>';
    $output .= '</div>';
    $output .= '</div>';

    $class = $close_class;
    $open = 0;
  }

  return $output;
}*/

function sports_to_string($sports_object) {
  $output = '';
  $output .= '<div class="ui special cards">';
  while ($sport = $sports_object->fetch(PDO::FETCH_ASSOC)) {
    $image = Image::sport_logo_load($sport['sport_id'])->create_src();
    $output .= '<div id="' . $sport['field'] . '-' . $sport['branch'] . '" class="ui modal standard" >'; // modal start
    $output .= '<i class="close icon"></i>';
    $output .= '<div class="header">تاریخچه</div>';
    $output .= '<div class="content">';
    $output .= '<div class="ui large image ">';
    $output .= "<img class='ui image bordered' src='{$image}'>";
    $output .= '</div>'; // ui large image
    $description = $sport['sport_description'] != NULL ? htmlentities($sport['sport_description']) : ' مطلبی در مورد این ورزش قرار داده نشده است.';
    $output .= '<div class="description">';
    $output .= '<div class="ui header teal" >' . $sport['field'] . ' ' . $sport['branch'] . '</div>';
    $output .= "<p>{$description}</p>";
    $output .= '</div>'; // description
    $output .= '</div>'; // content
    $output .= '</div>'; // ui modal // modal end
    $output .= '<div class="card">';
    $output .= '<div class="image">';
    $output .= '<div class="ui dimmer">';
    $output .= '<div class="content">';
    $output .= '<div class="center">';
    $output .= '<a class="ui inverted button" href="/masters/' . $sport['field'] . '/' . $sport['branch'] . '">استاتید</a>';
    $output .= '<div class="ui inverted button modal-show" data-modal-target ="#' . $sport['field'] . '-' . $sport['branch'] . '">تاریخچه</div>';
    $output .= '</div>'; // center
    $output .= '</div>'; // content
    $output .= '</div>'; // ui dimmer
    $output .= "<img src='{$image}'>";
    $output .= '</div>'; // dimmable image
    $output .= '<div class="content">';
    $output .= '<a href="/masters/' . $sport['field'] . '/' . $sport['branch'] . '" class="header" >' . htmlentities($sport['sportName']) . '</a>';
    $output .= '<div class="meta">';
    $output .= '</div>'; // meta
    $output .= '</div>'; //content
    $output .= '<div class="extra content">';
    $output .= '<a class="fl">';
    $output .= '<i class="users icon"></i>';
    $output .= "<span class='date_time' > ({$sport['students_number']}) </span> ورزشکار ";
    $output .= '</a>';
    $output .= '</div>'; // extra content

    //    if ($sport['sport_description'] != NULL) {
    //      $output .= '<p>' . htmlentities($sport['sport_description']) . '</p>';
    //    }
    //    else {
    //      $output .= '<p> مطلبی در مورد این ورزش قرار داده نشده است.</p>';
    //    }
    //    $output .= '<h5><a href="/masters/' . $sport['field'] . '/' . $sport['branch'] . '">مشاهده استاتید این رشته</a></h5>';
    //    $output .= '</div>';
    //    $output .= '</div>';

    //    $class = $close_class;
    //    $open = 0;
    $output .= '</div>'; // card
  }
  $output .= '</div>'; // ui special cards
  
  return $output;
  
}

function sport_distinct_by_stateName($stateName, $outputType = 'string') {
  global $db;
  global $field;
  $sportsQ = 'SELECT DISTINCT `field`, `sport`.`id` as `sport_id` ';
  $sportsQ .= 'FROM `sport`, `place`, `state` ';
  $sportsQ .= 'WHERE `state`.`stateName` = :stateName ';
  $sportsQ .= ' AND `state`.`stateID` = `place`.`stateID` ';
  $sportsQ .= ' AND `place`.`sportID` = `sport`.`id` ';
  $sportsPre = $db->prepare($sportsQ);
  $sportsPre->bindParam(':stateName', $stateName, PDO::PARAM_STR);
  $sportsPre->execute();
  if ($outputType == 'string') {
    $output = '';
    while ($sport = $sportsPre->fetch(PDO::FETCH_NUM)) {
      if ($sport[0] != $field) {
        $output .= '<option value ="' . $sport[0] . '">' . $sport[0] . '</option>';
      }
      else {
        $output .= '<option value ="' . $sport[0] . '" SELECTED>' . $sport[0] . '</option>';
      }
    }
    
    return $output;
  }
  else {
    return $sportsPre;
  }
  
}

function sport_distinct_by_stateID($stateID, $outputType = 'string') {
  global $db;
  global $sportName;
  $sportQ = 'SELECT DISTINCT `field` ';
  $sportQ .= 'FROM `place` JOIN `sport` ON place.sportID = sport.sportID ';
  $sportQ .= 'WHERE `stateID` = :stateID && `sportConfirm` = true ';
  $sport = $db->prepare($sportQ);
  $sport->bindParam(':stateID', $stateID, PDO::PARAM_STR);
  $sport->execute();
  $num = $sport->rowCount();
  if ($outputType == 'string') {
    $output = '';
    if (!$num) {
      $output .= '<option value="0" >ناموجود</option>';
    }
    else {
      while ($row = $sport->fetch(PDO::FETCH_NUM)) {
        if ($row[0] == $sportName) {
          $output .= '<option value ="' . $row[0] . '" SELECTED>' . $row[0] . '</option>';
        }
        else {
          $output .= '<option value ="' . $row[0] . '">' . $row[0] . '</option>';
        }
      }
    }
    
    return $output;
  }
  else {
    return $sport;
  }
  
}

function sportName_by_sportID($sportID) {
  global $db;
  $select = 'SELECT `field`, `branch` ';
  $select .= 'FROM `sport` ';
  $select .= 'WHERE `sport`.`id` = :sport_id ';
  $select .= 'LIMIT 1';
  $sport = $db->prepare($select);
  $sport->bindParam(':sport_id', $sportID, getPDOConstantType($sportID));
  $sport->execute();
  
  return $sport->fetch(PDO::FETCH_ASSOC);
}

function sport_id_by_sport_name($field, $branch) {
  $query = 'SELECT `sport`.`id` ';
  $query .= 'FROM `sport` ';
  $query .= 'WHERE ';
  $query .= 'field = :field ';
  $query .= 'AND branch = :branch ';
  $query .= 'LIMIT 1';
  //  echo $query;die;
  $parameters = array(
    ':field'  => $field,
    ':branch' => $branch,
  );
  
  return sql_query_executor($query, $parameters)->fetch(PDO::FETCH_COLUMN);
}

/*
 * return all sports names, descriptions, and number of students <br/>
 * and sort them based on number of students
 *
 * @return Object
 */
function all_sports_name() {
  $query = 'SELECT `sport`.`id` as `sport_id`,CONCAT_WS(" ", `sport`.`field`,`sport`.`branch`) AS sportName';
  $query .= ', `sport_description` , `sport`.`field`, `sport`.`branch` ';
  $query .= ', COUNT(`infotable`.`sportID`) as students_number ';
  $query .= ' FROM `sport` ';
  $query .= ' JOIN `infotable` ON `infotable`.`sportID` = `sport`.`id` ';
  $query .= ' WHERE `sport`.`sportConfirm` = 1 ';
  $query .= ' GROUP BY `sport`.`id` ';
  $query .= ' ORDER BY students_number DESC , sportName ';
  //  echo $query; die();
  $result = sql_query_executor($query);
  
  return $result;
}

function branch_by_sportName($sportName, $outputType = 'string') {
  global $db;
  global $sportID;
  $fieldsQ = 'SELECT  `branch`, `sport`.`id` as `sport_id`';
  $fieldsQ .= 'FROM `sport` ';
  $fieldsQ .= 'WHERE `field` = :sportName ';
  $fieldsPre = $db->prepare($fieldsQ);
  $fieldsPre->bindParam(':sportName', $sportName, PDO::PARAM_STR);
  $fieldsPre->execute();
  if ($outputType === 'string') {
    $output = '';
    while ($field = $fieldsPre->fetch(PDO::FETCH_NUM)) {
      if ($sportID != $field[1]) {
        $output .= '<option value ="' . $field[1] . '">' . $field[0] . '</option>';
      }
      else {
        $output .= '<option value ="' . $field[1] . '" SELECTED>' . $field[0] . '</option>';
      }
    }
    
    return $output;
  }
  else {
    return $fieldsPre;
  }
  
}

function branch_by_sportName_stateID($sportName, $stateID, $outputType = 'string') {
  global $db;
  global $sportID;
  $branchesSelectQ = 'SELECT `branch`, `sport`.`id` as `sport_id` ';
  $branchesSelectQ .= 'FROM  `sport` RIGHT JOIN  `place` ON  `sport`.`id` =  `place`.`sportID` ';
  $branchesSelectQ .= 'WHERE `place`.`stateID` = :stateID ';
  $branchesSelectQ .= ' AND `sport`.`field`   = :sportName ';
  $branchesPre = $db->prepare($branchesSelectQ);
  $branchesPre->bindParam(':stateID', $stateID, PDO::PARAM_INT);
  $branchesPre->bindParam(':sportName', $sportName, PDO::PARAM_STR);
  $branchesPre->execute();
  if ($outputType === 'string') {
    $output = '';
    while ($branch = $branchesPre->fetch(PDO::FETCH_NUM)) {
      if ($branch[1] != $sportID) {
        $output .= '<option value ="' . $branch[1] . '">' . $branch[0] . '</option>';
      }
      else {
        $output .= '<option value ="' . $branch[1] . '" SELECTED>' . $branch[0] . '</option>';
      }
    }
    
    return $output;
  }
  else {
    return $branchesPre;
  }
}

function belt_by_sport($sportID) {
  $query = 'SELECT `beltID`, `beltName` ';
  $query .= 'FROM `belt` ';
  $query .= 'WHERE `sportID` = :sportID ';
  $query .= 'ORDER BY `beltValue` ';
  $values = array(':sportID' => $sportID);
  $result = sql_query_executor($query, $values);
  
  return $result;
}

////////////////////////////// notifications
function user_notification($userID) {
  $query = "SELECT * FROM `notifications` ";
  $query .= "WHERE userID = :userID ";
  $values = array(':userID' => $userID);
  
  return sql_query_executor($query, $values)->fetch(PDO::FETCH_ASSOC);
}

function check_notifications($jump = FALSE) {
  global $cUser;
  //  var_dump($cUser);die;
  /*@jump if it set to true we check the notifications anyway (jump from if statement and execute the rest) */
  if (isset($_SESSION['notif_time'])) {
    if (time() - $_SESSION['notif_time'] < 300 && !$jump) { //time to wait before each message 600s = 10 min
      // if user last check for notifications is less than 10min return(or exit); else user_notification function is execute
      return;
    }
  }
  $notifications           = user_notification($cUser->user_id);
  $_SESSION['student_reg'] = $notifications['student_register'];
  $_SESSION['event_reg']   = $notifications['event_register'];
  $_SESSION['new_event']   = $notifications['new_event'];
  $_SESSION['notif_time']  = time();
}

function update_just_increment($value) { //this function created for use in array_map function.
  return $value . ' = ' . $value . ' +1 ';
}

function update_to_zero($value) {
  return $value . ' = 0';
}

function insert_update_notificatoins($columns_to_insert, $values_to_insert, $columns_to_update, $update_to_zero = FALSE) {
  /*
         * @columns_to_insert //which columns do we want to insert into
         * @values_to_insert // which values do we want to insert into those columns
         * @columns_to_update //if is inserted then which columns we want to update ( last_column_value++ )
         * @update_to_zero // change column value to zero. no matter what is it's value
         * */
  $table_name = 'notifications';
  $query      = insert_query_creator($columns_to_insert, $table_name);
  $query .= ' ON DUPLICATE KEY UPDATE ';
  if ($update_to_zero === FALSE) {
    $query .= implode(', ', array_map('update_just_increment', $columns_to_update));
  }
  else {
    $query .= implode(', ', array_map('update_to_zero', $columns_to_update));
    //            $values_to_insert = array_merge($values_to_insert,$values_to_update);
  }
  
  return sql_query_executor($query, $values_to_insert);
}

function update_notifications($columns_to_update, $value) { }

////////////////////////////// teacher
////////////////////////////// teacher classes
/**
 * @param $userID
 *
 * @author Ramin Sadegh nasab
 */
function teacher_new_class_form($userID) {
  /*
        * create a form for teacher new classes *
        * */
  global $week_day_assoc;
  //                    $sports = sports_by_user_id($userID);
  //                    $sport_attrs = array('name' => 'sportID','class' => 'chosen-select chosen-rtl');
  //                    $str_awards = select_element($sports,'sportID','sport_name',$sport_attrs);
  //                    echo $str_awards;
  $sport_attrs = array(
    'name'  => 'week_day[]',
    'class' => 'week_days',
  );
  $form_string = '<form action="teacherPhpFiles/add_new_class.php" method="post" name="new_class_form">';
  $form_string .= '<fieldset>';
  $form_string .= '<div class="new_row new_inner_row">';
  $form_string .= '<input class="new_input" name="gym_name" placeholder="نام باشگاه">';
  $form_string .= '<input class="new_input new_long_input" type="text" name="gym_address" placeholder="آدرس باشگاه" />';
  $form_string .= '</div>';
  $form_string .= '<div class="new_inner_row">';
  $form_string .= 'روز ';
  $form_string .= select_element($week_day_assoc, 'sportID', 'sport_name', $sport_attrs);
  $form_string .= 'از  ساعت ';
  $form_string .= '<input type = "text" class="new_input four_digit_input" name="start_time[]" required="required" placeholder="شروع"/ >';
  $form_string .= ' تا ';
  $form_string .= '<input type = "text" class="new_input four_digit_input" name="end_time[]" required="required" placeholder="اتمام"/>';
  $form_string .= '</div>';
  $form_string .= '<div class="new_inner_row">';
  $form_string .= '<span class="new_class btn btn_orange btn_small"> <span class="glyphicon glyphicon-plus-sign"></span> روز و ساعت</span>';
  $form_string .= '</div>';
  $form_string .= '<div class="new_inner_row">';
  $form_string .= '<input type="submit" class="btn btn_blue" name="submit" value="ثبت"/>';
  $form_string .= '</div>';
  $form_string .= '</fieldset>';
  $form_string .= '</form>';
  echo $form_string;
}

function teacher_class_sort($class_PDO_object) {
  $sorted_class = array();

  while ($class = $class_PDO_object->fetch(PDO::FETCH_ASSOC)) {
    $sorted_class[$class['class_id']]['gym_id']      = $class['gym_id'];
    $sorted_class[$class['class_id']]['gym_name']    = $class['gym_name'];
    $sorted_class[$class['class_id']]['gym_address'] = $class['gym_address'];
    $sorted_class[$class['class_id']]['gym_phone']   = $class['gym_phone'];
    if (isset($class['field'])) {
      $sorted_class[$class['class_id']]['field'] = $class['field'] . ' ' . $class['branch'];
    }
    $sorted_class[$class['class_id']]['class_id'] = $class['class_id'];
    //        if(is_string($sorted_class[$class['class_id']]['class_day_id'])) unset($sorted_class[$class['class_id']]['class_day_id']);
    $sorted_class[$class['class_id']]['class_day_id'][$class['class_day_id']]['class_day_id'] = $class['class_day_id'];
    $sorted_class[$class['class_id']]['class_day_id'][$class['class_day_id']]['day']          = $class['day'];
    $sorted_class[$class['class_id']]['class_day_id'][$class['class_day_id']]['start_time']   = $class['start_time'];
    $sorted_class[$class['class_id']]['class_day_id'][$class['class_day_id']]['end_time']     = $class['end_time'];
  }

  return $sorted_class;
}

function teacher_class_to_string($sorted_class, $userID = -1) {
  global $week_day;
  global $cUser;

  $output = '';
  foreach ($sorted_class as $class) {
    //    var_dump($class);die;
    $output .= '<div class="box" id="' . $class['class_id'] . '">';
    if (isset($cUser->user_id) && $cUser->user_id == $userID) {
      $output .= '<div class="edit-box">' . /*'<span class="icon edit_icon"></span>*/
        ' <span class="glyphicon glyphicon-remove new_errors remove_class"></span> </div>';
    }
    if (isset($class['field'])) {
      $output .= '<h2> رشته ' . htmlentities($class['field']) . '</h2>';
    }
    $output .= '<h3 class="description blue"> باشگاه ' . htmlentities($class['gym_name']) . '</h3>';
    $output .= '<p class="description blue address">' . htmlentities($class['gym_address']) . '</p>';
    if (isset($class['phone'])) {
      $output .= '<p class="description blue phone">' . htmlentities($class['gym_phone']) . '</p>';
    }
    $output .= '<div class="new_inner_row">';
    // sort class base on week days
    asort($class['class_day_id']);
    foreach ($class['class_day_id'] as $time) {
      //                var_dump($time);
      $output .= '<div class="new_inner_row">';
      $output .= '<span class="tag"> ' . htmlentities($week_day[$time['day']]) . '</span> ';
      $output .= '<span> ساعت </span>';
      $output .= '<span>' . htmlentities($time['start_time']) . ' </span> - ';
      $output .= '<span>' . htmlentities($time['end_time']) . '</span> ';
      $output .= '</div>'; // end of new_inner_row
    }
    $output .= '</div>'; // end of new_inner_row
    $output .= '</div>'; // end of box
    //            var_dump($class);
  }

  return $output;
}

/**
 * @param $columns_to_insert //which columns do we want to insert into
 * @param $values_to_insert  // which values do we want to insert into those
 *                           columns values are associative array key => columns to insert and value =>
 *                           values to insert
 *
 * @return bool|PDOStatement
 * @author Ramin Sadegh nasab
 */
function teacher_class_insert($columns_to_insert, $values_to_insert) {
  $table_name = 'class';
  $query      = insert_query_creator($columns_to_insert, $table_name);
  
  return sql_query_executor($query, $values_to_insert);
}

/**
 * @param $columns_values_to_insert
 *
 * @return bool|PDOStatement
 * @author Ramin Sadegh nasab
 */
function class_day_time_insert($columns_values_to_insert) {
  $table_name = 'class_day_time';
  $query      = insert_query_creator(array_keys($columns_values_to_insert), $table_name);
  
  return sql_query_executor($query, $columns_values_to_insert);
}

/**
 * @param null $class_id
 * @param null $teacher_id : determine that classes of which user will be fetched
 * @param null $limit      :array('offset' => 0,'limit' => 10)
 *
 * @return bool|\PDOStatement
 * @author Ramin Sadegh nasab
 */
function teacher_class_by_classId_teacherID($class_id = NULL, $teacher_id = NULL, $limit = NULL) {

  $columns_values = array();
  $query          = 'SELECT ';
  $query .= '`class`.`id` ,  `class_day_time`.`id` as `class_day_id` ,  `user_id` ,  `gym`.`id` as `gym_id` ,  `gym_name` ';
  $query .= ', `gym_address` ,  `gym_phone` , `class_day_time`.`id` as `class_day_id` ,  `day` ,  `start_time` ';
  $query .= ', `end_time` ,  `field` ,  `branch` ';
  $query .= 'FROM `class` ';
  $query .= 'JOIN `gym` ON `gym`.`id` = `class`.`gym_id` ';
  $query .= 'JOIN `class_day_time` ON `class_day_time`.`class_id` = `class`.`id` ';
  $query .= 'LEFT JOIN `sport` ON `sport`.`id` = `class`.`sport_id` ';
  $query .= 'WHERE ';
  if ($class_id) {
    $query .= '`class`.`id` = :class_id ';
    $columns_values[':class_id'] = $class_id;
  }
  if ($class_id && $teacher_id) {
    $query .= ' AND ';
  }
  if ($teacher_id) {
    $query .= '`class`.`user_id` = :user_id ';
    $columns_values[':user_id'] = $teacher_id;
  }
  $query .= 'ORDER BY  `class`.`created` DESC ';
  if ($limit != NULL && is_array($limit) && !empty($limit)) {
    $query .= 'LIMIT ' . implode(',', array_keys($limit));
    $columns_values = array_merge($columns_values, $limit);
  }
  
  return sql_query_executor($query, $columns_values);
}

function delete_class_by_classID($class_id) {
  $query = 'DELETE FROM `class` WHERE `class`.`id` = :class_id ';
  
  return sql_query_executor($query, array(':class_id' => $class_id));
}

function delete_class_day_time_by_classId($class_id) {
  $query = 'DELETE FROM `class_day_time` WHERE `class_day_time`.`class_id` = :class_id';
  
  return sql_query_executor($query, array(':class_id' => $class_id));
}

function delete_class_day_time_by_classDayTimeId($class_day_id) {
  $query = 'DELETE FROM `class_day_time` WHERE `class_day_time`.`id` = :class_day_id';
  
  return sql_query_executor($query, array(':class_day_id' => $class_day_id));
}

////////////////////////////// teacher images
function teacher_image_insert($teacherID, $picName) {
  $columns_to_insert = array(
    'teacherID',
    'picName',
  );
  $value             = array(
    ':teacherID' => $teacherID,
    ':picName'   => $picName,
  );
  $table_name        = 'teacherspics';
  $query             = insert_query_creator($columns_to_insert, $table_name);
  
  return sql_query_executor($query, $value);
}

function teacher_image_update($teacherID, $picName, $current_image_name) {
  $columns_to_update = array(
    'teacherID',
    'picName',
    'insertDate',
  );
  $values            = array(
    $teacherID,
    $picName,
    date("Y-m-d H:i:s"),
    $current_image_name,
  );
  $where             = 'picName';
  $table_name        = 'teacherspics';
  $query             = update_query_creator($columns_to_update, $table_name, $where, TRUE, TRUE);
  
  return sql_query_executor($query, $values, TRUE);
  //        echo json_encode($query);
}

function teacher_image_count($teacherID) {
  $query = 'SELECT count(*) FROM `teacherspics` ';
  $query .= 'WHERE teacherID = :teacherID ';
  $value = array(':teacherID' => $teacherID);
  $count = sql_query_executor($query, $value)->fetch(PDO::FETCH_NUM);
  
  return $count[0];
}

function teacher_image_upload($userID, $location_thumb, $image_name, $file_name, $action) {
  global $db;
  $img_width_large  = 640;
  $img_height_large = 480;
  $img_width_thumb  = 200;
  $img_height_thumb = 200;
  $name_thumb       = time() . '_' . $userID . '_thumb.jpg';
  if ($action == 'update') {
    $current_image_name = $image_name;
    //first delete both thumb and large files
    $large    = change_thumb_to_large($location_thumb . $current_image_name);
    $new_larg = change_thumb_to_large($location_thumb . $name_thumb);
    $db->beginTransaction();
    $update_pic = teacher_image_update($userID, $name_thumb, $current_image_name);
    // then insert new pics in thumb and large size
    // then upload image
    if ($update_pic) {
      $upload_img_large = image_upload_to_directory($new_larg['dirname'], $new_larg['basename'], $file_name, $img_width_large, $img_height_large);
      $upload_img_thumb = image_copy_resize($new_larg['dirname'] . '/' . $new_larg['basename'], $location_thumb . $name_thumb, $img_width_thumb, $img_height_thumb);
      if ($upload_img_thumb !== FALSE && $upload_img_large !== FALSE) {
        $db->commit();
        @unlink($location_thumb . $current_image_name);
        @unlink($large['dirname'] . '/' . $large['basename']);
        
        return '/teachers_pics/thumbs/' . $name_thumb;
      }
      else {
        $db->rollBack();
        if ($upload_img_thumb !== FALSE) {
          @unlink($location_thumb . $current_image_name);
        }
        if ($upload_img_large !== FALSE) {
          @unlink($large['dirname'] . '/' . $large['basename']);
        }
      }
    }
    
    return FALSE;
  }
  elseif ($action == 'insert') {
    $db->beginTransaction();
    // first insert picName in DB
    if (teacher_image_insert($userID, $name_thumb)) {
      // then upload image
      $location_large = str_replace('thumbs', 'larges', $location_thumb);
      $name_large     = str_replace('thumb', 'large', $name_thumb);
      // first upload large (640, 480)
      $upload_img_large = image_upload_to_directory($location_large, $name_large, $file_name, $img_width_large, $img_height_large);
      // then copy larges to thumb directory and resize it to 200, 200 .
      $upload_img_thumb = image_copy_resize($location_large . $name_large, $location_thumb . $name_thumb, $img_width_thumb, $img_height_thumb);
      if ($upload_img_thumb !== FALSE && $upload_img_large !== FALSE) {
        $db->commit();
        
        return '/teachers_pics/thumbs/' . $name_thumb;
      }
      else {
        if ($upload_img_thumb !== FALSE) {
          @unlink($location_thumb . $name_thumb);
        }
        if ($upload_img_large !== FALSE) {
          @unlink($location_large . $name_large);
        }
        $db->rollBack();
      }
    }
  }
  
  return FALSE;
}

function teacher_image_select($teacherID, $limit = 8) {
  $query = 'SELECT `picName` FROM `teacherspics` ';
  $query .= 'WHERE teacherID = :teacherID ';
  $query .= 'LIMIT :limit';
  $value = array(':teacherID' => $teacherID, ':limit' => $limit);
  
  return sql_query_executor($query, $value);
}

function user_wall_images($uid) {
  $query = 'SELECT * FROM ';
  $query .= '`wall` w ';
  $query .= 'INNER JOIN `wallpic` p ON w.postID = p.postID ';
  $query .= 'WHERE w.inserterid = :inserterID ';
  $query .= 'ORDER BY w.insertdate DESC ';
  $parameters = array(
    ':inserterID' => $uid,
  );

  return sql_query_executor($query, $parameters);
}

////////////////////////////// class
////////////////////////////// student confirmations and updates
function student_conf_deny($studentID, $teacherID, $confirm = TRUE) {
  //  if ($confirm == 'false') {
  //    $confirm = 0;
  //  }
  //  elseif ($confirm == 'true') {
  //    $confirm = 1;
  //  }
  $values_to_update = array(
    'is_confirm' => $confirm,
    'updateDate' => strftime("%Y-%m-%d %H:%M:%S", time()),
    'userID'     => $studentID,
    'superiorID' => $teacherID,
  );
  $table_name       = 'infotable';
  $where            = array(
    'userID'     => $studentID,
    'superiorID' => $teacherID,
  );
  $query            = new_update_query_creator($values_to_update, $table_name, $where);
  
  return sql_none_select_query_executor($query);
}

function student_change_teacher($studentID, $old_teacher, $new_teacher) {
  //update where student ID = :studentID and teacherID = teacherID
  $column_to_update = array(
    'superiorID',
    'is_confirm',
    'updateDate',
  );
  $table_name       = 'infotable';
  $where            = array(
    'userID',
    'superiorID',
  );
  $values           = array(
    $new_teacher,
    NULL,
    NULL,
    $studentID,
    $old_teacher,
  );
  $query            = update_query_creator($column_to_update, $table_name, $where, TRUE, TRUE);
  $result           = sql_query_executor($query, $values, TRUE);
  
  return $result->rowCount();
}

////////////////////////////// profile updates
function user_update($columns_values, $where) {
  $table_name = 'user';
  $query      = new_update_query_creator($columns_values, $table_name, $where);

  return sql_none_select_query_executor($query);
}

function profile_image_update($userID, $picName) {
  $columns_to_update = array(
    'picUrl',
  );
  $value             = array(
    ':userID' => $userID,
    ':picUrl' => $picName,
  );
  $table_name        = 'userpic';
  $where             = 'userID';
  $query             = update_query_creator($columns_to_update, $table_name, $where);
  
  return sql_query_executor($query, $value);
}

function address_update($userID, $mobile, $phone, $address) {
  $columns_to_update = array(
    'mobile',
    'phone',
    'address',
  );
  $values            = array(
    ':mobile'  => $mobile,
    ':phone'   => $phone,
    ':address' => $address,
    ':userID'  => $userID,
  );
  $table_name        = 'address';
  $where             = 'userID';
  $query             = update_query_creator($columns_to_update, $table_name, $where);
  
  return sql_query_executor($query, $values);
}

function user_pass_update($uid, $username, $email, $oldPassword = NULL, $newPassword = NULL, $rePassword = NULL) {
  global $errors;
  if ($oldPassword && $newPassword) {
    // give password from database
    $existing_password = password_by_userID($uid)->fetch(PDO::FETCH_NUM);
    // check to see two password are same??
    if (!password_check($oldPassword, $existing_password[0])) {
      $errors[] = 'شما مجاز به انجام این کار نمی باشید';
      
      return FALSE;
    }
    elseif (!two_entry_equality($newPassword, $rePassword)) {
      $errors[] = 'رمز عبور جدید و تکرار آن یکی نمی باشند';
      
      return FALSE;
    }
    $newPassword = password_encrypt($newPassword);
    //            two_entry_equality();
    $columns_to_update = array(
      'username',
      'password',
      'email',
    );
    $values            = array(
      ':username' => $username,
      ':password' => $newPassword,
      ':email'    => $email,
      ':uid'      => $uid,
    );
  }
  else {
    $columns_to_update = array(
      'username',
      'email',
    );
    $values            = array(
      ':username' => $username,
      ':email'    => $email,
      ':uid'      => $uid,
    );
  }
  $table_name = 'user_pass';
  $where      = 'uid';
  $query      = update_query_creator($columns_to_update, $table_name, $where);
  
  return sql_query_executor($query, $values);
}

function infotable_update($columns_values, $where) {
  //        $columns = array_keys($columns_values);
  $table_name = 'infotable';
  //        $where_keys = array_keys($where);
  //        $where_in_query = $where_keys[0];
  /*TODO : where clause must be flexible for every model of update in future. */
  //        $query = update_query_creator($columns, $table_name, $where_in_query);
  $query = new_update_query_creator($columns_values, $table_name, $where);
  //        echo $query;die();
  //        $columns_values = array_merge($columns_values,$where);
  return sql_none_select_query_executor($query);
}

function job_update($columns_values, $where) {
  /*
         * @columns_values (array) array(column => value)
         * @where
         * */
  $table_name = 'job';
  //        $columns = array_keys($columns_values);
  //        $where_for_query = array_keys($where);
  $query = new_update_query_creator($columns_values, $table_name, $where);
  
  return sql_none_select_query_executor($query);
}

function award_updates($columns_values, $where) {
  /*
         * @columns_values (array) array(column => value)
         * @where
         * */
  $table_name = 'award';
  $columns    = array_keys($columns_values);
  //where awdID = :awdID and userID = :userID
  $where_for_query = array_keys($where);
  $query           = update_query_creator($columns, $table_name, $where_for_query);
  $columns_values  = array_merge($columns_values, $where);
  
  return sql_query_executor($query, $columns_values);
}

function user_update_dynamic($columns_values, $where) {
  $table_name = 'user';
  $query      = new_update_query_creator($columns_values, $table_name, $where);
  //echo json_encode($query);
  //  die;
  return sql_none_select_query_executor($query);
}

////////////////////////////// insert
function award_insert($columns_values) {
  /*
         * insert into award table
         * */
  $columns_to_insert = array_keys($columns_values);
  $table_name        = 'award';
  $query             = insert_query_creator($columns_to_insert, $table_name);
  
  return sql_query_executor($query, $columns_values);
}

////////////////////////////// delete
function award_delete($columns_values) {
  $values = array();
  $where  = array_keys($columns_values);
  $query  = delete_query_creator($where, 'award');
  //        var_dump($columns_values);
  //        return $query;
  foreach ($columns_values as $column => $value) {
    $values[':' . $column] = $value;
  }
  //        return $query;
  //        return $values;
  return sql_query_executor($query, $values);
}

///////////////////////////// images
/**
 * @param $location
 * @param $name
 * @param $input_name
 * @param $img_width
 * @param $img_height
 *
 * @return bool
 */
function image_upload_to_directory($location, $name, $input_name, $img_width = NULL, $img_height = NULL) {
  $tmp_loc = $_FILES[$input_name]['tmp_name'];
  $path    = $location . '/' . $name;
  if (!move_uploaded_file($tmp_loc, $path)) {
    $name = FALSE;
  }
  if ($img_height || $img_width) {
    if ($name != FALSE) {
      if (!@resize_image($path, $img_width, $img_height)) {
        $name = FALSE;
      }
    }
  }
  
  return $name;
}

function image_copy_resize($image, $destination, $width, $height) {
  if (copy($image, $destination)) {
    return resize_image($destination, $width, $height);
  }
  
  return FALSE;
}

/**
 * Get image type (for resize_image()) <br/>
 * Detect type and process accordingly
 *
 * @param      $file
 * @param bool $ext
 *
 * @return bool|resource|string
 */
function open_image($file, $ext = FALSE) {
  $size = getimagesize($file);
  switch ($size["mime"]) {
    case "image/jpeg":
      $image     = imagecreatefromjpeg($file); //jpeg file
      $extension = '.jpg';
      break;
    /*case "image/gif":
                $im = imagecreatefromgif($file); //gif file
                break;*/
    case "image/png":
      $image     = imagecreatefrompng($file); //png file
      $extension = '.png';
      break;
    default:
      $image = FALSE;
      break;
  }
  if ($ext) {
    return $extension;
  }
  else {
    return $image;
  }
}

/**
 * Resizing given image
 *
 * @param $image
 * @param $width
 * @param $height
 *
 * @return bool
 */
function resize_image($image, $width, $height) {
  $image_size   = getimagesize($image);
  $image_width  = $image_size[0];
  $image_height = $image_size[1];
  //    if($image_width < 640) $new_width = $image_size[0]; else $new_width = 500;
  //    if($image_height < 500) $new_height = $image_size[1]; else $new_height = 500;
  /* facebook resolution */
  //    $new_width = $image_width < 960 ? $image_size[0] : 960;
  //    $new_height= $image_height < 720 ? $image_size[1] : 720;
  $new_width  = $image_width < $width ? $image_size[0] : $width;
  $new_height = $image_height < $height ? $image_size[1] : $height;
  //    $new_width = $image_width < 640 ? $image_size[0] * (640 / $image_size[0]) : 640;
  //    $new_height = $image_height < 480 ? $image_size[1] * (480 / $image_size[1]) : 480;
  $new_image = imagecreatetruecolor($new_width, $new_height); //Create our new image
  $old_image = open_image($image);
  imagecopyresized($new_image, $old_image, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height);
  
  return imagejpeg($new_image, $image); // true or false
}

///////////////////////////// profile images
function check_profile_image_existence($userID) {
  $image_exists = 'SELECT `picurl` ';
  $image_exists .= 'FROM `userpic` ';
  $image_exists .= 'WHERE `userpic`.`userID` = :userID';
  $parameters = array(':userID' => $userID);
  $result     = sql_query_executor($image_exists, $parameters)->fetch(PDO::FETCH_NUM);
  
  return $result[0];
}

function profile_image_insert($userID, $picName) {
  $columns_to_insert = array(
    'userID',
    'picUrl',
    'created',
  );
  $value             = array(
    ':userID'  => $userID,
    ':picUrl'  => $picName,
    ':created' => strftime("%Y-%m-%d %H:%M:%S", time()),
  );
  $table_name        = 'userpic';
  $query             = insert_query_creator($columns_to_insert, $table_name);
  
  return sql_query_executor($query, $value);
}

/**
 * @param $userID
 * @param $location
 * @param $image_name
 * @param $file_name
 * @param $action
 *
 * @return bool
 */
function profile_image_upload($userID, $location, $image_name, $file_name, $action) {
  global $cUser;

  $img_width  = 200;
  $img_height = 200;
  $name       = time() . '_' . $image_name . '.jpg';
  if ($action == 'update') {
    $image_name = basename($_SESSION['picurl']);
    // first delete current image
    // then upload image
    $update_profile_db = profile_image_update($cUser->user_id, $name);
    if ($update_profile_db) {
      $upload_img = image_upload_to_directory($location, $name, $file_name, $img_width, $img_height);
      if ($upload_img !== FALSE) {
        // then update picurl in DB
        $_SESSION['picurl'] = DS . 'profilePics' . DS . $name;
        //return profile_image_update($userID, $upload_img);
      }
      unlink($location . DS . $image_name);
      
      return TRUE;
    }
  }
  elseif ($action == 'insert') {
    // first upload image
    $upload_img = image_upload_to_directory($location, $name, $file_name, $img_width, $img_height);
    if ($upload_img !== FALSE) {
      // then update picurl in DB
      if (profile_image_insert($userID, $upload_img)) {
        $_SESSION['picurl'] = '/profilePics/' . $name;
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }
  
  return FALSE;
}

/////////////////////////////
function all_categories() {
  global $db;
  $query  = 'SELECT `id`, `name` FROM `category`';
  $result = $db->query($query);
  
  return $result;
}

/*function echo_category_for_select_element($PDOObject){
        $output = '';
        while($row = $PDOObject->fetch(PDO::FETCH_ASSOC)) {
            $output .= '<option value="'.$row['catID'].'">'.$row['catName'].'</option>';
        }
        return $output;
    }*/
///////////////////////////// user_pass
function password_by_userID($userID) {
  $getPassQ = 'SELECT `password` FROM `user_pass` ';
  $getPassQ .= 'WHERE `uid` = :uid ';
  $getPassQ .= 'LIMIT 1';
  $parameters = array(':uid' => $userID);
  
  return sql_query_executor($getPassQ, $parameters);
}

function username_email_existance($username = NULL, $email = NULL) {
  $values = array();
  $query  = 'SELECT  EXISTS ( ';
  $query .= 'SELECT 1 FROM `user_pass` ';
  $query .= 'WHERE ';
  if ($username) {
    $query .= 'username = :username ';
    $values[':username'] = $username;
  }
  if ($username && $email) {
    $query .= ' OR ';
  }
  if ($email) {
    $query .= 'email = :email ';
    $values[':email'] = $email;
  }
  $query .= 'LIMIT 1)';
  //        echo $query;
  //        $values = array(':username' => $username,':email' => $email);
  $result = sql_query_executor($query, $values)->fetch(PDO::FETCH_NUM);
  
  return array_shift($result);
}

//////////////////////////// state and place
function sport_place_existence($sportID, $stateID) {
  /* this function gives sportID and stateID and return 1 if the sport exists in that state or 0 if not
         * @sportID : int, string
         * @stateID : int, string
         * return : boolean
        */
  $columns_values = array(
    ':stateID' => $stateID,
    ':sportID' => $sportID,
  );
  $query          = 'SELECT  EXISTS ( ';
  $query .= 'SELECT 1 FROM `wall`.`place` ';
  $query .= 'WHERE `place`.`stateID` = :stateID AND `place`.sportID = :sportID ';
  $query .= 'LIMIT 1 )';
  
  return sql_query_executor($query, $columns_values)->fetch(PDO::FETCH_NUM);
}

function place_delete($where) {
  /* delete on record from place ID
         * where */
  return sql_none_select_query_executor(delete_query_creator_new($where, 'place'));
}

function place_insert($columns_values) {
  $insert_columns_values = array();
  foreach ($columns_values as $keys => $values) {
    $insert_columns_values[$keys] = $values;
  }
  $query = insert_query_creator_new($insert_columns_values, 'place');
  echo $query;

  return sql_none_select_query_executor($query);
}

////////////////////////////
function select_posts($columns_and_values = array()) {
  global $limit;
  global $page;
  $values = array();
  $limit  = $limit ? $limit : 15;
  $offset = $page > 0 ? ($page - 1) * $limit : 0;
  $query  = 'SELECT `wall`.`postID`,  `wall`.`inserterID` ,  `wall`.`title` ,  `wall`.`insertDate` ,  `wall`.`editDate` , ';
  $query .= '`wall`.`context`, `wall`.`catID`';
  $query .= ', `user`.`last_name`, `user`.`first_name` ';
  //  $query .= ', `cat`.`name` ';
  $query .= ', `pic`.`picName` ';
  //          $query .= ', foerID ';
  $query .= ', `picurl` ';
  //  $query  = 'SELECT * ';
  $query .= 'FROM `wall` ';
  $query .= 'JOIN `user` ON `user`.`userID` = `wall`.`inserterID` ';
  //  $query .= 'LEFT JOIN `postcategory` `cat` ON `cat`.`catID` = `wall`.`catID` ';
  $query .= 'LEFT JOIN `wallpic` `pic` ON  `pic`.`postID` = `wall`.`postID` ';
  $query .= 'LEFT JOIN `userpic` ON `userpic`.`userID` = `wall`.`inserterID` ';
  //        $query .= 'LEFT JOIN `follow` ON `follow`.`foingID` = `wall`.`inserterID` ';
  if (!empty($columns_and_values)) {
    foreach ($columns_and_values as $key => $value) {
      if ($value == NULL || $value == 'all') {
        continue;
      }
      preg_match('/:\w+/', $key, $matches);
      $values[$matches[0]] = $value;
    }
    $conditions = array_keys($columns_and_values);
    $qb         = '';
    $qb .= implode(' ', $conditions);
    if (!empty($qb)) {
      $query .= ' WHERE ' . $qb;
    }
  }
  //        $query .= ' GROUP BY (wall.postID) ';
  $query .= ' ORDER BY ';
  //        $query .= '(`follow`.`foerID` IN (71)) DESC ,';
  $query .= '  `wall`.`insertDate` DESC ';
  $query .= ' LIMIT :offset, :limit ';
  $values[':limit']  = $limit;
  $values[':offset'] = $offset;
  //  var_dump($values);
  //  echo($query);die();
  return sql_query_executor($query, $values);
}

function post_by_catID($cat_id) {
  //  global $fpdo;
  $tags = Tag::nid_by_cat_ids($cat_id);
  $nid  = array_map(function ($tag) { return $tag->entity_id; }, $tags);
  //  var_dump($nid);
  //  die();
  return posts_by_id($nid);
}

function post_by_sportID($sportID, $limit = 5, $page = 1) {
  $columns_and_values = array(
    ' `wall`.`sportID` = :sportID ' => $sportID,
  );
  
  return select_posts($columns_and_values, $limit, $page);
}

function post_by_user_id($userID) {
  $columns_and_values = array(
    ' `wall`.`inserterID` = :userID ' => $userID,
  );
  
  return select_posts($columns_and_values);
}

function post_by_postID($post_id) {
  $columns_and_values = array(
    ' `wall`.`postID` = :postID ' => $post_id,
  );
  
  return select_posts($columns_and_values);
}

function posts_by_id($pids) {
  /*
  global $fpdo;
  $posts = $fpdo
    ->from('wall w')
    ->select(NULL)
    ->select('*')
    ->innerJoin('user u ON u.userID = w.inserterid')
    ->leftJoin('wallpic wp ON wp.postid = w.postid')
    ->leftJoin('userpic p ON p.userid = u.userid')
    ->where('w.postid',$pids)
    ->limit($limit)
    ->offset($offset)
    ->execute();
//    ->getQuery();
  */
  //  /*
  global $limit;
  global $page;
  $offset = $page > 0 ? ($page - 1) * $limit : 0;
  $query  = 'SELECT * ';
  $query .= 'FROM wall ';
  $query .= 'INNER JOIN user ON user.userID = wall.inserterID ';
  $query .= 'LEFT JOIN wallpic ON wallpic.postID = wall.postID ';
  $query .= 'LEFT JOIN userpic ON userpic.userID = user.userID ';
  $query .= 'WHERE ';
  $query .= 'wall.postID IN (' . implode(',', array_map(function ($id) {
      global $db;

      return $db->quote($id);
    }, $pids)) . ')';
  $query .= 'limit :offset, :limit';
  //  */
  //  echo $posts->getQuery();die;
  //    $posts = $posts ->execute();
  //  var_dump($posts->fetchAll(PDO::FETCH_ASSOC));die;
  //  var_dump(sql_query_executor($query)->fetch(PDO::FETCH_ASSOC));die;
  //    echo $query;die;
  return sql_query_executor($query, array(
    ':limit'  => $limit,
    ':offset' => $offset,
  ));
}

function user_posts_pics($user_id) {
  global $limit;
  global $page;
  $offset = $page > 0 ? ($page - 1) * $limit : 0;
  $query  = 'SELECT * FROM ';
  $query .= '`wall` ';
  $query .= 'INNER JOIN `wallpic` ON `wall`.`postid` = `wallpic`.`postid` ';
  $query .= 'WHERE `wall`.`inserterID` = :inserterID ';
  $query .= 'ORDER BY `wall`.`insertdate` DESC ';
  $parameters = array(':inserterID' => $user_id);
  
  return sql_query_executor($query, $parameters);
}

function post_to_string($PDOObject, $view_mode = 'teaser') {
  global $cUser;

  $output           = '';
  $pic_loc          = '/profilePics/';
  $default_user_pic = '/_css/img/user-32.png';
  while ($post = $PDOObject->fetch()) {
    // RAMIN:FIXME fix this part for cat :|
    $post_id  = $post['postID'] != NULL ? $post['postID'] : $post[0];
    $user_pic = $post['picurl'] ? $pic_loc . $post['picurl'] : $default_user_pic;
    $output .= '<div class="news_part box_shadow" id="' . $post_id . '" >';
    $output .= '<div class="news_infos">';
    $output .= '<img class="img_profile news_circle" src="' . $user_pic . '">';
    $output .= '<div class="news_timeline_name">';
    $output .= '<a data-rest-url = "min-prof/' . $post['inserterID'] . '" class="profile_load" href="/' . $post['inserterID'] . '">' . htmlentities($post['first_name']) . ' ' . htmlentities($post['last_name']) . '</a>';
    $output .= '</div>'; // news_timeline_name
    $output .= '<div class="news_timeline_date">';
    $output .= '<a href="/post/' . htmlentities($post_id) . '">';
    if ($post['editDate'] == NULL) {
      $output .= 'تاریخ ثبت ' . '<span class="date_time">' . r_gregorian_to_jalali($post['insertDate']) . '</span>';
    }
    else {
      $output .= 'ویرایش در' . '<span class="date_time">' . r_gregorian_to_jalali($post['editDate']) . '</span>';
    }
    $output .= '</a>';
    $output .= '</div>'; // news_timeline_date
    $output .= '<div class="news_timeline_cat">';
    
    $tags = Tag::find_by_entity_id($post_id);
    //        var_dump($tags);die();
    $output .= Tag::tags_link($tags);
    $output .= '</div>';
    
    $output .= '<div class="split_bottom split-gray"></div>';
    $output .= '</div>'; // news_infos
    if ($commenter = isset($cUser->user_id)) {
      if ($post['inserterID'] == $cUser->user_id) {
        $output .= '<ul class="editable">';
        $output .= '<li><span class="glyphicon glyphicon-chevron-down"></span></li>';
        $output .= '<li class="postEdit">ویرایش</li>';
        $output .= '<li class="postDelete">حذف کامل</li>';
        $output .= '<li class="postDone">اعمال تغییرات </li>';
        $output .= '</ul>';
      }
    }
    $context = $post['context'];
    $class   = $rdmb = NULL;
    if ($view_mode == 'teaser') {
      if (mb_strlen($context, 'utf-8') > 710) {
        $class = 'rdmp';
        $rdmb  = '<button class="rdmb btn btn_blue"> ادامه مطلب </button>';
      }
    }
    $output .= "<p class='{$class}'> " . strip_tags(nl2br($context), '<br><b>') . " {$rdmb} </p>";
    if ($post['picName']) {
      $output .= '<div class="news_imgHolder" ><a class="venobox" href="/images/' . htmlentities($post['picName']) . '"><img class="box_shadow" src="/images/' . htmlentities($post['picName']) . '" /></a></div>';
    }
    $output .= post_like_part($post_id, $commenter);
    $output .= post_comment_part($post_id, $commenter, $post['inserterID']);
    $output .= '</div>';
  }
  $output = $output != '' ? '<div class="news_posts">' . $output . '</div>' : '';
  
  return $output;
}

function post_to_string_teaser($pdo_obj) {
  $output  = '';
  $pic_loc = '/images/';
  if ($has_pic = $pdo_obj->rowCount() > 0) {
    $output = '<div class="masonry_container">';
  }
  while ($post = $pdo_obj->fetch(PDO::FETCH_OBJ)) {
    $output .= '<a class="masonry-item" href="/post/' . $post->postID . '">';
    //    $output .= '<div >';
    $output .= "<img src='{$pic_loc}{$post->picName}' >";
    $output .= "<p>" . strip_tags(nl2br(substr($post->context, 0, 155)), '<br><b>') . "...</p>";
    //    $output .= '</div>';
    $output .= '</a>';
  }
  if ($has_pic) {
    $output .= '</div>';
  } // close js-masonry
  return $output;
}

/**
 * @param     $post_id
 * @param     $commenter
 * @param int $liker_show
 *
 * @return string
 * @author Ramin Sadegh nasab
 */
function post_like_part($post_id, $commenter, $liker_show = 3) {
  global $Acl;
  global $cUser;

  $output = $like_close = $like_text = $liker_text = '';
  // Like part
  // create like button for allowed person.
  $output .= '<div class="like-box">'; // open like part
  $like_close = '</div>'; // close like part
  // show Like button to user.
  if ($Acl->check_permission('post_like') && $commenter) {
    $output .= '<span class="bottom-half-border"></span>';
    $liked_by_user = Like::is_liked_by_person($post_id, $cUser->user_id);
    $like_text     = $liked_by_user ? 'Unlike' : 'Like';
    $like_color = $liked_by_user ? 'lightblue' : '';
    $like_icon = "<span class='glyphicon glyphicon-thumbs-up {$like_color}'></span>";
    $like_class = strtolower($like_text);
    $output .= "<a class=\"like_insert {$like_class} \" href=\"post/likes.php\" data-bundle = \"post\">{$like_icon}</a>";
  }
  $like_number = Like::find_likes_on($post_id);
  $persons = Like::find_liker_names($post_id, 'post', array('limit' => $liker_show));
//  var_dump($like_number);
//  var_dump($persons);
  $likers = array();
  $you_like_post = FALSE;
  foreach ($persons as $person) {
    if(isset($cUser->user_id) && $person->userID == $cUser->user_id) {
      $name = 'شما ';
      $you_like_post = TRUE;
    }
    else {
      $name = full_name($person->first_name, $person->last_name);
    }

    $likers[] = '<a class="lightblue" href="/' . $person->userID . '"> ' . $name . '</a>';
  }

  $output .= implode('و ', $likers);
  if ($like_number - $liker_show >= 1) {
    $output .= ' و';
    $output .= '<a href="javascript:;" class="lightblue">' . ($like_number - $liker_show);
    $output .= 'نفر دیگر';
    $output .= '</a>';
  }

  if ($you_like_post) {
    $output .= 'این مطلب را پسندیده اید ';
  }
  elseif ($like_number == 1 && !$you_like_post) {
    $output .= ' این مطلب را پسندیده';
  }
  elseif($like_number > 1 && !$you_like_post) {
        $output .= ' این مطلب را پسندیده اند';
  }
  else {
    $output .= 'اولین نفری باشید که این مطلب را می پسندد';
  }
  $output .= $like_close; // like-box

  return $output;
}

/**
 * @param     $post_id
 * @param     $commenter
 * @param     $post_owner
 *
 * @return string
 * @author Ramin Sadegh nasab
 */
function post_comment_part($post_id, $commenter, $post_owner) {
  global $cUser;
  $output  = '';
  $confirm = $commenter ? FALSE : 1;
  if ($comments = Comment::find_comments_on($post_id, $confirm)) {
    $output .= '<section class="comments">';
    $output .= Comment::comments_to_string($comments, $post_owner);
    $output .= '</section>'; // comments close
  }
  if (isset($cUser->user_id)) {
    $output .= '<div class="new_cm">';
    $output .= '<img src="' .$cUser->pic . '" >';
    $output .= "<div class='cm_insert new_cm_text' contenteditable='true' data-placeholder='نظر من... ' >نظر من... </div>";
    $output .= '</div>';
  }
  
  return $output;
}

//////////////////////////// sport
function sport_update($columns_values, $where) {
  /*
         * @columns_values (array) array(column => value)
         * @where
         * */
  $table_name      = 'sport';
  $columns         = array_keys($columns_values);
  $where_for_query = array_keys($where);
  $query           = update_query_creator($columns, $table_name, $where_for_query);
  $columns_values  = array_merge($columns_values, $where);
  
  return sql_query_executor($query, $columns_values);
}

/////////////////////////////
function search_people($searchText, $teacherID = FALSE) {
  global $page;
  global $limit;
  
  $offset = $page > 0 ? ($page - 1) * $limit : 0;
  $values = array(
    ':searchText' => '%' . $searchText . '%',
    'offset'      => $offset,
    ':limit'      => $limit,
  );
  /* TODO: use (like and %) for better search */
  $query = "SELECT SQL_CALC_FOUND_ROWS `user`.`userID`, ";
  $query .= "CONCAT_WS(' ', `user`.`first_name`, `user`.`last_name`) AS name, `infotable`.`infoID`, `infotable`.`sportID`, `infotable`.`jobID` ";
  $query .= ' ,`userpic`.`picurl` ';
  $query .= 'FROM `user` ';
  $query .= 'JOIN `infotable` ON `user`.`userID` = `infotable`.`userID` ';
  $query .= 'LEFT JOIN `userpic` ON `user`.`userID` = `userpic`.`userID` ';
  $query .= 'WHERE CONCAT_WS(" ", `user`.first_name, `user`.last_name) LIKE :searchText ';
  if ($teacherID != FALSE) {
    $query .= ' AND superiorID = :superiorID ';
    $values[':superiorID'] = $teacherID;
  }
  $query .= 'ORDER BY name ';
  $query .= 'LIMIT :offset, :limit';
  
  return sql_query_executor($query, $values);
}

function string_people_search($searchObject, $view_mode = 'search') {
  if ($searchObject->rowCount() < 1) {
    return FALSE;
  }
  $output = '';
  if ($view_mode == 'search') {
    $output .= '<div class="dummy-column">';
  }
  $output .= '<h2>افراد</h2>';
  while ($row = $searchObject->fetch(PDO::FETCH_ASSOC)) {
    $output .= '<div class="dummy-media-object" >';
    $user_pic = $row['picurl'] != NULL ? "<img src=\"/profilePics/{$row['picurl']}\" />" : '<img src="/_css/img/user-32.png" />';
    $output .= $user_pic;
    $output .= '<h3><a href="/' . $row['userID'] . '">' . htmlentities($row['name']) . '</a></h3>';
    $output .= '</div>';
  }
  if ($view_mode == 'search') {
    $output .= '</div>';
  }
  
  return $output;
}

function search_post($searchText) {
  global $limit;
  global $page;
  $offset = $page > 0 ? ($page - 1) * $limit : 0;
  $query  = 'SELECT SQL_CALC_FOUND_ROWS `post`.`id` , `context`, `created` ';
  $query .= 'FROM `post` ';
  $query .= 'WHERE `context` LIKE :searchText OR `title` LIKE :searchText '; //search on title and context
  $query .= 'ORDER BY `created` DESC ';
  $query .= 'LIMIT :offset, :limit';
  $values = array(
    ':searchText' => '%' . $searchText . '%',
    ':limit'      => $limit,
    ':offset'     => $offset,
  );
  $posts  = sql_query_executor($query, $values);
  
  return $posts;
}

function string_post_search($searchObject, $view_mode = 'search') {
  if ($searchObject->rowCount() < 1) {
    return FALSE;
  }
  $output = '';
  if ($view_mode == 'search') {
    $output = '<div class="dummy-column dummy-column-medium">';
  }
  $output .= '<h2>مطالب</h2>';
  while ($post = $searchObject->fetch(PDO::FETCH_ASSOC)) {
    $length  = mb_strlen($post['context'], 'utf-8');
    $length  = $length > 30 ? 30 : $length;
    $context = mb_substr($post['context'], 0, $length, 'utf-8');
    $output .= '<div class="dummy-media-object">';
    $output .= '<h3>';
    $output .= '<span class="glyphicon glyphicon-list-alt"></span> <a href="/post/' . htmlentities($post['id']) . '">' . $context . ' ... </a> ';
    $output .= '</h3>';
    $output .= '<span class="fl"> <span class="date_time">' . htmlentities(r_gregorian_to_jalali($post['created'])) . '</span></span>';
    $output .= '</div>';
  }
  if ($view_mode == 'search') {
    $output .= '</div>';
  }
  
  return $output;
}

/////////////////////////////
function find_person_by_id($userID) {
  $values       = array('userID' => $userID);
  $personSelect = 'SELECT `user`.`userID`, `first_name`, `last_name`, `birth_date`, `foot`, `weight`, `length`, `description`, `sex` ';
  $personSelect .= ',`picurl`, `statename`, `countyname`, `national_code`, degreeID ';
  $personSelect .= ', `username`, `email`, `mobile`, `phone`, `address`,`beltID` ';
  $personSelect .= ', `has_class`, `infotable`.`infoID` ';
  $personSelect .= ', `state`.`stateID` ';
  $personSelect .= 'FROM `user` ';
  $personSelect .= 'JOIN `infotable` ON `user`.`userID` = `infotable`.`userID` ';
  $personSelect .= 'LEFT JOIN `userpic` ON `user`.`userID` = `userpic`.`userID` ';
  $personSelect .= 'LEFT JOIN `address` ON `address`.`userID` = `user`.`userID` ';
  $personSelect .= 'JOIN `state` ON `user`.`stateID` = `state`.`stateID` ';
  $personSelect .= 'LEFT JOIN `user_pass` ON `user_pass`.`uid` = `user`.`userID` ';
  $personSelect .= 'WHERE `user`.`userID` = :userID ';
  $personSelect .= 'LIMIT 1';
  $person = sql_query_executor($personSelect, $values);
  
  return $person->fetch(PDO::FETCH_ASSOC);
}

function find_person_info_by_info_id($conditions, $limit = 1, $page = 1) {
  /*
         * conditions must be something like this => array('userID' => 2, 'AND beltID ' => 4 );
         * turn this condition to this => userID = :userID AND beltID = :beltID
         * and add condition value to array values => $values[':userID'] = 2;$values[':beltID'] = 4;
         * */
  $offset = ($page - 1) * $limit;
  $values = array(
    ':limit'  => $limit,
    ':offset' => $offset,
  );
  $query  = 'SELECT * ';
  $query .= 'FROM `infotable` ';
  $query .= 'WHERE ';
  foreach ($conditions as $logic_operation => $condition) {
    $key                = preg_replace('/[\s]|AND|OR/', '', $logic_operation);
    $values[':' . $key] = $condition;
    $query .= $logic_operation . ' = ' . ':' . $key . ' ';
  }
  $query .= 'LIMIT  :offset, :limit';
  //        var_dump($values);
  //        echo $query;
  return sql_query_executor($query, $values);
  
}

function sports_by_user_id($userID) {
  $sorted_sports = array();
  $query         = 'SELECT CONCAT_WS(" ",`field`,`branch`) as `sport_name`, `sport`.`id` as `sport_id`';
  $query .= 'FROM `infotable` ';
  $query .= 'JOIN `sport` ON `sport`.`id` = `infotable`.`sportID` ';
  $query .= 'WHERE `infotable`.`userID` = :userID';
  $values = array(':userID' => $userID);
  $sports = sql_query_executor($query, $values)->fetchAll(PDO::FETCH_ASSOC);
  foreach ($sports as $sport) {
    $sorted_sports[$sport['sport_id']] = $sport['sport_name'];
  };
  
  return $sorted_sports;
}

/**
 * This function find a teacher students base on entered parameters
 *
 * @author Ramin Sadegh nasab
 *
 * @param int  $superiorID       Teacher Id
 * @param bool $confirm_students If this argument is set to false, then find
 *                               all students confirmed and not confirmed else just find not confirmed
 *                               students
 * @param bool $just_new
 * @param bool $studentID        If this argument is set then just find a student
 *
 * @return bool|\PDOStatement
 */
function find_teacher_students($superiorID, $confirm_students = NULL, $just_new = FALSE, $studentID = FALSE) {
  $values = array();
  // pagination part
  global $limit;
  global $page;
  pagination_variables('GET');
  $query = 'SELECT  SQL_CALC_FOUND_ROWS infoid, `user`.`userID`, `first_name`, `last_name`, `birth_date`, `weight`, `length`';
  $query .= ', `picurl`, `statename`, `countyname`, `updateDate`, `infotable`.`is_confirm` ';
  $query .= 'FROM `user` ';
  $query .= 'LEFT JOIN `userpic` ON `user`.`userID` = `userpic`.`userID` ';
  $query .= 'JOIN `state` ON `user`.`stateID` = `state`.`stateID` ';
  $query .= 'JOIN `infotable` ON `user`.`userID` = `infotable`.`userID` ';
  $query .= 'WHERE `infotable`.`superiorID` = :superiorID ';
  $values[':superiorID'] = $superiorID;
  if ($studentID) { // if $studentID is set
    $query .= 'AND `infotable`.`userID` = :studentID ';
    $values[':studentID'] = $studentID;
  }
  if ($confirm_students !== NULL) // just show unconfirmed students of a teacher
  {
    $query .= 'AND `infotable`.`is_confirm` = :is_confirm ';
    $values[':is_confirm'] = $confirm_students;
  }
  if ($just_new) // just show new students that never seen and updated by teacher
  {
    $query .= 'AND `infotable`.`updateDate` IS NULL ';
  }
  // if {updateDate in null it comes first}, else {order rows on updateDate DESC (last updated row comes first)}
  $query .= ' ORDER BY CASE WHEN updateDate IS NULL THEN 0 ELSE 1 END, updateDate DESC,  `infotable`.insertDate DESC ';
  if ($studentID) // if we have studentID its better to limit the number of result
  {
    $query .= 'LIMIT 1';
  }
  elseif ($limit !== FALSE && $page !== FALSE) { // elseif we have limit and offset we have limit the results
    $offset = $page > 0 ? $limit * ($page - 1) : 0;
    $query .= 'LIMIT :offset, :limit';
    $values[':offset'] = $offset;
    $values[':limit']  = $limit;
  }
  //  var_dump($values);
  //  echo $query;
  //  die();
  $person = sql_query_executor($query, $values);
  
  return $person;
}

/**
 * This function give $students object for a teacher return a string for HTML
 * view
 *
 * @author Ramin Sadegh nasab
 *
 * @param object $students
 *
 * @return bool|string
 */
function string_teacher_students($students) {
  global $translate;
  if (!is_object($students)) {
    return FALSE;
  }
  if ($students->rowCount() == 0) {
    return FALSE;
  }
  $result = '<div class="studentCer">';
  while ($student = $students->fetch(PDO::FETCH_ASSOC)) {
    //            var_dump($student);
    $new = (!$student['updateDate']) ? $new = 'new_evt' : '';
    $result .= '<div class="Cnoti box_shadow ' . $new . '" id="' . $student['userID'] . '" data-dest="student_confirm.php">';
    $result .= '<div class="CnotiHeader"><span class="glyphicon glyphicon-flag"></span></div>';
    $result .= '<div class="CnotiBody padding">درخواست شاگردی ';
    $result .= "<a href = '/{$student['userID']}'>" . htmlentities($student['first_name']) . ' ' . ($student['last_name']) . "</a>";
    //        $result .= '</div>';
    $result .= '<ul class="edit-box"> ';
    $result .= '<li><a class="btn btn_orange" href="/setting/sports/' . $student['userID'] . '/' . $student['infoid'] . '/honors" > <span class="glyphicon glyphicon-edit"></span> ویرایش</a></li>';
    $result .= '<li><a  class="btn btn_orange conf_detail" href="javascript:;" >  <span class="glyphicon glyphicon-list"></span>  جزییات </a></li>';
    if (!$student['is_confirm']) {
      $result .= '<li><a class="btn btn_orange conf conf_accept" href="javascript:;" >  <span class="glyphicon glyphicon-ok"></span>  تایید </a></li>';
    }
    else {
      $result .= '<li><a class="btn btn_orange conf conf_deny" href="javascript:;" > <span class="glyphicon glyphicon-remove-sign"></span>  رد </a></li>';
    }
    $result .= '</ul>';
    $result .= '</div>';
    $wanted_value = array(
      'first_name',
      'last_name',
      'birth_date',
      'countyname',
      'statename',
      'weight',
      'length',
    );
    $result .= '<div class="CnotiDetails">';
    foreach ($student as $key => $info) { // create each part of user information
      if (!in_array($key, $wanted_value) || !$info) {
        continue;
      }
      if ($key == 'birth_date') {
        $info = '<span class="date_time">' . r_gregorian_to_jalali($info) . '</span>';
      } // change birth_date from gregorian to jalali
      if (array_key_exists(strtolower($key), $translate)) {
        $key = $translate[$key];
      }
      else {
        $key = 'فیلد';
      }
      $result .= '<div class="part"><label>' . $key . ':</label>' . $info . '</div>';
    }
    $result .= '</div>'; // end CnotiDetails
    $result .= '</div>'; // end Cnoti
  }
  $result .= '</div>'; // end studentCer
  return $result;
}

function find_person_award($userID, $pagination = array()) {
  global $page;
  global $limit;
  pagination_variables('get');
  $offset = $page > 0 ? ($page - 1) * $limit : 0;
  $values = array(':userID' => $userID);
  $award  = 'SELECT SQL_CALC_FOUND_ROWS awdID, infotable.userID, infotable.sportID, infotable.infoID ';
  $award .= ', sport.field, sport.branch, awdMedal, `user`.first_name, `user`.last_name ';
  $award .= ', award.awdName, superiorID, award.awdDescription, `belt`.`beltName` ';
  $award .= 'FROM infotable ';
  $award .= 'LEFT JOIN award ON `award`.userID = `infotable`.userID AND `award`.sportID = `infotable`.sportID ';
  $award .= 'LEFT JOIN sport ON `infotable`.sportID = sport.id ';
  $award .= 'LEFT JOIN  `user` ON `infotable`.superiorID = `user`.userID ';
  $award .= 'LEFT JOIN  `belt` ON  `belt`.`beltID` = `infotable`.`beltID` ';
  $award .= 'WHERE `infotable`.userID = :userID AND is_confirm =1 ';
  $award .= 'ORDER BY `awdDate` DESC ';
  if (!empty($pagination)) {
    $award .= 'LIMIT :offset, :limit ;';
    foreach ($pagination as $key => $value) {
      $$key = $value;
    }
    $values[':offset'] = $offset;
    $values[':limit']  = $limit;
  }
  //        echo $award;
  //        var_dump($values);
  return sql_query_executor($award, $values);
}

function awards_sort($awards) {
  foreach ($awards as $award) {
    $sports[$award['sportID']][] = $award;
  }
  foreach ($sports as $first_key => $sport) {
    $sports[$first_key]['gold']   = 0;
    $sports[$first_key]['silver'] = 0;
    $sports[$first_key]['bronze'] = 0;
    foreach ($sport as $sp) {
      $sports[$first_key]['teacher']   = htmlentities($sp['first_name']) . ' ' . htmlentities($sp['last_name']);
      $sports[$first_key]['branch']    = $sp['branch'];
      $sports[$first_key]['field']     = $sp['field'];
      $sports[$first_key]['belt']      = $sp['beltName'];
      $sports[$first_key]['teacherID'] = $sp['superiorID'];
      $sports[$first_key]['userID']    = $sp['userID'];
      $sports[$first_key]['infoID']    = $sp['infoID'];
      switch ($sp['awdMedal']) {
        CASE 1:
          $sports[$first_key]['gold']++;
          break;
        CASE 2:
          $sports[$first_key]['silver']++;
          break;
        CASE 3:
          $sports[$first_key]['bronze']++;
          break;
      }
    }
  }
  foreach ($sports as $first_key => $sport) {
    foreach ($sport as $second_key => $sp) {
      if (!isset($sp['infoID'])) {
        $result[$first_key][$second_key] = $sp;
        continue;
      }
      unset($sp['awardID'], $sp['userID'], $sp['sportID'], $sp['infoID'], $sp['field'], $sp['branch'], $sp['first_name'], $sp['superiorID'], $sp['last_name'], $sp['beltName']);
      $result[$first_key][] = $sp;
    }
  }
  
  return $result;
}

function awards_to_string($sorted_awards, $show_setting = FALSE) {
  $result    = '';
  $beltArray = array(
    'سفید' => '/_css/icon/belts/belt_white.png',
    'زرد'  => '/_css/icon/belts/belt_yellow.png',
    'سبز'  => '/_css/icon/belts/belt_green.png',
    'آبی'  => '/_css/icon/belts/belt_blue.png',
    'قرمز' => '/_css/icon/belts/belt_red.png',
    'مشکی' => '/_css/icon/belts/belt_black.png',
  );
  foreach ($sorted_awards as $award) {
    $sport_name = $award['field'] . ' ' . $award['branch'];
    $beltPic    = '/_css/icon/belts/dan.png';
    if (array_key_exists($award['belt'], $beltArray)) {
      $beltPic = $beltArray[$award['belt']];
    }
    $beltPic = '<img class="belt" src="' . $beltPic . '" title="کمربند ' . $award['belt'] . ' در رشته ' . $sport_name . '">';
    $result .= '<div class="box box_shadow">';
    $result .= '<div class="placeHolder">';
    $result .= '<h3>' . $award['field'] . '  <span class="teal">' . $award['branch'] . '</span> </h3>';
    $result .= '</div>'; // placeHolder end
    $result .= $beltPic;
    $result .= '<div class="boxmed">';
    $result .= '<span class="icon gold_icon">';
    $result .= '<label>' . $award['gold'] . '</label>';
    $result .= '</span>';
    $result .= '<span class="icon silver_icon">';
    $result .= '<label>' . $award['silver'] . '</label>';
    $result .= '</span>';
    $result .= '<span class="icon bronze_icon">';
    $result .= '<label>' . $award['bronze'] . '</label>';
    $result .= '</span>';
    $result .= '</div>'; // boxmed end
    $result .= '<div>'; //just for holding award teacher name, for current class
    $result .= '<a href="/' . $award['teacherID'] . '" class="">نام استاد: ' . $award['teacher'] . '</a>';
    if ($show_setting) {
      $result .= '<a href="/setting/sports/' . $award['userID'] . '/' . $award['infoID'] . '/honors"><span class="glyphicon glyphicon-cog"></span> تنظیمات </a>';
    }
    $result .= '</div>';
    unset($award['field'], $award['branch'], $award['belt'], $award['bronze'], $award['silver'], $award['gold'], $award['teacherID'], $award['teacher'], $award['userID'], $award['infoID']);
    ///// awards information
    foreach ($award as $key => $awd) {
      if (!$awd['awdID']) {
        unset($award[$key]);
      }
    }
    if (empty($award)) {
      return $result .= '</div>';
    }
    $result .= '<div class="collapse">';
    $result .= '<div class="collapse_header">';
    $result .= '<div data-toggle="collapse" class="icon arrow_up_white_icon">';
    $result .= 'افتخارات در رشته ' . $sport_name;
    $result .= '</div>'; // collapse data-toggle end
    $result .= '</div>'; // collapse_header end
    $result .= '<div class="collapse_body">';
    foreach ($award as $awd) {
      $medal = NULL;
      if ($awd['awdMedal']) {
        // medals name
        switch ($awd['awdMedal']) {
          case 1:
            $medal_name = 'طلا';
            break;
          case 2:
            $medal_name = 'نقره';
            break;
          default:
            $medal_name = 'برنز';
        }
        $medal = '<div class = "awd_medal new_errors fl">' . $medal_name . '</div>';
      }
      $result .= '<div class = "award_info">';
      $result .= '<h3 class = "awd_title">' . $awd['awdName'] . '</h3>';
      $result .= $medal;
      $result .= '<div class = "description">' . $awd['awdDescription'] . '</div>';
      $result .= '</div>'; // award_info end
    }
    $result .= '</div>'; // collapse_body end
    $result .= '</div>'; // collapse end
    $result .= '</div>'; // box end
  }
  
  return $result;
}

/**
 * @param int $userID
 *
 * @return string
 */
function awards_string_to_forms($userID) {
  $sports               = sports_by_user_id($userID);
  $awards               = find_person_award($userID);
  $medals               = array(
    1 => 'طلا',
    2 => 'نقره',
    3 => 'برنز',
  );
  $sport_attrs          = array(
    'name'  => 'sportID',
    'class' => 'chosen-select chosen-rtl',
  );
  $medal_attrs          = array(
    'name'  => 'medal',
    'class' => 'chosen-select chosen-rtl',
  );
  $medal_custom_options = array('' => '');
  $str_awards           = '';
  while ($award = $awards->fetch(PDO::FETCH_ASSOC)) {
    $awd_id = $award['awdID'] ? htmlentities($award['awdID']) : 0;
    $str_awards .= '<div class="new_row">';
    $str_awards .= '<form action="setting/honors.php">';
    $str_awards .= '<div class="new_inner_row box_shadow padding">';
    $str_awards .= '<input type="hidden" name="awd_id" value="' . $awd_id . '">';
    $str_awards .= '<label>عنوان : </label>';
    $str_awards .= '<input name="title" class="new_input new_medium_input" type="text" placeholder="عنوان افتخار" value="' . htmlentities($award['awdName']) . '"/>';
    $str_awards .= '<label>رشته : </label>';
    $str_awards .= select_element($sports, 'sportID', 'sport_name', $sport_attrs, $award['sportID']);
    $str_awards .= '<label>مدال : </label>';
    $str_awards .= select_element($medals, 'medal_value', 'medal_name', $medal_attrs, $award['awdMedal'], $medal_custom_options);
    $str_awards .= '<div class="new_row">';
    $str_awards .= '<label >توضیحات : </label>';
    $str_awards .= '<br />';
    $str_awards .= '<textarea class="new_long_input" name="description" id="description" cols="30" rows="10" placeholder="توضیحات">';
    $str_awards .= htmlentities($award['awdDescription']);
    $str_awards .= '</textarea>';
    $str_awards .= '</div>';
    $str_awards .= '<button class="btn btn_blue edit honor">اعمال تغییرات</button>';
    $str_awards .= '<button class="btn btn_orange remove honor">حذف</button>';
    $str_awards .= '</div>';
    $str_awards .= '</form>';
    $str_awards .= '</div>';
  }
  
  return $str_awards;
}

function address_by_person_id($userID) {
  $columns_values = array(':userID' => $userID);
  $query          = 'SELECT * FROM `address` WHERE userID = :userID LIMIT 1';
  
  return sql_query_executor($query, $columns_values)->fetch(PDO::FETCH_ASSOC);
}

/**
 * If person not exist redirect to home page else do not anything
 *
 * @param $userID
 */
function person_existence_check($userID) {
  global $cUser;

  if (!person_is_exist($userID)) {
    message('چنین فردی وجود ندارد');
    redirect_to(BASEPATH . urlencode($cUser->user_id));
  }
  if (!person_isConfirm_by_id($userID)) {
    //  Do Not show the Page
    message(' کاربر تایید نشده است');
    redirect_to(BASEPATH . urlencode($cUser->user_id));
  }
}

function person_is_exist($userID) {
  global $db;
  $existenceSelect = 'SELECT  EXISTS ( ';
  $existenceSelect .= 'SELECT 1 FROM `user` ';
  $existenceSelect .= 'WHERE `userID` = :userID ';
  $existenceSelect .= 'LIMIT 1 )';
  $existence = $db->prepare($existenceSelect);
  $existence->bindParam(':userID', $userID, getPDOConstantType($userID));
  $existence->execute();
  $existence = $existence->fetch(PDO::FETCH_NUM);
  
  return $existence[0] ? TRUE : FALSE;
}

function person_isConfirm_by_id($userID) {
  global $db;
  $confirmQ = 'SELECT count(*) FROM `infotable` ';
  $confirmQ .= 'WHERE `userID` = :userID AND is_confirm = 1 ';
  $confirm = $db->prepare($confirmQ);
  $confirm->bindParam(':userID', $userID, getPDOConstantType($userID));
  $confirm->execute();
  $num = $confirm->fetch(PDO::FETCH_NUM);
  
  return array_shift($num) > 0 ? TRUE : FALSE;
}

function is_teacher($userID) {
  $query = 'SELECT EXISTS ( ';
  $query .= 'SELECT 1 ';
  $query .= 'FROM `infotable` ';
  $query .= 'WHERE `infotable`.`userID` = :userID AND `is_teacher` = 1 )';
  $values = array(':userID' => $userID);
  $output = sql_query_executor($query, $values)->fetch(PDO::FETCH_NUM);
  
  return $output[0];
}

function is_referee($userID) {
  $query = 'SELECT EXISTS ( ';
  $query .= 'SELECT 1 ';
  $query .= 'FROM `infotable` ';
  $query .= 'WHERE `infotable`.`userID` = :userID AND `is_referee` = 1 )';
  $values = array(':userID' => $userID);
  $output = sql_query_executor($query, $values)->fetch(PDO::FETCH_NUM);
  
  return $output[0];
}

/////////////////////////////
/**
 * Determine that a person is an other person teacher
 *
 * @author Ramin Sadegh nasab
 *
 * @param      $teacherID
 * @param      $studentID
 * @param bool $sportID
 *
 * @return bool
 */
function is_a_teacher_student($teacherID, $studentID, $sportID = FALSE) {
  $values = array(
    'userID'     => $studentID,
    'superiorID' => $teacherID,
  );
  $query  = 'SELECT EXISTS ( ';
  $query .= 'SELECT 1 FROM `infotable` ';
  $query .= 'WHERE `userID` = :userID AND `superiorID` = :superiorID';
  if ($sportID) {
    $query .= ' AND `sportID` = :sportID ';
    $values[':sportID'] = $sportID;
  }
  $query .= ' ) ';
  $query .= 'LIMIT 1';
  $isStudent = sql_query_executor($query, $values)->fetch(PDO::FETCH_NUM);
  
  return $isStudent[0] ? TRUE : FALSE;
}

/**
 * @author Ramin Sadegh nasab
 *
 * @param $infoID
 * @param $teacherID
 *
 * @return bool
 */
function is_teacher_student_by_infoid($infoID, $teacherID) {
  $values = array(
    'infoID'     => $infoID,
    'superiorID' => $teacherID,
  );
  $query  = 'SELECT EXISTS ( ';
  $query .= 'SELECT 1 FROM `infotable` ';
  $query .= 'WHERE `infoID` = :infoID AND `superiorID` = :superiorID';
  $query .= ' ) ';
  $query .= 'LIMIT 1';
  $isStudent = sql_query_executor($query, $values)->fetch(PDO::FETCH_NUM);
  
  return $isStudent[0] ? TRUE : FALSE;
}

/*function is_a_student_teacher($teacherID, $studentID) {
        global $db;
        $isStudentQ  = 'SELECT EXISTS ( ';
        $isStudentQ .= 'SELECT 1 FROM `infotable` ';
        $isStudentQ .= 'WHERE `userID` = :userID AND `superiorID` = :superiorID ) ';
        $isStudentQ .= 'LIMIT 1';
        $isStudentPre = $db->prepare($isStudentQ);
        $isStudentPre->bindParam(':superiorID', $studentID, getPDOConstantType($studentID));
        $isStudentPre->bindParam(':userID', $teacherID, getPDOConstantType($teacherID));
        $isStudentPre->execute();
        $isStudent = $isStudentPre->fetch(PDO::FETCH_NUM);
        return array_shift($isStudent) ? true : false;
    }*/
function student_num_for_teacher($user_id) {
  /*global $db;
  $studentsCountQ = 'SELECT count(*) ';
  $studentsCountQ .= 'FROM `infotable` ';
  $studentsCountQ .= 'WHERE `superiorID` = :userID AND is_confirm = true';
  $studentsCount = $db->prepare($studentsCountQ);
  $studentsCount->bindParam(':userID', $userID, getPDOConstantType($userID));
  $studentsCount->execute();
  $students = $studentsCount->fetch(PDO::FETCH_NUM);*/
  $count = db_select()
    ->from('infotable')
    ->where('superiorid', $user_id)
    ->where('userid != ?', $user_id)
    ->count()
  ;

  return $count;
}

///////////////////////////// events
//////// new event functions
function event_load() {

}

///// old event functions
function select_evt_type_by_sportID($sportID) {
  $query = 'SELECT `id`, `evt_type_name`, `has_weight` ';
  $query .= 'FROM `evt_type` ';
  $query .= 'WHERE `sport_id` = :sport_id ';
  //        $query .= '';
  $value = array(':sport_id' => $sportID);
  
  return sql_query_executor($query, $value);
}

function evt_requirement_insert($assoc_array) {
  $keys             = array_keys($assoc_array);
  $values_to_insert = array();
  $query            = insert_query_creator($keys, 'evtrequirement');
  foreach ($assoc_array as $key => $value) {
    $values_to_insert[':' . $key] = $value;
  }
  unset($assoc_array);
  var_dump($query);
  var_dump($values_to_insert);
  die();

  return sql_query_executor($query, $values_to_insert);
}

function event_insert() {
  global $db;
  global $cUser;
  ////values to insert into tables
  //        $variableArray = array('subBranch','evt_type','event_title');
  //        $new = get_specified_superGlobals_from_array($variableArray, $method = 'post');
  //        var_dump($new);
  //        die();
  //        var_dump($db->lastInsertId());
  //        die();
  $event_values = array();
  //if we inserted into requirements table we must have it's ID: $db->lastInsertId();
  $event_values[':evtReqID']    = $_POST['requirements'] ? $db->lastInsertId() : NULL;
  $event_values[':sportID']     = $_SESSION['sportID']; // event sportID, shows that this sport ID is default sportID for users in this sport
  $event_values[':subBranchID'] = validate_presences(array('subBranch')) ? $_POST['subBranch'] : NULL;
  //        var_dump(validate_presences(array('subBranch')));
  //        var_dump($event_values);
  //        die();
  $event_values[':inserterID']  = $cUser->user_id;
  $event_values[':evtTypeID']   = $_POST['evt_type'];
  $event_values[':evtName']     = $_POST['event_title'];
  $event_values[':deadendDate'] = r_jalali_to_gregorian($_POST['deadline'], '/', '-');
  $event_values[':openingDay']  = r_jalali_to_gregorian($_POST['openingDay'], '/', '-');
  $event_values[':evtAddress']  = $_POST['address'];
  $event_values[':maxReg']      = validate_presences(array('max_reg'), 'post', TRUE) ? (int) $_POST['max_reg'] : NULL;
  // remove , from cost
  $event_values[':sex']         = $_POST['gender'];
  $event_values[':cost']        = validate_presences(array('event_cost'), 'post', TRUE) ? preg_replace('/,/', '', $_POST['event_cost']) : NULL;
  $event_values[':description'] = validate_presences(array('description'), 'post', TRUE) ? $_POST['description'] : NULL;
  // set otherSports & allSports
  if ($_POST['allow_sports'] == 0) {
    $event_values[':otherSports'] = FALSE;
    $event_values[':allSports']   = FALSE;
  }
  elseif ($_POST['allow_sports'] == 2) {
    $event_values[':otherSports'] = TRUE;
    $event_values[':allSports']   = TRUE;
  }
  elseif ($_POST['allow_sports'] == 1) {
    $event_values[':otherSports'] = TRUE;
    $event_values[':allSports']   = FALSE;
  }
  $event_values[':confirm_needed'] = TRUE;
  $columns_to_insert               = array(
    'evtReqID',
    'sportID',
    'subBranchID',
    'inserterID',
    'evtTypeID',
    'evtName',
    'sex',
    'deadendDate',
    'openingDay',
    'evtAddress',
    'maxReg',
    'cost',
    'description',
    'otherSports',
    'allSports',
    'confirm_needed',
  );

  ///// create insert query
  $insert_query = insert_query_creator($columns_to_insert, 'event');

  return sql_query_executor($insert_query, $event_values);
}

function event_update($columns_values, $where) {
  $table_name = 'event';
  /*TODO : where clause must be flexible for every model of update in future. */
  $query = new_update_query_creator($columns_values, $table_name, $where);

  //  echo $query;die();
  return sql_none_select_query_executor($query);
}

function evtallowsports_insert($allow_sport_id, $last_event_id) {
  $insert_columns = array(
    'evtID',
    'sportID',
  );
  $query          = insert_query_creator($insert_columns, 'evtallowsports');
  $values         = array(
    ':evtID'   => $last_event_id,
    ':sportID' => $allow_sport_id,
  );
  
  return sql_query_executor($query, $values);
}

function user_select_for_event_notification($user_properties) {
  global $db;
  $query = 'SELECT `infotable`.`userID`,`infotable`.`sportID` ';
  $query .= 'FROM `infotable` ';
  $query .= 'JOIN `user` ON `infotable`.`userID` = `user`.`userID` ';
  //$query .= 'LEFT JOIN `belt` ON `infotable`.`beltID` = `belt`.`beltID` // we might add belt table
  $query .= 'WHERE ';
  if (!empty($user_properties)) {
    if (isset($user_properties[':sportID'])) {
      $query .= '`sportID` IN (';
      $query .= implode(',', $user_properties[':sportID']);
      $query .= ')';
      unset($user_properties[':sportID']);
    }
    if (isset($user_properties[':is_teacher'])) {
      $query .= ' AND `infotable`.`is_teacher` = 1 ';
    }
    if (isset($user_properties[':is_referee'])) {
      $query .= ' AND `infotable`.`is_referee` = 1 ';
    }
    $query .= 'AND is_confirm = 1 ';
  }
  else {
    $query .= 'is_confirm = 1 ';
  }
  //        var_dump($query);
  //        $preQuery = $db->prepare($query);
  //        return $preQuery->execute();
  return $db->query($query);
  //        return sql_query_executor($query,$user_properties);
}

function event_select($eventID = NULL, $sportID = NULL) {
  global $limit;
  global $page;
  pagination_variables('get');
  $offset = $page > 0 ? ($page - 1) * $limit : 0;
  $values = array(
    ':limit'  => $limit,
    ':offset' => $offset,
  );
  $query  = 'SELECT SQL_CALC_FOUND_ROWS *, `sport`.`id` as `sport_id` ';
  $query .= 'FROM `event` ';
  $query .= 'JOIN `evt_type` ON `event`.`evttypeid` = `evt_type`.`id` ';
  $query .= 'LEFT JOIN evtrequirement ON event.evtReqID = evtrequirement.evtReqID ';
  $query .= 'JOIN `sport` ON `event`.`sportID`  = `sport`.`id` ';
  $query .= 'WHERE `sport`.`sportconfirm` = 1 ';
  if ($eventID != NULL) {
    $query .= 'AND eventID = :eventID ';
    $values[':eventID'] = $eventID;
  }
  elseif ($sportID != NULL) {
    $query .= ' AND `event`.`sportID` = :sportID';
    //            $query .= ' AND openingDay < now() ';
    $values[':sportID'] = $sportID;
  }
  //  echo $query;die;
  $query .= ' ORDER BY ';
  //  if ($eventID == NULL && $sportID == NULL && isset($cUser->user_id)) {
  //    $query .= '(`event`.`sportID` IN (:sportID)) DESC,';
  //    $values[':sportID'] = $_SESSION['sportID'];
  //  }
  $query .= '`event`.`created` DESC ';
  $query .= 'LIMIT :offset, :limit';

  return sql_query_executor($query, $values);
}

/**
 * check to see if person can register in an event or not
 *
 * @param array $event
 * @param       $userID
 * @param       $user_sportID
 *
 * @return string
 */
function event_allowed_person($event, $userID, $user_sportID) {
  if (!compare_two_date($event['deadendDate'])) {
    return 1;
  }
  //        elseif ($userID) {
  // retrieve person information
  //            do {
  $person = find_person_by_id($userID);
  $age    = ageCalc($person['birth_date']);
  if ($event['evtReqID']) {
    if ($event['max_age']) {
      if ($age > $event['max_age']) {
        return FALSE;
      }
    } // check to see user age not > max_age
    if ($event['min_age']) {
      if ($age < $event['min_age']) {
        return FALSE;
      }
    } // check to see user age not < min_age
    if ($event['must_teacher'] == 1) {
      if (!is_teacher($userID)) {
        return FALSE;
      }
    }
    if ($event['must_referee'] == 1) {
      if (!is_referee($userID)) {
        return FALSE;
      }
    }
  }
  do {
    if ($event['allSports']) {
      break;
    } // if allSports
    elseif ($user_sportID == $event['sportID']) {
      break;
    }
    else {
      //check to see if user sports is in this event
      $sports = event_allows_sports($event['eventID'], $user_sportID);
      if ($sports->rowCount() > 0) {
        break;
      }
    }
    
    return FALSE;
  } while (FALSE);
  //            } while (false);
  //        }
  return 2;
}

/**
 * Check to see user permission for event return a button or false for use in
 * event
 *
 * @param int $value
 * @param int $eventID
 *
 * @return string
 */
function register_button_for_events($value, $eventID) {
  if ($value == FALSE) {
    $register_button = FALSE;
  }
  elseif ($value == 1) {
    $register_button = '<button class="btn btn_orange">اتمام مهلت ثبت نام</button>';
  }
  elseif ($value == 2) {
    $register_button = '<button class="btn btn_green"><a href="/events/event-register/' . $eventID . '"> +ثبت نام</a></button>';
  }
  
  return $register_button;
}

/**
 * @author Ramin Sadegh nasab
 *
 * @param Object $event_PDOObject
 *
 * @return string
 */
function echo_quick_events($event_PDOObject) {
  //  $output = '<div class="noti_rightSide">';
  global $Acl;
  global $permission;
  $output = '';
  while ($event = $event_PDOObject->fetch(PDO::FETCH_ASSOC)) {
    //    var_dump($event);die();
    $output .= '<div class="event_box">';
    $output .= '<h3>' . htmlentities($event['evtName']) . '</h3>';
    $output .= '<p> ' . htmlentities($event['evt_type_name']) . ' در رشته ' . $event['field'] . ' ' . $event['branch'] . '</p>';
    //    if($event['description']) $output .= '<p>'.nl2br(htmlentities($event['description'])).'</p>';
    $output .= '<p> مهلت ثبت نام ' . '<span class="date_time">' . r_gregorian_to_jalali($event['deadendDate']) . '</span></p>';
    $output .= '<div class="btn_middle">';
    $output .= '<a class="btn btn_green" href="/events/event/' . $event['eventID'] . '">+ بیشتر</a>';
    if ($event['has_weight']) {
      if ($Acl->check_permission('event_table_edit', $permission['group_id'], $permission['user_id']) && $permission['user_id'] == $event['inserterID']) {
        $output .= '<a class="btn btn_orange" href="/events/events-table-management/' . $event['eventID'] . '">ویرایش جدول</a>';
      }
      if ($event['evt_table_close']) {
        $output .= '<a class="btn btn_blue" href="/events/events-table-show/' . $event['eventID'] . '">جدول </a>';
      }
    }
    $output .= '</div>';
    $output .= '</div>';
  }
  //  $output .= '</div>';
  if ($output == '') {
    $output = '<div class="event_box">
                <h2>هیچ رویدادی با این مشخصه یافت نشد</h2>
                <p class="new_errors">متاسفانه رویدادی یافت نشد</p>
                </div>';
  }
  echo $output;
}

/**
 * Echo an event as full event
 *
 * @author Ramin Sadegh nasab
 *
 * @param array  $event_array
 * @param string $registerButton
 */
function echo_full_events($event_array, $registerButton) {
  global $permission;
  global $Acl;
  if (!empty($event_array)) {
    $sports = event_allows_sports($event_array['eventID']);
    $output = '';
    //    $output = '<div class="noti_rightSide" style="width:900px;margin:10px auto;">';
    $output .= '<div class="event_box box_shadow">';
    $output .= '<h2>' . htmlentities($event_array['evtName']) . '</h2>';
    //    $output .= '<div class="icon_judge" style="visibility: hidden"></div>';
    $output .= '<p> ' . htmlentities($event_array['evt_type_name']) . ' در رشته <span class="new_success_message">' . $event_array['field'] . ' ' . $event_array['branch'] . '</span></p>';
    if ($sports) {
      $output .= '<p> امکان ثبت نام میان ورزشکاران رشته های زیر وجود دارد: <br/>';
      $output .= '<span class="new_errors">' . $event_array['field'] . ' ' . $event_array['branch'] . '</span>';
      while ($sport = $sports->fetch(PDO::FETCH_ASSOC)) {
        $output .= ', <span class="new_errors">' . $sport['field'] . ' ' . $sport['branch'] . '</span>';
      }
      $output .= '</p>';
    }
    if ($event_array['description']) {
      $output .= '<p class="description blue">' . nl2br(htmlentities($event_array['description'])) . '</p>';
    }
    if ($event_array['cost']) {
      $output .= '<p>هزینه ثبت نام <span class="new_errors">' . htmlentities(number_format($event_array['cost'], 0, '', ',')) . '</span> ریال</p>';
    }
    $output .= '<p> مهلت ثبت نام <span class="date_time">' . r_gregorian_to_jalali($event_array['deadendDate']) . '</span></p>';
    if ($registerButton) {
      $output .= '<div >';
      $output .= $registerButton;
      if ($event_array['has_weight']) {
        /* Ramin:FixMe
         * this part "$permission['user_id'] == $event_array['inserter_id']"
         * must be change and check from $Acl->check_co_work_permission().
         */
        if ($Acl->check_permission('event_table_edit', $permission['group_id'], $permission['user_id']) && $permission['user_id'] == $event_array['inserterID']) {
          $output .= '<a class="btn btn_orange" href="/events/events-table-management/' . $event_array['eventID'] . '">ویرایش جدول</a>';
        }
        $output .= '<a class="btn btn_blue" href="/events/events-table-show/' . $event_array['eventID'] . '">جدول وزن ها </a>';
      }
      $output .= '</div>';
    }
    $output .= '</div>'; // end of event box
    //    $output .= '</div>';
  }
  else {
    $output = '<div class="event_box">
                <h2>هیچ رویدادی با این مشخصه یافت نشد</h2>
                <p class="new_errors">متاسفانه رویدادی یافت نشد</p>
                </div>';
  }
  echo $output;
}

/**
 * return true or false if a sportID has exist in a particular event or false if not
 * return if sportID != null(true OR false) elseif(sportID==null) all events with evtID = @eventID
 *
 * @param      $eventID
 * @param null $sportID
 *
 * @return bool|\PDOStatement
 * @author Ramin Sadegh nasab
 */
function event_allows_sports($eventID, $sportID = NULL) {
  $values = array();
  $query  = 'SELECT *, `sport`.`id` as `sport_id` ';
  $query .= 'FROM `evtallowsports` ';
  $query .= 'JOIN `sport` ON `sport`.`id` = `evtallowsports`.`sportID` ';
  $query .= 'WHERE `evtID` = :evtID ';
  if ($sportID != NULL) {
    $query .= ' AND `evtallowsports`.`sportID` = :sportID ';
    $values[':sportID'] = $sportID;
  }
  $values[':evtID'] = $eventID;

  //echo $query;die;
  return sql_query_executor($query, $values);
}

function evt_type_edit_box($object) {
  $output = '';
  while ($evt_type = $object->fetch(PDO::FETCH_ASSOC)) {
    $checked = $evt_type['has_weight'] ? 'checked' : NULL;
    $output .= '<div class="split_bottom new_row evt_type_holder">';
    $output .= '<div class="edit-box"><ul>
<li><span class="glyphicon glyphicon-edit edit_evt_type blue" title="ویرایش"></span></li>
<li><span class="glyphicon glyphicon-remove  blue remove_evt_type"></span></li>
</ul></div>';
    //    action="/management/sport-setting/event_type.php" leave the action blank, and it's automatically set to this page?
    $output .= '<form method="post" action="management/sport-setting/event_type.php" class="evt_type" id="' . $evt_type['id'] . '">';
    $output .= "<input type= 'text' name ='evt_type_name' disabled class='new_input' value = '{$evt_type['evt_type_name']}' />";
    $output .= '<input type="checkbox" name="weight_req" disabled id="weight_req_' . $evt_type['id'] . '" ' . $checked . '/>';
    $output .= '<label for="weight_req_' . $evt_type['id'] . '">نیاز به جدول بندی دارد</label>';
    $output .= '<input type = "submit" name="submit" disabled class="btn btn_blue" value = "ثبت"/></input>';
    $output .= '</form>';
    $output .= '</div>';
  }
  
  return $output;
}

function evt_table_add_form($event_id) {
  global $week_day_assoc;
  $sport_attrs = array(
    'name'  => 'week_day[]',
    'class' => 'week_days',
  );
  $form_string = '';
  $form_string .= '<form action="events/weight_age_add.php" method="post" name="evt_weight_age">';
  $form_string .= '<input type="hidden" name ="evtID" readonly value="' . $event_id . '">';
  $form_string .= '<div class="new_row new_inner_row">';
  $form_string .= '<lable>عنوان رده سنی: </lable>';
  $form_string .= '<input class="new_input" name="age_class_name" required="required" placeholder="رده سنی (مثال: جوانان)">';
  $form_string .= '</div>';
  $form_string .= '<div class="new_row new_inner_row">';
  $form_string .= ' سن از  ';
  $form_string .= '<input class="new_input four_digit_input" type="text" name="min_age" required="required" placeholder="کمتر" />';
  $form_string .= ' تا ';
  $form_string .= '<input class="new_input four_digit_input" type="text" name="max_age" required="required" placeholder="بیشتر" />';
  $form_string .= '</div>';
  $form_string .= '<div class="new_inner_row">';
  $form_string .= 'عنوان وزن ';
  $form_string .= '<input class="new_input" name="weight_class_name[]" placeholder="مثال: وزن 1 یا سبک وزن">';
  $form_string .= 'از   ';
  $form_string .= '<input type = "text" class="new_input four_digit_input" name="min_weight[]" required="required" placeholder="کیلوگرم"/ >';
  $form_string .= ' تا ';
  $form_string .= '<input type = "text" class="new_input four_digit_input" name="max_weight[]" required="required" placeholder="کیلوگرم"/>';
  $form_string .= '</div>';
  $form_string .= '<div class="new_inner_row">';
  $form_string .= '<span class="new_weight btn_orange btn_small"> <span class="glyphicon glyphicon-plus-sign"></span> افزودن وزن جدید</span>';
  $form_string .= '</div>';
  $form_string .= '<div class="new_inner_row">';
  $form_string .= '<input type="submit" class="btn btn_blue new_long_input" name="submit" value="ثبت"/>';
  $form_string .= '</div>';
  $form_string .= '</form>';
  echo $form_string;
}

function event_table_insert($columns_values) {
  $table_name = 'match_weight';
  $query      = insert_query_creator_new($columns_values, $table_name);
  
  return sql_none_select_query_executor($query);
}

function evt_table_select($event_id) {
  $query = 'SELECT * ';
  $query .= 'FROM `match_weight` ';
  $query .= 'WHERE evtID = :evt_id';
  $columns_values = array(
    ':evt_id' => $event_id,
  );
  
  return sql_query_executor($query, $columns_values);
}

function evt_table_show($evt_table) {
  /*
   * render and return event table object
   */
  $sorted_table = array();
  while ($table = $evt_table->fetch(PDO::FETCH_ASSOC)) {
    $sorted_table[$table['age_class_name']]['evtID']                                                 = $table['evtID'];
    $sorted_table[$table['age_class_name']]['age_class_name']                                        = $table['age_class_name'];
    $sorted_table[$table['age_class_name']]['min_age']                                               = $table['min_age'];
    $sorted_table[$table['age_class_name']]['max_age']                                               = $table['max_age'];
    $sorted_table[$table['age_class_name']]['weight_class'][$table['matchWID']]['matchWID']          = $table['matchWID'];
    $sorted_table[$table['age_class_name']]['weight_class'][$table['matchWID']]['min_weight']        = $table['min_weight'];
    $sorted_table[$table['age_class_name']]['weight_class'][$table['matchWID']]['max_weight']        = $table['max_weight'];
    $sorted_table[$table['age_class_name']]['weight_class'][$table['matchWID']]['weight_class_name'] = $table['weight_class_name'];
  }
  $result = '';
  foreach ($sorted_table as $table_class) {
    $result .= '<div class="box padding">';
    $result .= '<div class="new_row">';
    $result .= '<h3> رده سنی: ' . htmlentities($table_class['age_class_name']) . '</h3>';
    $result .= ' از';
    $result .= '<span class="blue">' . htmlentities($table_class['min_age']) . '</span>';
    $result .= ' سال ';
    $result .= ' تا ';
    $result .= '<span class="blue">' . htmlentities($table_class['max_age']) . '</span>';
    $result .= ' سال';
    $result .= '</div>';
    foreach ($table_class['weight_class'] as $val) {
      $result .= '<div class="padding stripped">';
      //      $result .= '<div class="padding split_bottom">';
      $result .= '<span class="new_success_message"> ' . htmlentities($val['weight_class_name']) . '</span>';
      $result .= ' از ';
      $result .= '<span class="blue">' . htmlentities($val['min_weight']) . '</span>';
      $result .= '<small>کیلوگرم </small>';
      $result .= ' تا ';
      $result .= '<span class="blue">' . htmlentities($val['max_weight']) . '</span>';
      $result .= '<small>کیلوگرم </small>';
      //      $result .= '</div>';
      $result .= '<a href="/events/event-table/' . htmlentities($table_class['evtID']) . '/' . $val['matchWID'] . '" class="btn btn_orange fl"><small>مشاهده جدول</small></a>';
      $result .= '</div>';
    }
    $result .= '</div>';
  }
  
  return $result;
}

function match_weight_select($condition, $order_condition = NULL, $limit = NULL) {
  $columns_values = array();
  $query          = 'SELECT * FROM ';
  $query .= '`match_weight` ';
  $query .= 'WHERE ';
  //  $query .= implode(' ',array_keys($condition));
  foreach ($condition as $key => $value) {
    $query .= $key;
    preg_match('/:\w+/', $key, $matches);
    $columns_values[$matches[0]] = $value;
  }
  $query .= ' ORDER BY `max_weight` ';
  //  echo $query.'<br/>';
  if ($limit) {
    $query .= ' LIMIT ';
    foreach ($limit as $key => $part) {
      $query .= ":{$key} ";
      $columns_values[$key] = $part;
    }
  }
  //  echo $query;
  //  echo '<hr/>';
  //  var_dump($columns_values);
  //  die();
  return sql_query_executor($query, $columns_values);
}

function match_weight_age_class_name_distinct($event_id) {
  $query = 'SELECT DISTINCT `age_class_name` FROM ';
  $query .= '`match_weight` ';
  $query .= 'WHERE ';
  $query .= ' `evtID` = :event_id ';
  
  //  echo $query.'<br/>';
  return sql_query_executor($query, array(':event_id' => $event_id));
}

function mch_w_menu_creator($event_id, $main_menu_age_class_name) {
  $conditions_1 = array(
    ' evtID = :evt_id ' => $event_id,
    //  ' AND age_class_name = :age_class ' => $evt_one_match['age_class_name'],
  );
  $all_mch_w    = match_weight_select($conditions_1);
  $main_menu    = $sub_menu = array();
  while ($mch_w = $all_mch_w->fetch(PDO::FETCH_ASSOC)) {
    $url = "/events/event-table/{$mch_w['evtID']}/{$mch_w['matchWID']}";
    if (!array_search($mch_w['age_class_name'], $main_menu)) {
      $main_menu[$url] = $mch_w['age_class_name'];
    }
    if ($mch_w['age_class_name'] == $main_menu_age_class_name) {
      $sub_menu[$url] = '(' . $mch_w['min_weight'] . ' - ' . $mch_w['max_weight'] . ')';
    }
  }
  
  return array(
    'main_menu' => $main_menu,
    'sub_menu'  => $sub_menu,
  );
}

function event_capacity($event_id) {
  $query = 'SELECT COUNT(*) ';
  $query .= 'FROM `evt_register` ';
  $query .= 'WHERE event_id = :event_id';
  $columns_values = array(
    ':event_id' => $event_id,
  );
  
  return sql_query_executor($query, $columns_values)->fetchColumn()[0];
}

/**
 * This function checks to see did user payed for this event registration or
 * not?
 *
 * @param string $event_id ID for event that we want to check payment
 * @param string $user_id
 *                         check to see did this person pay for this event or
 *                         not
 *
 * @return boolean
 * @author Ramin
 */
function event_payment($event_id, $user_id) {
  $query = 'SELECT * FROM ';
  
  return TRUE;
}

/**
 * Insert values into evt_register table
 *
 * @param array $columns_values an array that contain columns that we want to
 *                              insert and their values
 *                              <br/> this array is an key => value pair array
 *                              <br/> key => insert able columns
 *                              <br/> value => values to insert into that
 *                              columns
 *
 * @return Boolean
 */
function event_register_insert($columns_values) {
  $query = insert_query_creator_new($columns_values, 'evt_register');
  
  return sql_none_select_query_executor($query);
}

/*
 * This function select anything that we want from evt_register table
 *
 * @param array $columns_values (in this format -> 'columns_name <=> :column_name' => value
 *
 * @return PDOObject
 */
function evt_register_select($columns_values_conditions) {
  $columns_values = array();
  $query          = 'SELECT * FROM ';
  $query .= '`infotable` ';
  $query .= 'JOIN `evt_register` ON `evt_register`.`infoID` = `infotable`.`infoID` ';
  $query .= 'JOIN `user` ON `infotable`.`userID` = `user`.`userID` ';
  $query .= 'WHERE ';
  //  $query .= implode(' ',array_keys($condition));
  foreach ($columns_values_conditions as $key => $value) {
    $query .= $key;
    preg_match('/:\w+/', $key, $matches);
    $columns_values[$matches[0]] = $value;
  }
  
  return sql_query_executor($query, $columns_values);
}

function matches_group_insert($columns_values) {
  $table_name = 'matches_group';
  $query      = insert_query_creator_new($columns_values, $table_name);
  
  return sql_none_select_query_executor($query);
}

function match_group($columns_values_conditions) {
  global $fpdo;
  $columns_values = array();
  $query          = 'SELECT * ';
  $query .= 'FROM `user` ';
  $query .= 'JOIN `matches_group` ON `matches_group`.`user_id` = `user`.`userID` ';
  $query .= 'JOIN `state` ON `state`.`stateID` = `user`.`stateID` ';
  $query .= 'LEFT JOIN `userpic` ON `userpic`.`userID` = `user`.`userID` ';
  $query .= 'WHERE ';
  foreach ($columns_values_conditions as $key => $value) {
    $query .= $key;
    preg_match('/:\w+/', $key, $matches);
    $columns_values[$matches[0]] = $value;
  }
  
  //  echo $query;die();
  return sql_query_executor($query, $columns_values);
}

function event_sorted_table_insert($columns_values) {
  $table_name = 'event_sorted_table';
  $query      = insert_query_creator_new($columns_values, $table_name);
  
  return sql_none_select_query_executor($query);
}

function event_table_group_select($columns_values_conditions) {
  $columns_values = array();
  $query          = 'SELECT * FROM ';
  $query .= 'event_sorted_table ';
  $query .= 'WHERE ';
  foreach ($columns_values_conditions as $key => $value) {
    $query .= $key;
    preg_match('/:\w+/', $key, $matches);
    $columns_values[$matches[0]] = $value;
  }

  return sql_query_executor($query, $columns_values);
}

function event_matches_data($columns_values_conditions) {
  global $page;
  $limit          = 1;
  $offset         = $page > 0 ? ($page - 1) * $limit : 0;
  $columns_values = array();
  $query          = 'SELECT * FROM ';
  $query .= '`matches_data` ';
  $query .= 'WHERE ';
  foreach ($columns_values_conditions as $key => $value) {
    $query .= $key;
    preg_match('/:\w+/', $key, $matches);
    $columns_values[$matches[0]] = (int) $value;
  }
  $query .= ' ORDER BY matches_data.created DESC ';
  $query .= 'LIMIT :offset, :limit';
  $columns_values[':limit']  = $limit;
  $columns_values[':offset'] = $offset;
  
  return sql_query_executor($query, $columns_values)->fetch(PDO::FETCH_ASSOC);
}

function student_event_register($teacher_id, $confirm = NULL) {
  global $page;
  global $limit;
  pagination_variables('get');
  $columns_values = array();
  $query          = 'SELECT  SQL_CALC_FOUND_ROWS ';
  //  $query .= '`user`.`userID`, `user`.`first_name`, `user`.`last_name`, `user`.`birth_date`, `user`.`weight` ';
  //  $query .= ', `pic`.`picurl` ';
  $query .= ' * , `u`.`userID`, `reg`.`id` as `reg_id` ';
  //  $query .= ', `info`.`updateDate`, `info`.`is_confirm` ';
  //  $query .= ', `st`.`statename`, `st``countyname` ';
  $query .= 'FROM `infotable` `info` ';
  $query .= 'JOIN `user` `u` ON `u`.`userID` = `info`.`userID` ';
  $query .= 'LEFT JOIN `userpic` `pic` ON `u`.`userID` = `pic`.`userID` ';
  $query .= 'JOIN `evt_register` `reg` ON `info`.`infoID` = `reg`.`info_id` ';
  $query .= 'JOIN `event` `e` ON `e`.`eventID` = `reg`.`event_id` ';
  $query .= 'JOIN `sport` `s` ON `e`.`sportID` = `s`.`id` ';
  //  $query .= 'JOIN `state` `st` ON `user`.`stateID` = `state`.`stateID` ';
  //  $query .= 'JOIN `infotable` `info` ON `user`.`userID` = `infotable`.`userID` ';
  $query .= 'WHERE `info`.`superiorID` = :superiorID ';
  $columns_values[':superiorID'] = $teacher_id;
  if ($confirm !== NULL) {
    $query .= 'AND `reg`.`confirm` = :confirm ';
    $columns_values[':confirm'] = $confirm;
  }
  // just show new students that never seen and updated by teacher
  // if {updateDate in null it comes first}, else {order rows on updateDate DESC (last updated row comes first)}
  $query .= ' ORDER BY ';
  $query .= 'CASE WHEN updateDate IS NULL THEN 0 ELSE 1 END, `reg`.`created` DESC,  `reg`.updated DESC ';
  //  $query .= ' `reg`.created DESC ';
  //  $query .= ', `info`.`updateDate` DESC ';
  $offset = $page > 0 ? $limit * ($page - 1) : 0;
  $query .= 'LIMIT :offset, :limit';
  $columns_values[':offset'] = $offset;
  $columns_values[':limit']  = $limit;
  //    var_dump($columns_values);
  //      die($query);
  return sql_query_executor($query, $columns_values);
}

function students_event_string($event_registers) {
  if (!is_object($event_registers) || $event_registers->rowCount() == 0) {
    return FALSE;
  }
  $result = '<div class="studentCer">';
  while ($event_register = $event_registers->fetch(PDO::FETCH_ASSOC)) {
    //    echo '<meta charset="utf-8" />';
    //            var_dump($event_register);die;
    $pic = $event_register['picurl'] != NULL ? '/profilePics/' . $event_register['picurl'] : '/_css/img/user-32.png';
    $result .= '<div class="Cnoti box_shadow" id ="' . $event_register['reg_id'] . '" data-dest="event_confirm.php" >';
    $result .= '<div class="CnotiHeader">';
    $result .= ' <span class="glyphicon glyphicon-calendar"></span> ';
    $result .= '</div>';
    $result .= '<div class="CnotiBody comment-img padding" >';
    $result .= '<img class="" src="' . $pic . '">';
    $result .= ' درخواست شرکت در مسابقات ';
    $result .= " <a href='/events/event/{$event_register['event_id']}'> {$event_register['evtName']} </a>";
    $result .= ' توسط ';
    $result .= '<a href="/' . $event_register['userID'] . '" <span class="">' . htmlentities($event_register['first_name']) . ' ' . htmlentities($event_register['last_name']) . '</span></a>';
    $result .= '<ul class="edit-box">';
    $result .= "<li><a class='btn btn_orange' href='/setting/sports/{$event_register['userID']}/{$event_register['infoID']}/honors'>ویرایش</a></li>";
    $result .= '<li><a class="btn btn_orange conf_detail"> <span class="glyphicon glyphicon-list"></span> جزئیات </a></li>';
    if ($event_register['confirm']) {
      $result .= '<li><a class="btn btn_orange conf conf_deny"> <span class="glyphicon glyphicon-remove-sign"></span>  رد درخواست</a></li>';
    }
    else {
      $result .= '<li><a class="btn btn_orange conf conf_accept"> <span class="glyphicon glyphicon-ok"></span> تایید </a></li>';
    }
    $result .= '</ul>'; // edit-box
    $result .= '</div>'; // cnoti body
    $result .= '<div class="CnotiDetails">';
    // parts
    $result .= '<div class="part">';
    $result .= '<label>نام: </label>';
    $result .= full_name($event_register['first_name'], $event_register['last_name']);
    $result .= '</div>'; // part
    $result .= '<div class="part">';
    $result .= '<label>وزن: </label>';
    $result .= '<span>' . $event_register['weight'] . '</span>';
    $result .= '</div>'; // part
    $result .= '<div class="part">';
    $result .= '<label>تاریخ تولد: </label>';
    $result .= '<span class="date_time">' . r_gregorian_to_jalali($event_register['birth_date']) . '</span>';
    $result .= '</div>'; // part
    $result .= '<div class="part">';
    $result .= '<label>سن: </label>';
    $result .= '<span title="' . $event_register['birth_date'] . '">' . ageCalc($event_register['birth_date']) . ' سال </span>';
    $result .= '</div>'; // part
    $result .= '<div class="part">';
    $result .= '<label></label>';
    $result .= '<span></span>';
    $result .= '</div>'; // part
    $result .= '</div>'; // cnoti details
    $result .= '</div>'; // Cnoti box_shadow
  }
  $result .= '</div>'; // end studentCer
  return $result;
}

function matches_data_insert($columns_values) {
  $table_name = 'matches_data';
  $query      = insert_query_creator_new($columns_values, $table_name);
  
  return sql_none_select_query_executor($query);
}

///////////////////////////// gym
function gym_insert($columns_to_insert, $values_to_insert) {
  /*
         * @columns_to_insert //which columns do we want to insert into
         * @values_to_insert // which values do we want to insert into those columns
         * values are assocative array key => columns to insert and value => values to insert
         * */
  $table_name = 'gym';
  $query      = insert_query_creator($columns_to_insert, $table_name);
  
  return sql_query_executor($query, $values_to_insert);
}

function gym_delete($gym_id) {
  $query          = 'DELETE FROM `gym` WHERE `gym`.`id` = :gym_id';
  $columns_values = array(':gym_id' => $gym_id);
  
  return sql_query_executor($query, $columns_values);
}

/////////////////////////////
function count_followers_following($userID, $show = 'follower') {
  global $db;
  $followCountQ = 'SELECT count(*) ';
  $followCountQ .= 'FROM `follow` ';
  if ($show == 'follower') {
    $followCountQ .= 'WHERE `foingID` = :userID ';
  }
  elseif ($show == 'following') {
    $followCountQ .= 'WHERE `foerID` = :userID ';
  }
  $followCount = $db->prepare($followCountQ);
  $followCount->bindParam(':userID', $userID, PDO::PARAM_INT);
  $followCount->execute();
  $following = $followCount->fetch(PDO::FETCH_NUM);
  
  return array_shift($following);
}

function is_follower($followedID, $followerID) {
  global $db;
  $isFollowerQ = 'SELECT count(*) ';
  $isFollowerQ .= 'FROM `follow` ';
  $isFollowerQ .= 'WHERE `foerID` = :foerID AND `foingID` = :foingID ';
  $isFollowerQ .= 'LIMIT 1 ';
  $isFollowerPre = $db->prepare($isFollowerQ);
  $isFollowerPre->bindParam(':foerID', $followerID, getPDOConstantType($followerID));
  $isFollowerPre->bindParam(':foingID', $followedID, getPDOConstantType($followedID));
  $isFollowerPre->execute();
  $userFlo = $isFollowerPre->fetch(PDO::FETCH_NUM);
  
  return array_shift($userFlo) ? TRUE : FALSE;
}

function follow($following, $follower) {
  $query  = 'INSERT INTO `follow` (`foerID`, `foingID`) VALUES (:followerID, :followingID)';
  $values = array(
    ':followingID' => $following,
    ':followerID'  => $follower,
  );
  
  return sql_query_executor($query, $values);
}

function unfollow($following, $follower) {
  $values = array(
    'foingID' => $following,
    'foerID'  => $follower,
  );
  $query  = delete_query_creator_new($values, 'follow');

  return sql_none_select_query_executor($query);
}

/////////////////////////////
function select_all_people($columns_and_values = array(), $question_mark = FALSE) {
  global $limit;
  global $page;
  $values              = array();
  $query_where_builder = array();
  $query               = "SELECT SQL_CALC_FOUND_ROWS `infotable`.`infoID`, `user`.`userID`, `user`.`first_name`, `user`.`last_name`, ";
  $query .= "`user`.`created`, `sport`.`id` as `sport_id`, `sport`.`field`, `sport`.`branch`, `state`.`stateID`, ";
  $query .= "CONCAT_WS(' ', `user`.`first_name`, `user`.`last_name`) as full_name, ";
  $query .= "`state`.`statename`, `state`.`countyname`, `infotable`.`insertdate`, `userpic`.`picurl`, ";
  $query .= "`job`.`jobname`, `infotable`.`is_confirm`, `infotable`.`superiorID` ";
  $query .= "FROM `infotable` ";
  $query .= "JOIN `user` ON `user`.`userID`   = `infotable`.`userID` ";
  $query .= "JOIN `sport` ON `sport`.`id` = `infotable`.`sportID` ";
  $query .= "JOIN `state` ON `state`.`stateID` = `user`.`stateID` ";
  $query .= "LEFT JOIN `userpic` ON `userpic`.`userID`= `user`.`userID` ";
  $query .= "JOIN `job` ON `job`.`jobID` = `infotable`.`jobID` ";
  if (!empty($columns_and_values)) {
    foreach ($columns_and_values as $key => $value) {
      if ($value == NULL || $value == 'all') {
        continue;
      }
      /* split :word from selects and put it in $matches[0] */
      preg_match('/:\w+/', $key, $matches);
      /**/
      $query_where_builder[$key] = $value;
      $values[$matches[0]]       = $value;
    }
    unset($columns_and_values);
    if (!empty($query_where_builder)) {
      $query .= ' WHERE ';
      $query .= implode(' AND ', array_keys($query_where_builder));
    }
  }
  $query .= " ORDER BY `infotable`.`insertDate` DESC, `infotable`.`updateDate` ";
  if ($limit) {
    $query .= " LIMIT ";
    if ($page != NULL) {
      $query .= ':offset,';
      $offset            = ($page - 1) * $limit;
      $values[':offset'] = $offset;
    }
    $query .= ':limit ';
    $values[':limit'] = $limit;

  }
  //            echo json_encode($values);
  //            die($query);

  return sql_query_executor($query, $values, $question_mark);
}

function find_persons_by_ids($uids) {
  $query = "SELECT SQL_CALC_FOUND_ROWS `user`.`userID`, `infotable`.`infoID`, `user`.`first_name`, `user`.`last_name`, ";
  $query .= "`user`.`created`, `sport`.`id` as `sport_id`, `sport`.`field`, `sport`.`branch`, `state`.`stateID`, ";
  $query .= "`state`.`statename`, `state`.`countyname`, `infotable`.`insertdate`, `userpic`.`picurl`, ";
  $query .= "`job`.`jobname`, `infotable`.`is_confirm`, `infotable`.`superiorID` ";
  $query .= "FROM `infotable` ";
  $query .= "JOIN `user` ON `user`.`userID`   = `infotable`.`userID` ";
  $query .= "JOIN `sport` ON `sport`.`id` = `infotable`.`sportID` ";
  $query .= "JOIN `state` ON `state`.`stateID` = `user`.`stateID` ";
  $query .= "LEFT JOIN `userpic` ON `userpic`.`userID`= `user`.`userID` ";
  $query .= "JOIN `job` ON `job`.`jobID` = `infotable`.`jobID` ";
  $query .= 'WHERE `user`.`userID` IN (' . implode(',', array_map(function ($id) {
      global $db;

      return $db->quote($id);
    }, $uids)) . ') ';
  $query .= 'GROUP BY `user`.`userID` ';
  $query .= "ORDER BY `infotable`.`insertDate` DESC, `infotable`.`updateDate` ";

  //  $parameters = array(':uids' => $uids);
  return sql_query_executor($query);
}

function echo_person_for_management($PDOObject) {
  global $cUser;

  $profile_pic_folder = '/profilePics/';
  $output             = '';
  while ($person = $PDOObject->fetch(PDO::FETCH_ASSOC)) {
    $picurl          = $person['picurl'] ? $profile_pic_folder . $person['picurl'] : '/_css/img/user-64.png';
    $full_name       = htmlentities($person['first_name']) . ' ' . htmlentities($person['last_name']);
    $full_sport_name = $person['field'] . ' ' . $person['branch'];
    $full_state_name = $person['statename'] . ',' . $person['countyname'];
    $output .= '<div class="person_management fr" id="' . $person['userID'] . '">';
    $output .= '<div class="person_info">';
    $output .= '<a class="fr" href="/' . $person['userID'] . '" title="مشاهده صفحه شخصی ' . $full_name . ' در رشته ' . $full_sport_name . '"><img class="img_profile" src="' . $picurl . '" alt="' . $full_name . ' در ' . $full_sport_name . '"/></a>';
    $output .= '<span class="new_errors" data-rest-url="min-prof/' . $person['userID'] . '"> ' . $full_name . ' </span>';
    $output .= '<div>';
    $output .= '<span> ' . $person['jobname'] . ' </span>';
    $output .= '<span class="blue"> در رشته' . ' ' . $full_sport_name . ' </span>';
    //    $output .= '<br/>';
    $output .= '</div>'; // no class div
    $output .= '<span class=""><span class="glyphicon glyphicon-map-marker"></span>' . $full_state_name . '</span>';
    $output .= '</div>'; // person_info
    //    $output .= '<div class="clearfix">';
    //    $output .= '<a class="btn btn_green" href="/' . $person['userID'] . '">صفحه شخصی</a>';
    if (isset($cUser->user_id) && $cUser->user_id == $person['superiorID']) {
      $output .= '<div class="edit-box">';
      //      $output .= '<a class="btn btn_orange conf conf_deny block" href="javascript:;">حذف</a>';
      $output .= '<a class="btn btn_blue block" href="/setting/sports/' . $person['userID'] . '/' . $person['infoID'] . '/honors">ویرایش</a>';
      $output .= '</div>';
    }
    //    $output .= '</div>'; // clearfix
    $output .= '</div>';
  }
  
  return $output;
}

function string_person_minimum($PDOObject) {
  $profile_pic_folder = '/profilePics/';
  $output             = '';
  while ($person = $PDOObject->fetch(PDO::FETCH_OBJ)) {
    $pic_source = $person->picurl ? $profile_pic_folder . $person->picurl : '/_css/img/user-64.png';
    //    $output .= '<div >';
    $output .= '<a class="min-person" href="/' . $person->userID . '"><img src ="' . $pic_source . '" title="' . htmlentities($person->first_name) . ' ' . htmlentities($person->last_name) . '"></a>';
    //    $output .= '</div>';
  }

  return $output;
}

/**
 * @param null  $uid
 * @param array $conditions
 *
 * @return bool|object
 * @author Ramin Sadegh nasab
 */
function user_load($uid = NULL, $conditions = array()) {
  $is_user = FALSE;
  $query   = db_select()
    ->from('`user` u')
    ->select(NULL)
    ->select('img.picurl as pic')
    ->select('up.username as username')
    ->select('up.email as email')
    ->select('inf.infoid as infid')
    ->select('inf.sportID as sid')
    ->select('inf.superiorID as supid')
    ->select('inf.jobID as jid')
    ->select('inf.beltID as bid')
    ->select('j.jobName as jName')
    ->select('j.jobValue as jValue')
    ->select('sp.field as spfield')
    ->select('sp.branch as spbranch')
    ->select('concat(sp.field," ",sp.branch) as spName')
    ->select('u.userID as user_id')
    ->select('u.stateID as stateid')
    ->select('st.stateName as state')
    ->select('st.countyName as county')
    ->select('concat(st.stateName," ",st.countyName) as stCounty')
    ->select('u.degreeID')
    ->select(' u.first_name, u.sex, u.last_name, u.national_code, u.birth_date, u.foot, u.length, u.weight, u.description')
    ->leftJoin('userpic img ON img.userID = u.userID')
    ->innerJoin('infotable inf ON inf.userID = u.userID')
    ->innerJoin('job j ON j.jobID = inf.jobID')
    ->innerJoin('state st ON u.stateid = st.stateID')
    ->innerJoin('sport sp ON inf.sportID = sp.id')
    ->innerJoin('user_pass up ON u.userID = up.uid')
  ;
  if ($uid) {
    $query->where('u.userID', $uid);
  }
  foreach ($conditions as $field => $value) {
    $query->where($field, $value);
  }
//    echo $query->getQuery();die;
  $result      = $query->execute();
  $user        = new User();
  $multi_value = array(
    'supid',
    'infid',
    'sid',
    'jid',
    'bid',
    'jName',
    'jValue',
    'spfield',
    'spbranch',
    'spName',
  );
  while ($user_info = $result->fetch(PDO::FETCH_ASSOC)) {
    $is_user = TRUE;
    foreach ($user_info as $key => $value) {
      if (!isset($user->$key)) {
        $user->$key = NULL;
      }

      if (!in_array($key, $multi_value)) {
        $value      = $key == 'pic' ? (new Image('profile', $value))->create_src('thumb') : $value;
        $user->$key = $value;
      }
      elseif (is_array($user->$key)) {
        $user_key                      =& $user->$key;
        $user_key[$user_info['infid']] = $value;
      }
      else {
        $user->$key = array($user_info['infid'] => $value);
      }
    }
  }
  if (!$is_user) {
    return FALSE;
  }
  $user->multi_sport = (count($user->infid) > 1) ? TRUE : FALSE;
  $user->user_link   = $user->link();
  $user->birth_date_jalali = r_gregorian_to_jalali($user->birth_date);
  //  var_dump($user);
  //  die;

  return $user;
}

/**
 * @param       $uids
 * @param array $conditions
 *
 * @return array
 * @author Ramin Sadegh nasab
 */
function user_load_multiple($uids, $conditions = array()) {
  $users = array();
  foreach ($uids as $uid) {
    $users[$uid] = user_load($uid, $conditions);
  }

  return $users;
}

/************** NEW Functions ******************/
/**
 * @param           $text
 * @param bool|TRUE $print
 *
 * @author Ramin Sadegh nasab
 */
function t($text, $print = TRUE) {
  if ($print) {
    echo $text;
  }
  else {
    return $text;
  }
}

/**
 * load a card by id
 *
 * @param null  $cid (card id)
 * @param array $conditions
 *
 * @return array
 * @author Ramin Sadegh nasab
 */
function card_load($cid = NULL, $conditions = array()) {
  $query = db_select()
    ->from('event_card')
    ->select(NULL)
    ->select('*')
    ->select('event_card.id as cid')
    ->select('user.weight as weight')
    ->innerJoin('user ON event_card.uid = user.userid')
    ->innerJoin('event ON event_card.eid = event.eventid')
    ->innerJoin('event_job ON event_card.ejid = event_job.id')
    ->innerJoin('sport ON event.sportid = sport.id')
    ->innerJoin('state ON user.stateid = state.stateid')
    ->orderBy('event_card.created DESC')
  ;
  if ($cid) {
    $query->where('event_card.id', $cid);
  }
  elseif (!empty($conditions)) {
    foreach ($conditions as $key => $value) {
      $key = $key == 'id' ? 'event_card.id' : $key;
      $query->where($key, $value);
    }
  }
  //  echo $query->getQuery();die;
  $result = $query->execute()->fetchAll(PDO::FETCH_OBJ);
  if ($result && count($result) == 1) {
    $result = array_shift($result);
  }

  return $result;
}

function full_name($name, $surname) {
  return ' ' . htmlentities($name) . ' ' . htmlentities($surname) . ' ';
}

function created_updated_date($created, $updated = NULL) {
  if (!empty($updated)) {
    $text = t(' ویرایش در ', FALSE);
    $date = $updated;
  }
  else {
    $text = t(' ثبت شده در ', FALSE);
    $date = $created;
  }

  return (object) array(
    'date' => r_gregorian_to_jalali($date),
    'text' => $text,
  );
}

function delete_multiple_files($files_object_array) {
  foreach ($files_object_array as $file) {
    $file->delete_image();
  }
}

function post_delete($pid) {
  $bundle               = 'post';
  $post_related_classes = array(
    'Tag' => array(
      'required',
    ),
    'Image',
  );

  $db = db_connection();
  $db->beginTransaction();
  $delete = Post::delete_by_id($pid);
  foreach ($post_related_classes as $class => $options) {
    if (is_int($class)) {
      $class   = $options;
      $options = array();
    }
    if (!class_exists($class)) {
      continue;
    }
//            Image::delete_by_entity_id()
    $delete = $class::delete_by_entity_id($pid, $bundle);

    if (!$delete && in_array('required', $options)) {
      break;
    }
    else {
      $delete = 1;
    }

  }

  if ($delete) {
    $images = Image::find_by_entity_id($pid, $bundle);
    delete_multiple_files($images);
    $db->commit();
  }
  else {
    $db->rollBack();
  }

  return $delete;
}

//////////////// debugging helper functions
function vdp($data, $die = TRUE, $file = __FILE__, $line = __LINE__) {
  $style = '<style>';
  $style .= '.debug-vdp {';
  $style .= 'font-size: .7em;font-family: arial, tahoma;background-color: #000;color:#fff;';
  $style .= 'padding: 3px 5px;';
  $style .= '}';
  $style .= '.debug-vdp .file, .debug-vdp .line {';
  $style .= 'color: red;';
  $style .= '}';
  $style .= '</style>';
  var_dump($data);
  echo $style;
  echo '<div class="debug-vdp">';
  echo 'FILE <span class="file">' . $file . '</span> ON LINE <span class="line">' . $line . '</span>';
  echo '</div>';

  if ($die) {
    die;
  }
}