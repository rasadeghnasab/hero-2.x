<?php
/**
 * Date: 11/18/14
 * Time: 7:49 PM
 */
// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected
// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for Windows, / for Unix)
defined('DS') ? NULL : define('DS', DIRECTORY_SEPARATOR);
$Document_root = preg_replace('/[\\\\\/]/i', DS, $_SERVER['DOCUMENT_ROOT']);
defined('SITE_ROOT') ? NULL : define('SITE_ROOT', $Document_root);
//defined('SITE_ROOT') ? NULL : define('SITE_ROOT', preg_replace('/(\\includes.*)/','', __DIR__));

/*defined('SITE_ROOT') ? null :
    define('SITE_ROOT', 'c:'.DS.'wamp'.DS.'www');*/
defined('LIB_PATH') ? NULL : define('LIB_PATH', SITE_ROOT . DS . 'includes' . DS);
defined('CONFIG_PATH') ? NULL : define('CONFIG_PATH', LIB_PATH. 'configs' . DS);
//
defined('CLASS_PATH') ? NULL : define('CLASS_PATH', SITE_ROOT . 'includes'.DS.'classes'.DS);
defined('LAYOUT_PATH') ? NULL : define('LAYOUT_PATH', SITE_ROOT . 'views' . DS . 'layouts_sections' . DS);
defined('VIEWS') ? NULL : define('VIEWS', SITE_ROOT . 'views' . DS);
// load local testing file
//var_dump($_SERVER);
$php_config = ($_SERVER['SERVER_ADDR'] == '127.0.0.1' && $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ) ?
  'local_testing_configs.php' : 'server_configs.php';

// load php config files
require_once(CONFIG_PATH . $php_config);


// load config file first
require_once(CONFIG_PATH . 'config.php');
// load basic functions next so that everything after can use them
require_once(LIB_PATH . 'db_connection.php');
require_once(CLASS_PATH . 'FluentPDO.php');
require_once(LIB_PATH . 'calendar.php');
require_once(LIB_PATH . 'functions.php');
require_once(VIEWS . 'template.php');
require_once(CLASS_PATH . 'Acl.php');
require_once(LIB_PATH . 'validation_functions.php');
require_once(LIB_PATH . 'session.php');
require_once(LIB_PATH . 'permissions.php');
