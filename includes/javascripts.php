<?php
$static_js = array(
  //'jquery_2.0.1',
  'jquery-1.11.3.min',
  'jquery-migrate-1.2.1',
  'plugins/jquery.autogrowtextarea.min',
  'plugins/pwt-date',
  'plugins/pwt-datepicker.min',
  'plugins/chosen.jquery.min',
  'plugins/time.ago',
  'plugins/venobox.min',
  'plugins/masonry.pkgd.min',
  'plugins/imagesloaded.pkg.min',
  'plugins/jquery.bracket.min',
  'plugins/jquery.qtip.min',
//  'file_upload/jquery.ui.widget',
//  'file_upload/jquery.iframe-transport',
//  'file_upload/jquery.fileupload',
//  'signUpScript',
//  'jquery.caption.min',
//  'sweet-alert.min',
//  'underscore',
//  'bootstrap.min',
//  'form.min',
//  'AnimOnScroll',
//  'classie',
//  'notificationFx',
//  'pageControl',
  'validate/rw.jquery.validate',
  'semantic/semantic.min',
  'script',
);

$js = isset($js) ? $js : NULL;
$hrefs = css_js_static_dynamic_combine($static_js, $js);


echo add_js($hrefs, '_js/');?>
