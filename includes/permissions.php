<?php
/**
 * User: Ramin
 * Date: 3/24/15
 * Time: 1:37 PM
 */
$permission = array(
    'permission_name' => isset($permission_name) ? $permission_name : NULL,
    'group_id' => isset($_SESSION['jobID']) ? $_SESSION['jobID'] : NULL,
    'user_id' => isset($cUser->user_id) ? $cUser->user_id : NULL,
);
if (!$Acl->check_permission($permission['permission_name'], $permission['group_id'], $permission['user_id'])) {
  $errors[] = ($Acl->role_message == '') ? 'شما اجازه دسترسی به این بخش را ندارید'  : $Acl->role_message;
  $error_message = errors_to_ul($errors);
  if (isset($return) && $return) {
    $result['success'] = FALSE;
    $result['message'] = $error_message;
    echo json_encode($result);
  }
  else require_once(SITE_ROOT.DS.'errors\403.php');
  die();
}