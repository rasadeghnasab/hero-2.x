<?php
/**
 * session.php
 * User: Ramin
 * Date: 11/18/14
 */
//$some_name = session_name("localhost");
//session_set_cookie_params(0, '/', '.localhost');
session_start();
// var_dump($_SESSION);die;
// var_dump($_SERVER);die;

$referer  = isset($_SERVER['HTTP_REFERER']) ? htmlentities($_SERVER['HTTP_REFERER']) : BASEPATH;
$back_btn = $referer == BASEPATH ? t('خانه', FALSE) : t('بازگشت', FALSE);
$back_btn = "<a href='{$referer}' class='btn btn_orange back_btn icon back_black_icon hidden-print'>{$back_btn}</a>";

if ($logged_in = check_login()) {
  $cUser = user_load($_SESSION['userID']);
  //  $cUser = user_load(70);
  check_notifications(); // check for notifications or SESSION['set notif_time'];
  //  var_dump($_SESSION);die;
  //  var_dump($cUser);die;
}
function login($username, $password = "") {
  $user_id = check_entered_password($username, $password);
  if (!$user_id) {
    return FALSE;
  }
  $user = find_person_by_id_for_login($user_id);

  return login_set_sessions($user);
}

function check_entered_password($username, $password) {
  $query = 'SELECT `uid`, `password` FROM `user_pass` ';
  $query .= 'WHERE username = :username OR email = :username ';
  $query .= 'LIMIT 1';
  $values = array(':username' => $username);
  $result = sql_query_executor($query, $values);
  if ($result == FALSE) {
    return FALSE;
  }
  $result = $result->fetch(PDO::FETCH_NUM);
  if (!password_check($password, $result[1])) {
    return FALSE;
  }

  return array_shift($result); //send first element of array (which is userID);
}

function login_set_sessions($user) {
  if (count($user) > 0) {
    foreach ($user as $property => $value) {
      $_SESSION[$property] = $value;
    }
    $_SESSION['picurl'] = $_SESSION['picurl'] ? '/profilePics/' . $_SESSION['picurl'] : '/_css/img/user-64.png';

    return TRUE;
  }

  return FALSE;
}

function logout() {
  /*unset($cUser->user_id);
  unset($_SESSION['sportID']);
  unset($_SESSION['jobID'],$_SESSION['jobValue']);
//        unset($_SESSION['chl']);
  unset($_SESSION['notif_time'],$_SESSION['picurl']);
  unset($_SESSION['student_reg'],$_SESSION['event_reg'],$_SESSION['new_event']);
  unset($_SESSION['birth_date'],$_SESSION['stateResult'],$_SESSION['fieldResult']);
  unset($_SESSION['teName'],$_SESSION['first_name']);*/
  session_destroy();
  redirect_to(BASEPATH);
}

function check_login() {

  if (isset($_SESSION['userID'])) {
    return TRUE;
  }
  /*  elseif($_COOKIE['cookie']){
      // if cookie is set we can retrieve all user information from data base
      first check the cookie with cookie table from databse
      if both are same
      then retrieve person information
      and set it to sessions;
  }
  */
  else {
    return FALSE;
  }
}

/**
 * set a message to SESSION['message'] and we can use it on other pages <br/>
 *
 * @param string|array $message
 * @param null   $class
 * @param bool   $semantic
 *
 * @return mixed : TRUE if a message set to SESSION and a message if we do not specified any $msg parameter.
 * <br/>
 * <br/>
 * see check_message;
 * @internal param string $msg
 *
 */
function message($message = "", $class = NULL, $semantic = FALSE) {
  if (!empty($message)) {
    // this is "set message"
    //    $_SESSION['message'] = $msg;
    $_SESSION['message'][] = errors_to_ul($message, $class, $semantic);

    return TRUE;
  }
  else {
    // this is "get message"
    return check_message();
  }
}

function check_message() {
  // Is there a message stored in the session?
  $message = '';
  if (isset($_SESSION['message'])) {
    // Add it as an attribute and erase the stored version
    if (is_array($_SESSION['message'])) {
      $message = implode(' ', $_SESSION['message']);
    }
    else {
      $message = $_SESSION['message'];
    }
    unset($_SESSION['message']);
  }

  return $message;
}

///////////////////////// find person for login
function find_person_by_id_for_login($userID) {
  $personSelect = 'SELECT `user`.`userID`, `first_name` ,`picurl`, `infotable`.`jobID` ';
  $personSelect .= ', `job`.`jobValue` ,`infotable`.`sportID`, `user`.`stateID` ';
  $personSelect .= 'FROM `user` ';
  $personSelect .= 'LEFT JOIN `userpic` ON `user`.`userID` = `userpic`.`userID` ';
  $personSelect .= 'JOIN `infotable` ON `user`.`userID` = `infotable`.`userID` ';
  $personSelect .= 'JOIN `job` ON `infotable`.`jobID` = `job`.`jobID` ';
  $personSelect .= 'WHERE `user`.`userID` = :userID ';
  //        $personSelect .= 'LIMIT 1';
  $value  = array(':userID' => $userID);
  $person = sql_query_executor($personSelect, $value);

  return $person->fetch(PDO::FETCH_ASSOC);
}
