<?php
/**
 * Date: 10/22/14
 * Time: 2:10 PM
 */
$errors    = array();
$translate = array(
  'username'        => 'نام کاربری',
  'password'        => 'رمز عبور',
  'oldpassword'     => 'رمز عبور فعلی',
  'newpassword'     => 'رمز عبور جدید',
  'repassword'      => 'تکرار رمز عبور',
  'name'            => 'نام',
  'email'           => 'آدرس ایمیل',
  'reemail'         => 'تکرار آدرس ایمیل',
  'gender'          => 'جنسیت',
  'year'            => 'سال',
  'day'             => 'روز',
  'month'           => 'ماه',
  'date'            => 'تاریخ',
  'phone'           => 'تلفن ثابت',
  'mobile'          => 'شماره موبایل',
  'address'         => 'آدرس',
  'weight'          => 'وزن',
  'length'          => 'قد',
  'first_name'      => 'نام',
  'fname'           => 'نام',
  'last_name'       => 'نام خانوادگی',
  'lname'           => 'نام خانوادگی',
  'national_code'   => 'کد ملی',
  'birth_date'      => 'تاریخ تولد',
  'statename'       => 'استان',
  'countyname'      => 'شهرستان',
  'countyresult'    => 'استان و شهرستان',
  'description'     => 'توضیحات',
  'event_title'     => 'عنوان',
  'title'           => 'عنوان',
  'searchtext'      => 'متن جستجو',
  'superior'        => 'نام استاد',
  'gym_address'     => 'آدرس باشگاه',
  'gym_name'        => 'نام باشگاه',
  'settlement_type' => 'نوع تسویه',
);
function has_presence($value) {
  return isset($value) && trim($value) != '';
}

/**
 * @param            $required_fields
 * @param string     $method
 * @param bool|FALSE $one_value_check
 *
 * @param bool       $no_error_set
 *
 * @return bool
 * @author Ramin Sadegh nasab
 */
function validate_presences($required_fields, $method = 'POST', $one_value_check = FALSE, $no_error_set = FALSE) {
  global $errors;
  global $translate;
  $output          = TRUE;
  $method          = strtolower($method);
  $required_fields = is_string($required_fields) ? (array) $required_fields : $required_fields;
  $variables       = getSuperGlobal($method);
  //  var_dump($variables);die;
  foreach ($required_fields as $field) {
    /*if ($method === 'post') {
      if (isset($_POST[$field])) {
        $value = trim($_POST[$field]);
      }
      else return FALSE;
    }
    elseif ($method === 'get') {
      if (isset($_GET[$field])) {
        $value = trim($_GET[$field]);
      }
      else return FALSE;
    }
    else return FALSE;
    */
    if ((!isset($variables[$field]) || !has_presence($variables[$field]))) {
      //      if ($one_value_check) $output = FALSE;
      $output = FALSE;
      if (!$no_error_set) {
        $field = strtolower($field);
        if (array_key_exists($field, $translate)) {
          $errors[$field] = $translate[$field] . " نمیتواند خالی باشد ";
        }
        else {
          $errors[$field] = "فیلد نمیتواند خالی باشد";
        }
      }
    }
  }

  return $output;
}

//////////////////////////////// string length ////////////////////////////////
function minAndMaxCheck($string, $max = 5000, $min = 0) {
  /* length check */
  $length = mb_strlen($string, 'utf-8');
  if ($min < $length && $length < $max) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

function has_max_length($value, $max) {
  return mb_strlen($value, 'utf-8') <= $max;
}

function has_min_length($value, $min) {
  return mb_strlen($value, 'utf-8') >= $min;
}

/**
 * @param array  $fields_with_max_lengths An array in the form 'field' => (int) max_length
 * @param array  $fields_with_min_lengths An array in the form 'field' => (int) min_length
 * @param string $method                  One of the Get or Post methods
 *
 * @return bool
 */
function validate_input_lengths($fields_with_max_lengths = array(), $fields_with_min_lengths = array(), $method = "POST") {
  global $errors;
  global $translate;
  $result = TRUE;
  $method = strtolower($method);

  // check fields max length
  if (!empty($fields_with_max_lengths)) {
    $values = get_specified_superGlobals_from_array(array_keys($fields_with_max_lengths), $method);
    foreach ($fields_with_max_lengths as $field => $max) {
      if (isset($values[$field])) {
        $value = trim($values[$field]);
      }
      else {
        $result = FALSE;
        continue;
      }

      if (!has_max_length($value, $max)) {
        $result = FALSE;
        if (array_key_exists(strtolower($field), $translate)) {
          $errors[$field] = $translate[$field] . " نباشد بیشتر از " . $max . " حرف باشد";
        }
        else {
          $errors[$field] = 'طول فیلد نباید  بیشتر از ' . $max . ' حرف باشد';
        }
      }
    }
  }

  // check fields min length
  if (!empty($fields_with_min_lengths)) {
    $values = get_specified_superGlobals_from_array(array_keys($fields_with_min_lengths), $method);
    foreach ($fields_with_min_lengths as $field => $min) {
      if (isset($values[$field])) {
        $value = trim($values[$field]);
      }
      else {
        $result = FALSE;
        continue;
      }
      if (!has_min_length($value, $min)) {
        $result = FALSE;
        if (array_key_exists(strtolower($field), $translate)) {
          $errors[$field] = $translate[$field] . " نباشد کمتر از " . $min . " حرف باشد";
        }
        else {
          $errors[$field] = 'طول فیلد نباید  کمتر از ' . $min . ' حرف باشد';
        }
      }
    }
  }

  return $result;
}

// * inclusion in a set
function has_inclusion_in($value, $set) {
  return in_array($value, $set);
}

function pair_equality_check($pairs, $method = 'POST') {
  global $errors;
  global $translate;
  $result    = FALSE;
  $method    = strtoupper($method);
  $variables = getSuperGlobal($method);
  foreach ($pairs as $key => $value) {
    if (!isset($variables[$key], $variables[$value])) {
      return FALSE;
    }
    if (!two_entry_equality($variables[$key], $variables[$value])) {
      $result = FALSE;
      if (array_key_exists(strtolower($key), $translate)) {
        $errors[$key] = 'مقدار ' . $translate[$key] . " و " . $translate[$value] . ' با یکدیگر برابر نیستند';
      }
      else {
        $errors[$key] = "مقدار دو فیلد باید برابر باشد";
      }
    }
  }

  return $result;
}

////////////////////////////// image functions
function image_upload_check($locations, $input_name, $file_size) {
  /*
   * check image parameters
   * @input_name string :name that is specified to the input element in HTML code
   * @location string :address for upload directory
   * @file_size int : file size in Mb
   * */
  global $errors;
  $file_size_byte = $file_size * 1024 * 1024;
  $tmp_loc        = $_FILES[$input_name]['tmp_name'];
  /* dar 3 if paiini b in dalil error gharar dade nashode ke, in error ha mamolan tavasote
   * karbarane mamoli tolid nmishavad va ehtemalan tavasote hacker ijad shode ast, pas dalili
   * nmibinam k peyghame khataii namayesh dade shavad */
  if (!is_array($_FILES) || empty($_FILES)) {
    return FALSE;
  }
  if (!is_uploaded_file($_FILES[$input_name]['tmp_name'])) {
    return FALSE;
  }
  if (!isset($_FILES[$input_name]['name']) || empty($_FILES[$input_name]['name'])) {
    return FALSE;
  }
  if (is_array($locations)) {
    foreach ($locations as $location) {
      if (!is_dir($location)) {
        array_push($errors, 'محل مشخص شده وجود ندارد');

        //                    array_push($log_errors, 'پوشه '.$location. ' وجود ندارد');
        return FALSE;
      }
    }
  }
  else {
    if (!is_dir($locations)) {
      array_push($errors, 'محل مشخص شده وجود ندارد');

      //                array_push($log_errors, 'پوشه '.$locations. ' وجود ندارد');
      return FALSE;
    }
  }
  if ($_FILES[$input_name]['size'] > $file_size_byte) {
    //            echo json_encode($file_size.' '.$_FILES[$input_name]['size']);die();
    array_push($errors, 'حجم فایل نباید بیشتر از ' . $file_size . ' مگابایت باشد');
  }
  $ext_array    = array(
    'image/jpeg',
    'image/jpg',
    'image/png',
  );
  $result_array = getimagesize($tmp_loc);
  if (!in_array($result_array['mime'], $ext_array)) {
    array_push($errors, 'پسوند فایل مجاز نمی باشد');
  }

  return TRUE;
}

////////////////////////////// date check
function check_date($year, $month = NULL, $day = NULL, $mod = '/') {
  global $errors;
  $output = TRUE;
  if ($month == NULL) {
    if (!r_jcheckdate($year, $mod)) {
      $output = FALSE;
    }
  }
  else {
    if (!jcheckdate($month, $day, $year)) {
      $output = FALSE;
    }
  }
  if (!$output) {
    $errors['date'] = 'تاریخ وارد شده صحیح نمی باشد';
  }

  return $output;
}

/////////////////////////////// Sanitize functions
function remove_bad_characters($text) {
  $pattern     = '/([\\\|\,\$\^\?\+\(\)\[\]\{\}\*<>&~`!;:#%=+\'\"\/@])|(^[\s])([\s&]{2})/i';
  $replacement = '';

  return preg_replace($pattern, $replacement, $text);
}

function Clean($input) {
  // this function is used to clean inputs from codes and character  and withe spaces
  $find    = array(
    '\\',
    "\0",
    "\n",
    "\r",
    "'",
    '"',
    "\x1a",
    '@',
    '#',
    '$',
    '%',
    '^',
    '&',
    '*',
    '(',
    ')',
    '~',
    '`',
    '-',
    '_',
    '=',
    '/',
    '+',
    '[',
    ']',
    '{',
    '}',
    ';',
    ':',
    '>',
    '.',
    ',',
    '<',
    '?',
    '!',
  );
  $replace = array(" ");
  $input   = str_replace($find, $replace, $input);
  $input   = preg_replace('/\s+/', '', $input); // For ALL white spaces
  return $input;
}

function user_input_clean($method = 'POST') {
  $method      = strtoupper($method);
  $superGlobal = getSuperGlobal($method);
  foreach ($superGlobal as $key => $user_input) {
    $superGlobal[$key] = remove_bad_characters($user_input);
  }

  return $superGlobal;
}

/////////////////////////////// errors
/**
 * @param            $errors
 * @param string     $class
 * @param bool|FALSE $semantic
 *
 * @return bool|string
 * @author Ramin Sadegh nasab
 */
function errors_to_ul($errors, $class = '', $semantic = FALSE) {
  $message = FALSE;
  if (is_string($errors)) {
    $errors = array($errors);
  }
  if (!empty($errors) && is_array($errors)) {
    $message = '';
    if ($semantic) {
      $div_class = $class;
      $ul_class  = 'list';
      $message .= "<div class='ui message {$div_class}'>";
      $message .= '<i class="close icon"></i>';
    }
    else {
      $ul_class  = $class;
      $div_class = NULL;
    }
    $message .= "<ul class='{$ul_class}'>";
    foreach ($errors as $error) {
      $message .= "<li>{$error}</li>";
    }
    $message .= "</ul>";
    $message .= "</div>";
  }

  return $message;
}

/////////////////////////// validate
function check_url($url) {
  //if it's valid it return's the URL else FALSE
  return filter_var($url, FILTER_VALIDATE_URL);
}