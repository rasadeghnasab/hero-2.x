<?php
require_once(__DIR__ . '/includes/initial.php');
if ($logged_in || isset($_POST['start'])) {
  require_once(__DIR__ . '/post/wall.php');
}
else {
  require_once(__DIR__ . '/HomePage.php');
}
