<?php
require_once(__DIR__ . '/../includes/initial.php');
$refer = "";
/*if (isset($_SERVER['HTTP_REFERER'])) {
  $refer = $_SERVER['HTTP_REFERER'];
  echo $refer;
}*/ // to see form which page user referred to skip from Brute force attack
$required_fields         = array(
    'username',
    'password',
);
$fields_with_max_lengths = array(
    'username' => 60,
    'password' => 60,
);
$fields_with_min_length  = array(
    'username' => 5,
    'password' => 8,
);

validate_presences($required_fields);
validate_input_lengths($fields_with_max_lengths, $fields_with_min_length);

if (errors_to_ul($errors)) {
  message($errors, 'login-message'); // set $_session['message'] = $message;
  redirect_to(BASEPATH);
}
if (login($_POST['username'], $_POST['password'])) {
  check_notifications(TRUE);
  redirect_to(BASEPATH);
}
else {
  $errors[] = ' نام کاربری یا رمز عبور اشتباه می باشد، لطفا دوباره امتحان کنید';
  message($errors, 'sticky_message');
  redirect_to(BASEPATH);
}
