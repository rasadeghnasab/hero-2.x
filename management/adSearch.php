<?php
/*
 * sportMaster Advance search
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
///*
$required_fields         = array(
    'stateResult',
    'countyResult',
    'fieldResult',
    'branchResult',
    'level',
);
$fields_with_max_lengths = array(
    'stateResult' => 30,
    'fieldResult' => 30
);
validate_presences($required_fields);
validate_input_lengths($fields_with_max_lengths);
if (!empty($errors)) {
  $errors['errors'] = TRUE;
  echo json_encode($errors);
  die();
}
//*/
pagination_variables();
$stateName = $_POST['stateResult'];
$stateID   = $_POST['countyResult'];
$field     = $_POST['fieldResult'];
$sportID   = $_SESSION['sportID'];
$level     = $_POST['level'] > $_SESSION['jobID'] ? $_SESSION['jobID'] : $_POST['level'];
$result = select_all_people($page, $limit, $stateName, $stateID, $field, $sportID, $level);
echo json_encode($result);
