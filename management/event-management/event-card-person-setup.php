<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/29/2015
 * Time: 12:15 AM
 */
/* permission validation */
$permission_name = 'management';
$return          = TRUE;
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
$done = FALSE;
$data = NULL;

if (isset($_POST['eid'])) {
  $required_fields = array(
    'eid',
    'evt_job_card',
    'uid',
  );

  validate_presences($required_fields);
  if (!$message = errors_to_ul($errors, 'new_errors')) {
    $attributes = array(
      'uid'  => $_POST['uid'],
      'ejid' => $_POST['evt_job_card'],
      'eid'  => $_POST['eid'],
    );
    $event_card = eventCard::make($attributes);
    $done       = $event_card->save();
  }
  if ($done) {
  //echo json_encode(card_load($done));die;
    $data = node_view('event-card', card_load($done), 'node', 'full', 'function');
  }
}
elseif (isset($_POST['id'])) {
  $id   = $_POST['id'];
  $card = eventCard::find_by_id($id);
//  echo json_encode($card);die;
  $done = $card ? $card->delete() : FALSE;
  $data = 'پاک شد';
}
if ($done) {
  $message = isset($id) ? t('کارت مورد نظر با موفقیت حذف شد', FALSE) : t('کارت مورد نظر با موفقیت افزوده شد', FALSE);
}
else {
  $message = $message ? $message : t('عملیات مورد نظر ناموفق بود لطفا دوباره امتحان نمایید', FALSE);
}

$result = array(
  'done'    => $done,
  'data'    => $data,
  'message' => array(
    'text'  => $message,
    'class' => $done ? 'success_message' : 'error_message',
  ),
);

echo json_encode($result);
