<?php
//var_dump($_GET);die;
/**
 * User: Ramin Sadegh nasab
 * Date: 8/28/2015
 * Time: 5:41 PM
 *
 *
 * This page show all cards for a special event (by event_id) and we can create
 * and remove cards directly from here
 *
 * @sid = sport_id
 * @eid = event_id
 *
 * <p> htaccess file is in /management/.htaccess</p>
 * */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
$sid = isset($_SESSION['sportID'], $cUser->user_id) && $_SESSION['jobValue'] >= 15 ? $_SESSION['sportID'] : redirect_to(BASEPATH);
$eid = $_GET['eid'];
$cid = $_GET['cid'];
?>
<?php
$title = t('کارت مسابقات', FALSE);
layout_template('head_section');
layout_template('header');
?>

  <div class="content">
    <h1><?php t('کارت مسابقات'); ?></h1>
    <?php layout_template('sport_master_navigation'); ?>
    <?php
    $sub_menu = array(
      "/management/events/{$eid}/event-management" => 'مدیریت رویداد',
      '/management/setting/event-jobs'             => 'سمت های رویدادها',
    );
    //    echo sub_menu_creator($sub_menu_array, 'subMenu split_bottom');
    echo sub_menu_creator($sub_menu, 'child');
    echo $back_btn;
    // list all cards
    ?>
    <div class="ui cards">
      <!--    <div class="ui segment">-->
      <?php $nodes = card_load($cid); ?>
      <?php node_view('event-card', $nodes, 'node', 'full'); ?>
    </div>
    <div class="print btn btn_black btn-block hidden-print">چاپ</div>
  </div>


<?php layout_template('foot/foot_section'); ?>