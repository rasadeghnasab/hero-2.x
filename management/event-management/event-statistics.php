<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/28/2015
 * Time: 5:41 PM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
$sid = isset($_SESSION['sportID'], $cUser->user_id) && $_SESSION['jobValue'] >= 15 ? $_SESSION['sportID'] : redirect_to(BASEPATH);
$eid = $_GET['eid'];
// TODO:: create filter for this page
$event = Event::find_by_id_sid($eid, $sid);
$users = $event->event_users();
?>
<?php
$title_text = "آمار رویداد {$event->evtName}";
$title = t($title_text, FALSE);
layout_template('head_section');
layout_template('header');
?>
<div class="content">
  <h1><?= $title_text; ?></h1>
  <div class="ui segment">
    <?php
    if($users) {
      node_view_multiple('user', $users, 'node/user', 'management');
    }
    else {
      no_node_view('user', 'file');
    }

    ?>
  </div>
  <?= $back_btn; ?>
</div>
