<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/28/2015
 * Time: 6:21 PM
 *
 * This page is for event management
 *
 * <p> htaccess file is in /management/.htaccess</p>
 * */

/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');

$sid = isset($_SESSION['sportID'], $cUser->user_id) && $_SESSION['jobValue'] >= 15 ? $_SESSION['sportID'] : redirect_to(BASEPATH);
$eid = $_GET['eid'];

$pdo_event = event_select($eid, $sid);
$event = event_fetch_pdo($pdo_event);
?>
<?php
$title = t('مدیریت مسابقه', FALSE);
layout_template('head_section');
layout_template('header');
?>

<div class="content">
  <h1><?= t('مدیریت مسابقه', FALSE); ?></h1>
  <?php layout_template('sport_master_navigation'); ?>
  <div class="ui cards blue">
    <a href = "<?php print BASEPATH; ?>management/events/<?= $eid; ?>/event-management/cards/" class = "card">
      <div class = "content">
        <div class = "header">
          <i class = "icon list"></i> <?php print t('کارت مسابقات'); ?>
        </div>
        <div class = "description">
          <?php print t('مدیریت (ایجاد و حذف) کارت مسابقات برای مسئولین مسابقه'); ?>
        </div>
      </div>
    </a>
    <a href = "<?php print BASEPATH; ?>management/events/<?= $eid; ?>/event-management/statistics/" class = "card">
      <div class = "content">
        <div class = "header">
          <i class = "icon list"></i> <?php print t('آمار مسابقات'); ?>
        </div>
        <div class = "description">
          <?php print t('آمار تعداد ثبت نام ها، و سایر آمار در مورد مورد مسابقه'); ?>
        </div>
      </div>
    </a>
  </div>
</div>

<?php layout_template('foot/foot_section'); ?>