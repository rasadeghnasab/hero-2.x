<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/28/2015
 * Time: 10:56 PM
 */

require_once(__DIR__ . '/../../includes/initial.php');
$stateName = isset($_POST['stateResult']) ? $_POST['stateResult'] : NULL;
/*if jobValue > 15 that means user is sport master, but if else it mean user is state master */

$stateID = ($_SESSION['jobValue'] >= 15) ? $_POST['countyResult'] : $cUser->stateid;
$jobID   = isset($_POST['level']) && ($_POST['level'] < $_SESSION['jobID'] || $_POST['level'] == 'all') ? $_POST['level'] : $_SESSION['jobID'];
$name    = validate_presences(array('name'), 'POST', TRUE) ? '%' . trim($_POST['name']) . '%' : NULL;
$field   = validate_presences(array('field'), 'POST', TRUE) ? trim($sport['field']) : NULL;;
$sid = ($_SESSION['jobValue'] >= 25) ? NULL : $_SESSION['sportID'];

$columns_and_values = array(
  '`state`.`stateID` = :stateID'                     => $stateID,
  '`stateName` = :stateName'                         => $stateName,
  '`sport`.`id` = :sportID'                     => $sid,
  '`field` = :field'                                 => $field,
  'CONCAT_WS(" ", first_name, last_name) LIKE :name' => $name,
  '`infotable`.`jobID` = :jobID'                     => $jobID,
);

$persons = select_all_people($columns_and_values);
$results = array();
while ($person = $persons->fetch(PDO::FETCH_ASSOC)) {
  $results['name']  = $person['name'];
  $results['value'] = $person['userID'];
}
$result = array(
  'success' => TRUE,
  'results' => $results
);

echo json_encode($results);