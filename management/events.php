<?php
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$sportID = isset($_SESSION['sportID'], $cUser->user_id) && $_SESSION['jobValue'] >= 15 ? $_SESSION['sportID'] : redirect_to(BASEPATH);
//    $stateID = isset($_SESSION['stateID']) ? $_SESSION['stateID'] : null; baraye namayande ostan
$sport = sportName_by_sportID($sportID);
$_GET['limit'] = 6;
$url = '/management/events?page=';
?>
<?php
layout_template('head_section');
layout_template('header');
?>

<div class = "content">
  <?php layout_template('sport_master_navigation'); ?>
  <?php
  echo $back_btn;
  $sub_menu_array = array(
      '/management/events/' => 'رویدادها',
      '/events/create/' => 'رویداد جدید +',
      '/management/setting/events-type-edit' => 'انواع رویدادها',
      '/management/setting/event-jobs' => 'سمت های رویدادها',
  );
  echo sub_menu_creator($sub_menu_array, 'child');
  $events = event_select(NULL, $sportID);
  $pagination = pagination_function($url);
  $fetched_events = event_fetch_pdo($events);
  ?>
  <div class="ui segment">
  <?php node_view_multiple('event', $fetched_events); ?>
  </div>
  <div class = "paginate"><?php echo $pagination; ?></div>
</div>

<?php layout_template('foot/foot_section'); ?>