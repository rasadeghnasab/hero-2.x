<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/11/2015
 * Time: 11:18 PM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');

if($days_left = Settlement::request_not_allowed()) {
  message("شما تا {$days_left} روز دیگر نمیتوانید در خواست تسویه ارسال نمایید", 'info', TRUE);
  redirect_to('/management/financial/requests');
}

layout_template('head_section');
layout_template('header');
?>
<div class="content mangement">
  <?php
  layout_template('sport_master_navigation');
  $sub_menu_array = array(
    '/management/financial/requests'  => 'درخواست ها و دریافت ها',
    //    '/management/financial/not-settlement' => 'دریافت نشده ها',
    '/management/financial/events'    => 'رویدادها',
    '/management/financial/registers' => 'ثبت نام ها',
    '/management/financial/apply'     => 'درخواست تسویه',
  );
  echo sub_menu_creator($sub_menu_array, 'child');
  ?>

  <?php
  $show_content      = FALSE;
  $pays_type_options = array(
    'conditions' => array(
      'pay.status'    => Pay::PAID,
      'pay.bundle'    => 'event',
      'pay.their > ?' => 0,
      'pay.sport_id'  => current($cUser->sid),
    )
  );
  if ($pays_total = Pay::pays_types($pays_type_options)) {
    $pays_total   = array_shift($pays_total);
    $pays         = Pay::dynamic_select($pays_type_options);
    $show_content = TRUE;
  }
  else {
    $output = '';
    $output .= '<div class="ui center aligned secondary segment">';
    $output .= 'هم اکنون شما هیچ طلبی از سیستم ندارید.';
    $output .= '</div>'; // ui segment
    echo $output;
  }
  ?>
  <?php if ($show_content): ?>
    <?php node_view('financial', $pays_total, 'node', 'total-their'); ?>
    <div class="ui segment">
      <div class="ui items divided ">
        <?php node_view_multiple('financial', $pays, 'node', 'event-register-their'); ?>
      </div>
    </div>
  <?php endif; ?>
</div>
<?php layout_template('foot/foot_section'); ?>
