<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/11/2015
 * Time: 8:35 PM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
layout_template('head_section');
layout_template('header');
?>
  <div class="content mangement">
    <?php
    layout_template('sport_master_navigation');
    $sub_menu_array = array(
      '/management/financial/requests'  => 'درخواست ها و دریافت ها',
      //    '/management/financial/not-settlement' => 'دریافت نشده ها',
      '/management/financial/events'    => 'رویدادها',
      '/management/financial/registers' => 'ثبت نام ها',
      '/management/financial/apply'     => 'درخواست تسویه',
    );
    echo sub_menu_creator($sub_menu_array, 'child');
    ?>
    <h1>امور مالی</h1>

    <?php
    ///*
    $options = array(
      'conditions' => array(
//        'user_id'  => $cUser->user_id,
        'sport_id' => current($cUser->sid),
      ),
//      'order_by' => array(
//        'financial_settlement.created DESC'
//      ),
    );
    //  */
    //  var_dump(Pay::pays_types());
    $nodes = Settlement::dynamic_select($options);
//        var_dump($nodes);
//        die;
    ?>
<!--    <div class="ui list">-->
    <table class="ui table">
      <thead>
      <tr>
        <th>نوع تراکنش</th>
        <th>مبلغ</th>
        <th>توسط</th>
        <th>تاریخ</th>
      </tr>
      </thead>
      <tbody>
      <?php node_view_multiple('financial', $nodes, 'node'); ?>
      </tbody>
    </table>
<!--    </div>-->

  </div>


<?php layout_template('foot/foot_section'); ?>