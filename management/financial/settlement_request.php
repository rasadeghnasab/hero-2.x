<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/11/2015
 * Time: 11:18 PM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');

if($days_left = Settlement::request_not_allowed()) {
  message("شما تا {$days_left} روز دیگر نمیتوانید در خواست تسویه ارسال نمایید", 'info', TRUE);
  redirect_to('/management/financial/requests');
}

if (isset($_POST['submit'])) {

  $requirement_fields = array(
    'settlement_type',
  );

  if (validate_presences($requirement_fields)) {
    //var_dump($_POST);
    //die;
    $settlement_type = $_POST['settlement_type'];
    switch ($settlement_type) {
      case 'events':
        /*
        db->transaction;
          change all event records for this sport to settlement_calc where pay.status = PAID
          calculate total_count
          insert that in financial_settlement table
        db->commit;
        */
        $done = array();
        $data_base = db_connection();
        $data_base->beginTransaction();

        // insert a record in financial_settlement table
        $pays_type_options    = array(
          'conditions' => array(
            'pay.status'    => Pay::PAID,
            'pay.their > ?' => 0,
            'pay.bundle'    => 'event',
            'pay.sport_id'  => current($cUser->sid),
          ),
        );

        // calculate total cost
        $total_price = Pay::pays_types($pays_type_options);
        if($total_price != FALSE) {
          $total_price = array_shift($total_price);
          $total_price = $total_price->their_total;
        }
        else {
          $done[] = FALSE;
        }

        $done[] = Settlement::make_insert(array('bundle' => Settlement::REQUEST, 'amount' => $total_price, 'settlement_till' => date('Y-m-d H:i:s')));

        $fQuery = db_update()
          ->update('pay')
          ->set('status', Pay::SETTLEMENT_CALC)
          ->where('status', Pay::PAID)
          ->where('bundle', 'event')
          ->where('sport_id', current($cUser->sid))
        ;
        $done[] = Pay::executable_query($fQuery);

        !in_array(FALSE, $done) ? $data_base->commit() : $data_base->rollBack();
        break;
      case 'registered':
        /*
        db->transaction;
          change all registered records for this sport to settlement_calc where pay.status = PAID
          calculate total_count
          insert that in financial_settlement table
        db->commit;
        */
        $done = array();
        $data_base = db_connection();
        $data_base->beginTransaction();

        // insert a record in financial_settlement table
        $pays_type_options    = array(
          'conditions' => array(
            'pay.status'    => Pay::PAID,
            'pay.their > ?' => 0,
            'pay.bundle'    => 'register',
            'pay.sport_id'  => current($cUser->sid),
          ),
        );

        // calculate total cost
        $total_price = Pay::pays_types($pays_type_options);
        if($total_price != FALSE) {
          $total_price = array_shift($total_price);
          $total_price = $total_price->their_total;
        }
        else {
          $done[] = FALSE;
        }

        $done[] = Settlement::make_insert(array('bundle' => Settlement::REQUEST, 'amount' => $total_price, 'settlement_till' => date('Y-m-d H:i:s')));

        $fQuery = db_update()
          ->update('pay')
          ->set('status', Pay::SETTLEMENT_CALC)
          ->where('status', Pay::PAID)
          ->where('bundle', 'register')
          ->where('sport_id', current($cUser->sid))
        ;
        $done[] = Pay::executable_query($fQuery);

        !in_array(FALSE, $done) ? $data_base->commit() : $data_base->rollBack();

        break;
      case 'all':
        /*
        db->transaction;
          change all records for this sport to settlement_calc where pay.status = PAID;
          calculate total_count
          insert that in financial_settlement table
        db->commit;
        */
        $done = array();
        $data_base = db_connection();
        $data_base->beginTransaction();

        // insert a record in financial_settlement table
        $pays_type_options    = array(
          'conditions' => array(
            'pay.status'    => Pay::PAID,
            'pay.their > ?' => 0,
            'pay.sport_id'  => current($cUser->sid),
          ),
        );

        // calculate total cost
        $total_price = Pay::pays_types($pays_type_options);
        if($total_price != FALSE) {
          $total_price = array_shift($total_price);
          $total_price = $total_price->their_total;
        }
        else {
          $done[] = FALSE;
        }

        $done[] = Settlement::make_insert(array('bundle' => Settlement::REQUEST, 'amount' => $total_price, 'settlement_till' => date('Y-m-d H:i:s')));

        $fQuery = db_update()
          ->update('pay')
          ->set('status', Pay::SETTLEMENT_CALC)
          ->where('status', Pay::PAID)
          ->where('sport_id', current($cUser->sid))
        ;
        $done[] = Pay::executable_query($fQuery);

        !in_array(FALSE, $done) ? $data_base->commit() : $data_base->rollBack();

        break;
    }

    if(in_array(FALSE, $done)) {
      $result_message = 'درخواست شما قابل اجرا نبود لطفا دوباره امتجان نمایید';
      $class = 'error';
    }
    else {
      $result_message = 'درخواست شما با موفقیت انجام شد.';
      $class = 'success';
    }
    message(t($result_message, FALSE), NULL, TRUE);
  }
  else {
    // if errors has any content we show theme
    message($errors, NULL, TRUE);
  }

}
layout_template('head_section');
layout_template('header');
?>
<div class="content mangement">
  <?php
  layout_template('sport_master_navigation');
  $sub_menu_array = array(
    '/management/financial/requests'  => 'درخواست ها و دریافت ها',
    //    '/management/financial/not-settlement' => 'دریافت نشده ها',
    '/management/financial/events'    => 'رویدادها',
    '/management/financial/registers' => 'ثبت نام ها',
    '/management/financial/apply'     => 'درخواست تسویه',
  );
  echo sub_menu_creator($sub_menu_array, 'child');
  ?>

  <?php
  $show_content               = FALSE;
  $register_total             = $event_total = array();
  $pays_type_options          = array(
    'conditions' => array(
      'pay.status'    => Pay::PAID,
      'pay.their > ?' => 0,
      'pay.sport_id'  => current($cUser->sid),
    ),
  );
  $pays_type_register_options = array(
    'conditions' => array(
      'pay.status'    => Pay::PAID,
      'pay.their > ?' => 0,
      'pay.bundle'    => 'register',
      'pay.sport_id'  => current($cUser->sid),
    ),
  );
  $pays_type_event_options    = array(
    'conditions' => array(
      'pay.status'    => Pay::PAID,
      'pay.their > ?' => 0,
      'pay.bundle'    => 'event',
      'pay.sport_id'  => current($cUser->sid),
    ),
  );

  if ($pays_total = Pay::pays_types($pays_type_options)) {
    $pays_total = array_shift($pays_total);

    $register_total = Pay::pays_types($pays_type_register_options);
    if($register_total) {
      $register_total = array_shift($register_total);
    }

    $event_total = Pay::pays_types($pays_type_event_options);
    if($event_total) {
      $event_total = array_shift($event_total);
    }

    $pays         = Pay::dynamic_select($pays_type_options);
    $show_content = TRUE;
  }
  else {
    $output = '';
    $output .= '<div class="ui center aligned secondary segment">';
    $output .= 'هم اکنون شما هیچ طلبی از سیستم ندارید.';
    $output .= '</div>'; // ui segment
    echo $output;
  }
  ?>
  <?php if ($show_content): ?>
    <?php $pays_total->bundle = NULL; ?>
    <?php node_view('financial', $pays_total, 'node', 'total-their'); ?>
    <?php node_view('financial', $register_total, 'node', 'total-their'); ?>
    <?php node_view('financial', $event_total, 'node', 'total-their'); ?>
    <div class="ui segment red right aligned">
      <?php node_form_template('settlement-apply', $_POST); ?>
    </div>
    <div class="ui segment blue">
      <div class="ui items divided ">
        <?php node_view_multiple('financial', $pays, 'node', 'event-register-their'); ?>
      </div>
    </div>
  <?php endif; ?>
</div>
<?php layout_template('foot/foot_section'); ?>
