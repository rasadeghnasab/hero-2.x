<?php
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
$sportID = $_SESSION['sportID'];
//    $stateID = isset($_SESSION['stateID']) ? $_SESSION['stateID'] : null; //baraye namayande ostan
$sport = sportName_by_sportID($sportID);
pagination_variables();
if (isset($_POST['submit'])) {
  $required_fields = array(
      'stateResult',
      'countyResult',
      'level',
  );
  $fields_with_max_lengths = array(
      'stateResult' => 30,
      'name' => 30
  );
  validate_presences($required_fields);
  validate_input_lengths($fields_with_max_lengths);
  if (!$message = errors_to_ul($errors, 'new_errors')) {
    $stateName = $_POST['stateResult'];
    /*if jobValue > 15 that means user is sport master, but if else it mean user is state master */
    $stateID = ($_SESSION['jobValue'] >= 15) ? $_POST['countyResult'] : $_SESSION['stateID'];
    /**/
    $jobID = ($_POST['level'] < $_SESSION['jobID'] || $_POST['level'] == 'all') ? $_POST['level'] : $_SESSION['jobID'];
    $name  = validate_presences(array('name'), 'POST', TRUE) ? '%' . trim($_POST['name']) . '%' : NULL;
    $field = validate_presences(array('field'), 'POST', TRUE) ? trim($sport['field']) : NULL;;
    $columns_and_values = array(
        '`state`.`stateID` = :stateID' => $stateID,
        '`stateName` = :stateName' => $stateName,
        '`sport`.`id` = :sportID' => $sportID,
        '`field` = :field' => $field,
        'CONCAT_WS(" ", first_name, last_name) LIKE :name' => $name,
        '`infotable`.`jobID` = :jobID' => $jobID
    );
    $persons            = echo_person_for_management(select_all_people($columns_and_values));
    $pagination         = pagination_function();
    $result             = $pagination . $persons . $pagination;
    $result             = $result != '' ? $result : '<div class="person_management new_errors">داده ای یافت نشد</div>';
    echo json_encode($result);
    die();
  }
} else {
  $columns_and_values = array('`infotable`.`sportID` = :sportID' => $sportID);
  $result             = echo_person_for_management(select_all_people($columns_and_values));
}
$pagination = pagination_function();
?>

<?php $title = 'پنل مدیریت';
layout_template('head_section');
layout_template('header');
?>

  <div class = "content management">
    <?php layout_template('sport_master_navigation'); ?>
    <div class = "panel">
      <?php echo $back_btn; ?>
      <div class = "new_row">
        <form action = "" id="management_form" method = "POST">
          <div class = "new_inner_row">
            <label for = "stateResult">استان</label>
            <?php
            $state_attrs = array(
                'id' => 'stateResult',
                'name' => 'stateResult',
                'class' => 'chosen-select chosen-rtl',
                'data-ajax-dest' => 'management/sportMasterSCFB.php'
            );
            $state_option = array('همه' => 'all');
            echo select_element(state_distinct_select($sportID), 'stateName', 'stateName', $state_attrs, '', $state_option); ?>
            <label for = "county">شهر </label>
            <?php
            $county_attrs = array(
                'id' => 'countyResult',
                'name' => 'countyResult',
                'class' => 'chosen-select chosen-rtl',
                'data-ajax-dest' => 'management/sportMasterSCFB.php'
            );
            echo select_element(stateID_county_by_stateName($stateName), 'stateID', 'countyName', $county_attrs, '', $state_option); ?>
            <label for = "field">رشته </label>
            <select id = "field" name = "fieldResult" class = "chosen-select chosen-rtl" required = "required" disabled>
              <option
                  value = "<?php echo htmlentities($sport['field']); ?>"><?php echo htmlentities($sport['field']); ?></option>
            </select>
            <label for = "branch">سبک </label>
            <select id = "branch" name = "branchResult" class = "chosen-select chosen-rtl"
                    required = "required" disabled>
              <option value = "<?php echo $sportID; ?>"><?php echo $sport['branch']; ?></option>
            </select>
            <label for = "superior">سمت</label>
            <?php
            $job_attrs = array(
                'id' => 'superior',
                'name' => 'level',
                'class' => 'chosen-select chosen-rtl'
            );
            echo select_element(job_select($_SESSION['jobID']), 'jobID', 'jobName', $job_attrs, '', $state_option); ?>
          </div>
          <div class = "new_inner_row">
            <label for = "name">نام - نام خانوادگی</label>
            <input id = "name" name = "name" class = "new_input" type = "text"
                   minlenght = "5" maxlength = "30" placeholder = "نام - نام خانوادگی"
                <?php if (isset($name) && !empty($name)) echo 'value="' . htmlentities($name);
                '"' ?>>
            <label for = "limit">تعداد </label>
            <select id = "limit" name = "limit" class = "chosen-select chosen-rtl">
              <?php limit_query($limit); ?>
            </select>
            <input type = "hidden" name = "page" value = "1">
            <input type = "submit"
                   class = "btn btn_orange btn_medium"
                   name = "submit"
                   id = "searchPeople"
                   value = "جستجو">
          </div>
        </form>
      </div>
    </div>
    <?php echo $pagination; ?>

    <?php echo $result; ?>
    <?php echo $pagination; ?>

  </div>
<?php layout_template('foot/foot_section');