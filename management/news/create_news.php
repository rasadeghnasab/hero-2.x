<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/22/2015
 * Time: 9:56 PM
 */
// in this page we can show last newses and link to create/update and delete theme
//var_dump($_POST);die;
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
// Make sure you are using a correct path here.
//include_once ('ckeditor/ckeditor.php');

$sportID = $_SESSION['sportID'];
if (isset($_POST['body'])) {
//  var_dump(Post::make(array(
//    'context' => $_POST['body'],
//  )));die;
  $done = News::make_insert(array(
      'body' => $_POST['body'],
    )
  );
  if($done) {
    message('خبر جدید با موفقیت ثبت شد');
    redirect_to(BASEPATH.'management/news/');
  }
  $message = errors_to_ul('ثبت خبر موفقیت آمیز نبود، لطفا دوباره امتحان نمایید', 'error_message');
}
//    $stateID = isset($_SESSION['stateID']) ? $_SESSION['stateID'] : null; //baraye namayande ostan
$sport = sportName_by_sportID($sportID);
pagination_variables();
?>
<?php $title = 'پنل مدیریت';
layout_template('head_section');
layout_template('header');
?>

<div class="content management">
  <?php
  layout_template('sport_master_navigation');
  // load last newses
  ?>
  <h1>ثبت خبر جدید</h1>
  <form action="" method="post">
    <div class="new_row">
<!--      <input id="fileupload" type="file" name="files[]" data-url="--><?php //echo BASEPATH;?><!--" multiple>-->
        <textarea class="ckeditor" name="body" id="" placeholder="متن خبر خود را در این قسمت وارد نمایید" cols="30" rows="10"><?php echo form_values('body','value'); ?></textarea>
    </div>
    <input type="submit" class="btn btn_blue" value="ثبت خبر"/>
  </form>
</div>

<?php layout_template('foot/foot_section', array('js' => array('add' => array('ckeditor/ckeditor')))); ?>
