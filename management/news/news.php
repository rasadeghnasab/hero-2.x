<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/22/2015
 * Time: 9:40 PM
 */
// in this page we can show last newses and link to create/update and delete theme
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
$sport_id = $_SESSION['sportID'];
$user_id = $cUser->user_id;
//    $stateID = isset($_SESSION['stateID']) ? $_SESSION['stateID'] : null; //baraye namayande ostan
//$sport = sportName_by_sportID($sportID);
?>
<?php $title = 'پنل مدیریت';
layout_template('head_section');
layout_template('header');
?>

<div class="content">
  <?php
  layout_template('sport_master_navigation');
  $url = '/management/news';
  $sub_menu_array = array(
    '/management/news/create' => '<span class="glyphicon glyphicon-plus-sign"><span> افزودن ',
  );
  echo sub_menu_creator($sub_menu_array, 'child');
  // load last newses
  $newses = News::news_select(array($sport_id), $user_id);
  $count = array_pop($newses);
  $pagination = pagination_function($url, 3, $count);
  ?>
  <div class="new_row">
    <?php
    echo $pagination;
    echo News::newses_html($newses);
    echo $pagination;
    ?>
  </div>
</div>
<?php layout_template('foot/foot_section'); ?>
