<?php
/**
 * Date: 3/1/15
 * Time: 10:37 PM
 */
/*if user is not logged in, will redirect to homepage */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
/* check to see if user logged in or not and if, set $sport_id from his/her $_SESSION['sportID'] */
$sportID    = $_SESSION['sportID'];
$sport_name = sportName_by_sportID($sportID);
$sport_name = $sport_name['field'] . ' ' . $sport_name['branch'];
if (isset($_POST['sport_description'])) {
    $result = array();
    // validation sport_description
    $required_fields         = array('sport_description');
    $fields_with_max_lengths = array(
            'sport_description' => 3000,
    );

    validate_presences($required_fields);
    validate_input_lengths($fields_with_max_lengths);

    if (!$result['message'] = errors_to_ul($errors)) {
        // update `sport`.`sport_description`
        $columns_values = array('sport_description' => $_POST['sport_description']);
        $where          = array('id' => $sportID);
        $update_result  = sport_update($columns_values, $where);
        // just return true or false
        $result['done']    = $update_result ? TRUE : FALSE;
        $result['message'] = $result ? NULL : 'عملیات ناموفق بود لطفا دوباره سعی نمایید';
        echo json_encode($result);
        die();
    }
    else {
        $result['done'] = FALSE;
        echo json_encode($result);
        die();
    }
}
// html contents
$title = 'تنظیمات ورزش ' . $sport_name;
layout_template('head_section');
layout_template('header');
// html contents
?>
    <div class="content">
        <?php
        echo $back_btn;
        layout_template('sport_master_navigation');
        $sub_menu_array = array(
                '/management/setting/description'      => 'تاریخچه',
                '/management/setting/events-type-edit' => 'انواع رویدادها',
                '/management/setting/belts'            => 'کمربندها',
                '/management/setting/logo'             => 'آرم رشته',
                '/management/setting/event-jobs'       => 'سمت های رویدادها',
        );
        //
        echo sub_menu_creator($sub_menu_array, 'child');
        ?>
        <div class="new_row">
            <div class="new_inner_row">
                <?php
                /*help statement (?) goes here */
                $help = 'در این قسمت میتوانید توضیحاتی در مورد ' . $sport_name . ' وارد نمایید';
                /*show help tooltip for persons how don't know anything about this page*/
                help_tooltip($help);
                ?>
                <h3 class="" for="sport_description">توضیحات (تاریخچه)
                    ورزش <?= $sport_name; ?></h3>
            </div>
            <div class="new_inner_row">
                <div id="sport_description" contentEditable="true">
                    <?php
                    /*show sport description*/
                    echo sport_description_select($sportID);
                    ?>
                </div>
            </div>
        </div>
        <button class="btn btn_orange img_middle btn_medium sport_description">
            <span class="glyphicon glyphicon-edit"></span> ویرایش
        </button>
    </div>
<?php layout_template('foot/foot_section'); ?>