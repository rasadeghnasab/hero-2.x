<?php
/**
 * User: Ramin
 * Date: 3/12/15
 * Time: 12:56 AM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
$title = 'ویرایش انواع رویدادهای ورزش';
require_once(__DIR__ . '/../../includes/initial.php');
if (isset($_POST['job_name']) || isset($_POST['id'])) {
    $result = array(
            'done'    => NULL,
            'message' => array('text', 'class'),
            'data'    => NULL,
    );
    // if !isset($_POST['evt_type_name']) that means we have an Id and no name which means we want to delete item
    if (!isset($_POST['job_name'])) {
        //    $event_job_used = Event::is_event_type_used($_POST['id']);
        // TODO:: this line must be real check
        if ($event_job_used = TRUE) {
            $result['done'] = eventJob::delete_by_id($_POST['id']);
        }
        else {
            $event_job_message = 'شما از سمت مورد نظر در حداقل یک رویداد استفاده کرده اید، دیگر امکان حذف آن وجود نخواهد داشت';
        }
    }
    else {
        $id         = isset($_POST['id']) ? $_POST['id'] : NULL;
        $attributes = array(
                'sid'  => $_SESSION['sportID'],
                'name' => $_POST['job_name'],
        );
        if ($id == NULL) {
            $event_job = eventJob::make($attributes);
        }
        else {
            $event_job = eventJob::find_by_id($id);
            if ($event_job) {
                $event_job->name = $_POST['job_name'];
            }
        }
        //    echo json_encode($event_job);die;
        $result['done'] = $event_job ? $event_job->save() : FALSE;
        if ($event_job->id && $result['done']) {
            $result['data'] = node_view('event-job', $event_job, 'node', 'edit', 'function');
        }
    }
    $event_job_message = isset($event_job_message) ? $event_job_message : NULL;
    if ($result['done']) {
        $result['message']['text']  = $event_job_message ? $event_job_message : 'عملیات با موفقیت انجام شد';
        $result['message']['class'] = 'success_message';
    }
    else {
        $result['message']['text']  = $event_job_message ? $event_job_message : 'لطفا دوباره امتحان نمایید';
        $result['message']['class'] = 'error_message';
    }
    echo json_encode($result);
    die();
}
layout_template('head_section');
layout_template('header');
?>
    <div class="content">
        <?php
        layout_template('sport_master_navigation');
        $sub_menu_array = array(
                '/management/setting/description'      => 'تاریخچه',
                '/management/setting/events-type-edit' => 'انواع رویدادها',
                '/management/setting/belts'            => 'کمربندها',
                '/management/setting/logo'             => 'آرم رشته',
                '/management/setting/event-jobs'       => 'سمت های رویدادها',
        );
        //
        echo sub_menu_creator($sub_menu_array, 'child');
        ?>
        <h1><?php t('سمت های قابل انتصاب در رویدادها'); ?></h1>
        <?php node_form_template('event-jobs'); ?>

        <?php
        //    $evt_type = array(
        //      'name'  => 'evt_type',
        //      'id'    => 'evt_type',
        //      'class' => 'chosen_evt_type chosen-rtl new_normal_input asterisk'
        //    );
        $event_jobs = eventJob::find_by_id_sid(NULL, $_SESSION['sportID']);

        if ($event_jobs) {
            node_view_multiple('event-job', $event_jobs, 'node', 'edit');
        }
        ?>
        <?php echo $back_btn; ?>
    </div>
<?php layout_template('foot/foot_section'); ?>