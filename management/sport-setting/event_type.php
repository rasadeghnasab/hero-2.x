<?php
/**
 * User: Ramin
 * Date: 3/12/15
 * Time: 12:56 AM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
$title = 'ویرایش انواع رویدادهای ورزش';
require_once(__DIR__ . '/../../includes/initial.php');
if (isset($_POST['evt_type_name']) || isset($_POST['id'])) {
  $result = array(
    'done'    => NULL,
    'message' => array('text', 'class'),
    'data'    => NULL,
  );
  // if !isset($_POST['evt_type_name']) that means we have an Id and no name which means we want to delete item
  if (!isset($_POST['evt_type_name'])) {
    $event_type_used = Event::is_event_type_used($_POST['id']);
    if(empty($event_type_used)) {
      $result['done'] = eventType::delete_by_id($_POST['id']);
    }
    else {
      $event_type_message = 'شما از نوع رویداد مورد نظر در حداقل یک رویداد استفاده کرده اید، دیگر امکان حذف آن وجود نخواهد داشت';
    }
  }
  else {
    $has_weight = isset($_POST['weight_req']) ? 1 : 0;
    $id = isset($_POST['id']) ? $_POST['id'] : NULL;
    $attributes = array(
      'sport_id'      => $_SESSION['sportID'],
      'evt_type_name' => $_POST['evt_type_name'],
      'has_weight'    => $has_weight
    );
    if ($id == NULL) {
      $event_type = eventType::make($attributes);
    }
    else {
      $event_type = eventType::find_by_id($id);
      if ($event_type) {
        $event_type->evt_type_name = $_POST['evt_type_name'];
        $event_type->has_weight = $has_weight;
      }
    }
    $result['done'] = $event_type ? $event_type->save() : FALSE;
    if ($event_type->id && $result['done']) {
      $result['data'] = $event_type->edit_box();
    }
  }
  $event_type_message = isset($event_type_message) ? $event_type_message : NULL;
  if ($result['done']) {
    $result['message']['text'] = $event_type_message ? $event_type_message : 'عملیات با موفقیت انجام شد';
    $result['message']['class'] = 'success_message';
  }
  else {
    $result['message']['text'] = $event_type_message ? $event_type_message : 'لطفا دوباره امتحان نمایید';
    $result['message']['class'] = 'error_message';
  }
  echo json_encode($result);
  die();
}
layout_template('head_section');
layout_template('header');
?>
  <div class="content">
    <?php
    layout_template('sport_master_navigation');
    $sub_menu_array = array(
      '/management/setting/description'      => 'تاریخچه',
      '/management/setting/events-type-edit' => 'انواع رویدادها',
      '/management/setting/belts'            => 'کمربندها',
      '/management/setting/logo'             => 'آرم رشته',
      '/management/setting/event-jobs' => 'سمت های رویدادها',
);
    //
    echo sub_menu_creator($sub_menu_array, 'child');
    ?>
    <h1>ویرایش نوع رویداد</h1>
    <?php node_form_template('event-type'); ?>


    <?php
    //    $evt_type = array(
    //      'name'  => 'evt_type',
    //      'id'    => 'evt_type',
    //      'class' => 'chosen_evt_type chosen-rtl new_normal_input asterisk'
    //    );
    $event_types = eventType::find_by_sport_id($_SESSION['sportID']);
    foreach ($event_types as $evt_type) {
      echo $evt_type->edit_box();
    }
    ?>
    <?php echo $back_btn; ?>
  </div>
<?php layout_template('foot/foot_section'); ?>