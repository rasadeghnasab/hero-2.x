<?php
/**
 * User: Ramin
 * Date: 3/12/15
 * Time: 12:56 AM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
$title = 'ویرایش انواع رویدادهای ورزش';
require_once(__DIR__ . '/../../includes/initial.php');
if (isset($_POST['belt_name']) || isset($_POST['id'])) {
  $result = array(
      'done'    => NULL,
      'message' => array('text', 'class'),
      'data'    => NULL,
  );
  if (!isset($_POST['belt_name'])) {
    $result['done'] = Belt::delete_by_id($_POST['id']);
  }
  else {
    $id             = isset($_POST['id']) ? $_POST['id'] : NULL;
    $attributes = array(
      'sportID' => $_SESSION['sportID'],
      'beltName' => $_POST['belt_name'],
      'beltValue' => 1,
      'reqTime' => 1,
      'beltID' => $id,
    );
    $belt       = Belt::make($attributes);
    $result['done'] = $belt->save();
    if($belt->beltID && $result['done']) {
      $result['data'] = $belt->edit_box();
    }
  }
  if ($result['done']) {
    $result['message']['text']  = 'عملیات با موفقیت انجام شد';
    $result['message']['class'] = 'success_message';
  }
  else {
    $result['message']['text']  = 'لطفا دوباره امتحان نمایید';
    $result['message']['class'] = 'error_message';
  }
  echo json_encode($result);
  die();
}
layout_template('head_section');
layout_template('header');
?>
  <div class = "content">
    <?php
    // help must goes here
    echo $back_btn;
    layout_template('sport_master_navigation');
    $sub_menu_array = array(
        '/management/setting/description'      => 'تاریخچه',
        '/management/setting/events-type-edit' => 'انواع رویدادها',
        '/management/setting/belts' => 'کمربندها',
        '/management/setting/logo' => 'آرم رشته',
        '/management/setting/event-jobs' => 'سمت های رویدادها',
    );
    //
    echo sub_menu_creator($sub_menu_array, 'child');
    ?>
    <h1>ویرایش کمربند ها</h1>
    <fieldset class = "split_bottom new_row">
      <legend>
        <span class="glyphicon glyphicon-plus-sign"></span>
        <span>افزودن کمربند</span>
      </legend>
      <form action = "management/sport-setting/sport_belts.php" id="new_belt" method="post">
        <input type = "text" class = "new_input" name = "belt_name" placeholder="رنگ کمربند"/>
        <!--        <div class="new_row">-->
        <input type = "submit" class="btn btn_green" value="ثبت" />
        <!--        </div>-->
      </form>
    </fieldset>

    <?php
    //    $evt_type = array(
    //      'name'  => 'evt_type',
    //      'id'    => 'evt_type',
    //      'class' => 'chosen_evt_type chosen-rtl new_normal_input asterisk'
    //    );
    $belts = Belt::belts_by_sport_id($_SESSION['sportID']);
    foreach($belts as $belt) {
      echo $belt->edit_box();
    }
    ?>
  </div>
<?php layout_template('foot/foot_section'); ?>