<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/3/2015
 * Time: 2:10 AM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
$title = 'تنظیمات لوگو رشته';
require_once(__DIR__ . '/../../includes/initial.php');
$logo_src = Image::sport_logo_load(current($cUser->sid))->create_src();
layout_template('head_section');
layout_template('header');
?>
  <div class="content">
    <?php
    // help must goes here
    echo $back_btn;
    layout_template('sport_master_navigation');
    $sub_menu_array = array(
      '/management/setting/description'      => 'تاریخچه',
      '/management/setting/events-type-edit' => 'انواع رویداد',
      '/management/setting/belts'            => 'کمربندها',
      '/management/setting/logo'             => 'آرم رشته',
      '/management/setting/event-jobs'       => 'سمت های رویدادها',
    );
    echo sub_menu_creator($sub_menu_array, 'child');
    ?>
    <h1>تغییر لوگو رشته</h1>

    <fieldset class="ui segment red">
      <form action="image_handle/sport_logo.php" id="sport_logo" method="post" enctype="multipart/form-data">
        <div class="">
          <input type="file" class="new_input" name="sport_logo"/>
          <input type="submit" class="btn btn_green" value="تغییر تصویر"/>
          <img class="ui image medium bordered floated left sport-logo-image" src="<?= $logo_src; ?>" alt="">
        </div>
      </form>
    </fieldset>
  </div>

<?php layout_template('foot/foot_section'); ?>