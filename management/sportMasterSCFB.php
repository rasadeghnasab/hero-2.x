<?php
/**
 * select State, County, Field AND Branch for sportMaster
 * Date: 10/15/14
 * Time: 8:42 PM
 */
/* permission validation */
$permission_name = 'management';
/* permission validation */
require_once(__DIR__ . '/../includes/initial.php');
if (isset($_POST['stateName']) && !isset($_POST['stateID'])) {
  $stateName = $_POST['stateName'];
  if (!empty($stateName)) {
    if ($stateName == 'all') {
      echo '<option value ="all">همه</option>';
    }
    else {
      echo select_element(stateID_county_by_stateName($stateName), 'stateID', 'countyName', NULL, NULL, array('همه' => 'all'));
    }
  }
  // agar parameter haye state va county ersal shode bashand
}
else {
  if (isset($_POST['stateName'], $_POST['stateID']) && !isset($_POST['sportName']) && !isset
      ($_POST['sportID']) && !isset ($_POST['jobID'])
  ) {
    $stateID   = $_POST['stateID'];
    $stateName = $_POST['stateName'];
    if (!empty($stateID) && !empty($stateName)) {
      if ($stateID == 'all' || $stateName == 'all') {
        if ($stateName == 'all') {
          echo '<option value ="all">همه</option>';
          echo sport_distinct_select();
        }
        else {
          if ($stateID == 'all') {
            echo '<option value ="all">همه</option>';
            echo sport_distinct_by_stateName($stateName);
          }
        }
      }
      else {
        echo '<option value ="all">همه</option>';
        echo sport_distinct_by_stateID($stateID);
      }
    }
    // agar parametr haye state,county va field ersal shode bashand
  }
  else {
    if (isset($_POST['stateID'], $_POST['stateName'], $_POST['sportName']) && !isset($_POST['sportID'])) {
      $stateID   = $_POST['stateID'];
      $sportName = $_POST['sportName'];
      $stateName = $_POST['stateName'];
      if (!empty($stateID) && !empty($sportName) && !empty($stateName)) {
        if ($sportName == 'all' || $stateID == 'all' || $stateName == 'all') {
          if ($sportName == 'all') {
            echo '<option value ="all">همه</option>';
          }
          else {
            if ($stateID == 'all' || $stateName == 'all') {
              echo '<option value ="all">همه</option>';
              echo branch_by_sportName($sportName);
            }
          }
        }
        else {
          echo '<option value ="all">همه</option>';
          echo branch_by_sportName_stateID($sportName, $stateID);
        }
      }
    }
  }
}