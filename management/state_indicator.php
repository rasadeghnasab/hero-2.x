<?php
/**
 * Created by PhpStorm.
 * User: Raminli
 * Date: 3/4/15
 * Time: 9:49 PM
 */
/*if user has no permission, will redirect to homepage */
$permission_name = 'management';
// include initial files
require_once(__DIR__ . '/../includes/initial.php');
/* check to see if user logged in or not and if, set $sport_id from his/her $_SESSION['sportID'] */
$sportID = isset($_SESSION['sportID'], $cUser->user_id) && $_SESSION['jobValue'] >= 15 ? $_SESSION['sportID'] : redirect_to(BASEPATH);
$sport_name = sportName_by_sportID($sportID);
$sport_name = $sport_name['field'] . ' ' . $sport_name['branch'];
// html contents
$title = 'مدیریت نمایندگان استان در ورزش ' . $sport_name;
layout_template('head_section');
layout_template('header');
// html contents
?>
  <div class = "content">
    <?php layout_template('sport_master_navigation'); ?>
    <?php
    // create sub menu for state_indicator page
    $sub_menu_array = array(
        '#' => 'افزودن',
        '2' => 'ویرایش',
        '3' => 'asd'
    );
    echo sub_menu_creator($sub_menu_array, 'child');
    ?>
    <?php
    /*show help tooltip for persons how don't know anything about this page*/
    help_tooltip('در این صفحه میتوانید نمایندگان استان خود را مدیریت کنید، <br/> آنها را اضافه، حذف و ویرایش نمایید.');
    ?>
    <?php
    // back button
    echo $back_btn; ?>
    <div class = "new_row">
      <p>برای اینکه یک رئیس سبک مشخص کند که یک فرد نماینده استان است یا خیر باید کارهای زیر انجام شود</p>
      <ul>
        <li>نماینده استان به عنوان شاگرد رئیس سبک در سیستم ثبت نام نماید</li>
        <li>رئیس سبک به بخش مدیریت شاگردان رفته و به آن نماینده استان درجه نمایندگی استان بدهد</li>
        <li>در همان قسمت استان و شهر آن نماینده استان را تغییر دهد</li>
      </ul>
    </div>
    <div class = "new_row">
      <div class = "new_inner_row">
      </div>
    </div>
  </div>
<?php layout_template('foot/foot_section'); ?>