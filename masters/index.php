<?php require_once(__DIR__ . '/../includes/initial.php'); ?>
<?php
$variables = array(
  'field',
  'branch',
);
//var_dump($_GET);die;
$is_valid = validate_presences($variables,'GET');
$sportID = $is_valid ? sport_id_by_sport_name($_GET['field'], $_GET['branch']) : $is_valid;
if (!$sportID) {
  require_once(__DIR__ . '/masters.php');
}
else {
//  $sportID = $_GET['sportID'];
  require_once(__DIR__ . '/masters_map.php');
}
