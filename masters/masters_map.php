<?php
/************* select sport master *************/
$columns_and_values = array(
  '`sport`.`id` = :sportID' => $sportID,
  '`infotable`.`jobID` = :jobID' => 4
);
$limit = 1;
$sport_master       = echo_person_for_management(select_all_people($columns_and_values));
$sportName = sportName_by_sportID($sportID);
$sportName = full_name($sportName['field'], $sportName['branch']);
/************* end of select sport master *************/
/************* select masters in all over the country **************/
//$title = 'اساتید و ورزش ها';
$title = "نمایندگان استان و اساتید رشته ورزشی  {$sportName}";
$help  = 'برای نمایش نمایندگان هر استان <br/> بر روی استان مورد نظر کلیک نمایید';
layout_template('head_section');
layout_template('header');
// sort persons for jquery usage, return an object which contains state_indicators
state_and_persons_by_sportID($sportID);
//die;
$state_indicator = state_person_sort(state_and_persons_by_sportID($sportID));
//die;
?>
<?php echo "<script> var map = {$state_indicator}  </script>"; ?>
  <script>console.log(map);</script>
  <div class = "content">
    <?php help_tooltip($help); ?>
    <h1>نمایندگان استان در رشته ورزشی <?php print $sportName;?></h1>
    <?= $sport_master; ?>
    <?php
    echo $back_btn;
    include('map.php');
    ?>
  </div>
<?php layout_template('foot/foot_section'); ?>