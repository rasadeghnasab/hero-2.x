<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/22/2015
 * Time: 9:40 PM
 */
//var_dump($_GET);die;
require_once(__DIR__ . '/../includes/initial.php');
//vdp('$data');
$url = '/news';
if (isset($_GET['sport_id']) && ($_GET['sport_id'] != '' && $_GET['sport_id'] != 'all')) {
  $sport_id = $_GET['sport_id'];
  $url .= '/' . $sport_id;
}
else {
  $url .= '/all';
  $sport_id = '';
}

?>
<?php $title = 'اخبار';
layout_template('head_section');
layout_template('header');
?>
<div class="content">
  <div class = "new_row split_bottom">
    <form id = "news_sport_select_form" action = "" method = "get">
      <label for = "sport_id"> اخبار در ورزش: </label>
      <?php
      $sport_attrs = array(
        'id' => 'sport_id',
        'name' => 'sport_id',
        'class' => 'chosen-select chosen-rtl new_input',
      );
      $sports_custom_options = array('همه ورزش ها' => 'all');
      $selected_value = $sport_id;
//      var_dump($selected_value);die;
      ?>
      <?php echo select_element(all_sports_name(), 'sport_id', 'sportName', $sport_attrs, $selected_value, $sports_custom_options); ?>
<!--      <input class = "btn btn_orange" type = "submit" value = "جستجو" />-->
    </form>
  </div>
  <?php
//  $_GET['limit'] = 2;
  $sport_array = array();
  if($sport_id != 0) array_push($sport_array, $sport_id);
//  var_dump($sport_array);die;
  $newses = News::news_select($sport_array);
//  die;
  $count = array_pop($newses);
  $pagination = pagination_function($url, 5, $count);
  ?>
  <div class="new_row">
  <?php
  echo $pagination;
  echo News::newses_html($newses);
  echo $pagination;
  ?>
  </div>
</div>
<?php layout_template('foot/foot_section'); ?>
