<?php
/**
 * User: Ramin
 * Date: 4/17/15
 * Time: 6:51 PM
 */
// this page do create, update, and delete actions on comment.

// here :: check permission for put a comment. TODO :: (in future we can set it)
/* permission options */
$permission_name = 'comment_insert';
$return = TRUE;
/* permission options */

require_once(__DIR__ . '/../includes/initial.php');
$required_fields = array('body', 'bundle', 'entity_id');
$max_length = array('body' => 1800);
$min_length = array('body' => 1);
$message = array();
$done = FALSE;

$result = array(
  'done'    => FALSE,
  'message' => NULL,
);
if (isset($_POST['cm_id']) && isset($_POST['action'])) {
  // update or delete comment
  $action = $_POST['action'];
  $comment = Comment::find_by_id($_POST['cm_id']);
  if ($comment->author_id == $cUser->user_id && $action != 'confirm') {
    if ($action == 'update') {
//      $comment = Comment::find_by_id($_POST['cm_id']);
      //      $comment         = Comment::make($comment->entity_id, $comment->author_id, $_POST['body'], $comment->id);
      // TODO :: MUST THINK ABOUT WHATS HAPPENING WHEN COMMENT INSERT IS DONE SUCCESSFULLY AND WHEN IT DOES NOT.
      $comment->body = $_POST['body'];
      $comment->confirm = 0;
      $done = $comment->save();
      if ($done) {
        $message = 'تغییرات با موفقیت اعمال شد پس از تایید نویسنده مطلب، نظر شما به نمایش در خواهد آمد';
        $data = $comment->comment_to_string($comment, $cUser->user_id);
      }
    }
    elseif ($action == 'remove') {
//      $comment = Comment::find_by_id($_POST['cm_id']);
      $message = 'نظر با موفقیت حذف شد';
      $done = $comment->delete();
    }
  }
  else {
    pagination_variables();
//    $post = posts_by_id(array($comment->entity_id))->fetch(PDO::FETCH_OBJ);
    $post = Post::find_by_id($comment->entity_id);
    if ($post && $action == 'confirm' && $post->user_id == $cUser->user_id) {
      $done = $comment->comment_confirm();
    }
    elseif ($post && $action == 'remove' && $post->user_id == $cUser->user_id) {
      //      $comment = Comment::find_by_id($_POST['cm_id']);
//      echo json_encode('remove');die;
      $message = 'نظر با موفقیت حذف شد';
      $done = $comment->delete();
    }
  }
}
else {
  // create comment
  validate_presences($required_fields);
  validate_input_lengths($max_length, $min_length);

  if (!$message = errors_to_ul($errors)) {
    $cm_attributes = array(
      'entity_id' => $_POST['entity_id'],
      'author_id' => $cUser->user_id,
      'bundle'    => $_POST['bundle'],
      'body'      => $_POST['body'],
    );
//    echo json_encode($cm_attributes);die;
    $post = Post::find_by_id($_POST['entity_id']);
    if($cUser->user_id == $post->user_id) {
      $cm_attributes['confirm'] = 1;
    }

    $comment = Comment::make($cm_attributes);
    $done = $comment->save();
    $id = $done ? $db->lastInsertId() : NULL;
    $data = $comment->comment_to_string($comment, $cUser->user_id);
    if ($done) {
      $message = 'نظر شما ثبت شد، پس از تایید به نمایش در خواهد آمد';
    }
  }
}
$result['done'] = $done;
$message_class = $done ? 'success_message' : 'error_message';
$data = isset($data) ? $data : NULL;
$result['message']['text'] = $message;
$result['message']['class'] = $message_class;
//$result['data'] = $data;
echo json_encode($result);