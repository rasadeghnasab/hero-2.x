<?php
    require_once(__DIR__ . '/../includes/initial.php');

if(isset($_POST['end'])) {
    if(isset($_SESSION['imgPath'])) unlink($_SESSION['imgPath']);
    unset($_SESSION['imgPath']);
    header('Location: ../homePage.php');
}
// Check file existences if any file uploaded
$message = array();
//$successMessage = array();
if (is_array($_FILES)){
    if(is_uploaded_file($_FILES['userImage']['tmp_name'])){
        if (isset($_FILES['userImage']['name'])) {
            if(!empty($_FILES['userImage']['name'])) {
                if($_FILES['userImage']['size'] < 5242880){
                    $name = remove_bad_characters($_FILES['userImage']['name']);
                    $location = '../tempimages';
                    if (is_dir( $location )) {
                        $location .= '/';
                        if(!file_exists($location.$name)){
                            $tmp_loc = $_FILES['userImage']['tmp_name'];

                        // Check actual MIME type. no file except jpg and jpeg and png file can't uploaded
                        // ************* We must check validate file size, file extension and file name.
                        $ext_array = array('image/jpeg','image/jpg','image/png');
                        $result_array = getimagesize($tmp_loc);
                            if(in_array($result_array['mime'], $ext_array)) {
                                if(isset($_SESSION['imgPath'])) unlink($_SESSION['imgPath']);
                                if(move_uploaded_file($tmp_loc, $location.$name)){

                                    /* inja taghire andaze */
                                    if(resize_image($location.$name, 640, 480)){

                                        $message['success'] = $location.$name;
                                        $_SESSION['imgPath'] = $location.$name;

                                    } else $message['error'] = 'عملیات ناموفق';
                                } else $message['error'] = 'بارگذاری ناموفق';
                            } else $message['error'] = 'پسوند نامعتبر';
                        } else {
                                $message['success'] = $location.$name;
                                $_SESSION['imgPath'] = $location.$name;
                        }
                    } else $message['error'] = 'چنین پوشه ای وجود ندارد';
                } else $message['error'] = 'حجم فایل زیاد است';
            } else $message['error'] = 'لطفا یک فایل انتخاب نمایید';
        } else $message['error'] = 'لطفا یک فایل انتخاب نمایید';
    }
}
echo json_encode($message);
?>