<?php
/**
 * User: Ramin
 * Date: 4/21/15
 * Time: 10:27 PM
 */
/* permission options */
$permission_name = 'post_like';
$return          = TRUE;
/* permission options */
require_once(__DIR__ . '/../includes/initial.php');
$message        = array();
$done           = FALSE;
$id             = FALSE;
$result         = array(
  'done'    => FALSE,
  'message' => NULL,
  'data'    => '',
);
$required_field = array(
  'entity_id',
  'bundle',
);
$max_length     = array(
  'entity_id' => 12,
  'bundle'    => 255,
);
$post_id = NULL;
if (validate_presences($required_field) && validate_input_lengths($max_length)) {
  $post_id = $_POST['entity_id'];
  if ($like = Like::is_liked_by_person($_POST['entity_id'], $cUser->user_id, $_POST['bundle'])) {
    $done = $like->delete();
  }
  else {
    $like_attributes = array(
      'entity_id' => $_POST['entity_id'],
      'author_id' => $cUser->user_id,
      'bundle' => $_POST['bundle'],
    );
    $like = Like::make($like_attributes);
    $done = $like->save();
    $id   = $done ? $db->lastInsertId() : FALSE;
  }
}
//$likes_number = Like::find_likes_on($post_id);
$result['done'] = $done;
//$result['data']['text'] = $id ? 'Unlike' : 'Like';
//$result['data']['likes'] = $likes_number;
$result['data'] = post_like_part($post_id, TRUE);
//$result['data'] = $id;
echo json_encode($result);