<?php
require_once(__DIR__ . '/../includes/initial.php');
$post_id = isset($_GET['id']) ? $_GET['id'] : '1';
layout_template('head_section');
layout_template('header');
?>
<div class="content">
  <?php if ($Acl->check_permission('post_add')) { node_form_template('post'); } ?>
  <?php node_view('post', Post::find_by_id($post_id)); ?>
</div>
<?php layout_template('foot/foot_section'); ?>