<?php
/*
 * Date: 9/11/14
 * Time: 9:16 PM
 */

/* permission options */
$permission_name = 'post_delete';
$return          = TRUE;
/* permission options */

require_once(__DIR__ . '/../includes/initial.php');
$done = isset($_POST['id']) ? post_delete($_POST['id']) : FALSE;
$message = $done ?
  array('success' => t('متن مورد نظر با موفقیت حذف شد.', FALSE)) :
  array('error' => t('عملیات ناموفق بود، لطفا دوباره سعی کنید', FALSE));

echo json_encode($message);
