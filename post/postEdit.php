<?php

/* permission options */
$permission_name = 'post_edit';
$return          = TRUE;
/* permission options */
require_once(__DIR__ . '/../includes/initial.php');
$required_fields   = array(
  'id',
  'context',
);
$fields_length_min = array(
  'context' => 16,
);

validate_presences($required_fields);
validate_input_lengths(NULL, $fields_length_min);

if (!$message = errors_to_ul($errors)) {
  $id      = $_POST['id'];
  $context = trim($_POST['context']);
  settype($id, 'int');
  $post = Post::find_by_id($id);
  if($post) {
    $post->context = $context;
    $done = $post->save();
  }
  if($done) {
    $message['success'] = TRUE;
    $date = created_updated_date(NULL, date('Y-m-d H-i-s'));
    $link_tag = "<span>{$date->text}</span><span class='date_time'>{$date->date}</span>";
    $message['updated'] = $link_tag;
  }
}

echo json_encode($message);
