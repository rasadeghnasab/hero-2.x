<?php
/* permission options */
$permission_name = 'post_add';
$return          = TRUE;
/* permission options */
require_once(__DIR__ . '/../includes/initial.php');
$message         = array();
$required_fields = array(
  'mainText',
  //  'catID',
  'submit',
);
if($cUser->multi_sport) {
  $required_fields[] = 'sport';
}
/*$fields_with_max_lengths = array(
  'catID' => 4
);*/
//echo json_encode($_POST);
//die;

$post_id = $done = FALSE;
if (!validate_presences($required_fields) && (isset($_POST['catID']) && !empty($_POST['catID']))) {
  $sport_id = $cUser->multi_sport ? $_POST['sport'] : current($cUser->sid);
//  $catID   = (int) $_POST['catID'];
  $post_columns_values = array(
    'user_id'     => $cUser->user_id,
    'sport_id'       => $sport_id,
    //    'catID' => $_POST['catID'],
    'context' => $_POST['mainText'],
    'bundle' => 'post',
  );
//  echo json_encode(post_insert($post_columns_values));die();
  $db->beginTransaction();
  if ($post_id = $done = POST::make_insert($post_columns_values)) {
    foreach ($_POST['catID'] as $cat_id) {
      $tag_attributes = array(
        'entity_id' => $post_id,
        'cat_id'    => (int) $cat_id,
        'bundle'    => 'post',
      );
      if (!$done = $tag = Tag::make_insert($tag_attributes)) {
        break;
      }
    }
  }
  else {
    $message = 'عملیات ناموفق بود، لطفا دوباره سعی نمایید';
  }
  $done ? $db->commit() : $db->rollBack();
  if ($done && isset($_SESSION['imgPath'])) {
    /* TODO: register must have something like this in registerCmpl */
    $name = time() . '_' . $cUser->user_id . '.jpg';
    /* mikhastam inje y chizi ezafe konam k age file karbar jpg bod jpg basaze va age png
    bod png vali felan bikhial shodam va hame ro be jpg tabdil mikonam 1393.06.18 */
    $targetPath = SITE_ROOT . DS . 'files' . DS . 'picture' . DS . 'large' . DS . $cUser->user_id . DS . $name;
//    $targetPath = '../images/' ;
    if (rename($_SESSION['imgPath'], $targetPath)) {
      $post_image_columns_values = array(
        'bundle'     => 'post',
        'image_name' => $name,
        'entity_id'  => $post_id,
      );
      if (!Image::make_insert($post_image_columns_values)) {
        $message = 'متاسفانه متن شما بدون تصویر ثبت شد';
      }
      else {
        unset($_SESSION['imgPath']);
      }
    }
  }
//  $data['data'] = post_to_string(post_by_postID($post_id));
  $data['data'] = node_view(NULL, POST::find_by_id($post_id), NULL, 'full', 'function');
}
$data['message'] = $message;
$data['done']    = $done;
echo json_encode($data);




