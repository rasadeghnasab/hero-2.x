<?php
    require_once(__DIR__ . '/../includes/initial.php');

$message = array();
if(isset($_SESSION['id'])){
    if($_SESSION['jobVal'] >= 3) {
//        $_POST['submit'] = 'true';
        if(isset($_POST['submit'])) {
//            $_POST['title'] = 'سلام';
//            $_POST['mainText'] = ';';
//            $_POST['catID'] = 5;
            if(isset($_POST['title']) && isset($_POST['mainText'])  && isset($_POST['catID'])) {

                $title = htmlentities(trim($_POST['title']), null, 'utf-8');
                $mainText = htmlentities(trim($_POST['mainText']), null, 'utf-8');
                $catID = settype($_POST['catID'], 'int');
//                if(minAndMaxCheck($title)) echo 'yes';
//                die();
                /* empty must be double checked */
                if( minAndMaxCheck($title) && minAndMaxCheck($mainText)  && trim($catID) !== '' ) {
                    if(empty($message)){
                        $postInsertQuery = 'INSERT INTO `wall` (inserterID, sportID, catID,title, context)
                      VALUES (:inserterID,:sportID,:catID,:title, :context)';
                        $InsertStmt = $db->prepare($postInsertQuery);
                        $InsertStmt->bindParam(':inserterID', $_SESSION['id'], PDO::PARAM_INT);
                        $InsertStmt->bindParam(':sportID', $_SESSION['sportID'], PDO::PARAM_INT);
                        $InsertStmt->bindParam(':catID', $catID, PDO::PARAM_INT);
                        $InsertStmt->bindParam(':title', $title, PDO::PARAM_STR);
                        $InsertStmt->bindParam(':context', $mainText, PDO::PARAM_STR);
                        if($InsertStmt->execute()){
                            $id = $db->lastInsertId();
                            /* TODO: register must have something like this in registerCmpl */
                            if(isset($_SESSION['imgPath'])){

                                /* mikhastam inje y chizi ezafe konam k age file karbar jpg bod jpg basaze va age png
                                bod png vali felan bikhial shodam va hame ro be jpg tabdil mikonam 1393.06.18 */
                                $name = time().'_'.$_SESSION['id'].'.jpg';
                                $targetPath = POST_IMAGE_PATH.DS.$name;
                                if(rename($_SESSION['imgPath'], $targetPath)) {
                                    $imgInsertQuery = 'INSERT INTO `wallPic` (picName, postID) VALUES ( :picName, :postID)';
                                    $imgInsertStmt = $db->prepare($imgInsertQuery);
                                    $imgInsertStmt->bindParam(':picName', $name);
                                    $imgInsertStmt->bindParam(':postID', $id);
                                    $imgInsertStmt->execute();
                                    unset($_SESSION['imgPath']);
                                } else {
                                    $message['error'] = 'متاسفانه متن شما بدون تصویر ثبت شد';
                                }
                            }
                                    $lastPostSel = 'SELECT `postID`,`title`,`inserterID`, `insertDate`,`context`,`last_name`,
                            `catID` FROM `wall` JOIN `user` ON wall.inserterID = user.userID WHERE postID = :id LIMIT 1';
                                    $result = $db->prepare($lastPostSel);
                                    $result->bindParam(':id', $id, PDO::PARAM_INT);
                                    $result->execute();
                                    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
                                        $row['insertDate'] = r_gregorian_to_jalali($row['insertDate']);
                                        #-------------------------------------------------------------------------------------
                                        //                        show last inserted post category
                                        $catQuery = 'SELECT `catName` FROM `postcategory` WHERE `catID` = :catID  LIMIT 1';
                                        $catResult = $db->prepare($catQuery);
                                        $catResult->bindParam(':catID', $catID, PDO::PARAM_INT);
                                        $catResult->execute();
                                        $cat = $catResult->fetch(PDO::FETCH_NUM);
                                        $row['catName'] = $cat[0];
                                        if($_SESSION['id'] == $row['inserterID']) $row['edit'] = true; else $row['edit']
                                            = false;
                                        if(isset($targetPath)) $row['pic'] = '../images/'.$name;
                                        //                      if inserted, show last inserted post
                                        $message = $row;
                                }
                             } else {
                            $message['error'] = 'عملیات ناموفق بود، لطفا دوباره سعی نمایید';
                        }
                    }
                } else {
                    $message['error'] = 'لطفا تمامی موارد را پرکنید';
                }
            }
        } else {
            $message['error'] = 'لطفا از راه معمول به ثبت موارد بپردازید';
        }
    } else {
        $message['error'] = 'شما مجاز به ثبت نوشته نیستید';
    }
} else {
    $message['error'] = 'باید به سیستم وارد شوید';
}

if(isset($message) && !empty($message)) echo json_encode($message);;

?>


