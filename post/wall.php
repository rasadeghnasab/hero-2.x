<?php
$userID = isset($cUser->user_id) ? $cUser->user_id : NULL;
if (isset($_POST['start'])) {
  $start = $_POST['page'] = $_POST['start'];
  settype($start, 'int');
  if (trim($start) !== '' && $start >= 0) {
    $_POST['limit'] = 1;
    $page  = $start;
    if (isset($_SESSION['catID'])) {
//      $sendArray['data'] = post_to_string(post_by_catID((array) $catID));
      $sendArray['data'] = node_view_multiple('post', Post::find_by_cat_id($_SESSION['catID']), 'node', 'teaser', 'function');
    }
    elseif (isset($_SESSION['sport'])) {
//      $sportID           = $_SESSION['sport'];
//      $sendArray['data'] = post_to_string(post_by_sportID($sportID));
      $sendArray['data'] = node_view_multiple('post', Post::find_by_id_sid(NULL, $_SESSION['sport']), 'node', 'teaser', 'function');
    }
    else {
      $options = array(
        'conditions' => array(
          'bundle' => 'post',
        ),
      );
      $sendArray['data'] = node_view_multiple('post', Post::find_by_entity_id(NULL, 'post'), 'node', 'teaser', 'function');
    }
    $sendArray['start'] = $start + $limit;
    if ($sendArray['data'] == '') {
      $sendArray['noRow'] = 'خبر دیگری وجود ندارد';
    }
  }
  else {
    $sendArray['noRow'] = 'یکی از فیلدها دستکاری شده است';
  }
  echo json_encode($sendArray);
  die;
}
else {
  pagination_variables();
}
?>
<?php
$description = 'آموزش فنون هنرهای رزمی، بانک اطلاعاتی هنرهای رزمی،';
$title       = 'ورزش | بانک ورزشی | اخبار ورزشی | قهرمانان رزمی';
?>
<?php html_meta_desc_keyword_tag($description); ?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php
/*
 * if this line
 * <script> var scroll_load_permitted = true; </script>
 * exist on the top of any page, that means if we reach at the end of that page new post(s) or new(s)
 * will load */
?>
  <script> var scroll_load_permitted = true; </script>
  <!--    <ul class = "errors wallErrors"></ul>-->
  <div class="content">
    <?php
    if ($Acl->check_permission('post_add')) {
      node_form_template('post');
    }
    ?>
    <!--        <div class = "news_posts">-->
    <!-- ---------------------------------------------- -->
    <?php
    if (empty($_POST) && empty($_GET)) {
      $_GET['limit'] = 4;
      $_SESSION['catID'] = NULL;
      $_SESSION['sport'] = NULL;
//        var_dump(post_like_part(315, TRUE));
//        die;
//      var_dump(Post::select_all_desc());die;
//      var_dump(Post::find_by_entity_id(NULL, 'post'));die;
      node_view_multiple(NULL, Post::find_by_entity_id(NULL, 'post'), NULL, 'teaser');
    }
    elseif (isset($_GET['catID'])) {
      $_GET['limit'] = 10;
      node_view_multiple('post', Post::find_by_cat_id($_GET['catID']), NULL, 'teaser');
      $_SESSION['catID'] = $_GET['catID'];
    }
    elseif (isset($_GET['sport'])) {
      $_GET['limit'] = 10;
//      echo post_to_string(post_by_sportID($_GET['sport']));
      node_view_multiple('post', Post::find_by_id_sid($_GET['sport']), NULL, 'teaser');
      $_SESSION['sport'] = $_GET['sport'];
    }
    ?>
    <!-- ---------------------------------------------- -->
    <span id="more" class="new_errors"></span>
    <!--        </div>-->
    <img src='' id="waitImg" class="loading wallWait"/>
    <!--            <button class="editingBtn" style="display: hidden">button</button>-->
  </div>
  <span id="pageStart" class="hidden"><?php echo $_GET['limit'] + 1; ?></span>
<?php layout_template('foot/foot_section'); ?>