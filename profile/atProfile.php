<?php
    $complete = incomplete_atprofile($weight, $length, $foot);
    $title = htmlentities($first_name) . " " . htmlentities($last_name);
    layout_template('head_section');
?>
    <div class = "modal">
        <div class="icon close_icon modal_close"></div>
        <div class = "modal_container">
            <!--        --><?php //if ($description != ''): ?>
            <div class = "modal_description">
                <header>
                    <h1>درباره من</h1>
                    <!--                <img class="modal_header_img" src="-->
                    <?php //echo $_SESSION['picurl'];?><!--" alt=""/>-->
                </header>
                <?php $description = $description ? $description : 'متنی درباره ' . htmlentities($first_name) . ' ' . htmlentities($last_name) . ' موجود نمی باشد';
                    echo '<p>' . $description . '</p>'; ?>
            </div>
            <!--        --><?php //endif; ?>
            <div class = "modal_image"><img src = ""></div>
        </div>
    </div>

<?php layout_template('header'); ?>

    <div class = "content">
        <!--<div class="frame">-->
        <div class = "image_pulse">
            <div class = "pulse1"></div>
            <div class = "pulse2"></div>
            <img src = "<?php echo $profilePic; ?>" class = "profile_img show_modal_description">
<!--            <div class = "voidbox"></div>-->
        </div>
        <div class="new_row teNames">
            <span class = "profile_name"><?php echo htmlentities($first_name); ?></span>
            <span class = "profile_family"><?php echo htmlentities($last_name); ?></span>
            <div class="new_inner_row">
            <span class = "profile_location"> <span class="glyphicon glyphicon-map-marker"></span> <?php echo $profileLocation; ?></span>
            </div>
            <?php if ($complete && $my_profile) {
                echo "<a class=\"disProf\" href=\"/setting/\"> پروفایل شما ناقص است لطفا برای تکمیل آن اینجا کلیک کنید ! </a>";
            } ?>
        </div>
        <div class = "profile_info st_profile_info">
            <center>
                <table>
                    <tr>
                        <th>قد</th>
                        <th>وزن</th>
                        <th>سن</th>
                        <th>نیمه تخصصی</th>
                        <!--            <th>دنبال شده</th>-->
                        <th> تیم ملی</th>
                    </tr>
                    <tr>
                        <td> <?php echo $length ? (int) $length : ' ? '; ?> <label>سانتی متر</label></td>
                        <td> <?php echo $weight ? (int) $weight : ' ? '; ?>  <label>کیلوگرم</label></td>
                        <td> <?php echo (int) $profileAge; ?>  <label>سال</label></td>
                        <td> <?php echo $foot ? (int) $foot : '- '; ?> </td>
                        <!--            <td> -->
                        <?php //echo '<a href="followList.php?user='.$getUrl.'">'.$following.'</a>'; ?><!-- </td>-->
                        <td> <?php echo $nationalTeam; ?> </td>
                    </tr>
                </table>
            </center>
            <div class = "profile_field">
                <?php echo $awards; ?>
            </div>
        </div>
        <!--</div>-->
    </div>
<?php layout_template('foot/foot_section') ?>