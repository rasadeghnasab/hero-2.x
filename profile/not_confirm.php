<?php
/**
 * Date: 11/25/14
 * Time: 12:45 PM
 */
//require_once(__DIR__ . '/../includes/initial.php');
?>
<html>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php
$stateQ = 'SELECT DISTINCT `stateName` FROM `state` JOIN `place` ON `state`.`stateID` = `place`.`stateID`';
$stateResult = $db->query($stateQ);
?>
  <div class="content">
    <ul class="errors">
      <li>متاسفانه شما هنوز تایید نشده اید منتظر بمانید و یا استاد خود را تغییر
        دهید
      </li>
    </ul>
    <select id="stateResult" name="stateResult"
            data-ajax-dest="register/registerSCFB.php" required="required">
      <option value="0">انتخاب استان</option>
      <?php echo state_distinct_select(); ?>
    </select><label for="layer">استان</label>

    <select id="countyResult" name="countyResult"
            data-ajax-dest="register/registerSCFB.php" required="required">
      <option value="0">انتخاب شهر</option>

    </select><label for="countyResult">شهر</label>

    <select id="fieldResult" name="fieldResult"
            data-ajax-dest="register/registerSCFB.php"
            style="text-align:center;"
            required="required">
      <option value="0">انتخاب رشته</option>
    </select><label for="fieldResult">رشته</label>

    <select id="branchResult" name="branch"
            data-ajax-dest="/register/registerSCFB.php"
            style="text-align:center;"
            required="required">
      <option value="0">انتخاب سبک</option>
    </select><label for="branchResult">سبک</label>
  </div>
<?php layout_template('foot/foot_section'); ?>
