<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/9/15
 * Time: 9:55 PM
 */
require_once(__DIR__ . '/../includes/initial.php');
if (!isset($_GET['user_id'])) {
  $destination = $referer ? $referer : BASEPATH;
  redirect_to($destination);
}
require_once(__DIR__ . '/../profile/profile_init.php');
$user_id = $_GET['user_id'];
?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php layout_template('profile_head', $profile_variables); ?>

  <div class="content">
    <!--  --><?php //echo post_to_string_teaser(user_posts_pics($user_id)); ?>
    <?php
    //  /*
    $additional_table_join = array(
      'image' => array(
        'join_type' => 'join',
        'columns'   => array(
          'image.*',
          'post.id as post_id',
        ),
      ),
    );
    $options               = array(
      'conditions' => array(
        'user_id' => $user_id,
      ),
      'group_by'   => array(
        'post.id'
      ),
    );
    //*/
    ?>

    <?php
    $posts = Post::dynamic_select($options, $additional_table_join);
    if ($posts):
      echo '<div class="masonry_container">';
      node_view_multiple('post', $posts, 'node', 'masonry');
      echo '</div>';
    else:
      echo no_node_view('picture');
    endif;
    ?>
  </div>
<?php layout_template('foot/foot_section'); ?>
<?php

