<?php
$_GET['limit'] = 5;
if (isset($_POST['start'], $_POST['user_id'])) {
    require_once(__DIR__ . '/../includes/initial.php');
    $start   = $_POST['start'];
    $user_id = $_POST['user_id'];
    settype($start, 'int');
    if (trim($start) !== '' && $start >= 2) {
        $_POST['limit'] = 1;
        $_POST['page']  = $page = $start;

        $sendArray['data']  = node_view_multiple('post', Post::find_by_uid($user_id), 'node', 'teaser', 'function');
        $sendArray['start'] = $start + $limit;
        if ($sendArray['data'] == '') {
            $sendArray['noRow'] = 'مطلب دیگری وجود ندارد';
        }
    }
    else {
        $sendArray['noRow'] = 'یکی از فیلدها دستکاری شده است';
    }
    echo json_encode($sendArray);
    die();
}
else {
    require_once(__DIR__ . '/profile_init.php');
    pagination_variables();
    //  $posts = post_to_string(post_by_user_id($user_id));
    $posts = node_view_multiple('post', Post::find_by_uid($user_id, 'post'), 'node', 'teaser', 'function');
}
$awards          = find_person_award($user_id);
$unsorted_awards = $awards->fetchAll(PDO::FETCH_ASSOC);
//var_dump($unsorted_awards);die;
$sorted_awards = awards_sort($unsorted_awards);
$awards        = awards_to_string($sorted_awards, $show_setting);
//$profileFollowed = count_followers_following($user_id); // find number of followers for a person
?>
<?php $title = full_name($first_name, $last_name); ?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php layout_template('profile_head', $profile_variables); ?>
<script> var scroll_load_permitted = true; </script> <!-- define this line for scroll control -->
<div class="content profile-page" <?php echo 'data-user = "' . $user_id . '"'; ?>
     data-destination="profile/profile.php">
    <!--    <div class="frameTe"  -->
    <?php //echo 'data-user = ' . $user_id; ?><!-->
    <?php if ($complete && $my_profile) {
        echo "<a class=\"disProf\" href=\"/setting/\"> پروفایل شما ناقص است لطفا برای تکمیل آن اینجا کلیک کنید ! </a>";
    } ?>
    <div class="partition-1-3">
        <div class="personal_info box_shadow">
            <table>
                <tr>
                    <th>قد</th>
                    <th>وزن</th>
                    <th>سن</th>
                </tr>
                <tr>
                    <td> <?php echo $length; ?> </td>
                    <td> <?php echo $weight; ?> </td>
                    <td> <?php echo $profileAge; ?> </td>
                </tr>
            </table>
        </div>
        <?php
        //    $pics        = teacher_image_select($user_id);
        //    $pics  = user_wall_images($user_id);

        $gallery = FALSE;
        //    $gallery = node_view_multiple('image', Image::find_by_uid($user_id, 'post'), 'node', 'profile-gallery', 'function');
        if ($gallery) {
            echo '<div class = "profile_gallery col-box box_shadow">';
            echo '<h5>آخرین تصاویر</h5>';
            echo $gallery;
            echo '<div class = "clearfix"></div>';
            echo '</div>';
        }
        $columns_values = array(
                '`infotable`.`superiorID` = :superiorID' => $user_id,
                '`infotable`.`userID` != :userID '       => $user_id,
        );
        $limit          = 15;
        if ($is_teacher) {
            $students = string_person_minimum(select_all_people($columns_values));
            if ($students != '') {
                //        $students = '<h2>شاگردان</h2>'.$students.$students.$students.$students.$students.$students.$students.$students;
                $students = '<h5>آخرین شاگردان</h5>' . $students;
                $students = "<div class='col-box box_shadow'>$students</div>";
                echo $students;
            }
        }
        ?>
        <?php
        $followers_id = Follow::followers($user_id);
        //        var_dump($followers_id);
        //    die;
        if (!empty($followers_id)) {
            $persons   = find_persons_by_ids($followers_id);
            $followers = string_person_minimum($persons);
            echo '<div class="col-box box_shadow">';
            echo '<h5>آخرین دنبال کنندگان</h5>';
            echo $followers;
            echo '</div> <!-- Follower -->';
        }
        ?>

    </div>
    <!-- end partition-1-3 -->
    <!--  <div class = "left_side"></div>-->
    <!-- The left Side Filed Ends -->
    <!-- This search for number of fields and echo them -->
    <div class="partition-2-3">
        <div class="profile_field"><?php echo $awards; ?></div>
        <!--    </div>-->

        <!--    <div class = "news_posts_te">-->
        <?php if ($my_profile && $Acl->check_permission('post_add', NULL, $user_id)) {
            echo new_post_form();
        }
        ?>
        <?php
        echo $posts;
        ?>
        <!--    </div>-->
        <span id="more" class="new_errors"></span>
        <!--    <div class = "clearfix"></div>-->
    </div>
    <!--  <div class = "clearfix"></div>-->
</div>

<span id="pageStart" class="hidden"><?= $_GET['limit'] + 1; ?></span>

<?php layout_template('foot/foot_section'); ?>
