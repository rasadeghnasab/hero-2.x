<?php
require_once(__DIR__ . '/../includes/initial.php');
if (isset($_GET['user_id'])) {
    $user_id = $_GET['user_id'];
}
//is_confirm by default set to false;
$is_confirm = FALSE;
// my_profile by default set to true;
$my_profile = FALSE;
// this profile teacher shows that is this person (who looking at this profile) is teacher of this current profile?
$this_prof_teacher = FALSE; //$this_prof_teacher by default set to false
$awards = '';
if (!person_is_exist($user_id)) {
    //        die('person doesn\'t exist');
    //        $is_exists = false;
    require_once(__DIR__ . '/../errors/404.php');
    die();
}
if (isset($cUser->user_id) && ($_GET['user_id'] == $cUser->user_id)) {
    // if it is my profile
    $my_profile = TRUE;
}
if ($is_confirm = person_isConfirm_by_id($user_id)) {
    //      'not confirmed person';
    $is_confirm = TRUE;
}
if ($is_confirm) {
    if ($logged_in && !$my_profile) {
        $this_prof_teacher = is_a_teacher_student($cUser->user_id, $user_id);
    }
    $show_setting = $my_profile || $this_prof_teacher ? TRUE : FALSE;
    $following_num = count_followers_following($user_id, 'following');
    $followers_num = count_followers_following($user_id);
    $user_information = find_person_by_id($user_id); // find person information
    //        var_dump($user_information);die();
    $first_name = htmlentities($user_information['first_name']);
    $last_name = htmlentities($user_information['last_name']);
    $profileLocation = htmlentities($user_information['statename'] . " ، " . $user_information['countyname']);
    $weight = htmlentities($user_information['weight']);
    $length = htmlentities($user_information['length']);
    $profileAge = ageCalc($user_information['birth_date']);
    $profilePic = $user_information['picurl'] ? "/profilePics/" . $user_information['picurl'] : '\_css\img\user-64.png';
    $profilePic = htmlentities($profilePic);
    $description = $user_information['description'] ? nl2br(htmlentities($user_information['description'])) : ' توضیحی در مورد ' . $first_name . ' ' . $last_name . ' موجود نمیباشد ';
    $foot = $user_information['foot'] ? 'چپ' : 'راست';
    $has_class = $user_information['has_class'];
    $nationalTeam = "ثابت";
    $is_teacher = is_teacher($user_id);
    $complete = incomplete_atprofile($weight, $length, $foot);
    $cover_photo_init = Image::user_active_cover_photo($user_id);
    $cover_photo =  $cover_photo_init ? $cover_photo_init[0]->cover_photo_html('thumb',$user_id) : (new Image('cover_photo'))->cover_photo_html();
    /*if($cover_photo) {
        $cover_photo = $cover_photo[0]->image_html();
    } else {*/

//    }
//    var_dump($cover_photo);
//    die;
    //  if (is_teacher($user_id)) {
    //  } else {
    //    require_once(__DIR__ . '/atProfile.php');
    //  }
} elseif ($my_profile) { // if person is_not_confirm but this is his/her personal profile
    require_once(__DIR__ . '/not_confirm.php'); // TODO: complete this page in future
    die();
} else { // if person is_not_confirm and this is not his/her profile
    require_once(__DIR__ . '/../errors/404.php'); //TODO: must create this page
    die();
}

//if (!isset($my_profile, $logged_in)) redirect_to("profile.php"); // if anyone want to access to teProfile directly

// find number of students for a person (who is a teacher)


$profile_variables = array(
    'user_id' => $user_id,
    'this_prof_teacher' => $this_prof_teacher,
    'my_profile' => $my_profile,
    'logged_in' => $logged_in,
    'is_teacher' => $is_teacher,
    'awards' => $awards,
    'has_class' => $has_class,
    'foot' => $foot,
    'description' => $description,
    'profileLocation' => $profileLocation,
    'weight' => $weight,
    'length' => $length,
    'profileAge' => $profileAge,
    'profilePic' => $profilePic,
    'show_setting' => $show_setting,
    'following_num' => $following_num,
    'followers_num' => $followers_num,
    'first_name' => htmlentities($first_name),
    'last_name' => htmlentities($last_name),
    'cover_photo' => $cover_photo,
);

if ($is_teacher) {
    $profileStudents = student_num_for_teacher($user_id);
    $profile_variables['profileStudents'] = $profileStudents;
}

if (!$my_profile && $logged_in) {
    $userFlo = is_follower($user_id, $cUser->user_id);
    $this_prof_student = is_a_teacher_student($user_id, $cUser->user_id);
    $profile_variables['userFlo'] = $userFlo;
    $profile_variables['this_prof_student'] = $this_prof_student;
}
