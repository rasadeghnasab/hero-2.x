<?php
//var_dump($_GET);
require_once(__DIR__ . '/../includes/initial.php');
if (!isset($_GET['user_id']) || !is_teacher($_GET['user_id'])) {
  redirect_to(BASEPATH);
}
$user_id = $_GET['user_id'];
require_once(__DIR__ . '/profile_init.php');

$user = find_person_by_id($user_id);

$title = 'شاگردان استاد '.$user['first_name'].' '.$user['last_name'];
layout_template('head_section');
layout_template('header');
layout_template('profile_head', $profile_variables);
?>

<div class = "content">
  <?php
  $columns_values = array(
      '`infotable`.`superiorID` = :superiorID' => $user_id,
      '`infotable`.`userid` != :userid' => $user_id,
  );
  echo echo_person_for_management(select_all_people($columns_values));
  ?>
</div>



<?php
layout_template('foot/foot_section');
?>
