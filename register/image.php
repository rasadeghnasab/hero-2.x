<?php
    require_once(__DIR__ . '/../includes/initial.php');
    $location = SITE_ROOT.DS.'tempimages';
    $address_location = '/tempimages/';
    $message = array();
    /* validate image properties */
    image_upload_check($location, 'profilePic', 7);
    /* if errors is empty then upload the image */
    if(!$message['error'] = errors_to_ul($errors,'errors'))
    {
        /* remove bad characters from image */
        $name = remove_bad_characters($_FILES['profilePic']['name']);
        if(isset($_SESSION['profilePic'])) {
            /* split pic name in the profilePic in session */
            $path = pathinfo($_SESSION['profilePic']);
            $file = $path['filename'].'.jpg';
            /* delete previous image and unlink the session */
            unlink($location.DS.$file);
            unset($_SESSION['profilePic']);
        }
        if($result = !image_upload_to_directory($location,$name,'profilePic', 150, 150)) { $message['error2'] = $result; }
        else {
            $message['success'] = $address_location.$name;
            $_SESSION['profilePic'] = $address_location.$name;
        }
    }
    echo json_encode($message);
