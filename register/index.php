<?php require_once(__DIR__ . '/../includes/initial.php'); ?>
<?php
if (isset($_POST['submit'])) {
    $required_fields         = array(
            'fname',
            'lname',
            'day',
            'month',
            'year',
            'gender',
            'email',
            //            'reemail',
            //            'national_code',
            'username',
            'password',
            'repassword',
            'mobile',
            //            'phone',
            'countyResult',
            'branch',
            //            'address',
            'superior',
            //            'degree',
    );
    $fields_with_max_lengths = array(
            'fname'      => 20,
            'lname'      => 20,
            'day'        => 2,
            'month'      => 2,
            'year'       => 4,
            'gender'     => 1,
            'email'      => 30,
            //            'reemail'       => 30,
            //            'national_code'  => 10,
            'username'   => 60,
            'password'   => 60,
            'repassword' => 60,
            'mobile'     => 11,
            //            'phone'         => 11,
            //            'address'       => 255,
            'superior'   => 5,
            //            'degree'       => 5
    );
    $fields_with_min_length  = array(
            'fname'      => 3,
            'lname'      => 3,
            'email'      => 8,
            //            'reemail'       => 8,
            //            'national_code'         => 10,
            'username'   => 5,
            'password'   => 8,
            'repassword' => 8,
            'mobile'     => 9,
            //            'phone'         => 9,
            //            'address'       => 10,
    );
    $field_pairs             = array(
            'password' => 'repassword',
            //            'email'    => 'reemail'
    );
    validate_presences($required_fields);
    validate_input_lengths($fields_with_max_lengths, $fields_with_min_length);
    pair_equality_check($field_pairs);
    if (!$message = errors_to_ul($errors, 'errors')) {
        $day   = $_POST["day"];
        $month = $_POST["month"];
        $year  = $_POST["year"];
        //    unset($_POST['day'], $_POST['month'], $_POST['year'] /*,$_POST['reemail']*/);
        if (check_date($year, $month, $day)) {
            $_SESSION['birth_date'] = jalali_to_gregorian($year, $month, $day, '-');
        }; //validate entered date
        // encrypt password
        if (username_email_existance(remove_bad_characters($_POST['username']), remove_bad_characters($_POST['email']))) {
            $errors[] = 'نام کاربری یا ایمیل قبلا ثبت شده است';
        };
        if (!$message = errors_to_ul($errors, 'errors')) {
            $password = password_encrypt($_POST['password']);
            unset($_POST['repassword']);
            $cleaned_input = user_input_clean();
            foreach ($cleaned_input as $key => $input) {
                $_SESSION[$key] = $input;
            }
            $_SESSION['password'] = $password;
            redirect_to(BASEPATH . 'register/registerCmpl.php');
        }
    }
}
?>
<?php layout_template('head_section') ?>
<?php layout_template('header') ?>
<?php echo isset($message) && $message ? $message : '<ul class="errors"></ul>'; ?>
<form id="signup" method="post" enctype="multipart/form-data">
    <!-- progressbar -->
    <ul id="progressbar">
        <li class="active">مشخصات فردی</li>
        <li>مشخصات ورزشی</li>
        <li>تایید اطلاعات</li>
    </ul>
    <!-- field sets -->
    <fieldset>
        <h2 class="fs-title">مشخصات فردی</h2>
        <!--        <h3 class="fs-subtitle">This is step 1</h3>-->
        <!--        <div id="profImg" class="SignUpProfPic">-->
        <!--    <img class = "SignUpProfPic box_shadow" src = "/_css/img/user-64.png" />-->
        <!--<input id = "profImgUpload" name = "profImgUpload" class = "hidden"
               type = "file" />-->
        <!--        </div>-->
        <input class="new_long_input new_input required validate nameValidation box_shadow en-fa"
               id="first_name"
               name="fname"
               type="text"
               width="50"
                <?php echo form_values('fname'); ?>
               placeholder="نام"
               data-help="نام خود را در این کادر وارد نمایید، این کادر فقط مقادیر فارسی را میپذیرد"
               required>
        <input class="new_long_input new_input required validate nameValidation box_shadow  en-fa"
               id="last_name"
               type="text"
               name="lname"
               data-help="نام خانوادگی خود را در این کادر وارد نمایید، این کادر فقط مقادیر فارسی را میپذیرد"
                <?php echo form_values('lname'); ?>
               required
               placeholder="نام خانوادگی">
        <input class="new_long_input new_input required validate usernameValidation box_shadow"
               id="username"
               type="text"
               name="username"
               data-help="یک نام کاربری برای خود انتخاب نمایید، این نام می بایست از حروف انگلیسی تشکیل شده باشد و نیز توسط سایر اعضا انتخاب نشده باشد"
                <?php echo form_values('username'); ?>
               required
               placeholder="نام کاربری">
        <!--        <label for="time" id="timeLabel">تاریخ تولد</label>-->
        <div class="datefield new_long_input" id="time">
            تاریخ تولد
            <input class="new_input new_input_num validate numberValidation box_shadow"
                   id="day"
                   type="text"
                   name="day"
                   maxlength="2"
                    <?php echo form_values('day'); ?>
                   required
                   placeholder="روز"/>/
            <input class="new_input new_input_num validate numberValidation box_shadow"
                   id="month"
                   type="text"
                    <?php echo form_values('month'); ?>
                   name="month"
                   maxlength="2"
                   required
                   placeholder="ماه"/>/
            <input class="new_input new_input_num validate numberValidation box_shadow"
                   id="year"
                   type="text"
                    <?php echo form_values('year'); ?>
                   name="year"
                   maxlength="4"
                   required
                   placeholder="سال"/>
        </div>
        <div class="datefieldShow new_inner_row"></div>
        <div class="new_row">
            <label for="gender" class="hidden">جنسیت</label>
            <select id="gender" name="gender"
                    class="new_input new_long_input box_shadow" required>
                <option value="1" <?php echo form_values('gender', 'select', 1); ?>>
                    مرد
                </option>
                <option value="0" <?php echo form_values('gender', 'select', 0); ?>>
                    زن
                </option>
            </select>
            <label for="Email" class="hidden">ایمیل</label>
            <input class="new_input new_long_input validate emailValidation box_shadow"
                   id="Email"
                   type="text"
                   data-help="توجه داشته باشید آدرس ایمیل نباید www ندارد. مثال: my.name@gmail.com"
                   <?php echo form_values('email', 'text'); ?>name="email"
                   required placeholder="آدرس ایمیل"
                   required>
            <label for="password" class="hidden">رمز عبور</label>
            <input class="new_input new_long_input matchedwithE2 validate required box_shadow"
                   id="password"
                   type="password"
                   name="password"
                   maxlength="32"
                   minlength="8"
                   required
                   autocomplete="off"
                   data-help="رمز عبور باید حداقل 8 حرف باشد"
                   placeholder="رمز عبور">
            <label for="repassword" class="hidden">تکرار رمز عبور</label>
            <input class="new_input new_long_input validate matchedwithE2 required box_shadow"
                   id="repassword"
                   type="password"
                   name="repassword"
                   maxlength="32"
                   minlength="8"
                   data-help="رمز عبور باید حداقل 8 حرف باشد"
                   autocomplete="off"
                   required
                   placeholder="تکرار رمز عبور">
            <!--<input class="new_input new_long_input validate numberValidation required" id="national_code" type="text" name="national_code"
                       minlength="10"
                       maxlength="10" required placeholder="کد ملی">-->
            <input class="new_input new_long_input validate numberValidation required box_shadow"
                   id="phone"
                   type="text"
                    <?php echo form_values('mobile'); ?>
                   required
                   name="mobile"
                   maxlength="11"
                   minlength="11"
                   placeholder="موبایل">
            <?php
            /*
                $deg_custom_option = array('تحصیلات' => 0);
                $deg_attrs = array('id' => 'degree', 'name' => 'degree', 'class' => 'new_input new_long_input validate2 required', 'required' => 'required');
                echo select_element(degree_select_all(),'degID','degName',$deg_attrs,'',$deg_custom_option);
            */
            ?>
        </div>

        <input type="button" name="next"
               class="btnForce form_next action-button" value="بعدی"/>
    </fieldset>
    <fieldset>
        <h2 class="fs-title">مشخصات ورزشی</h2>

        <div class="new_row">
            <!--        <h3 class="fs-subtitle">Your presence on the social network</h3>-->
            <label for="stateResult">استان</label>
            <?php
            $state_custom = array(' استان خود را انتخاب نمایید' => '');
            $state_attrs  = array(
                    'id'             => "stateResult",
                    'name'           => "stateResult",
                    'data-ajax-dest' => "register/registerSCFB.php",
                    'class'          => "new_input new_long_input validate2 box_shadow",
                    'required',
            );
            echo select_element(state_distinct_select(TRUE), 'stateName', 'stateName', $state_attrs, form_values('stateResult', 'value'), $state_custom);
            ?>
            <label for="countyResult">&nbsp;&nbsp;&nbsp;شهر</label>
            <select id="countyResult" name="countyResult"
                    data-ajax-dest="register/registerSCFB.php"
                    class="new_input new_long_input validate2 box_shadow"
                    required>
                <option value="">انتخاب شهر</option>
            </select>
            <label for="fieldResult">&nbsp;رشته</label>
            <select id="fieldResult" name="fieldResult"
                    data-ajax-dest="register/registerSCFB.php"
                    class="new_input new_long_input validate2 box_shadow"
                    style="text-align:center;"
                    required>
                <option value="">انتخاب رشته</option>
            </select>
            <label for="branchResult">&nbsp;&nbsp;سبک</label>
            <select id="branchResult" name="branch"
                    data-ajax-dest="register/registerSCFB.php"
                    class="new_input new_long_input validate2 box_shadow"
                    style="text-align:center;"
                    required>
                <option value="0">انتخاب سبک</option>
            </select>
            <label for="superior">&nbsp;سمت</label>
            <?php
            $job_custom_option = array('انتخاب سمت' => '');
            $job_attrs         = array(
                    'id'    => 'superior',
                    'name'  => "job",
                    'class' => "new_input new_long_input validate2 box_shadow",
            );
            echo select_element(job_select(3), 'jobID', 'jobName', $job_attrs, '', $job_custom_option);
            ?>
            <label for="branchResult">&nbsp;استاد</label>
            <select class="new_input new_long_input box_shadow" name="superior"
                    id="teName">
                <option value="">نام استاد خود را انتخاب نمایید</option>
            </select>
        </div>
        <input type="button" name="next" class="form_next action-button step-2"
               value="بعدی"/>
        <input type="button" name="previous" class="form_previous action-button"
               value="قبلی"/>
    </fieldset>
    <fieldset>
        <h2 class="fs-title">تایید مشخصات</h2>

        <p> این جانب
            <span id="SignUpCheckName"
                  class="new_errors"><?php form_values('fname', 'value') . ' ' . form_values('lname', 'value') ?></span>
            <!-- با شماره ملی
            <span id="SignUpCheckId" class="new_errors"></span>-->
            با دقت لازم فرم ها را تکمیل نموده و نسبت به صحت اطلاعات وارد شده
            اطمینان لازم را دارم .
            <br/>
            آدرس ایمیل
            <span id="SignUpCheckEmail"
                  class="new_errors"><?php form_values('email', 'value') ?></span>
        </p>

        <p>
            <input type="checkbox"> ضمن تایید مندرجات <a href="#"> قوانین</a>
            مربوط به سایت را نیز مطالعه
            نموده ام.
        </p>
        <input type="submit" name="submit" class="submit action-button"
               value="ثبت"/>
        <input type="button" name="previous" class="form_previous action-button"
               value="قبلی"/>
    </fieldset>
</form>
<?php layout_template('foot/foot_section'); ?>
