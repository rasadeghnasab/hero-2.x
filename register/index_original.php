<?php
    require_once(__DIR__ . '/../includes/initial.php');
    if (isset($cUser->user_id)) redirect_to(BASEPATH);
?>

<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<div class="main_frame">
<div class="frame">
<div class="row">
<br/>

<div class="SignUpSteps">
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="line"></div>
    <div class="lineDashed"></div>
    <div id="Step1" class="labeld labeling">اطلاعات اولیه</div>
    <div id="Step2" class="labeld">اطلاعات دقیق</div>
    <div id="Step3" class="labeld">تایید نهایی</div>
    <div class="labeld">پرداخت</div>
    <div class="labeld">نتیجه</div>
</div>
<!-- end of SignUpSteps -->
<?php echo $message ? $message : '<ul class="errors"></ul>'; ?>
<!-- end of errors -->
<form action="signUpEngine.php" method="post" enctype="multipart/form-data" novalidate="">
    <div class="stepPages" id="SignUpStepOne">
	  				<span>
	    					<input class="balloon validate nameValidation" id="first_name" name="fname"
                                   type="text" width="50" placeholder="نام"
                                   required="required"><label
                            for="first_name">نام</label>
	  				</span>
	  				<span>
	    					<input class="balloon validate nameValidation" id="last_name" type="text" name="lname"
                                   required="required"><label
                            for="last_name">نام
                            خانوادگی</label>
	  				</span>
	  				<span>
<!--    <span class="balloon" id="time" type="text"/>سلام دوستان</span><label for="time"> نام</label>-->
<div class="balloon datefield" id="time">
    <input class="validate numberValidation" id="day" type="text" name="day" maxlength="2" required="required"
           placeholder="روز"/> /
    <input class="validate numberValidation" id="month" type="text" name="month" maxlength="2" required="required"
           placeholder="ماه"/>/
    <input class="validate numberValidation" id="year" type="text" name="year" maxlength="4" required="required"
           placeholder="سال"/>
</div>
    <label for="time" id="timeLabel">تاریخ تولد</label>
<!--    <input class="balloon" id="first_name" type="text" width="50" maxlength="18" placeholder="نام"><label for="first_name" >نام</label>-->
</span>

	  				<span>
	    				<select id="gender" name="gender" class="balloondd">
                            <option value="1"> مرد</option>
                            <option value="0"> زن</option>
                        </select><label for="gender">جنسیت</label>

					</span>

        <div class="datefieldShow"></div>

					<span>
	    					<input class="balloon blonEmail validate emailValidation matchedwithE1" id="Email"
                                   type="email" name="email" required><label for="Email">آدرس ایمیل</label>
					</span>
	                	<span>
	    					<input class="balloon blonEmail validate emailValidation matchedwithE1" id="reemail"
                                   type="email" name="reemail" required><label for="reemail">تکرار آدرس ایمیل</label>
					</span>
	               		<span>
	    					<input class="balloon validate numberValidation" id="national_code" type="text" name="national_code"
                                   minlength="10"
                                   maxlength="10" required="required"><label for="national_code"> کد ملی </label>
	  				</span>
	  				<span>
	    					<input class="balloon validate usernameValidation" id="username" type="text"
                                   name="username"
                                   required="required"><label for="username">نام کاربری</label>
	  				</span>
	  				<span>
	    					<input class="balloon matchedwithE2 validate" id="password" type="password" name="password"
                                   maxlength="32" required="required"><label for="password">رمز عبور</label>
	  				</span>
	  				<span>
	    					<input class="balloon validate matchedwithE2" id="repassword" type="password"
                                   name="repassword" maxlength="32" required="required"><label for="re_password">تکرار
                            رمز عبور</label>
	  				</span>
        <br/>
        <button class="nextButton btnForce" onClick="SignUpNext(1);return false;"> مرحله بعدی</button>
    </div>
    <!-- end of SingUpStepOne -->
    <div class="stepPages" id="SignUpStepTwo">
        <div id="profImg" class="SignUpProfPic">
            <img src="/_css/img/user-64.png"/>

            <div class="labeld profImg">افزودن عکس</div>
            <input id="profImgUpload" name="profImgUpload"
                   style="visibility:hidden;"
                   type="file"/>
        </div>
        <br/>
			                <span>
			    				<input class="balloon validate2 numberValidation" id="phone" type="text" name="mobile"
                                       maxlength="11" minlength="11"><label
                                    for="phone">موبایل</label>
			  			</span>
			                <span>
			    				<input class="balloon validate2 numberValidation" id="stPhone" type="text" name="phone"
                                       minlenght="11" maxlength="11"><label
                                    for="stPhone">تلفن
                                    ثابت</label>
			  				</span>
			                <span>
                                <?php
                                    $stateQ = 'SELECT DISTINCT `stateName` FROM `state` JOIN `place` ON `state`.`stateID` = `place`.`stateID`';
                                    $stateResult = $db->query($stateQ);
                                ?>
                                <select id="stateResult" name="stateResult" data-ajax-dest="registerSCFB.php"
                                        class="balloondd
                                validate2" required="required">
                                    <option value="0">انتخاب استان</option>
                                    <?php echo state_distinct_select(null,'string'); ?>
                                </select><label for="layer">استان</label>
		  				</span>
		                	<span>
			                    <select id="countyResult" name="countyResult" data-ajax-dest="registerSCFB.php"
                                        class="balloondd validate2"
                                        required="required">
                                    <option value="0">انتخاب شهر</option>

                                </select><label for="countyResult">شهر</label>
		  				</span>
		  				<span>
						     	<select id="fieldResult" name="fieldResult" data-ajax-dest="registerSCFB.php"
                                        class="balloondd  validate2"
                                        style="text-align:center;"
                                        required="required">
                                    <option value="0">انتخاب رشته</option>
                                </select><label for="fieldResult">رشته</label>
		   				</span>
		                	<span>
			                    <select id="branchResult" name="branch" data-ajax-dest="registerSCFB.php"
                                        class="balloondd  validate2" style="text-align:center;"
                                        required="required">
                                    <option value="0">انتخاب سبک</option>
                                </select><label for="branchResult">سبک</label>
		   		</span>
			          <span>
			    		<input class="balloon blonAdr  validate2" id="address" name="address" type="text"
                               required><label
                              for="address">آدرس
                              کامل</label>
				</span>
			          <span>
<!--                          <select id="licence" name="licence" class="balloondd validate2" required="required">-->
                              <?php
                                  $deg_attrs = array('id' => 'degree', 'name' => 'degree', 'class' => 'balloondd validate2', 'required' => 'required');
                                  echo select_element(degree_select_all(),'degID','degName',$deg_attrs); //unset($degreeQ, $degResult,$row) // unset all request variables ?>
<!--                          </select>-->
                          <label for="licence">تحصیلات </label>
		  		</span>
			         <span>
<!--                          <select id="superior" name="level" class="balloondd  validate2">-->
<!--                              <option value="0">انتخاب سمت</option>-->
                              <?php
                                  $job_custom_option = array('انتخاب سمت' => 0);
                                  $job_attrs = array('id' => 'superior', 'name'=>"job", 'class' => "balloondd  validate2");
                                  echo select_element(job_select(),'jobID','jobName',$job_attrs,'',$job_custom_option); ?>
<!--                          </select>-->
                         <label for="superior">سمت</label>
			  	</span> <!--<span>
			    		<input class="balloon  validate2 nameValidation" id="TeName"     name="tename" type="text" required="required">

                                        <label for="Tename">نام استاد</label>
                        
				</span>-->

<span>
                            <select class="balloondd" name="superior" id="teName" disabled='disabled'>

                                <option value="0">نام استاد</option>

                            </select>

                            <label for="teName">نام استاد</label>                    </span>
        <!--<span id="coIdCardArea">
                <input class="balloon  validate2 hidden" id="coIdCard" type="file" required><label
                for="coIdCard">فتوکپی
                شناسنامه
            </label>
          </span>-->
        <br/>
        <button onClick="SignUpNext(2);return false;" class="nextButton btnForce2"> مرحله بعدی</button>
        <button
            onClick="SignUpNext(-1);return false;" class="nextButton backButton"> مرحله قبلی
        </button>
    </div>
    <!-- end of SignUpStepTwo -->
    <div class="stepPages" id="SignUpStepThree">
        <p> این جانب
            <span id="SignUpCheckName">امین باقری</span> با شماره ملی <span
                id="SignUpCheckId"> 3300100021 </span> با دقت لازم فرم ها را تکمیل نموده و نسبت به صحت
            اطلاعات وارد شده اطمینان لازم را دارم .
            <br/>
            آدرس ایمیل <span id="SignUpCheckEmail"> amindotb@gmail.com</span>
        </p>

        <p>
            <input type="checkbox"> ضمن تایید مندرجات <a href="#"> قوانین</a> مربوط به سایت را نیز مطالعه
            نموده ام.
        </p>                <br/>
        <input type="submit" onClick="SignUpNext(3);" class="nextButton btnForce" value="پرداخت">
        <button onClick="SignUpNext(-2);return false;" class="nextButton backButton"> مرحله قبلی</button>
    </div>
    <!-- end of SignUpStepThree -->
</div>
<!-- end of row -->

</form>
</div>
<!-- end of frame -->
</div>
<!-- end of main_frame -->
<?php layout_template('foot/foot_section');?>