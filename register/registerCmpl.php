<!--<meta charset="UTF-8">-->
<?php
require_once(__DIR__ . '/../includes/initial.php');
$output = "";
if (!isset($_SESSION['fname'])) {
    redirect_to(BASEPATH);
}
$first_name = $_SESSION['fname'];
$last_name = $_SESSION['lname'];
$stateID = $_SESSION['countyResult']; //$stateID = '4';
//    $degreeID       =    $_SESSION['degree'];
$degreeID = '5';
$sex = $_SESSION['gender'];
//    $national_code   =    $_SESSION['national_code'];
$national_code = NULL;
$birth_date = $_SESSION['birth_date'];
//    $address        =    $_SESSION['address'];
$address = NULL;
$mobile = $_SESSION['mobile'];
//    $phone          =    $_SESSION['phone'];
$phone = NULL;
$email = $_SESSION['email'];
$username = $_SESSION['username'];
$password = $_SESSION['password'];
$sportID = $_SESSION['branch'];
$jobID = $_SESSION['job'];
$superiorID = $_SESSION['superior'];
// find first beltID in user sport
$result = belt_by_sport($sportID);
$belt = $result->fetch(PDO::FETCH_NUM);
$beltID = $belt[0];
///////////
//if (isset($_SESSION['profilePic'])) {
//  $profilePic = SITE_ROOT . $_SESSION['profilePic'];
//  unset($_SESSION['profilePic']);
//}
$db->beginTransaction();
$user_attrs = array(
    'stateID'       => $stateID,
    'first_name'    => $first_name,
    'last_name'     => $last_name,
    'sex'           => $sex,
    'national_code' => $national_code,
    'birth_date'    => $birth_date,
);
$user = User::make($user_attrs);
$transaction_done = FALSE;
if ($transaction_done = $user->save()) {
    $uid = $db->lastInsertId();
}
$address_attrs = array(
    'userID'  => $uid,
    'mobile'  => $mobile,
);
$address_obj = Address::make($address_attrs);
if ($address_obj->save()) {
    /* user_pass table insert query */
    $user_passInsertQ = 'INSERT INTO `user_pass` (`uid`, `username`, `password`, `email`) ';
    $user_passInsertQ .= 'VALUES (:uid, :username, :password, :email)';
    $user_passInsert = $db->prepare($user_passInsertQ);
    $user_passInsert->bindParam(':uid', $uid, PDO::PARAM_INT);
    $user_passInsert->bindParam(':username', $username, PDO::PARAM_STR);
    $user_passInsert->bindParam(':password', $password, PDO::PARAM_STR);
    $user_passInsert->bindParam(':email', $email, PDO::PARAM_STR);
    if ($user_passInsert->execute()) {
        /* infotable table insert query */
        $infoInsertQ = 'INSERT INTO `infotable` (`infoID`, `userID`, `sportID`, `superiorID`, `jobID`,`beltID`) ';
        $infoInsertQ .= 'VALUES (NULL, :userID, :sportID, :superiorID, :jobID, :beltID)';
        $infoInsert = $db->prepare($infoInsertQ);
        $infoInsert->bindParam(':userID', $uid, PDO::PARAM_INT);
        $infoInsert->bindParam(':sportID', $sportID, PDO::PARAM_INT);
        $infoInsert->bindParam(':superiorID', $superiorID, PDO::PARAM_INT);
        $infoInsert->bindParam(':jobID', $jobID, PDO::PARAM_INT);
        $infoInsert->bindParam(':beltID', $beltID, PDO::PARAM_INT);
        if ($infoInsert->execute()) {
//            if (isset($profilePic)) {
//                $name = time() . '_' . rand(500, 1000) . '.jpg';
//                $path = SITE_ROOT . DS . 'profilePics' . DS;
//                $profilePic = preg_replace('/^\.\.\//', '', $profilePic);
//                $insertProfilePicQ = 'INSERT INTO `userpic` (`userID`, `picUrl`) ';
//                $insertProfilePicQ .= 'VALUES (' . $uid . ', "' . $name . '")';
//                if ($db->exec($insertProfilePicQ)) {
//                    if (rename($profilePic, $path . $name)) {
//                        $columns_to_update = array('student_register');
//                        $columns_to_insert = array('userID', 'student_register');
//                        $values_to_insert = array(
//                            'userID'           => $superiorID,
//                            'student_register' => 1
//                        );
//                        if (insert_update_notificatoins($columns_to_insert, $values_to_insert, $columns_to_update)) {
//                            $db->commit();
//                        } else {
//                            $db->rollBack();
//                        }
//                        //                                return true;
//                    } else {
//                        $db->rollBack();
//                        //                                return false;
//                    }
//                } else {
//                    $db->rollBack();
//                    //                            return false;
//                }
//            } else {
                $columns_to_update = array('student_register');
                $columns_to_insert = array('userID', 'student_register');
                $values_to_insert = array(
                    'userID'           => $superiorID,
                    'student_register' => 1
                );
                if (insert_update_notificatoins($columns_to_insert, $values_to_insert, $columns_to_update)) {
                    $db->commit();
                } else {
                    $db->rollBack();
                }
//            }
        }
    } else {
        $db->rollBack();
        //                return false;
    }
} //}
else {
    $db->rollBack();
}
$output = "<p>" . $first_name . " " . $last_name . " حساب کاربری شما فعال شد</p>
        <p> به جمع ورزشکاران ----- خوش آمدید </p>";
///*
unset($_SESSION['level']);
unset($_SESSION['licence']);
unset($_SESSION['address']);
unset($_SESSION['branch']);
unset($_SESSION['city']);
unset($_SESSION['phone']);
unset($_SESSION['mobile']);
unset($_SESSION['password']);
unset($_SESSION['username']);
unset($_SESSION['idnum']);
unset($_SESSION['email']);
unset($_SESSION['gender']);
unset($_SESSION['year']);
unset($_SESSION['month']);
unset($_SESSION['day']);
unset($_SESSION['fname']);
unset($_SESSION['lname']);
//*/
?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<ul class="errors">
</ul>
<div id="signup">
    <ul id="progressbar">
        <li>پرداخت</li>
        <li class="active">نتیجه</li>
    </ul>
    <fieldset>
        <h2 class="fs-title">ثبت نام موفقیت آمیز بود</h2>
        <?php echo $output; ?>
        <a class="btn btn_blue" href="<?php BASEPATH; ?>">شروع</a>
    </fieldset>
</div>
<?php layout_template('foot/foot_section'); ?>
