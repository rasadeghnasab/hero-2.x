<?php
// SCFB is stand for : State, County, Field, Branch
require_once(__DIR__ . '/../includes/initial.php');

/* find all county in a state */
if (isset($_POST['stateName']) && !isset($_POST['stateID'])) {
  //    echo "state is OK.";
  $stateName = $_POST['stateName'];
  //    echo "justState";
  if (!empty($stateName)) {
    $countyQ = 'SELECT DISTINCT `state`.`stateID`, `countyName` FROM `state` WHERE stateName = :stateName';
    $county  = $db->prepare($countyQ);
    $county->bindParam(':stateName', $stateName, PDO::PARAM_STR);
    $county->execute();
    while ($row = $county->fetch(PDO::FETCH_NUM)) {
      echo '<option value ="' . $row[0] . '">' . $row[1] . '</option>';
    }
  }
  //    die();
  // agar parametr haye state va county ersal shode bashand
}
elseif (isset($_POST['stateID']) && !isset($_POST['sportName']) && !isset($_POST['sportID']) && !isset($_POST['jobID'])) {
  $stateID = $_POST['stateID'];
  if (!empty($stateID)) {
    $sportQ = 'SELECT DISTINCT `field` FROM `place` JOIN `sport` ON place.sportID = sport.id WHERE `stateID` = :stateID && `sportConfirm` = true';
    $sport  = $db->prepare($sportQ);
    $sport->bindParam(':stateID', $stateID, PDO::PARAM_STR);
    $sport->execute();
    $num = $sport->rowCount();
    if (!$num) {
      echo '<option value="0" >رشته ورزشی در شهر مورد نظر موجود نمی باشد</option>';
    }
    else {
      while ($row = $sport->fetch(PDO::FETCH_NUM)) {
        echo '<option value ="' . $row[0] . '">' . $row[0] . '</option>';
      }
    }

  }
  // agar parametr haye state,county va field ersal shode bashand
}
else if (isset($_POST['sportName'], $_POST['stateID'])) {
  $sportName = $_POST['sportName'];
  $stateID   = $_POST['stateID'];
  if (!empty($sportName) && !empty($stateID)) {
    $sportQ = 'SELECT DISTINCT `sport`.`id`, `branch` FROM `sport` JOIN `place` ON `sport`.`id` = `place`.`sportID` WHERE  `field` = :sportName AND stateID = :stateID';
    //            $sportQ = 'SELECT DISTINCT `branch` FROM sport WHERE sportID = (SELECT `sportID` FROM place WHERE stateID = 5 AND sportID = sport.sportID);';
    $sport = $db->prepare($sportQ);
    $sport->bindParam(':sportName', $sportName, PDO::PARAM_STR);
    $sport->bindParam(':stateID', $stateID, PDO::PARAM_INT);
    $sport->execute();
    $num = $sport->rowCount();
    if (!$num) {
      echo '<option value="0" >رشته ورزشی در شهر مورد نظر موجود نمی باشد  </option>';
    }
    else {
      while ($row = $sport->fetch(PDO::FETCH_NUM)) {
        echo '<option value ="' . $row[0] . '">' . $row[1] . '</option>';
      }
    }
  }
}
else if (isset($_POST['stateID'], $_POST['sportID'], $_POST['jobID'])) {
  $stateID = $_POST['stateID'];
  $sportID = $_POST['sportID'];
  $jobID   = $_POST['jobID'];
  ////////////////////////
  $jobIDQ   = 'SELECT `jobValue`,`jobName` FROM `job` WHERE `jobID` = :jobID';
  $jobIDPre = $db->prepare($jobIDQ);
  $jobIDPre->bindParam(':jobID', $jobID, PDO::PARAM_INT);
  $jobIDPre->execute();
  $jobIDFound = $jobIDPre->fetch(PDO::FETCH_NUM);
  //        var_dump($jobIDFound);
  //        die();
  //        $jobVal = array_shift($jobIDFound);
  $jobVal = $jobIDFound[0];
  ///////////////////////
  $newJobIDQ = 'SELECT `jobID` FROM `job` WHERE `jobValue` > ' . $jobVal . ' LIMIT 1';
  $newJobID  = $db->query($newJobIDQ);
  $newJobID  = $newJobID->fetch(PDO::FETCH_NUM);
  $newJobID  = array_shift($newJobID);
  $newJobID  = (int) $newJobID;
  //        var_dump($newJobID);
  //        die();
  if ($jobIDFound[1] == 'نماینده استان') {
    $superiorQ   = 'SELECT `first_name`,`last_name`, `user`.`userID` FROM infoTable JOIN user ON infoTable.userID
             = user.userID WHERE sportID = :sportID AND jobID =' . $newJobID . ' LIMIT 1';
    $superiorPre = $db->prepare($superiorQ);
    $superiorPre->bindParam(':sportID', $sportID, PDO::PARAM_INT);
    $superiorPre->execute();
    $num = $superiorPre->rowCount();
    if ($num) {
      $superior = $superiorPre->fetch(PDO::FETCH_NUM);
      //            $superior = $superior[0] .' '.$superior[1];
      //            $superior[0]['ID'] = $newJobID;
      //            echo json_encode('does');
      //            die();
      echo '<option value = ' . $superior[2] . '>' . $superior[0] . ' ' . $superior[1] . '</option>';

    }
    else {
      echo '<option value="0">بدون استاد</option>';
    }
    die();
  }
  else {
    $superiorQ = 'SELECT  `first_name`, `last_name`, `user`.`userID` ';
    $superiorQ .= 'FROM  `infotable` ';
    $superiorQ .= 'JOIN  `user` ON  `infotable`.`userID` =  `user`.`userID` ';
    $superiorQ .= 'WHERE  `stateID` = :stateID AND `sportID` = :sportID ';
    $superiorQ .= 'AND `jobID` >= :jobID AND jobID != 5  AND is_confirm = true AND is_teacher = true ';
    $superiorPre = $db->prepare($superiorQ);
    $superiorPre->bindParam(':stateID', $stateID, PDO::PARAM_INT);
    $superiorPre->bindParam(':sportID', $sportID, PDO::PARAM_INT);
    $superiorPre->bindParam(':jobID', $newJobID, PDO::PARAM_INT);
    $superiorPre->execute();
    $num = $superiorPre->rowCount();
    if ($num) {
      while ($superior = $superiorPre->fetch(PDO::FETCH_NUM)) {
        echo '<option value = ' . $superior[2] . '>' . $superior[0] . ' ' . $superior[1] . '</option>';
      }
    }
    else {
      echo '<option value="0">بدون استاد</option>';
    }
    die();
  }
}
else if (isset($_POST['idnum'])) {
  $ntCode = $_POST['idnum'];
  if (strlen($ntCode) == 10 && !empty($ntCode)) {
    $ntCodeFindQ = 'SELECT `national_code` FROM `user` WHERE `national_code` = :national_code LIMIT 1';
    $ntCodePre   = $db->prepare($ntCodeFindQ);
    $ntCodePre->bindParam(':national_code', $ntCode, PDO::PARAM_INT);
    $ntCodePre->execute();
    if ($ntCodePre->rowCount()) {
      echo json_encode('exist');
    }
    else {
      echo json_encode('notExist');
    }
    die();
  }
  else {
    echo json_encode('wrong');
  }
}
else if (isset($_POST['email'])) {
  $email = $_POST['email'];
  if (strlen($email) <= 50 && !empty($email)) {
    $emailFindQ   = 'SELECT `email` FROM `user_pass` WHERE `email` = :email LIMIT 1';
    $emailFindPre = $db->prepare($emailFindQ);
    $emailFindPre->bindParam(':email', $email, PDO::PARAM_INT);
    $emailFindPre->execute();
    if ($emailFindPre->rowCount()) {
      echo json_encode('exist');
    }
    else {
      echo json_encode('notExist');
    }
    die();
  }
  else {
    echo json_encode('wrong');
  }
}
else if (isset($_POST['username'])) {
  $username = $_POST['username'];
  if (strlen($username) <= 50 && !empty($username)) {
    $usernameFindQ   = 'SELECT `username` FROM `user_pass` WHERE `username` = :username LIMIT 1';
    $usernameFindPre = $db->prepare($usernameFindQ);
    $usernameFindPre->bindParam(':username', $username, PDO::PARAM_INT);
    $usernameFindPre->execute();
    if ($usernameFindPre->rowCount()) {
      echo json_encode('exist');
    }
    else {
      echo json_encode('notExist');
    }
    die();
  }
  else {
    echo json_encode('wrong');
  }
}
