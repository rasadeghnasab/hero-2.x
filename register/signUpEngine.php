<!--<meta charset="UTF-8">-->
<?php
require_once(__DIR__ . '/../includes/initial.php');
if ($referer == "register.php") { // will change when uploaded *********
  $errors['refer'] = 'درخواست پذیرفته نشد';
}
$required_fields = array(
    'fname',
    'lname',
    'day',
    'month',
    'year',
    'gender',
    'email',
    //            'reemail',
    //            'national_code',
    'username',
    'password',
    'repassword',
    'mobile',
    //            'phone',
    'countyResult',
    'branch',
    //            'address',
    'superior',
    //            'degree',
);
$fields_with_max_lengths = array(
    'fname'      => 20,
    'lname'      => 20,
    'day'        => 2,
    'month'      => 2,
    'year'       => 4,
    'gender'     => 1,
    'email'      => 30,
    //            'reemail'       => 30,
    //            'national_code'  => 10,
    'username'   => 60,
    'password'   => 60,
    'repassword' => 60,
    'mobile'     => 11,
    //            'phone'         => 11,
    //            'address'       => 255,
    'superior'   => 5,
    //            'degree'       => 5
);
$fields_with_min_length = array(
    'fname'      => 3,
    'lname'      => 3,
    'email'      => 8,
    //            'reemail'       => 8,
    //            'national_code'         => 10,
    'username'   => 5,
    'password'   => 8,
    'repassword' => 8,
    'mobile'     => 9,
    //            'phone'         => 9,
    //            'address'       => 10,
);
$field_pairs = array(
    'password' => 'repassword',
    //            'email'    => 'reemail'
);

validate_presences($required_fields);
validate_input_lengths($fields_with_max_lengths, $fields_with_min_length);
pair_equality_check($field_pairs);

//        echo 'validate_presences';  var_dump(validate_presences($required_fields));
//        echo 'validate_input_lengths';var_dump(validate_presences($required_fields));
//        echo 'pair_equality_check'; var_dump(validate_presences($required_fields));
if (empty($errors)) {
  $day   = $_POST["day"];
  $month = $_POST["month"];
  $year  = $_POST["year"];
  unset($_POST['day'], $_POST['month'], $_POST['year'] /*,$_POST['reemail']*/);
  if (check_date($year, $month, $day)) {
    $_SESSION['birth_date'] = jalali_to_gregorian($year, $month, $day, '-');
  }; //validate entered date
  // encrypt password
  $password = password_encrypt($_POST['password']);
  if (username_email_existance(remove_bad_characters($_POST['username']), remove_bad_characters($_POST['email']))) {
    $errors[] = 'نام کاربری یا ایمیل قبلا ثبت شده است';
  };
  unset($_POST['repassword']);
}
if ($message = errors_to_ul($errors, 'errors')) {
  message($errors); // set $_session['message'] = $message;
  redirect_to(BASEPATH.'register/');
}
$cleaned_input = user_input_clean();
foreach ($cleaned_input as $key => $input) {
  $_SESSION[$key] = $input;
}
$_SESSION['password'] = $password;
redirect_to(BASEPATH.'register/registerCmpl.php');