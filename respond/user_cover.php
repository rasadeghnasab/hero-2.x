<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/15/2015
 * Time: 9:21 PM
 */
require_once(__DIR__ . '/../includes/initial.php');
$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : NULL;
$my_profile = isset($cUser->user_id) && ($_GET['user_id'] == $cUser->user_id);
$person_exists = person_is_exist($user_id);
$is_confirm = person_isConfirm_by_id($user_id);
if ($is_confirm && $person_exists) {
  $this_prof_teacher = false;
  if ($logged_in && !$my_profile) {
    $this_prof_teacher = is_a_teacher_student($cUser->user_id, $user_id);
  }
//  $show_setting     = $my_profile || $this_prof_teacher ? TRUE : FALSE;
//  $following_num    = count_followers_following($user_id, 'following');
//  $followers_num    = count_followers_following($user_id);
  $user_information = find_person_by_id($user_id); // find person information
  //        var_dump($user_information);die();
  $first_name       = htmlentities($user_information['first_name']);
  $last_name        = htmlentities($user_information['last_name']);
  $profileLocation = htmlentities($user_information['statename'] . " ، " . $user_information['countyname']);
//  $weight          = htmlentities($user_information['weight']);
//  $length          = htmlentities($user_information['length']);
//  $profileAge      = ageCalc($user_information['birth_date']);
  $profilePic      = $user_information['picurl'] ? "/profilePics/" . $user_information['picurl'] : '\_css\img\user-64.png';
  $profilePic      = htmlentities($profilePic);
//  $description     = $user_information['description'] ? nl2br(htmlentities($user_information['description'])) : ' توضیحی در مورد ' . $first_name . ' ' . $last_name . ' موجود نمیباشد ';
//  $foot            = $user_information['foot'] ? 'چپ' : 'راست';
//  $has_class       = $user_information['has_class'];
//  $nationalTeam    = "ثابت";
  $is_teacher      = is_teacher($user_id);
  $cover_photo_init = Image::user_active_cover_photo($user_id);
  $cover_photo =  $cover_photo_init ? $cover_photo_init[0]->cover_photo_html('thumbs') : (new Image('cover_photo'))->cover_photo_html();

  //  $complete = incomplete_atprofile($weight,$length,$foot);
  //  if (is_teacher($user_id)) {
  //  } else {
  //    require_once(__DIR__ . '/atProfile.php');
  //  }
}
else {
  echo '<p class="btn btn_large btn_warning">هنوز تایید نشده</p>';
  die;
}
//elseif ($my_profile) { // if person is_not_confirm but this is his/her personal profile
//  require_once(__DIR__ . '/not_confirm.php'); // TODO: complete this page in future
//  die();
//}
//else { // if person is_not_confirm and this is not his/her profile
//  echo "require_once('../errors/404.php')"; //TODO: must create this page
//  die();
//}

//if (!isset($my_profile, $logged_in)) redirect_to("profile.php"); // if anyone want to access to teProfile directly

// find number of students for a person (who is a teacher)


//$profile_variables = array(
//  'user_id'           => $user_id,
//  'this_prof_teacher' => $this_prof_teacher,
//  'my_profile'        => $my_profile,
//  'logged_in'         => $logged_in,
////  'is_teacher'        => $is_teacher,
////  'awards'            => $awards,
////  'has_class'         => $has_class,
////  'foot'              => $foot,
////  'description'       => $description,
//  'profileLocation'   => $profileLocation,
////  'weight'            => $weight,
////  'length'            => $length,
////  'profileAge'        => $profileAge,
//  'profilePic'        => $profilePic,
////  'show_setting'      => $show_setting,
//  'following_num'     => $following_num,
//  'followers_num'     => $followers_num,
//  'first_name'         => $first_name,
//  'last_name'          => $last_name,
//);
if($is_teacher) {
  $profileStudents = student_num_for_teacher($user_id);
//  $profile_variables['profileStudents'] = $profileStudents;
}

if (!$my_profile && $logged_in) {
  $userFlo           = is_follower($user_id, $cUser->user_id);
  $this_prof_student = is_a_teacher_student($user_id, $cUser->user_id);
  $profile_variables['userFlo'] = $userFlo;
  $profile_variables['this_prof_student'] = $this_prof_student;
}

?>
<div class = "min-cover-photo">
  <!--  <img class="cover-photo-img" src = "../teachers_pics/edit1.jpg" alt = ""  />-->
  <?php echo $cover_photo; ?>
  <?php echo "<img src= '{$profilePic}'  class='show_modal_description profile_img box_shadow' title='دربار من'>"; ?>
<?php if (!$my_profile && $logged_in) : ?>
  <div class = "cover_buttons">
  <?php if($this_prof_student): ?>
    <button id = "askbtn"
            class = "btn btn_white askbutn <?php echo $this_prof_student ? " btn_checked" : " btn_check" ?>"
      <?= 'data-user = "' . $user_id . '"'; ?>>
      <?php if ($this_prof_student) {
        echo 'استاد شما';
      }
      else echo 'درخواست' ?></button>
  <?php endif;  ?>

    <button class = "btn folbutn btn_white rqtbtn <?php if ($userFlo) echo " btn_checked" ?>"
      <?= 'data-user = "' . $user_id . '"'; ?>>
      <?php if ($userFlo) {
        echo '<span class="glyphicon glyphicon-remove"></span> <span class="text_holder">لغو دنبال</span>';
      }
      else echo '<span class="glyphicon glyphicon-check"></span> <span class="text_holder">دنبال</span>'; ?>
    </button>
  </div>
<?php endif; ?>
<div class = "profile_info">
  <div class = "profile_name">
    <a href = "/<?php echo $user_id; ?>"><?php echo $first_name . ' ' . $last_name ?></a>
  </div>
    <span class = "profile_location">
      <span class = "glyphicon glyphicon-map-marker"></span>
      <?php echo $profileLocation ?>
    </span>

</div>