<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 10/25/14
 * Time: 10:31 PM
 */
require_once(__DIR__ . '/../includes/initial.php');
pagination_variables();
if (isset($_POST['searchtext'])) {
  /*$required_fields         = array(
    'searchtext'
  );*/
  $fields_with_max_lengths = array(
    'searchtext' => 60,
  );
//  validate_presences($required_fields);
  validate_input_lengths($fields_with_max_lengths);
  if (!empty($errors)) {
    $errors['errors'] = TRUE;
    echo errors_to_ul($errors);
    die();
  }
  else {
    $searchText = trim($_POST['searchtext']);
    $limit = 10;
    $persons    = string_people_search(search_people($searchText));
    $posts      = string_post_search(search_post($searchText));
    if ($persons != '' || $posts != '') {
      echo json_encode($persons . $posts);
    }
    else {
      $output = '<div class="dummy-column dummy-column-medium">';
      $output .= '<h2 class="dummy-media-object">برای <strong class="blue">' . htmlentities($searchText) . '</strong> موردی یافت نشد</h2>';
      $output .= '</div>';
      echo json_encode($output);
    }
  }
}










