<?php

require_once(__DIR__ . '/../includes/initial.php');
$output = '';
if (isset($_GET['submit'])) {
    //  pagination_variables('GET');
    $required_fields         = array('searchtext', 'radio');
    $fields_with_max_lengths = array('searchtext' => 60);

    validate_presences($required_fields, 'GET');
    validate_input_lengths($fields_with_max_lengths, array(), 'GET');

    if (!$message = errors_to_ul($errors)) {
        $persons    = $posts = '';
        $limit      = 10;
        $searchText = htmlentities(trim($_GET['searchtext']));
        if (isset($_GET['radio'])) {
            $radioValue = $_GET['radio'];
            if ($searchText != '') {
                if ($radioValue == 'person') {
                    $persons = string_people_search(search_people($searchText), $view_mode = 'full');
                    $output  = $persons == '' ? 'فردی یافت نشد' : $persons;
                }
                if ($radioValue == 'post') {
                    $posts  = string_post_search(search_post($searchText), $view_mode = 'full');
                    $output = $posts == '' ? 'مطلبی یافت نشد' : $posts;
                }
            }
        }
    }
}
else {
    $_GET['radio'] = 'person';
}

?>
<?php
$title = 'جستجو';
layout_template('head_section');
layout_template('header');
?>

    <div class="content">
    <div>
        <form method="get" action="/search/index.php">
            <input type="text"
                   maxlength="60"
                   name="searchtext"
                   class="new_input new_normal_input"
                    <?php echo form_values('searchtext', 'text', '', 'get'); ?>
                   placeholder="جستجو"/>
            <label> جستجو در :</label>
            <input type="radio"
                   id="radio1"
                   name="radio"
                   value="person" <?php echo form_values('radio', 'radio', 'person', 'get'); ?> />
            <label for="radio1">افراد</label>

            <input type="radio"
                   id="radio2"
                   name="radio"
                   value="post" <?php echo form_values('radio', 'radio', 'post', 'get'); ?>/>
            <label for="radio2">مطالب</label>

            <!--<input type="radio" id="radio3" name="radio" value="event"/>
            <label for="radio3">رویدادها</label>
      -->
            <!--      <input type = "radio"
             id = "radio4"
             name = "radio"
             value = "all" <?php /*echo form_values('radio', 'radio', 'all', 'get'); */ ?> />
      <label for = "radio4">همه</label>
--> <input type="submit"
           class="btn btn_blue"
           name="submit"
           value="جستجو">
        </form>
    </div>
    <div class="full-search">
        <?php echo $output; ?>
    </div>
<?php layout_template('foot/foot_section'); ?>