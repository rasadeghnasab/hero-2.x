<?php

	// in file ('search.inc.php') baraye jostejo dar database be manzore peyda kardane Ostanha, Shahrha, Reshteha va Sabk haye anha tarahi shode ast.

	// agar parametri ba name all_state va meghdare 'all' ersal shode bashad, in ghesmat ejra mishavad.
    require_once(__DIR__ . '/../includes/initial.php');
    if(isset($_POST['all_state'])){
		//echo "first state is OK.";
		// meghdare parametre daryafti ra dar darone yek moteghayer vared mikonim ke bad az in ba aan moteghayer kar konim
		$all_state = $_POST['all_state'];
		if($all_state == 'all'){
			// check mikonad ke bebinad parametr khali nabashad
			if(!empty($all_state)){
				$query ="SELECT DISTINCT `state` FROM `place`";
				$result = $mysqli->query($query, MYSQLI_STORE_RESULT);
				// Iterate through the result set
				while(list($state) = $result->fetch_row()) {
					echo '<option value="'.$state.'">'.$state.'</option>';
				}
			$mysqli->close();
			// agar meghdare parametre all_state chizie be gheyra az 'all' bashad.	
			} else { 
				echo '<option value="0">lotfan dast kari nakonid</option>';
			}
		}
	// agar faghat parametre state ersal shoed bashad in ghemat ejra mishavad
	} else if (isset($_POST['state']) && !isset($_POST['county']) && !isset($_POST['field'])){
		//echo "state is OK.";
		$state = $_POST['state'];
		//echo "justState";
		if(!empty($state)){
			$query = "SELECT DISTINCT `county` FROM `place` WHERE `state` = '".$state."'";
			$result = $mysqli->query($query, MYSQLI_STORE_RESULT);
			while(list($county) = $result->fetch_row()) {
					echo '<option value="'.$county.'">'.$county.'</option>';
				}
			$mysqli->close();
		}

	// agar parametr haye state va county ersal shode bashand
	} else if (isset($_POST['state']) && isset($_POST['county']) && !isset($_POST['field']) ){
		$state = $_POST['state'];
		$county = $_POST['county'];
		if(!empty($state) && !empty($county)){
			$query = "SELECT DISTINCT `field` FROM `sport` WHERE `placeID` = (SELECT `placeID` FROM `place` WHERE `state` ='".$state."' AND `county`='".$county."')";
			$result = $mysqli->query($query, MYSQLI_STORE_RESULT);
			while(list($field) = $result->fetch_row()) {
					echo '<option value="'.$field.'">'.$field.'</option>';
				}
			$mysqli->close();
		}

	// agar parametr haye state,county va field ersal shode bashand
	} else if (isset($_POST['state']) && isset($_POST['county']) && isset($_POST['field'])) {
		$state = $_POST['state'];
		$county = $_POST['county'];
		$field = $_POST['field'];
		if(!empty($state) && !empty($county) && !empty($field)){
			$query = "SELECT DISTINCT `branch` FROM `sport` WHERE field='".$field."' AND `placeID` = (SELECT `placeID` FROM `place` WHERE `state` ='".$state."' AND `county`='".$county."')";
			$result = $mysqli->query($query, MYSQLI_STORE_RESULT);
			while(list($branch) = $result->fetch_row()) {
					echo '<option value="'.$branch.'">'.$branch.'</option>';
				}
			$mysqli->close();
		}
	}

?>