<?php
/*
 * if awd_id has exists -> award must updated where awd_id = :awd_id and user_id = user_id
 * else must inserted for $cUser->user_id //(INSERT if exists UPDATE)
 * //check to see if this awd_id belong to this user_id = $cUser->user_id
 * if we have
*/
if (isset($_POST['awd_id'])) {
    /* permission options */
    $permission_name = 'honors';
    $return          = TRUE;
    /* permission options */
    require_once(__DIR__ . '/../includes/initial.php');
    $required_fields         = array(
            'awd_id',
            'title',
            'description',
            'sportID',
            'action',
    );
    $fields_with_max_lengths = array(
            'awd_id'      => 30,
            'title'       => 50,
            'medal'       => 3,
            'description' => 300,
    );
    $fields_with_min_lengths = array(
            'title' => 4,
    );

    // validation
    validate_presences($required_fields);
    validate_input_lengths($fields_with_max_lengths, $fields_with_min_lengths);

    if (!$result = errors_to_ul($errors)) {
        $awd_id = $_POST['awd_id'] != 0 ? $_POST['awd_id'] : NULL;
        $userID = $cUser->user_id;
        if ($_POST['action'] == 'remove') {
            $columns_values = array(
                    'awdID'  => $awd_id,
                    'userID' => $userID,
            );
            $result         = award_delete($columns_values);
            $result         = $result != FALSE ? TRUE : FALSE;
            //                if(!$result) echo json_encode('not remove');
            //                die();
        }
        else {
            $medal       = isset($_POST['medal']) ? $_POST['medal'] : NULL;
            $description = isset($_POST['description']) ? $_POST['description'] : NULL;
            if ($awd_id) {
                //   if has $awd_id -> update
                //   awards_update();
                $columns_values = array(
                        'awdName'        => $_POST['title'],
                        'awdMedal'       => $medal,
                        'awdDescription' => $description,
                        'sportID'        => $_POST['sportID'],
                );
                $where          = array(
                        'awdId'  => $_POST['awd_id'],
                        'userID' => $userID,
                );
                if (award_updates($columns_values, $where)) {
                    $result['success'] = TRUE;
                }

            }
            else {
                // else if awd_id = NULL -> insert
                // awards_insert();
                $columns_values = array(
                        'awdName'        => $_POST['title'],
                        'awdMedal'       => $medal,
                        'awdDescription' => $description,
                        'sportID'        => $_POST['sportID'],
                        'userID'         => $cUser->user_id,
                        'awdInserterID'  => $userID,
                );
                if (award_insert($columns_values)) {
                    $result['success'] = TRUE;
                }
                $result['id'] = $db->lastInsertId();
            }
        }
    }
    echo json_encode($result);
    die();
}
?>
<h1>افتخارات من</h1>

<!-- add new honors -->
<div class="new_row">
    <!-- when a button with class new_honor is clicked, a new form for inserting a new honor will added after that button -->
    <span class="new_honor btn btn_blue btn_small"> <span
                class="glyphicon glyphicon-plus-sign"></span> افزودن افتخار جدید</span>
</div>
<?php
echo awards_string_to_forms($user_id);
?>


<?php
//$url_gets      = urlencode($_GET['id'].'/'.$_GET['info'].'/honors');
//    $url_site_name = rawurlencode('');
//    $url = $url_site_name.$url_gets;
$url_gets = $_GET['id'] . '/' . $_GET['info'] . '/honors';
echo pagination_function('/setting/sports/' . $url_gets); //'setting/sport/'$_GET['page']
?>
