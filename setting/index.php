<?php
$permission_name = 'page_view';
require_once(__DIR__ . '/../includes/initial.php');
if (!person_is_exist($cUser->user_id)) {
    //    if(!person_is_exist($_GET['userID'])) {
    message('چنین فردی وجود ندارد');
    redirect_to('/' . urlencode($cUser->user_id));
}
if (!person_isConfirm_by_id($cUser->user_id)) {
    //  Do Not show the Page
    message('این کاربر تایید نشده است');
    redirect_to('/' . urlencode($cUser->user_id));
}
if (isset($_POST['primary_submit'])) {
    $required_fields         = array(
        //            'Fname'   ,
        //            'Lname'   ,
        'length',
        'weight',
        'national_code',
        'degreeID',
        'description',
    );
    $fields_with_max_lengths = array(
        //            'Fname'   => 30,
        //            'Lname'   => 30,
        'length'        => 3,
        'weight'        => 3,
        'national_code' => 10,
        'degreeID'      => 2,
        'description'   => 3000,
        //            'national_code'=> 10
    );
    $fields_with_min_lengths = array(
            'length'        => 2,
            'national_code' => 10,
            'weight'        => 2,
            'degreeID'      => 1,
    );
    // validation
    validate_presences($required_fields);
    validate_input_lengths($fields_with_max_lengths, $fields_with_min_lengths);

    $location    = SITE_ROOT . DS . 'profilePics';
    $file_name   = 'profImgUpload';
    $image_check = image_upload_check($location, $file_name, 5);
    if (!$message = errors_to_ul($errors)) {
        if ($image_check) {
            /* upload image */
            if ($image = check_profile_image_existence($cUser->user_id)) {
                // update image
                if (!profile_image_upload($cUser->user_id, $location, $cUser->user_id, $file_name, 'update')) {
                    ;
                }
                $message = 'تصویر به درستی دریافت نشد';
            }
            else {
                // insert image
                $img_width  = 200;
                $img_height = 200;
                if (!profile_image_upload($cUser->user_id, $location, $cUser->user_id, $file_name, 'insert')) {
                    $message = 'تصویر به درستی دریافت نشد';
                }
            }
        }
        foreach ($required_fields as $variable_name) {
            $$variable_name = $_POST[$variable_name];
        }
        /* update user table information */
        $db->beginTransaction();
        $update = user_update(array(
                'length'        => $length,
                'weight'        => $weight,
                'degreeID'      => $degreeID,
                'description'   => $description,
                'national_code' => $national_code,
        ), array('userID' => $cUser->user_id));
        if ($update) {
            $db->commit();
            $message = 'تغییرات اطلاعات فردی با موفقیت اعمال شد';
        }
        else {
            $db->rollBack();
            $message = 'تغییرات اعمال نشد لطفا دوباره امتحان کنید';
        }
        // update userTable //create a function to dynamically add fields and values to query.
    }
}
elseif (isset($_POST['address_submit'])) {
    $required_fields         = array(
            'mobile',
            'phone',
            'address',
    );
    $fields_with_max_lengths = array(
            'mobile'  => 11,
            'phone'   => 11,
            'address' => 255,
    );
    if (!validate_presences($required_fields) || !validate_input_lengths($fields_with_max_lengths)) {
        $errors[] = 'فرم ارسالی ایراد دارد';
    }
    if (!$message = errors_to_ul($errors)) {
        $update  = address_update($cUser->user_id, $_POST['mobile'], $_POST['phone'], $_POST['address']);
        $message = $update ? 'تغییرات اطلاعات ارتباطی با موفقیت اعمال شد' : 'تغییرات اعمال نشد لطفا دوباره امتحان کنید';
    }
}
elseif (isset($_POST['security_submit'])) {
    $required_fields         = array(
            'username',
            'email',
    );
    $fields_with_max_lengths = array(
            'username' => 30,
            'email'    => 40,
    );
    $fields_with_min_lengths = array(
            'username' => 8,
            'email'    => 10,
    );
    if (isset($_POST['oldpassword']) && has_presence($_POST['oldpassword'])) {
        $required_fields1         = array(
                'oldpassword',
                'newpassword',
                'repassword',
        );
        $fields_with_max_lengths1 = array(
                'oldpassword' => 40,
                'newpassword' => 40,
                'repassword'  => 40,
        );
        $fields_with_min_lengths1 = array(
                'oldpassword' => 8,
                'newpassword' => 8,
                'repassword'  => 8,
        );
        $required_fields          = array_merge($required_fields, $required_fields1);
        $fields_with_max_lengths  = array_merge($fields_with_max_lengths, $fields_with_max_lengths1);
        $fields_with_min_lengths  = array_merge($fields_with_min_lengths, $fields_with_min_lengths1);
    }
    validate_presences($required_fields);
    validate_input_lengths($fields_with_max_lengths, $fields_with_min_lengths);

    if (!$message = errors_to_ul($errors)) {
        if (isset($_POST['oldpassword']) && has_presence($_POST['oldpassword'])) {
            // change password
            $update = user_pass_update($cUser->user_id, $_POST['username'], $_POST['email'], $_POST['oldpassword'], $_POST['newpassword'], $_POST['repassword']);
        }
        else {
            $update = user_pass_update($cUser->user_id, $_POST['username'], $_POST['email']);
        }
        if (!$message = errors_to_ul($errors)) {
            $message = $update ? 'تغییرات امنیتی با موفقیت اعمال شد' : 'تغییرات اعمال نشد لطفا دوباره امتحان کنید';
        }
    }
}
$person = find_person_by_id($cUser->user_id);

$addInfo = address_by_person_id($cUser->user_id);
/* primary information */
$first_name    = $person['first_name'];
$last_name     = $person['last_name'];
$userPic       = $person['picurl'] ? '/profilePics/' . $person['picurl'] : '/_css/img/user-64.png';
$state         = $person['statename'];
$county        = $person['countyname'];
$weight        = $person['weight'];
$length        = $person['length'];
$birth_date    = r_gregorian_to_jalali($person['birth_date']);
$description   = $person['description'];
$national_code = $person['national_code'];
$degreeID      = $person['degreeID'];
$username      = $person['username'];
$email         = $person['email'];
//$foot       = $person ? 'چپ' : 'راست';
/* address information */
$mobile  = $addInfo['mobile'];
$phone   = $addInfo['phone'];
$address = $addInfo['address'];
?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php echo '<ul class="errors"></ul>'; ?>
    <div class="content">
        <div class="setting">
            <div class="tab">
                <div class="warnning">
                    <!--          <span style="margin: 0 auto;"> -->
                    <?php //echo $message ?><!-- </span>-->
                </div>
                <ul class="tabs">
                    <li><a href="#">اطلاعات فردی</a></li>
                    <li><a href="#">اطلاعات ارتباطی</a></li>
                    <li><a href="#">امنیت</a></li>
                    <?php if (FALSE/*is_teacher($_SESSION['userID']*/) {
                        echo '<li><a href="#">قرار دادن تصاویر</a></li>';
                    } ?>
                </ul>
                <!-- / tabs -->
                <div class="tab_content">
                    <div class="tabs_item">
                        <!--            <label class=""> -->
                        <div class="input-group">
                            <img id="image_setting"
                                 class="set_img fl"
                                 src="<?php echo $userPic ?>">
                        </div>
                        <form method="post"
                              action="/setting/"
                              class="form-grouper"
                              enctype="multipart/form-data">
                            <input id="profImage"
                                   class="hidden"
                                   name="profImgUpload"
                                   type="file"/>

                            <div class="input-group">
                                <label class="">قد </label>
                                <input type="text" name="length" id="length"
                                       class="new_input"
                                       placeholder="سانتی متر"
                                       data-help='قد خود را به سانتی متر وارد نمایید'
                                       value="<?php echo $length ?>">
                            </div>
                            <div class="input-group">
                                <label class="">وزن </label>
                                <input type="text" name="weight" id="weight"
                                       class="new_input"
                                       value="<?php echo $weight ?>"
                                       data-help='وزن خود را به کیلوگرم وارد نمایید'
                                       placeholder="کیلوگرم"
                                        >
                            </div>
                            <div class="input-group">
                                <label class="">کد ملی</label>
                                <input type="text" name="national_code"
                                       id="national_code" class="new_input"
                                       maxlength="10"
                                       value="<?php echo $national_code; ?>"
                                       data-help='کد ملی ده رقمی خود را وارد نمایید'
                                       placeholder="کد ملی"
                                        >
                            </div>
                            <div class="input-group">
                                <!-- <label class = "">کد ملی :</label>
                <input type = "text" name = "national_code" disabled id = "national_code" class = "new_input" value = "<?php /*echo $national_code */ ?>">-->
                                <label class="">تحصیلات </label>
                                <?php
                                $degree_attrs = array(
                                        'name'      => 'degreeID',
                                        'id'        => 'degreeID',
                                        'data-help' => 'در این تحصیلات خود را مشخص نمایید',
                                        'class'     => 'chosen-select chosen-rtl',
                                );
                                echo select_element(degree_select_all('PDOObject'), 'degID', 'degName', $degree_attrs, $degreeID);
                                ?>
                            </div>
                            <div class="input-group">
                                <label class="">درباره من </label>

                                <div class="input-container">
                <textarea type="text" name="description" id="description"
                          class="new_medium_input"
                          data-help="در این کادر متنی در مورد خود بنویسید"
                        /><?php echo $description; ?></textarea>
                                </div>
                            </div>
                            <input type="submit"
                                   class="btn btn_orange btn_medium"
                                   name="primary_submit"
                                   value="ثبت">
                        </form>
                    </div>
                    <!-- / tabs_item -->
                    <div class="tabs_item">
                        <form action="/setting/" method="post">
                            <div class="input-group">
                                <label class="">شماره موبایل </label><input
                                        type="text"
                                        name="mobile"
                                        class="new_input"
                                        value="<?php
                                        echo $mobile;
                                        ?>">
                            </div>
                            <div class="input-group">
                                <label class="">شماره ثابت </label><input
                                        type="text"
                                        name="phone"
                                        class="new_input"
                                        value="<?php
                                        echo $phone;
                                        ?>">

                            </div>
                            <div class="input-group">
                                <label class="">آدرس </label><input type="text"
                                                                    name="address"
                                                                    class="new_input new_medium_input"
                                                                    value="<?php
                                                                    echo $address;
                                                                    ?>">
                            </div>
                            <input type="submit"
                                   class="btn btn_orange btn_medium"
                                   name="address_submit"
                                   value="ثبت">
                        </form>
                    </div>
                    <!-- / tabs_item -->
                    <div class="tabs_item">
                        <form action="/setting/" method="post"
                              class="form-grouper">
                            <div class="input-group">
                                <label for="username" class="">نام
                                    کاربری</label>
                                <input type="text"
                                       name="username"
                                       id="username"
                                       class="new_medium_input new_input"
                                       placeholder="نام کاربری"
                                       value="<?php echo $username; ?>"
                                        >
                            </div>
                            <div class="input-group">
                                <label class="">آدرس ایمیل </label>
                                <input type="text"
                                       name="email"
                                       class="new_medium_input new_input"
                                       value="<?php
                                       echo $email; ?>"
                                        >
                            </div>
                            <div class="input-group">
                                <label class="">رمز عبور فعلی</label><input
                                        type="password"
                                        name="oldpassword"
                                        class="new_input">
                            </div>
                            <div class="input-group">
                                <label class="">رمز عبورجدید</label><input
                                        type="password"
                                        name="newpassword"
                                        class="new_input">
                            </div>
                            <div class="input-group">
                                <label class="">تکرار رمز عبور</label><input
                                        type="password"
                                        name="repassword"
                                        class="new_input">
                            </div>
                            <input type="submit"
                                   class="btn btn_orange btn_medium"
                                   name="security_submit"
                                   value="ثبت">
                        </form>
                    </div>
                    <!-- / tabs_item -->
                    <?php if (is_teacher($cUser->user_id)) { ?>
                        <div class="tabs_item">
                            <?php
                            /*help statement (?) goes here */
                            $help = 'برای تغییر تصویر دلخواه بر روی آن کلیک کرده و تصویر جدید مورد نظر را انتخاب نمایید. پس از تغییر تصویر هیچ عمل دیگری نیاز نمیباشد.';
                            /*show help tooltip for persons how don't know anything about this page*/
                            help_tooltip($help);
                            ?>
                            <div class="new_row">
                                <?php
                                //echo '<img src="/_css/img/user-64.png" class="teacher_pic">';
                                $i    = 0;
                                $pics = teacher_image_select($cUser->user_id);
                                while ($pic = $pics->fetch(PDO::FETCH_NUM)) {
                                    echo "<img src='/teachers_pics/thumbs/{$pic[0]}' title='{$first_name} {$last_name}' class='teacher_pic'>";
                                    $i++;
                                }
                                $j = 4 - $i;
                                for ($i = 0; $i < $j; $i++) {
                                    echo '<img src="/_css/img/user-64.png" class="teacher_pic">';
                                }
                                ?>
                            </div>
                            <form id="teacher_images"
                                  action="#"
                                  method="post"
                                  enctype="multipart/form-data">
                                <input type="file"
                                       id="teacher"
                                       class="teacher hidden"
                                       name="teacher_image_upload"
                                       value="submit">
                            </form>
                        </div> <!-- / tabs_item -->
                    <?php } ?>
                </div>
                <!-- / tab_content -->
            </div>
            <!-- / tab -->
        </div>
    </div>
<?php layout_template('foot/foot_section'); ?>