<?php
$permission_name = 'page_view';
require_once(__DIR__ . '/../../includes/initial.php');
if (!person_is_exist($cUser->user_id)) {
    //    if(!person_is_exist($_GET['userID'])) {
    message('چنین فردی وجود ندارد');
    redirect_to(BASEPATH);
}
if (!person_isConfirm_by_id($cUser->user_id)) {
    //  Do Not show the Page
    message('این کاربر تایید نشده است');
    redirect_to(BASEPATH . urlencode($cUser->user_id));
}
if (isset($_POST['address_submit'])) {
    $required_fields         = array(
            'mobile',
            'phone',
            'address',
    );
    $fields_with_max_lengths = array(
            'mobile'  => 11,
            'phone'   => 11,
            'address' => 100,
    );
    $fields_with_min_lengths = array(
            'mobile'  => 11,
            'phone'   => 11,
            'address' => 15,
    );
    if (!validate_presences($required_fields) || !validate_input_lengths($fields_with_max_lengths, $fields_with_min_lengths)) {
        $class = 'error';
    }

    if (!$message = errors_to_ul($errors)) {
        $update = address_update($cUser->user_id, $_POST['mobile'], $_POST['phone'], $_POST['address']);
        if ($update) {
            $errors[] = 'تغییرات اطلاعات ارتباطی با موفقیت اعمال شد';
            $class    = 'success';
        }
        else {
            $errors[] = 'تغییرات اعمال نشد لطفا دوباره امتحان کنید';
            $class    = 'error';
        }
    }

    message($errors, $class, TRUE);
    // prevent resubmit information on refresh with Ctrl + r
    redirect_to($referer);
}

$addInfo = address_by_person_id($cUser->user_id);
$mobile  = $addInfo['mobile'];
$phone   = $addInfo['phone'];
$address = $addInfo['address'];

?>
<?php $title = 'تغییر اطلاعات ارتباطی'; ?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php //echo '<ul class="errors"></ul>'; ?>
    <div class="content">
        <div class="setting">
            <div class="tab">
                <div class="warnning">
                    <!--          <span style="margin: 0 auto;"> -->
                    <?php //echo $message ?><!-- </span>-->
                </div>
                <ul class="tabs">
                    <li><a href="/setting/personal">اطلاعات فردی</a></li>
                    <li class="current"><a href="/setting/connection">اطلاعات
                            ارتباطی</a></li>
                    <li><a href="/setting/security">امنیت</a></li>
                    <?php if (FALSE/*is_teacher($_SESSION['userID']*/) {
                        echo '<li><a href="#">قرار دادن تصاویر</a></li>';
                    } ?>
                </ul>
                <!-- / tabs -->
                <div class="tab_content">
                    <div class="tabs_item">
                        <form action="/setting/connection" method="post" class="formValidator">
                            <div class="input-group">
                                <label class="">شماره موبایل </label><input
                                        type="text"
                                        name="mobile"
                                        class="new_input numberValidation"
                                        value="<?php echo $mobile; ?>"
                                        maxlength="11"
                                        minlength="11"
                                        >
                            </div>
                            <div class="input-group">
                                <label class="">شماره ثابت </label><input
                                        type="text"
                                        name="phone"
                                        class="new_input numberValidation"
                                        value="<?php echo $phone; ?>"
                                        maxlength="11"
                                        minlength="11"
                                        >

                            </div>
                            <div class="input-group">
                                <label class="">آدرس </label>
                                <input type="text" name="address"
                                       data-no-lang
                                       class="new_input new_long_input remainChars addressValidation"
                                       value="<?php echo $address; ?>"
                                       maxlength="100"
                                       minlength="11"
                                        >
                            </div>
                            <input type="submit"
                                   class="btn btn_orange btn_medium"
                                   name="address_submit"
                                   value="تغییر اطلاعات">
                        </form>
                    </div>
                    <!-- / tabs_item -->

                </div>
            </div>
        </div>
    </div>
<?php layout_template('foot/foot_section'); ?>