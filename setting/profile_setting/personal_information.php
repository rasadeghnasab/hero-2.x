<?php
$permission_name = 'page_view';
require_once(__DIR__ . '/../../includes/initial.php');
if (!person_is_exist($cUser->user_id)) {
  //    if(!person_is_exist($_GET['userID'])) {
  message('چنین فردی وجود ندارد', 'error', TRUE);
  redirect_to(BASEPATH);
}
if (!person_isConfirm_by_id($cUser->user_id)) {
  //  Do Not show the Page
  message('این کاربر تایید نشده است', 'error', TRUE);
  redirect_to(BASEPATH . urlencode($cUser->user_id));
}
if (isset($_POST['primary_submit'])) {
    $required_fields         = array(
        //            'Fname'   ,
        //            'Lname'   ,
        'length',
        'weight',
        'national_code',
        'degreeID',
        'description',
    );
    $fields_with_max_lengths = array(
        //            'Fname'   => 30,
        //            'Lname'   => 30,
        'length'        => 3,
        'weight'        => 3,
        'national_code' => 10,
        'degreeID'      => 2,
        'description'   => 3000,
        //            'national_code'=> 10
    );
    $fields_with_min_lengths = array(
            'length'        => 2,
            'national_code' => 10,
            'weight'        => 2,
            'degreeID'      => 1,
    );
    if (!validate_presences($required_fields) || !validate_input_lengths($fields_with_max_lengths, $fields_with_min_lengths)) {
        $class = 'error';
    }
    $location    = SITE_ROOT . DS . 'profilePics';
    $file_name   = 'profImgUpload';
    $image_check = image_upload_check($location, $file_name, 5);
    if (!$message = errors_to_ul($errors)) {
        if ($image_check) {
            /* upload image */
            if ($image = check_profile_image_existence($cUser->user_id)) {
                // update image
                if (!profile_image_upload($cUser->user_id, $location, $cUser->user_id, $file_name, 'update')) {
                    ;
                }
                $errors[] = 'تصویر به درستی دریافت نشد';
                $class = 'warning';
            }
            else {
                // insert image
                $img_width  = 200;
                $img_height = 200;
                if (!profile_image_upload($cUser->user_id, $location, $cUser->user_id, $file_name, 'insert')) {
                    $errors[] = 'تصویر به درستی دریافت نشد';
                    $class = 'warning';
                }
            }
        }
        foreach ($required_fields as $variable_name) {
            $$variable_name = $_POST[$variable_name];
        }
        /* update user table information */
        $db->beginTransaction();
        $update = user_update(array(
                'length'        => $length,
                'weight'        => $weight,
                'degreeID'      => $degreeID,
                'description'   => $description,
                'national_code' => $national_code,
        ), array('userID' => $cUser->user_id));
        if ($update) {
            $db->commit();
            $errors[] = 'تغییرات اطلاعات فردی با موفقیت اعمال شد';
            $class = 'success';
        }
        else {
            $db->rollBack();
            $errors[] = 'تغییرات اعمال نشد لطفا دوباره امتحان کنید';
            $class = 'error';
        }
        // update userTable //create a function to dynamically add fields and values to query.
    }

    message($errors, $class, TRUE);
    // prevent resubmit information on refresh
    redirect_to($referer);
}
$person = $cUser;
/* primary information */
$first_name    = $person->first_name;
$last_name     = $person->last_name;
$userPic       = $person->pic;
$state         = $person->state;
$county        = $person->county;
$weight        = $person->weight;
$length        = $person->length;
$birth_date    = $person->birth_date;
$description   = $person->description;
$national_code = $person->national_code;
$degreeID      = $person->degreeID;
$username      = $person->username;
$email         = $person->email;
//$foot       = $person ? 'چپ' : 'راست';
/* address information */
//$addInfo = address_by_person_id($person->user_id);
//$mobile  = $addInfo['mobile'];
//$phone   = $addInfo['phone'];
//$address = $addInfo['address'];

?>
<?php $title = 'تغییر اطلاعات شخصی'; ?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php echo '<ul class="errors"></ul>'; ?>
<div class="content">
  <div class="setting">
    <div class="tab">
      <div class="warnning">
        <!--          <span style="margin: 0 auto;"> -->
        <?php //echo $message ?><!-- </span>-->
      </div>
      <ul class="tabs">
        <li class="current"><a href="/setting/personal">اطلاعات فردی</a></li>
        <li><a href="/setting/connection">اطلاعات ارتباطی</a></li>
        <li><a href="/setting/security">امنیت</a></li>
      </ul>
      <!-- / tabs -->
      <div class="tab_content">
        <div class="tabs_item">
          <!--            <label class=""> -->
          <div class="input-group">
            <img id="image_setting"
                 class="set_img fl"
                 src="<?php echo $userPic ?>">
          </div>
          <form method="post"
                action="/setting/personal"
                class="form-grouper formValidator"
                enctype="multipart/form-data">
            <input id="profImage"
                   class="hidden"
                   name="profImgUpload"
                   type="file"/>

            <div class="input-group">
              <label for="length" class="">قد </label>
              <input type="text" name="length" id="length"
                     class="new_input numberValidation"
                     placeholder="سانتی متر"
                     data-help='قد خود را به سانتی متر وارد نمایید'
                     maxlength="3"
                     minlength="2"
                     data-wnrange = '40-250'
                     value="<?php echo $length ?>">
            </div>
            <div class="input-group">
              <label for="weight" class="">وزن </label>
              <input type="text" name="weight" id="weight"
                     class="new_input numberValidation"
                     value="<?php echo $weight ?>"
                     data-help='وزن خود را به کیلوگرم وارد نمایید'
                     maxlength="3"
                     minlength="2"
                     data-wnrange = '10-200'
                     placeholder="کیلوگرم"
                >
            </div>
            <div class="input-group">
              <label class="">کد ملی</label>
              <input type="text" name="national_code"
                     id="national_code" class="new_input numberValidation"
                     maxlength="10"
                     minlength="10"
                     value="<?php echo $national_code; ?>"
                     data-help='کد ملی ده رقمی خود را وارد نمایید'
                     placeholder="کد ملی"
                >
            </div>
            <div class="input-group">
              <!-- <label class = "">کد ملی :</label>
                <input type = "text" name = "national_code" disabled id = "national_code" class = "new_input" value = "<?php /*echo $national_code */ ?>">-->
              <label for="degreeID" class="">تحصیلات </label>
              <?php
              $degree_attrs = array(
                'name'      => 'degreeID',
                'id'        => 'degreeID',
                'data-help' => 'در این تحصیلات خود را مشخص نمایید',
                'class'     => 'chosen-select chosen-rtl',
              );
              echo select_element(degree_select_all('PDOObject'), 'degID', 'degName', $degree_attrs, $degreeID);
              ?>
            </div>
            <div class="input-group">
              <label class="">درباره من </label>

              <div class="input-container">
                <textarea type="text" name="description" id="description"
                          data-no-lang
                          class="new_medium_input remainChars addressValidation"
                          data-help="در این کادر متنی در مورد خود بنویسید"
                          maxlength="3000"
                          minlength="11"
                        ><?php echo $description; ?></textarea>
              </div>
            </div>
            <input type="submit"
                   class="btn btn_orange btn_medium"
                   name="primary_submit"
                   value="تغییر اطلاعات">
          </form>
        </div>
        <!-- / tabs_item -->
      </div>
    </div>
  </div>
</div>
<?php layout_template('foot/foot_section'); ?>
