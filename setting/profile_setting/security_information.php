<?php
$permission_name = 'page_view';
require_once(__DIR__ . '/../../includes/initial.php');
if (!person_is_exist($cUser->user_id)) {
    //    if(!person_is_exist($_GET['userID'])) {
    message('چنین فردی وجود ندارد', 'error', TRUE);
    redirect_to(BASEPATH);
}
if (!person_isConfirm_by_id($cUser->user_id)) {
    //  Do Not show the Page
    message('این کاربر تایید نشده است', 'error', TRUE);
    redirect_to(BASEPATH . urlencode($cUser->user_id));
}

if (isset($_POST['security_submit'])) {
    $required_fields         = array(
            'username',
            'email',
    );
    $fields_with_max_lengths = array(
            'username' => 30,
            'email'    => 40,
    );
    $fields_with_min_lengths = array(
            'username' => 8,
            'email'    => 10,
    );
    if (isset($_POST['oldpassword']) && has_presence($_POST['oldpassword'])) {
        $required_fields1         = array(
                'oldpassword',
                'newpassword',
                'repassword',
        );
        $fields_with_max_lengths1 = array(
                'oldpassword' => 40,
                'newpassword' => 40,
                'repassword'  => 40,
        );
        $fields_with_min_lengths1 = array(
                'oldpassword' => 8,
                'newpassword' => 8,
                'repassword'  => 8,
        );
        $required_fields          = array_merge($required_fields, $required_fields1);
        $fields_with_max_lengths  = array_merge($fields_with_max_lengths, $fields_with_max_lengths1);
        $fields_with_min_lengths  = array_merge($fields_with_min_lengths, $fields_with_min_lengths1);
    }

    if (!validate_presences($required_fields) || !validate_input_lengths($fields_with_max_lengths, $fields_with_min_lengths)) {
        $class = 'error';
    }
    if (!$message = errors_to_ul($errors)) {
        if (isset($_POST['oldpassword']) && has_presence($_POST['oldpassword'])) {
            // change password
            $update = user_pass_update($cUser->user_id, $_POST['username'], $_POST['email'], $_POST['oldpassword'], $_POST['newpassword'], $_POST['repassword']);
        }
        else {
            $update = user_pass_update($cUser->user_id, $_POST['username'], $_POST['email']);
        }

        if ($update) {
            $errors[] = 'تغییرات امنیتی با موفقیت اعمال شد';
            $class    = 'success';
        }
        else {
            $errors[] = 'تغییرات اعمال نشد لطفا دوباره امتحان کنید';
            $class    = 'error';
        }
    }
    message($errors, $class, TRUE);
    // prevent resubmit information on refresh
    redirect_to($referer);
}

//$person = find_person_by_id($cUser->user_id);

/* primary information */
//$first_name    = $person['first_name'];
//$last_name     = $person['last_name'];
//$userPic       = $person['picurl'] ? '/profilePics/' . $person['picurl'] : '/_css/img/user-64.png';
//$state         = $person['statename'];
//$county        = $person['countyname'];
//$weight        = $person['weight'];
//$length        = $person['length'];
//$birth_date    = r_gregorian_to_jalali($person['birth_date']);
//$description   = $person['description'];
//$national_code = $person['national_code'];
//$degreeID      = $person['degreeID'];
$username      = $cUser->username;
$email         = $cUser->email;
//$foot          = $person ? 'چپ' : 'راست';
/* address information */
//$addInfo = address_by_person_id($cUser->user_id);
//$mobile  = $addInfo['mobile'];
//$phone   = $addInfo['phone'];
//$address = $addInfo['address'];

?>
<?php $title = 'تغییر اطلاعات امنیتی'; ?>
<?php layout_template('head_section'); ?>
<?php layout_template('header'); ?>
<?php echo '<ul class="errors"></ul>'; ?>
<div class="content">
    <div class="setting">
        <div class="tab">
            <div class="warnning">
                <!--          <span style="margin: 0 auto;"> -->
                <?php //echo $message ?><!-- </span>-->
            </div>
            <ul class="tabs">
                <li><a href="/setting/personal">اطلاعات فردی</a></li>
                <li><a href="/setting/connection">اطلاعات ارتباطی</a></li>
                <li class="current"><a href="/setting/security">امنیت</a></li>
                <?php if (FALSE/*is_teacher($_SESSION['userID']*/) {
                    echo '<li><a href="#">قرار دادن تصاویر</a></li>';
                } ?>
            </ul>
            <!-- / tabs -->
            <div class="tab_content">
                <div class="tabs_item">
                    <form action="/setting/security" method="post"
                          class="form-grouper formValidator">
                        <div class="input-group">
                            <label for="username" class="">نام
                                کاربری</label>
                            <input type="text"
                                   name="username"
                                   id="username"
                                   class="new_medium_input new_input"
                                   maxlength="30"
                                   minlength="8"
                                   required
                                   placeholder="نام کاربری"
                                   value="<?php echo $username; ?>"
                                    >
                        </div>
                        <div class="input-group">
                            <label for="email" class="">آدرس ایمیل </label>
                            <input type="text"
                                   name="email"
                                   id="email"
                                   class="new_medium_input new_input"
                                   maxlength="40"
                                   minlength="10"
                                   required
                                   value="<?php echo $email; ?>"
                                    >
                        </div>
                        <div class="input-group">
                            <label for="oldpassword" class="">رمز عبور فعلی</label><input
                                    type="password"
                                    name="oldpassword"
                                    id="oldpassword"
                                    class="new_input">
                        </div>
                        <div class="input-group">
                            <label for="newpassword" class="">رمز عبورجدید</label><input
                                    type="password"
                                    name="newpassword"
                                    id="newpassword"
                                    class="new_input">
                        </div>
                        <div class="input-group">
                            <label for="repassword" class="">تکرار رمز عبور</label><input
                                    type="password"
                                    name="repassword"
                                    id="repassword"
                                    class="new_input">
                        </div>
                        <input type="submit"
                               class="btn btn_orange btn_medium"
                               name="security_submit"
                               value="ثبت">
                    </form>
                </div>
                <!-- / tabs_item -->

            </div>
        </div>
    </div>
</div>
<?php layout_template('foot/foot_section'); ?>
