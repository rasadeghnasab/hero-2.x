<?php
/*
 * if it is my profile and I'm a student I can just edit my honors
 * if it is my profile and I'm a teacher I can edit my honors and my classes
 * if it is not my profile and It is my student's profile I can edit his/her belt, is_teacher, is_referee
 * if it is not my profile and It is my student's profile and I'm a sport master I can set him/her as state indicator
 */
/* permission validation */
$permission_name = 'page_view';
/* permission validation */
require_once(__DIR__ . '/../../includes/initial.php');
$user_id    = isset($_GET['id']) ? $_GET['id'] : NULL;
$viewer_id  = isset($cUser->user_id) ? $cUser->user_id : 0;
$is_teacher = $honors = $classes = $your_student = $self_teacher = FALSE;
pagination_variables('GET');
if ($viewer_id == $user_id) { // it is viewer user profile
    //your honors
    $honors = TRUE;
    //if your are a teacher your classes
    $classes = $is_teacher = is_teacher($user_id);
}
elseif (is_teacher_student_by_infoid($_GET['info'], $viewer_id)) { // viewer student profile
    // belt, is_teacher, is_referee
    $your_student = TRUE;
}
else {
    // you can not access to this page
    require_once(__DIR__ . '/../../errors/403.php');
    exit;
}

if ($Acl->check_permission('self_teacher') && is_teacher_student_by_infoid($_GET['info'], $viewer_id)) {
    $self_teacher = TRUE;
}

?>
<?php
$title = 'ویرایش اطلاعات ';
$title .= $honors ? $_SESSION['first_name'] : 'شاگردان';
layout_template('head_section');
layout_template('header');
?>
    <div class="content">
        <?php echo $back_btn; ?>
        <?php
        if (!$your_student || $self_teacher) {
            teacher_submenu($user_id, $_GET['info'], TRUE, TRUE, $is_teacher);
        }
        if ($honors && isset($_GET['type']) && $_GET['type'] == 'honors') {
            require_once(SITE_ROOT . DS . 'setting' . DS . 'honors.php');
        }
        if ($classes && isset($_GET['type']) && $_GET['type'] == 'classes') {
            require_once(SITE_ROOT . DS . 'teacherPhpFiles' . DS . 'teacher-classes-edit.php');
        }
        if ($your_student || ($self_teacher && isset($_GET['type']) && $_GET['type'] == 'students')) require_once(SITE_ROOT . DS . 'setting' . DS . 'user_sport_setting.php')
        ?>
    </div>
<?php
layout_template('foot/foot_section');
?>