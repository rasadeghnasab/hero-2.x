<?php
/*
 * if person who looking at this page -> /setting/sports/#user_id/#info_id/honors
 * is a teacher of this user with this user id -> (#user_id) this page (user_sport_setting.php) will include
 */
if (isset($_POST['submit'])) {
  /* permission options */
  $permission_name = 'student_setting';
  $return = TRUE;
  /* permission options */
  require_once(__DIR__ . '/../includes/initial.php');
  $required_fields = array(
    'belt',
    'infoid',
    'weight',
  );
  $sportID = $_SESSION['sportID'];
  /* if this teacher is a sport master
   * he/she can change user state and change user job
   */
  $sport_master = $Acl->check_permission('change_student_state', $permission['group_id'], $permission['user_id']);
  /* if sport_master
   * 1 - update user jobID in infotable
   * 2 - update user state in user table
   * 3 - if select from place where stateID = x AND sportID = y return 0 result
   *     insert one row in place table.
   * //4 - check for all state in this sport that has state indicator or not?
   * 4 - select this user state before update it and after update check in infotable table
   *     to see is there any state_indicator in this state or not ??? if return 0 result we must
   *     DELETE FROM place table WHERE stateID = x and sportID = y
   *     $sportID = $_SESSION['sportID']
   */
  // add job, countyResult to $required_fields
  if ($sport_master) {
    array_merge($required_fields, array(
      'job',
      'countyResult'
    ));
  }
  if (!validate_presences($required_fields) || $error = errors_to_ul($errors)) {
    echo json_encode($result['error'] = $error);
    die();
  }
  // change user weight
  $conditions = array('infoid' => $_POST['infoid']);
  $person = find_person_info_by_info_id($conditions)->fetch(PDO::FETCH_ASSOC);
  $where = array('userID' => $person['userID']);
  $weight_update_conds = array('weight' => $_POST['weight']);
  $weight_result = user_update_dynamic($weight_update_conds, $where);

  $is_teacher = isset($_POST['is_teacher']) ? 1 : NULL;
  $is_referee = isset($_POST['is_referee']) ? 1 : NULL;
  /* If jobID > 2 then he/she is a teacher or more
   * if is not then hi/she is just a student
   * */
  if ($sport_master) {
    $is_teacher = $_POST['job'] >= 2 ? 1 : NULL;
  }
  $columns_values = array(
    'beltID'     => $_POST['belt'],
    'is_teacher' => $is_teacher,
    'is_referee' => $is_referee
  );
  if ($sport_master) {
    $columns_values = array_merge($columns_values, array('jobID' => $_POST['job']));
    $conditions = array('infoid' => $_POST['infoid']);
    $person = find_person_info_by_info_id($conditions)->fetch(PDO::FETCH_ASSOC);
    $person_more_detail = find_person_by_id($person['userID']);
    $where = array('userID' => $person['userID']);
    $state_update_conds = array('stateID' => $_POST['countyResult']);
    $state_result = user_update_dynamic($state_update_conds, $where);

    if ($state_result) {
      /* check for master existence in person stateID */
      $columns_and_values = array(
        '`state`.`stateID` = :stateID' => $person_more_detail['stateID'],
        '`sport`.`id` = :sportID' => $sportID,
        '`infotable`.`jobID` > :jobID' => 1
      );
      pagination_variables();
      $master_on_state = select_all_people($columns_and_values)->rowCount();
      if ($master_on_state == 0) {
        /* if a person > 0 that means we have at least on teacher in that state :)
         * so that we don't have to do anything
         * else we must remove the place table record (stateID = $stateID && sportID = $sportID);
         */
        /* remove the place table record (stateID = $stateID && sportID = $sportID) */
        $where = array(
          '`stateID`'     => $person_more_detail['stateID'],
          'AND `sportID`' => $sportID
        );
        place_delete($where);
      }
      /* check **** new stateID existence **** in place table WHERE stateID = ? AND sportID = ? */
      $place = sport_place_existence($sportID, $_POST['countyResult']);
      if ($place) {
        /* it has no result, we must insert new one */
        $columns_values = array(
          'sportID' => $sportID,
          'stateID' => $_POST['countyResult']
        );
//        echo json_encode('hi');
        place_insert($columns_values);
        echo json_encode(place_insert($columns_values));
        die;
      }
    }
  }
  //////////////////////////
  $where = array('infoID' => $_POST['infoid']);
  infotable_update($columns_values, $where);
  /* if update_result is true it has two meaning
   * 1 - state_update_result is done by sport_master and it's true
   * 2 - the user is not sport_master and he is a normal teacher thus state_update_result is true by default
  */
  echo json_encode(TRUE);
  die();
}
/* if this teacher is a sport master
 * he/she can change user state and change user job
 */
$sport_master = $Acl->check_permission('change_student_state', $permission['group_id'], $permission['user_id']);
$conditions = array('infoid' => $_GET['info']);
$person = find_person_info_by_info_id($conditions)->fetch(PDO::FETCH_ASSOC);
$person_more_detail = find_person_by_id($person['userID']);

if ($sport_master) {
  // student job
  $job_attrs = array(
    'id'    => 'job',
    'name'  => 'job',
    'class' => 'chosen-select chosen-rtl'
  );
  $jobs = select_element(job_select($_SESSION['jobID']), 'jobID', 'jobName', $job_attrs, $person['jobID']);
  // student state name
  $state_attrs = array(
    'id'             => "stateResult",
    'name'           => 'stateResult',
    'data-ajax-dest' => "register/registerSCFB.php",
    'class'          => "chosen-select chosen-rtl",
    'required'
  );
  $state = select_element(state_distinct_select(), 'stateName', 'stateName', $state_attrs, $person_more_detail['statename']);
  // student county name
  $county_attrs = array(
    'id'    => "countyResult",
    'name'  => "countyResult",
    'class' => "chosen-select chosen-rtl",
    'required'
  );
  $county = select_element(stateID_county_by_stateName($person_more_detail['statename']), 'stateID', 'countyName', $county_attrs, $person_more_detail['stateID']);
}
?>
<h1>اطلاعات ورزشی
  <?= $person_more_detail['first_name'] . ' ' . $person_more_detail['last_name']; ?>
</h1>
<div class="new_inner_row">
  <form action="setting/user_sport_setting.php" class="form-grouper"
        method="post" id="<?= $person['userID'] ?>">
    <div class="split_bottom">
      <div class="input-group">
        <label for="belt">کمربند</label>
        <?php
        $belt_attrs = array(
          'id'    => 'belt',
          'name'  => 'belt',
          'class' => 'chosen-select chosen-rtl'
        );
        echo select_element(belt_by_sport($person['sportID']), 'beltID', 'beltName', $belt_attrs, $person['beltID']);
        ?>
      </div>
      <input type="hidden" id="infoid" name="infoid"
             value="<?= $person['infoID']; ?>">
      <input id="teacher" type="checkbox" name="is_teacher"
             value="1" <?php echo $person['is_teacher'] ? 'checked' : '' ?>/><label
        for="teacher">استاد</label>
      <input id="referee" type="checkbox" name="is_referee"
             value="1" <?php echo $person['is_referee'] ? 'checked' : '' ?>/><label
        for="referee">داور</label>
    </div>
    <?php if ($sport_master): ?>
    <div class="new_row split_bottom">
      <div class="input-group">
        <label for="job">سمت</label>
        <?= $jobs; ?>
      </div>
    </div>
    <div class="new_row split_bottom">
      <div class="input-group">
        <label for="state">استان</label>
        <?php
        // state name select
        echo $state;
        ?>
      </div>
      <div class="input-group">

        <label for="countyResult">شهر</label>
        <?php
        // city name select
        echo $county;
        ?>
      </div>
      <?php endif; ?>
    </div>
    <div class="new_row split_bottom">
      <div class="input-group">
        <label for="weight">وزن</label>
      <input type="text" name="weight" id = "weight" class="new_input" placeholder="وزن" value="<?= $person_more_detail['weight']; ?>">
      </div>
    </div>
    <input type="submit" value="ثبت" name="submit"
           class="btn btn_medium btn_blue std_spt_setting">
  </form>
</div>