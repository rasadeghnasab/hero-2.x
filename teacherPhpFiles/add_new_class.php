<?php
/*
  * give form information for new class
  * and sort and process theme into usable data for database insertion
 * */

/* permission options */
$permission_name = 'classes';
$return          = TRUE;
/* permission options */
require_once(__DIR__ . '/../includes/initial.php');
$new_class_info = array();
// check input data
// validation required no validation is done here
$required_fields         = array(
    'gym_address',
    'gym_name',
    //        'week_day'   ,
    //        'start_time'   ,
    //        'end_time'   ,
);
$fields_with_max_lengths = array(
    'gym_address' => 250,
    'gym_name' => 60,
    //        'week_day'         => 1,
    //        'start_time'       => 2,
    //        'end_time'   ,
);
$fields_with_min_length  = array(
    'gym_address' => 10,
    'gym_name' => 5,
);

validate_presences($required_fields);
validate_input_lengths($fields_with_max_lengths, $fields_with_min_length);
/*
 * if we have no error in validation, error is empty and we can set database fields
 */
$gym_values_to_insert = array();
if (!$message = errors_to_ul($errors)) {
  //  sort the $_POST data to use.
  $gym_values_to_insert['gym_address'] = $_POST['gym_address'];
  $gym_values_to_insert['gym_name']    = $_POST['gym_name'];
  foreach ($_POST as $key => $items) {
    if (is_array($items)) {
      foreach ($items as $item_key => $item) {
        $new_class_info[$item_key][$key] = $item;
      }
    }
  }
  //  database data insertion
  // insert into gym table and return gym_id
  $db_insert = TRUE;
  $db->beginTransaction();
  if ($db_insert = $last_insert_id = Gym::make_insert($gym_values_to_insert)) {
    // insert into teacher_class table and return class_id
    $teacher_class_values_insert            = array();
    $teacher_class_values_insert['gym_id']  = $last_insert_id;
//    if ($db_insert = teacher_class_insert(array_keys($teacher_class_values_insert), $teacher_class_values_insert)) {
    if ($db_insert = $class_id = gymClass::make_insert($teacher_class_values_insert)) {
      $class_day_time_values_insert = array();
      foreach ($new_class_info as $info) {
        // insert into class_day_time table
        $class_day_time_values_insert['class_id']   = $class_id;
        $class_day_time_values_insert['day']        = $info['week_day'];
        $class_day_time_values_insert['start_time'] = $info['start_time'];
        $class_day_time_values_insert['end_time']   = $info['end_time'];
        if (!classDayTime::make_insert($class_day_time_values_insert)) {
          $db_insert = FALSE;
          break;
        }
      }
    }
  };
//  echo json_encode($db_insert);
//  die;

  if ($db_insert == FALSE) {
    $db->rollBack();
    echo json_encode('false');
  }
  else {
    $db->commit();
//    $sorted_class = teacher_class_sort(teacher_class_by_classId_teacherID($class_id));
    $options = array(
      'conditions' => array(
        'class.id' => $class_id,
      ),
    );
//    echo json_encode($class_id);
    $classes = gymClass::dynamic_select($options);
    $sorted_class = gymClass::classes_sort($classes);
//    echo json_encode(teacher_class_to_string($sorted_class, $userID));
    ;
    $message = node_view_multiple('class', $sorted_class, 'node/class', 'full', 'function');
  }
}

echo json_encode($message);
