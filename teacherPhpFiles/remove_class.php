<?php
/**
 * Date: 2/25/15
 * Time: 11:01 PM
 */
/* permission options */
$permission_name = 'classes';
$return          = TRUE;
/* permission options */
require_once(__DIR__ . '/../includes/initial.php');
$required_fields = array(
    'class_id',
);

validate_presences($required_fields);

if ($message = errors_to_ul($errors)) {
  echo json_encode(FALSE);
  die();
}

$class_id = $_POST['class_id'];
// check to see is there any record with this class_id and user_id
//$class = teacher_class_by_classId_teacherID($_POST['class_id'], $cUser->user_id, array(':limit' => 1));
$_POST['limit'] = 1;
$class = $options = array(
  'conditions' => array(
    'class.id' => $class_id,
    'user_id' => $cUser->user_id
  ),
);
$classes = $transaction = gymClass::dynamic_select($options);
// if a record exists then delete it
if ($transaction != FALSE) {

  $classes = array_shift($classes);

  $db->beginTransaction();

  $transaction = Gym::delete_by_id($classes->gym_id);
  if ($transaction) $transaction = gymClass::delete_by_id($class_id);
  if ($transaction) $transaction = classDayTime::delete_by_entity_id($class_id);
  $transaction ? $db->commit() : $db->rollBack();
  $transaction = $transaction ? TRUE : FALSE;
}

echo json_encode($transaction);
