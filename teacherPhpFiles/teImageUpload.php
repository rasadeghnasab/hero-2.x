<?php
/**
 * Date: 11/10/14
 * Time: 9:24 PM
 */
/* permission options */
$permission_name = 'teacher_image_upload';
$return          = TRUE;
/* permission options */
require_once(__DIR__ . '/../includes/initial.php');
// if user not logged in must be redirect
$cUser->user_id    = $cUser->user_id;
$location  = '../teachers_pics/thumbs/';
$file_name = 'teacher_image_upload';
$message   = 'تصویر به درستی دریافت نشد';
//    sleep(8);
if (image_upload_check($location, $file_name, 5)) {
  $current_img = basename($_POST['current_img']);
  $current_img = preg_replace('/\?.*/i', '', $current_img);
  if (!file_exists($location . $current_img)) {
    if (teacher_image_count($cUser->user_id) < 4) {
      // insert
      $image_upload = teacher_image_upload($cUser->user_id, $location, $cUser->user_id, $file_name, 'insert');
      if ($image_upload !== FALSE) {
        $data['success'] = $image_upload;
      }
    }
    else {
      $data['success'] = htmlentities($_POST['current_img']);
    }
  }
  else {
    // update
    $image_upload = teacher_image_upload($cUser->user_id, $location, $current_img, $file_name, 'update');
    if ($image_upload !== FALSE) {
      $data['success'] = $image_upload;
    }
  }
}
else {
  $errors[] = $message;
}
if (!empty($data)) {
  echo json_encode($data);
}
else {
  echo json_encode(errors_to_ul($errors));
}

