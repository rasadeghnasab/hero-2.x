<?php
/**
 * Date: 1/12/15
 */
?>
<h1>کلاس ها و باشگاه های من </h1>
<fieldset class = "profile_field">
  <legend>افزودن کلاس جدید</legend>
  <div class = "box">
    <div class = "new_inner_row">
      <?php
      /* * create a form for teacher new classes * */
//      teacher_new_class_form($userID);
      node_form_template('new-class', array('user_id' => $user_id));
      ?>
    </div>
    <!-- new_inner_row -->
  </div>
    </fieldset>
<!-- box -->
  <?php
  // show all of teacher classes
  $options = array(
    'conditions' => array(
      'class.user_id' => $cUser->user_id
    ),
  );
  $classes = gymClass::dynamic_select($options);
  if ($classes) {
    $sorted_class = gymClass::classes_sort($classes);
    node_view_multiple('class', $sorted_class, 'node/class', 'full');
  }
  else {
    echo no_node_view('class');
  }

//  echo teacher_class_to_string($sorted_class, $userID);
  ?>
