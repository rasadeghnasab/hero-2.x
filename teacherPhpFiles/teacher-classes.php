<?php
/**
 * User: Ramin
 * Date: 12/15/14
 */
/* permission options */
//$permission_name = 'classes';
/* permission options */
$user_id = $_GET['teacher_id'];
require_once(__DIR__ . '/../includes/initial.php');
$options      = array(
  'conditions' => array(
    'user_id' => $user_id,
  ),
);
$classes      = gymClass::dynamic_select($options);
$sorted_class = gymClass::classes_sort($classes);
/*
$options = array(
  'conditions' => array(
    'class.user_id' => $cUser->user_id
  ),
);
//*/
$user         = user_load($user_id);
$teacher_name = $user->full_name();
layout_template('head_section');
layout_template('header');
?>
  <div class="content">
    <?php teacher_submenu($user_id, current($user->infid), FALSE, TRUE, TRUE); ?>

    <h1>کلاس ها و سانس های استاد <?= $teacher_name; ?></h1>

    <div class="profile_field">
      <?php
      if ($sorted_class) {
        node_view_multiple('class', $sorted_class, 'node/class', 'full');
      }
      else {
        echo no_node_view('class');
      }
      ?>
    </div>
    <?= $back_btn; ?>
  </div>
<?php layout_template('foot/foot_section'); ?>