<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/28/2015
 * Time: 8:48 PM
 */

$attributes = array(
  'id'    => 'evt-job-card-id',
  'name'  => 'evt_job_card',
  'class' => 'chosen-select chosen-rtl new_input',
);

$uid_attributes = array(
  'id'    => 'evt-job-card-uid',
  'name'  => 'uid',
  'class' => 'chosen-select chosen-rtl new_input',
);

$event_jobs = eventJob::find_by_id_sid(NULL, $sid);
$columns_and_values = array(
  '`sport`.`id` = :sportID'           => $sid,
  '`infotable`.`is_confirm` = :is_confirm' => 1,
);
$limit              = $page = NULL;
pagination_variables('get', array('page' => NULL, 'limit' => NULL));
$persons       = select_all_people($columns_and_values);
$persons_array = array();
while ($person = $persons->fetch(PDO::FETCH_ASSOC)) {
  $persons_array[$person['userID']] = full_name($person['full_name'], "({$person['statename']})");
}
//var_dump($persons_array);die;
// ajaxable input, select box for event-job
?>
<div class="ui segment">
  <form action="management/event-management/event-card-person-setup.php" method="post" id="person-card-setup">
    <input type="hidden" name="eid" class="hidden" value="<?= $form['eid']; ?>">
    <!--    <input type="text" class="new_input card-user-name" placeholder="--><?php //t('نام فرد مورد نظر'); ?><!--">-->
    <!--    <input type="hidden" name="name" class="hidden">-->

    <?php print select_element($persons_array, NULL, NULL, $uid_attributes); ?>
    <?php print select_element($event_jobs, 'id', 'name', $attributes); ?>
    <input type="submit" name="submit" class="btn btn_orange" value="<?php t('ثبت'); ?>">
  </form>
</div>
