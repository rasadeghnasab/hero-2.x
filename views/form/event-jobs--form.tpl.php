<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 7/1/2015
 * Time: 1:37 AM
 */
?>
<fieldset class="split_bottom new_row">
  <legend>
    <span class="glyphicon glyphicon-plus-sign"></span>
    <span><?php t('افزودن سمت برای رویدادها'); ?></span>
  </legend>

  <form action="management/sport-setting/event-job.php" id="new_event_job" method="post">
    <input type="text" class="new_input" name="job_name" placeholder="<?php t('نام سمت'); ?>"/>
    <input type="submit" class="btn btn_green" value="ثبت"/>
  </form>
</fieldset>