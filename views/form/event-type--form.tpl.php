<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 7/1/2015
 * Time: 1:37 AM
 */
?>
<fieldset class="split_bottom new_row">
  <legend>
	<span class="glyphicon glyphicon-plus-sign"></span>
	<span>افزودن نوع رویداد</span>
  </legend>

  <form action="management/sport-setting/event_type.php" id="new_evt_type"
		method="post">
	<input type="text" class="new_input" name="evt_type_name"
		   placeholder="نام نوع رویداد"/>
	<input type="checkbox" id="new_weight_req" name="weight_req"/>
	<label for="new_weight_req">نیاز به جدول بندی دارد</label>
	<!--        <div class="new_row">-->
	<input type="submit" class="btn btn_green" value="ثبت"/>
	<!--        </div>-->
  </form>
</fieldset>