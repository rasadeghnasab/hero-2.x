<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/9/2015
 * Time: 1:21 AM
 */
global $week_day;

$sport_attrs = array(
  'name'  => 'week_day[]',
  'class' => 'week_days'
);
?>
<form action="teacherPhpFiles/add_new_class.php" method="post" name="new_class_form">
  <div class="new_row new_inner_row">
    <input class="new_input" name="gym_name" placeholder="نام باشگاه">
    <input class="new_input new_long_input" type="text" name="gym_address" placeholder="آدرس باشگاه"/>
  </div>

  <div class="new_inner_row">
    <?php t('روز'); ?>
    <?php print select_element($week_day, 'sportID', 'sport_name', $sport_attrs); ?>
    <?php t('از ساعت'); ?>
    <input type="text" class="new_input four_digit_input" name="start_time[]" required="required" placeholder="شروع"/ >
    <?php t('تا'); ?>
    <input type="text" class="new_input four_digit_input" name="end_time[]" required="required" placeholder="اتمام"/>
  </div>
  <div class="new_inner_row">
    <span class="new_class btn btn_orange btn_small"> <span class="glyphicon glyphicon-plus-sign"></span> روز و ساعت</span>
  </div>
  <div class="new_inner_row">
    <input type="submit" class="btn btn_blue" name="submit" value="ثبت"/>
  </div>
</form>