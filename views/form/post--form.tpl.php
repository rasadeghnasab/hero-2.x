<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 7/1/2015
 * Time: 1:37 AM
 */
global $cUser;
$cat_attrs = array(
  'name'  => "catID[]",
  'id'    => "catID",
  'class' => "chosen-select chosen-rtl",
  'multiple',
  'style' => "width: 60%"
);
//var_dump($cUser);die;
?>
<div class="news_new">
  <form id="uploadImgForm" class="hidden" action="post/imageInsert.php"
        enctype="multipart/form-data" method="post">
    <input type="hidden" name="<?= ini_get("session.upload_progress.name") ?>"
           value="123">
    <input type="file" id="inputImg" name="userImage"
           class="setimg inpImg hidden">
  </form>
  <form id="wall_post" action="post/postInsert.php" data-ajax="true"
        method="post" novalidate="novalidate">
    <div class="input-container">
        <textarea rows="5" class="main_text" id="mainText" name="mainText"
                  placeholder="اطلاعات ورزشی خود را در اینجا با دیگران به اشتراک بگذارید..."
                  required="required"></textarea>

      <div id="imgBack"></div>
      <div class="clearfix"></div>
    </div>
    <!-- input-container -->
    <div class="news_footer">
        <span class="fl glyphicon glyphicon-plus set_img"
              style="font-size: .4em;"></span>
      <span class="fl glyphicon glyphicon-camera set_img"></span>
      <input class="btn btn_white btnForce" id="submitNews" type="submit"
             name="submit" value="ثبت">
		<span class="subject">
    <label for="catID">موضوع</label>
          <?= select_element(all_categories(), 'id', 'name', $cat_attrs); ?>
    </span>

        <?php
        if ($cUser->multi_sport):
          $my_sports = array();
          foreach($cUser->sid as $infid => $sid) {
            $my_sports[$sid] = $cUser->spName[$infid];
          }
          $sport_attrs = array(
            'name'  => "sport",
            'id'    => "sport_id",
            'class' => "chosen-select chosen-rtl",
            'style' => "width: 10%"
          );
          ?>
      <span>
        <span class="sport">
          <label for="sport_id">ورزش</label>
          <?= select_element($my_sports, 'id', 'name', $sport_attrs); ?>
        </span>
      </span>
          <?php endif; ?>
    </div>
    <!-- footer end -->
  </form>
  <!--form end-->
</div> <!--news_new-->
