<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/14/2015
 * Time: 12:25 AM
 */
?>

<form action="" method="post" class="ui yellow">

  <input type="radio" name="settlement_type" value="all" id="settlement-all" <?= form_values('settlement_type', 'radio', 'all'); ?>>
  <label for="settlement-all"><?php t('تسویه همه'); ?></label>

  <input type="radio" name="settlement_type" value="registered" id="settlement-registered"  <?= form_values('settlement_type', 'radio', 'registered'); ?>>
  <label for="settlement-registered"><?php t('تسویه ثبت نام ها'); ?></label>

  <input type="radio" name="settlement_type" value="events" id="settlement-events"  <?= form_values('settlement_type', 'radio', 'events'); ?>>
  <label for="settlement-events"><?php t('تسویه رویدادها'); ?></label>

  <input type="submit" name="submit" value="<?php t('تسویه'); ?>" class="btn btn_blue btn-block btn_medium">
</form>
