<?php
    $person_management = $_SESSION['jobValue'] > 30 ? '/admin/management/' : '/management/'; ?>
<div class="subMenu split_bottom hidden-print">
    <ul>
        <li><a href="<?php echo $person_management; ?>"> افراد</a></li>
        <li><a href="#"> اخبار</a></li>
        <li><a href="#"> آمار</a></li>
        <li><a href="#"> رشته ها</a></li>
        <li><a href="/management/events/"> رویدادها</a></li>
        <li><a href="#"> تنظیمات</a></li>
    </ul>
</div>