<!DOCTYPE html>
<head>
  <meta charset="utf-8"/>
  <title><?php global $title;
    echo $title ?></title>
  <?php global $meta_tag;
  print $meta_tag; ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="HandheldFriendly" content="true">
  <!--      <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">-->
  <?php
  $static_css = array(
    'normalize',
    'chosen.min',
    'pwt-datepicker',
    'jquery.bracket.min',
    'jquery.qtip.min',
    'semantic/semantic',
    'semantic/semantic_custom',
    'style',
    //    'bootstrap',
    //    'captionjs.min',
    //    'sweet-alert',
  );
  $css        = isset($css) ? $css : NULL;
  $hrefs      = css_js_static_dynamic_combine($static_css, $css);
  //  var_dump($hrefs);
  //  die;
  echo add_css($hrefs, '_css/');
  ?>
  <!--  <link href="data:image/x-icon;base64,AAABAAEAEBAAAAAAAABoBQAAFgAAACgAAAAQAAAAIAAAAAEACAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAx8fHAGhoaADZ2dkAg4ODAFhYWAC3t7cAAgICAPT09AD9/f0A////AKCgoACpqakA+Pj4AKurqwDo6OgAxsbGAPr6+gCbm5sA4eHhAFdXVwABAQEAaWlpAPz8/ADR0dEApqamACUlJQDs7OwAwcHBAP7+/gD39/cAqqqqAN7e3gBvb28A19fXAKysrADg4OAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoKCgoKCh0KCgoKCgoKCgoKCgoKCgodGAoFCgoKCgoKCgoKCgoKChMKCgoKCgoKCgoKCgoKCh0gCgoRHQoKCgoKCgoKChcJIwwIBwoKCgoKCgoKHwocCiQfCgAKCgoKCgoKCiMKBhQOHxIAIQoKCgoKCgoiHx8VAAAAABUKCgoKCgoKHR8fAAAAAAAACgoKCgoKCgofDAAAABkEBw8dCgoKCgoNDgMVFgAAGgoVCh0KCgoKHQoKCgoKCgEbHgsKCgoKCgoKCgoKCgoKChAdAgoKCgoKCgoKCgoKCh0KCh0KCgoKCgoKCgoKCgoKCgoKCv//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=" rel="icon" type="image/x-icon" />-->
  <link href="/_css/icon/favicon_transparent.png" rel="icon"/>
  <!--    <script type="text/javascript" src="/_js/modernizr.custom.js"></script>-->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <script
    src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->
  <script>
    var basePath = '<?php echo BASEPATH; ?>';
  </script>
</head>
<body class="<?= isset($body_class) ? $body_class : NULL; ?>">