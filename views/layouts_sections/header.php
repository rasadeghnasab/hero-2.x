<?php
global $logged_in;
global $Acl;
global $cUser;
//global $management_notification;
//global $my_notifications;
/* TODO: must think more for header view permission to reduce database querying */
//var_dump($Acl);die;
$teacher = $management = $admin = $registered_user = FALSE;
if (isset($cUser->user_id)) {
  if ($Acl->check_permission('admin')) {
    $registered_user = $teacher = $management = $admin = TRUE;
  }
  elseif ($Acl->check_permission('management')) {
    $registered_user = $teacher = $management = TRUE;
  }
  elseif ($Acl->check_permission('teacher')) {
    $registered_user = $teacher = TRUE;
  }
  elseif ($Acl->check_permission('page_view')) {
    $registered_user = TRUE;
  }
}
//var_dump($teacher);
?>
<header class = "header">
  <div id="morphsearch" class="morphsearch hidden-print">
    <form class="morphsearch-form">
      <span class="glyphicon glyphicon-search"></span>
      <input name="searchtext" class="morphsearch-input" type="search" placeholder="جستجو ..."/>
      <button class="morphsearch-submit" type="submit">Search</button>
    </form>
    <div class="morphsearch-content">

    </div><!-- /morphsearch-content -->
    <span class="morphsearch-close"><span class="glyphicon glyphicon-remove"></span><abbr
        title = "برای خروج میتوانید از دکمه Escape نیز استفاده نمایید">Esc</abbr></span>
  </div><!-- /morphsearch -->

  <div class = "logo" alt = "لوگو سایت قهرمانان، heros.ir">قهرمانان</div>

  <?php if ($registered_user) { ?>
    <div class = "profset hidden-print">
<!--      <div class="header-notification fr">-->
<!--        <span class="glyphicon glyphicon-globe badge-holder">-->
<!--          <span class="new_badge">2</span>-->
<!--        </span>-->
<!--        <span class="glyphicon glyphicon-envelope"></span>-->
<!--      </div>-->
      <div id = "profMenu">
        <ul>
          <!-- <li><a href="/" > تکنیک </a></li>-->
          <li>
            <a href = "/setting/personal"> <span class="glyphicon glyphicon-cog"></span> تنظیمات </a>
          </li>
          <li><a href = "/login/logout.php"> <span class="glyphicon glyphicon-log-out"></span> خروج </a></li>
        </ul>
      </div>
            <span class = "name profSetIcon">
                <span class = "glyphicon glyphicon-chevron-down prof_menu_open"></span>
                <a href = "<?php echo '/' . $cUser->user_id; ?>"><?php echo htmlentities($_SESSION['first_name']); ?></a>
            </span>
      <a href = "<?php echo '/' . $cUser->user_id; ?>"><img class = "prfoImg" src = '<?php echo $_SESSION['picurl'] ?>'></a>
    </div>
  <?php
  }
  else {
    ?>
    <div class = "profset hidden-print">
      <span class = "name">
        <a href = "<?php echo BASEPATH;?>">ورود </a>
        <span> / </span>
        <a href = "<?php echo BASEPATH.'register'?>">ثبت نام</a>
      </span>
    </div>
  <?php
  };
  ?>

  <div class="overlay"></div>
  <ul class = "menu nav hidden-print">
    <?php
    $menu = '';
    if ($registered_user) {
      $menu .= '<li><a href="/" class="no-title" title="آموزش فنون در ورزش های مختلف">تکنیک</a></li>';
    }
    else {
      $menu .= '<li><a href="/martial" class="no-title" title="آموزش فنون در ورزش های مختلف">تکنیک</a></li>';
    }
    $menu .= '<li><a href="/masters" class="no-title" title="بنیان گذاران و اساتید رشته های مختلف رزمی">ورزش  ها</a></li>';
        $menu .= '<li><a href="/news/all" class="no-title" title="اخبار ورزشی در رشته های مختلف رزمی">اخبار</a></li>';
    $badge_evt_class = (isset($_SESSION['new_event']) && $_SESSION['new_event'] > 0) ? ' class = "badge-holder" ' : NULL;
    $menu .= "<li $badge_evt_class >";
    if (isset($_SESSION['new_event']) && $_SESSION['new_event'] > 0) {
      $menu .= '<span class="badge">'.$_SESSION["new_event"].'</span>';
    }
    $menu .= '<a href="/events" class="no-title" title="رویدادها و جدول مسابقات در رشته های مختلف رزمی">  رویداد ها</a>';
    $menu .= '</li>';
//    $menu .= '<li><a href="/search/">جستجو</a></li>';
//    $badge_teacher_class = (isset($_SESSION['student_reg']) && $_SESSION['student_reg'] > 0) ? ' class = "badge-holder" ' : NULL;
    if ($teacher) { //if $_SESSION['jobValue'] >= 5 (is teacher and more)
      $menu .= "<li>" ;
      $menu .= '<a href="/control">شاگردان من </a>';
      // if teacher have any new student registration
      if (isset($_SESSION['student_reg']) && $_SESSION['student_reg'] > 0)
      {
        $menu .= "<span class='badge'>{$_SESSION['student_reg']}</span>";
      }
      $menu .='</li>';
    }
    // if user jobValue >= 10 that mean a person greater than a teacher so the user can access to management or admin
    if ($management || $admin) {
      $menu .= '<li><a ';
      $menu .= $management ? 'href="'.BASEPATH.'management/"' : 'href="'.BASEPATH.'admin/"';
      $menu .= '>مدیریت</a></li>';
    }
//    if($registered_user) {
//      $menu .= '<li>';
//      $menu .= "<a href=".BASEPATH."my-posts>نوشته های من</a>";
//      $menu .= '</li>';
//    }
//    if ($registered_user)     {
//      $sport = sportName_by_sportID($_SESSION['sportID']);
//      $sport_name = $sport['field'].' '.$sport['branch'];
//      $menu .= '<li><a href="/masters/'.$_SESSION['sportID'].'">'. $sport_name .'</a></li>';
//    }
    //    if ($registered_user) {
    //      $menu .= '<li><a href="/request/">درخواست</a></li>';
    //    }
    //    $menu .= '<!--<li><a href="#">فروشگاه</a></li>-->';
    echo $menu;
    ?>
  </ul>
  <img src = "<?php echo BASEPATH; ?>_css/img/loading.gif" class = "loading" /></div>
  <div class = "goToTop"></div>
</header><!-- end of header -->
<?php layout_template('page-message'); ?>