<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 5/10/15
 * Time: 12:43 AM
 */
?>
<div class = "modal">
  <div class = "modal_close"><span class = "glyphicon glyphicon-remove"></span></div>
  <div class = "modal_container">
    <div class = "modal_description">
      <header>
        <h1>درباره من</h1>
        <!--                <img class="modal_header_img" src="-->
        <?php //echo $_SESSION['picurl'];?><!--" alt=""/>-->
      </header>
      <?php echo '<p>' . $description . '</p>'; ?>
    </div>
    <!--<div class = "modal_image">
      <span class = "slidshow_btn next_btn"></span>
      <span class = "slidshow_btn prev_btn"></span><img src = "">
    </div>-->
  </div>
</div>
<div class = "cover-photo box_shadow">
  <!--  <img class="cover-photo-img" src = "../teachers_pics/edit1.jpg" alt = ""  />-->
<!--  <form name="cover_photo" enctype="multipart/form-data" method="post" action="upload.php" />-->
<!--    <input type="file" class="hidden cover-photo-file" name="my_field" value="" id="dnd_field" />-->
<!--  </form>-->
  <?php if($my_profile && $logged_in): ?>
    <div class = "edit-box">
      <span class = "cover_image_btn glyphicon glyphicon-camera btn_white btn btn_medium box_shadow"></span>
    </div>
    <form id="cover-photo-form" class="hidden" action="image_handle/profile_cover.php" enctype="multipart/form-data" method="post">
      <input id="cover-photo-file" type="file" name="cover_photo" class="cover-photo-file" multiple accept="image/*">
      <input type="text" name="cover_field"/>
    </form>
  <?php endif; ?>
<!--  <div class = "cover-photo-img" style = "background-image: url(/teachers_pics/edit12.jpg)"></div>-->
  <?php echo $cover_photo; ?>
  <div class="profile_img">
    <?php echo "<img src= '{$profilePic}'  class='show_modal_description box_shadow' title='دربار من'>"; ?>
    <?php if($my_profile && $logged_in && FALSE): ?>
    <span class="edit-box">
      <span class="profile-img-change btn btn_white glyphicon glyphicon-camera"></span>
    </span>
      <form id="profile-photo-form" class="hidden" action="/image_handle/profile_profile.php" enctype="multipart/form-data" method="post">
      <input id="profile-photo-file" type="file" name="profile_photo" class="profile-photo-file" accept="image/*">
<!--      <input type="text" name="profile_field"/>-->
    </form>
    <?php endif; ?>
  </div>
  <?php if (!$my_profile && $logged_in) : ?>
    <div class = "cover_buttons">
      <?php if($this_prof_student): ?>
      <button id = "askbtn"
              class = "btn btn_white askbutn <?php echo $this_prof_student ? " btn_checked" : " btn_check" ?>"
          <?= 'data-user = "' . $user_id . '"'; ?>>
        <?php if ($this_prof_student) {
          echo 'استاد شما';
        }
        else echo 'درخواست' ?></button>
      <?php endif;  ?>
      <button id = "rqtbtn" class = "btn folbutn btn_white <?php if ($userFlo) echo " btn_checked" ?>"
          <?= 'data-user = "' . $user_id . '"'; ?>>
        <?php if ($userFlo) {
          echo '<span class="glyphicon glyphicon-remove"></span> <span class="text_holder">لغو دنبال</span>';
        }
        else echo '<span class="glyphicon glyphicon-check"></span> <span class="text_holder">دنبال</span>'; ?>
      </button>
    </div>
  <?php endif; ?>
  <div class = "profile_info  box_shadow">
    <div class = "profile_name">
      <a href = "/<?php echo $user_id; ?>"><?php echo $first_name . ' ' . $last_name ?></a>
    </div>
    <span class = "profile_location">
      <span class = "glyphicon glyphicon-map-marker"></span>
      <?php echo $profileLocation ?>
    </span>

  </div>

</div>
<div class = "profile_menu box_shadow">
  <div class="profile_menu_container">
  <div><a href = "<?php echo "/{$user_id}"; ?>">صفحه شخصی</a></div>
  <div><a href = "<?php echo "/pictures/{$user_id}"; ?>">تصاویر</a></div>
  <div><a href = "<?php echo "/following/{$user_id}"; ?>">دوستان خوش تکنیک من
    <span class = "muted"><?php echo $following_num; ?> </span></a></div>
  <div><a href = "<?php echo "/followers/{$user_id}"; ?>">دنبال کنندگان
    <span class = "muted follower_num"><?php echo $followers_num; ?> </span></a></div>
  <?php if($is_teacher): ?>
  <div><a href = "<?php echo "/students/$user_id"; ?>">شاگردان <span class = "muted"><?php echo $profileStudents; ?><span>
  </a></div>
  <?php endif; ?>

  <?php if ($has_class): ?>
    <div><a class = "" href = "/teacher-classes/<?php echo $user_id; ?>">باشگاه ها</a></div>
  <?php endif; ?>
<!--  <div class = "clearfix"></div>-->
  </div>
</div>
