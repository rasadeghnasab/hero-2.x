<?php
global $Acl;
// if have admin access  $person_management '/admin/management/'
// if have management access  $person_management '/management/'
    $person_management = $_SESSION['jobValue'] > 30 ? '/admin/management/' : '/management/'; ?>
<div class="subMenu hidden-print">
    <ul>
        <li><a href="<?php echo $person_management; ?>"> ورزشکاران</a></li>
        <li><a href="<?php echo $person_management; ?>news/1" > اخبار </a></li>
<!--        <li><a href="#"> آمار</a></li>-->
        <li><a href="<?php echo $person_management; ?>events/"> رویدادها</a></li>
<!--        <li><a href="--><?php //echo $person_management; ?><!--state_indicator/"> نمایندگان استان</a></li>-->
        <li><a href="<?php echo $person_management; ?>setting/description"> تنظیمات ورزش</a></li>
      <?php if($Acl->check_permission('financial_management')): ?>
        <li><a href="<?php echo $person_management; ?>financial">امور مالی</a></li>
      <?php endif; ?>
    </ul>
</div>