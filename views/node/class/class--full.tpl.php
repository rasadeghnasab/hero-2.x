<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/8/2015
 * Time: 11:02 PM
 */
global $week_day;
global $cUser;
?>
<table class="ui selectable inverted table" id="<?= $content['class_id']; ?>">
  <thead>
  <?php if (isset($cUser->user_id) && $content['user_id'] == $cUser->user_id): ?>
    <tr>
      <td style="position: relative;" colspan="3">
        <div class="edit-box">
          <span class="glyphicon glyphicon-remove new_errors remove_class"></span>
        </div>
      </td>
    </tr>
  <?php endif; ?>
  <?php if (isset($content['field'])): ?>
    <tr>
      <th colspan="3">
        <h2><?= t('رشته') ?> <?= $content['field']; ?></h2>
      </th>
    </tr>
  <?php endif; ?>
  <tr>
    <th><?php t('روز'); ?></th>
    <th><?php t('از ساعت'); ?></th>
    <th><?php t('تا ساعت'); ?></th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($content['class_day_time_id'] as $class_day_time): ?>
    <tr>
      <td><?= htmlentities($week_day[$class_day_time['day']]); ?></td>
      <td><?= htmlentities($class_day_time['start_time']); ?></td>
      <td><?= htmlentities($class_day_time['end_time']); ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>