<?php
//  $output = '<div class="noti_rightSide">';
global $Acl;
global $permission;
//var_dump($content);
if(empty($content)) return;
$sport_name = "{$content['field']} {$content['branch']}";
?>
  <div class="event_box">
    <h3> <?= htmlentities($content['evtName']); ?> </h3>
    <p> <?= htmlentities($content['evt_type_name']) ?> در رشته <?= $sport_name ?> </p>
    <p> مهلت ثبت نام <span class="date_time"> <?= r_gregorian_to_jalali($content['deadendDate']); ?> </span></p>
  <div class="btn_middle">
  <a class="btn btn_green" href="/events/event/<?= $content['eventID']; ?>">+ بیشتر</a>
  <?php if ($content['has_weight']): ?>
    <?php if ($content['evt_table_close']): ?>
      <a class="btn btn_blue" href="/events/events-table-show/<?= $content['eventID']; ?>">جدول </a>
    <?php endif; endif; ?>
    <?php if ($Acl->check_permission('event_table_edit', $permission['group_id'], $permission['user_id']) && $permission['user_id'] == $content['inserterID']): ?>
      <?php if($content['has_weight']): ?>
      <a class="btn btn_orange" href="/events/events-table-management/<?= $content['eventID']; ?>">ویرایش جدول</a>
      <?php endif; ?>
      <a class="btn btn_orange" href="/management/events/<?= $content['eventID']; ?>/event-management">مدیریت رویداد</a>
    <?php endif; ?>
  </div>
  </div>
<?php //if ($output == ''): ?>
<?php if (FALSE): ?>
  <div class="event_box">
    <h2>هیچ رویدادی با این مشخصه یافت نشد</h2>
    <p class="new_errors">متاسفانه رویدادی یافت نشد</p>
  </div>
<?php endif; ?>
