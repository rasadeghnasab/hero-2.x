<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/29/2015
 * Time: 11:05 PM
 */
//var_dump($content);
$name       = full_name($content['first_name'], $content['last_name']);
$sport_name = full_name($content['field'], $content['branch']);
$open_date  = r_gregorian_to_jalali($content['openingDay']);
//  $weight = $content['weight'];
$age = ageCalc($content['birth_date']);

$user        = user_load($content['uid']);
$user_image  = $user->pic;
$sport_image = Image::sport_logo_load($content['sportID'])->create_src();
?>
<div class="ui card">
  <div class="content">
    <div class="edit-box hidden-print">
      <a class="person-card-remove" href="management/event-management/event-card-person-setup.php" data-id="<?= $content['cid'];?>"><span class="glyphicon glyphicon-remove"></span></a>
      <a title="چاپ" href="/management/events/<?= $content['eventID']; ?>/event-management/cards/full/<?= $content['cid']; ?>"><span class="glyphicon glyphicon-print"></span></a>
    </div>
    <img class="ui avatar image large" src="<?= $sport_image ?>">
    <?= $sport_name ?>
  </div>
  <div class="content"><?= $content["evtName"] ?>
    <span class="meta date_time fl"><?= $open_date ?></span>
  </div>
  <div class="image ui small centered bordered">
    <img src="<?= $user_image ?>">
  </div>
  <div class="content">
    <a class="header" href="/<?= $content["uid"] ?>">
      <span><?= $name ?></span>
    </a>

    <div class="meta">
      <span class="cinema"><span>سمت: </span><?= $content["name"] ?></span>
      <span class="fl"><span>استان</span> <?= $content["stateName"] ?> </span>
    </div>
    <div class="description">
      <p></p>
    </div>
    <div class="extra content">
      <span class="right floated"><?= $content["weight"] ?> <span>کیلوگرم</span></span>
      <span class="left floated"><?= $age ?> <span>سال</span></span>
    </div>
  </div>
</div>