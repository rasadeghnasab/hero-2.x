<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 7/1/2015
 * Time: 1:37 AM
 */
?>
<div class="split_bottom new_row evt_type_holder">
  <div class="edit-box">
	<ul>
	  <li><span class="glyphicon glyphicon-edit edit-evt-job blue" title="ویرایش"></span></li>
	  <li><span class="glyphicon glyphicon-remove  blue remove_evt_job" title="حذف"></span></li>
	</ul>
  </div>
  <form method="post" action="management/sport-setting/event-job.php" class="evt-job" id="<?= $content['id'] ?>">
	<input type= 'text' name ='job_name' disabled class='new_input new_medium_input' value = '<?= $content['name'] ?>' />
	<input type = "submit" name="submit" disabled class="btn btn_blue" value = "ثبت"/>
  </form>
</div>