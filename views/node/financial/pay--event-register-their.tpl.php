<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/12/2015
 * Time: 2:51 PM
 */
//var_dump($content);

$bundle = isset($content['bundle']) ? $content['bundle'] : NULL;

switch ($bundle) {
  case 'event':
    $event = Event::find_by_id($content['entity_id']);
    break;
  case 'register':
    break;
  default:
    return TRUE;
}
$date = r_gregorian_to_jalali($content['created']);

$sport      = Sport::find_by_id($content['sport_id']);
$user = user_load($content['user_id']);
if(!$user || !$sport) {
  return FALSE;
}
//var_dump($user);

?>
<div class="item">
  <a class="ui tiny image circular" href="<?= $user->user_link; ?>">
    <img src="<?= $user->pic; ?>">
  </a>
  <div class="content">
    <a class="header"  href="<?= $user->user_link; ?>"><?= $user->full_name(); ?></a>
    <?php if($bundle == 'event'): ?>
    <div class="description">
      <p>
        <span>پرداخت مبلغ </span>
        <strong class="ui label purple"><?= number_format($content['their']); ?><span class="rial">ریال</span>
        </strong> در تاریخ <span class="date_time"><?= $date; ?></span>
        <span> به منظور شرکت در رویداد</span>
        <a href="<?= $event->link(); ?>"><?= $event->evtName; ?></a>
        <span> در رشته</span>
        <a href="<?= $sport->link(); ?>"><?= $sport->full_name(); ?></a>
      </p>
    </div>
    <?php elseif($bundle == 'register'): ?>
      <div class="description">
        <p>
          <span>پرداخت مبلغ </span>
          <strong class="ui label purple"><?= number_format($content['their']); ?><span class="rial">ریال</span>
          </strong> در تاریخ <span class="date_time"><?= $date; ?></span>
          <span> ثبت نام در رشته</span>
          <a href="<?= $sport->link(); ?>"><?= $sport->full_name(); ?></a>
        </p>
      </div>
    <?php endif; ?>
  </div>
</div>