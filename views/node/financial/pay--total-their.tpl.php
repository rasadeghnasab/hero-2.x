<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/12/2015
 * Time: 2:02 AM
 */
//var_dump($content);die;
switch($content['bundle']) {
  case 'event':
    $title = t('طلب شما', FALSE);
    $text = t('ثبت نام در رویدادها', FALSE);
    break;
  case 'register':
    $title = t('طلب شما', FALSE);
    $text = t('ثبت نام', FALSE);
    break;
  default:
    $title = t('طلب کل', FALSE);
    $text = t('فعالیت', FALSE);
}
?>
<div class="ui circular segment inverted">
  <h2 class="ui header inverted">
    <?= number_format($content['their_total']); ?>
  </h2>
  <div class="sub header"><?= $title; ?></div>
  <div class="sub header">
    <?= t('از ', FALSE) . $content['count'] . ' ' . $text;?>
  </div>
</div>
