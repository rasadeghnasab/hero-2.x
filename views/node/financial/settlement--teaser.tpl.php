<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 9/11/2015
 * Time: 11:24 PM
 */

//var_dump($content);
$translate = array(
  'request' => t('درخواست تسویه', FALSE),
  'paid'  => t('پرداخت', FALSE),
);

$full_name = full_name($content['first_name'], $content['last_name']);
$date = r_gregorian_to_jalali($content['created']);
$zebra_class = isset($content['pos']) ? " class = '{$content['pos']}' " : NULL;
?>

<tr <?= $zebra_class; ?>>
  <td><?= $translate[$content['bundle']]; ?></td>
  <td><strong class="blue ui label"><?= number_format($content['amount']); ?> <span><?= t('ریال', FALSE); ?></span></strong></td>
  <td><?= $full_name; ?></td>
  <td><span class="date_time"><?= $date; ?></td>
  <td></td>
</tr>