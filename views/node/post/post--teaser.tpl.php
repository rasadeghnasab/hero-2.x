<?php
// no html entities in node view
/**
 * User: Ramin Sadegh nasab
 * Date: 9/5/2015
 * Time: 3:22 AM
 */
//var_dump($content);

global $cUser;

// RAMIN:FIXME fix this part for cat :|
$user      = user_load($content['user_id']);
$full_name = $user->full_name();
$created   = $content['updated'] != '' ? r_gregorian_to_jalali($content['updated']) : r_gregorian_to_jalali($content['created']);
$date      = created_updated_date($content['created'], $content['updated']);
?>
<div class="news_posts">
    <div class="news_part box_shadow" id="<?= $content['id']; ?>">
        <div class="news_infos">
            <img class="img_profile news_circle" src="<?= $user->pic; ?>"
                 alt="<?= $full_name; ?>">
            <div class="news_timeline_name">
                <a data-rest-url="min-prof/<?= $content['user_id'] ?>"
                   class="profile_load" href="/<?= $content['user_id']; ?>">
                    <?= $full_name; ?>
                </a>
            </div>
            <!--news_timeline_name-->
            <div class="news_timeline_date">
                <a href="<?= $content['link']; ?>">
                    <span><?= $date->text; ?> </span> <span
                            class="date_time"><?= $date->date; ?></span>
                </a>
            </div>
            <div class=" news_timeline_cat">
                <?php print Tag::tags_link(Tag::find_by_entity_id($content['id'])); ?>
            </div>
            <div class="split_bottom split-gray"></div>
        </div>
        <!-- news_info -->
        <?php if ($commenter = isset($cUser->user_id) && $content['user_id'] == $cUser->user_id): ?>
            <ul class="editable">
                <li><span class="glyphicon glyphicon-chevron-down"></span></li>
                <li class="postEdit">ویرایش</li>
                <li class="postDelete">حذف کامل</li>
                <li class="postDone">اعمال تغییرات</li>
            </ul>
        <?php endif; ?>
        <?php
        $rdmb = $class = NULL;
        if (mb_strlen($content['context'], 'utf-8') > 710) {
            $class = 'rdmp';
            $rdmb  = '<button class="rdmb btn btn_blue"> ادامه مطلب </button>';
        }
        ?>
        <article class="<?= $class; ?>">
            <?= $content['context']; ?> <?= $rdmb; ?>
        </article>
        <?php if ($images = Image::find_by_entity_id($content['id'], 'post')): ?>
            <div class="news_imgHolder">
                <?php foreach ($images as $image): ?>
                    <a class="venobox"
                       href="<?= $image->create_src('large'); ?>">
                        <img class="box_shadow"
                             src="<?= $image->create_src('large'); ?>"/>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php echo post_like_part($content['id'], $commenter); ?>
        <?php echo post_comment_part($content['id'], $commenter, $content['user_id']); ?>
    </div>
</div>
