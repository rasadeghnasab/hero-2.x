<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/31/2015
 * Time: 7:49 PM
 */
global $cUser;
//var_dump($cUser);die;
if(is_array($content['infid'])) {
  $manager_sid = $cUser->sid[key($cUser->sid)];
  $selected_info_id = in_array($manager_sid, $content['sid']) ? array_search($manager_sid, $content['sid']) : $content['sid'][$content['sid']];
}
else {
  $selected_info_id = $content['infid'];
}
//var_dump($content);die;
?>

<div class="person_management fr" id="">
  <div class="person_info">
    <a href="" class="fr"><img src="<?= $content['pic'] ?>" alt="" class="img_profile"></a>
    <span class="new_errors" data-rest-url="min-prof/<?= $content['uid']; ?>"></span>
    <div>
      <span><?= $content['jName'][$selected_info_id]; ?></span>
      <span class="blue"> در رشته <?= $content['spName'][$selected_info_id]; ?></span>
    </div>
    <span class=""><span class="glyphicon glyphicon-map-marker"></span><?= $content['state']; ?></span>
  </div>
  <div class="edit-box">
    <a class="btn btn_blue block" href="/setting/sports/<?= "{$content['uid']}/{$selected_info_id}"; ?>/honors">ویرایش</a>
  </div>
</div>