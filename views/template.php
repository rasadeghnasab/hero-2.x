<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/4/2015
 * Time: 10:55 PM
 */
/**
 * @param array  $addresses
 * @param string $css_folder
 * @param string $ext
 *
 * @return string
 * @author Ramin Sadegh nasab
 */
function add_css(array $addresses, $css_folder = "", $ext = '.css') {
  $absolute_path = 'http://'.$_SERVER['SERVER_NAME'];
  $base_path = BASEPATH;
  $path = $absolute_path . $base_path;
  $output = '';
  foreach ($addresses as $address) {
    $href = check_url($address) ? $address : $path . $css_folder . $address . $ext;
    $output .= "<link rel = 'stylesheet' type = 'text/css' href = '{$href}'/>";
  }

  return $output;
}

/**
 * @param array  $addresses
 * @param string $js_folder
 * @param string $ext
 *
 * @return string
 * @author Ramin Sadegh nasab
 */
function add_js(array $addresses, $js_folder = "", $ext = '.js') {
  $absolute_path = 'http://'.$_SERVER['SERVER_NAME'];
  $base_path = BASEPATH;
  $path = $absolute_path . $base_path;
  $output = '';
  foreach ($addresses as $address) {
    $href = check_url($address) ? $address : $path . $js_folder . $address . $ext;
    $output .= "<script rel = 'stylesheet' type = 'text/javascript' src = '{$path}{$js_folder}{$address}{$ext}'></script>";
  }

  return $output;
}

/**
 * Combine static and dynamic javascript/ css files and output theme for print on the page (<link> or <script> tags)
 * @param      $static
 * @param null $dynamic
 *
 * @return array
 * @author Ramin Sadegh nasab
 */
function css_js_static_dynamic_combine($static, $dynamic = NULL) {
  if ($dynamic != NULL) {
    $add    = isset($dynamic['add']) ? $dynamic['add'] : FALSE;
    $remove = isset($dynamic['remove']) ? $dynamic['remove'] : FALSE;
    if ($remove) {
      foreach ($static as $key => $href) {
        if (in_array($href, $remove)) {
          unset($static[$key]);
        }
      }
    }
    if ($add) {
      $static = array_merge($static, $add);
    }
    $hrefs = $static;
  }
  else {
    $hrefs = $static;
  }

  return $hrefs;
}

function layout_template($template_name = '', $variables = array()) {
  foreach ($variables as $key => $value) {
    $$key = $value;
  }
  $path = VIEWS . 'layouts_sections' . DS . $template_name;
  if (!isset(pathinfo($template_name)['extension'])) {
    $path .= '.php';
  }
  require_once($path);
}

function node_view($node_name, $variables = array(), $node_type = 'node', $view_mode = 'full', $type = 'file', $htmlentities = TRUE) {
  $content = array();
  if(!$variables) return false;
//  var_dump($variables->get_node_type());die;
//  var_dump(get_class($variables));die;
  if(is_object($variables) && ($obj_node_name = get_class($variables)) != 'stdClass') {
    $node_type = ($node_type && $node_type != 'node') ? $node_type : $variables->get_node_type();
    $node_name = strtolower(preg_replace(array('/_/', '/\B([A-Z]{1})/'), array('-', '-$1'), $obj_node_name));
  }


  foreach ($variables as $key => $value) {
    $value = $htmlentities && !is_array($value) ? htmlentities($value) : $value;
    $content[$key] = $value;
  }

  // if its a file like an image (we create source for it);
  if(is_object($variables) && isset($variables->fid) && get_class($variables) == 'image') {
    $content['source']['thumb']  = $variables->create_src();
    $content['source']['large']  = $variables->create_src('large');
    $content['source']['actual'] = $variables->create_src('actual');
  }

  if(empty($content)) {
    if($type == 'file') {
      echo '<h3>This content is empty</h3>';
      return;
    }
    elseif($type == 'functions') {
      return '<h3>This content is empty</h3>';
    }
  }
  if ($type == 'file') {
    $path = VIEWS . $node_type . DS . $node_name . '--' . $view_mode . '.tpl.php';
    if (file_exists($path)) {
      include($path);
    }
    else {
      die("file: {$path} not exist");
    }
  }
  elseif ($type == 'function') {
    $node_type = str_replace(array('-', '/'), '_', $node_type);
    $node_name = str_replace('-', '_', $node_name);
    $view_mode = str_replace('-', '_', $view_mode);

    $function = "theme_{$node_type}_{$node_name}__{$view_mode}";
    $path = VIEWS . DS . 'themes' . DS . 'theme-functions.php';
//    echo json_encode($function);die;
    require_once($path);

    if(function_exists($function)) {
      return $function($content);
    }
    else {
      die("function {$function} does not exist");
    }
  }
}

function node_view_multiple($node_name, $nodes = array(), $node_type = 'node', $view_mode = 'teaser', $type = 'file', $htmlentities = TRUE) {
  $count = 0;
  $output = '';
  if(is_object($nodes)) {
    return node_view($node_name, $nodes, $node_type, $view_mode, $type, $htmlentities);
  }
  foreach ($nodes as $key => $node) {
    if(!is_array($node) && !is_object($node)) continue;
    $position = $count % 2 ? 'odd' : 'even';
    if (is_array($node)) {
      $node['pos'] = $position;
    }
    elseif (is_object($node)) {
      $node->pos = $position;
    }
    $count++;

    $output .= node_view($node_name, $node, $node_type, $view_mode, $type, $htmlentities);
  }

  return $output;
}

/**
 * View form
 * @param string  $node_name
 * @param array  $variables
 * @param string $node_type
 *
 * @author Ramin Sadegh nasab
 */
function node_form_template($node_name, $variables = array(), $node_type = 'form') {
//<?php include(SITE_ROOT . '/views/form/product--form.tpl.php');
  $path = VIEWS . $node_type . DS . $node_name . '--' . $node_type . '.tpl.php';
  $form = array();
  foreach($variables as $key => $variable) {
    if(!is_numeric($key)) {
      $form[$key] = $variable;
    }
  }

  if(file_exists($path)) {
    include($path);
  }
  else {
    die("file <span class='file'> {$path} </span> is not exist");
  }
}

/**
 * A function for viewing message when no node found in a query
 * @param string $node_name
 * @param string $type
 *
 * @return mixed
 * @author Ramin Sadegh nasab
 */
function no_node_view($node_name, $type = 'function') {
  $nodes     = array('node_name' => $node_name);
  $node_type = $node_name = 'no-node';
  return node_view($node_name, $nodes, $node_type, 'full', $type, FALSE);
}

/**
 * @param $data
 *
 * @author Ramin Sadegh nasab
 */
function html_meta_tag($data) {
  static $meta_tag;
  global $meta_tag;
  $tag = $attributes = '';
  if (!is_array($data)) {
    $meta_tag .= $data;
    return;
  }
  foreach ($data as $key => $value) {
    if (is_string($value)) {
      $$key = $value;
    }
    else {
      foreach ($value as $key_2 => $value_2) {
        $$key .= " $key_2 = '$value_2' ";
      }
    }
  }
  $output_tag = "<{$tag} {$attributes}/>";
  $meta_tag .= $output_tag;
}

/**
 * Create description and keyword meta tags
 * @param bool|FALSE $description
 * @param bool|FALSE $keyword
 *
 * @author Ramin Sadegh nasab
 */
function html_meta_desc_keyword_tag($description = FALSE, $keyword = FALSE) {
  $descriptions = array(
    'tag'        => 'meta',
    'attributes' => array(
      'name'    => 'description',
      'content' => $description,
    ),
  );
  if (!$keyword) {
    $keyword     = 'ورزش، اخبار رزمی، بانک ورزشی، اطلاعات ورزشی، هنرهای رزمی، heros.ir، ';
  }
  $keywords = array(
    'tag'        => 'meta',
    'attributes' => array(
      'name'    => 'keywords',
      'content' => $keyword,
    ),
  );

  html_meta_tag($descriptions);
  html_meta_tag($keywords);
}

//////////// Some specific view functions TODO:: all this functions must change
function event_fetch_pdo($pdo_object) {

  return $pdo_object ? $pdo_object->fetchAll(PDO::FETCH_OBJ) : FALSE;
}