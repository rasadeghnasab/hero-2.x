<?php
/**
 * User: Ramin Sadegh nasab
 * Date: 8/28/2015
 * Time: 7:34 PM
 */
//////////////////// event
/**
 * @param $content
 *
 * @return string
 * @author Ramin Sadegh nasab
 */
function theme_node_event_job__edit($content) {
  $output = '';
  $output .= '<div class="split_bottom new_row evt_type_holder">';
  $output .= '<div class="edit-box">';
  $output .= '<ul>';
  $output .= '<li><span class="glyphicon glyphicon-edit edit-evt-job blue" title="ویرایش"></span></li>';
  $output .= '<li><span class="glyphicon glyphicon-remove  blue remove_evt_job" title="حذف"></span></li>';
  $output .= '</ul>';
  $output .= '</div>';
  $output .= '<form method="post" action="management/sport-setting/event-job.php" class="evt-job" id="' . $content['id'] . '">';
  $output .= "<input type= 'text' name ='evt_type_name' disabled class='new_input new_medium_input' value = '{$content['name']}' />";
  $output .= '<input type = "submit" name="submit" disabled class="btn btn_blue" value = "ثبت"/></input>';
  $output .= '</form>';
  $output .= '</div>';

  return $output;
}

function theme_node_event_card__full($content) {
  $name       = full_name($content['first_name'], $content['last_name']);
  $sport_name = full_name($content['field'], $content['branch']);
  $open_date  = r_gregorian_to_jalali($content['openingDay']);
//  $weight = $content['weight'];
  $age = ageCalc($content['birth_date']);

  $user        = user_load($content['uid']);
  $user_image  = $user->pic;
  $sport_image = Image::sport_logo_load($content['sportID'])->create_src();

  $output = '';
  $output .= "<div class='ui card'>";
  $output .= "<div class='content'>";
//  $output .= "<div class='right floated meta'>14h</div>";
  $output .= "<a class='person-card-remove' href='management/event-management/event-card-person-setup.php' data-id='{$content['cid']}'><span class='glyphicon glyphicon-remove fl'></span></a>";
  $output .= "<img class='ui avatar image large' src='{$sport_image}'> ";
  $output .= "{$sport_name}";
  $output .= "</div>"; // .content
  $output .= "<div class='content'>{$content['evtName']}";
  $output .= "<span class='meta date_time fl'>{$open_date}</span>"; // .date_time
  $output .= "</div>"; // .content
  $output .= "<div class='image ui small centered bordered'>";
  $output .= "<img src='{$user_image}'>";
  $output .= "</div>"; // .image
  $output .= "<div class='content'>";
  $output .= "<a class='header' href='/{$content['uid']}'>";
  $output .= "<span>{$name}</span>";
  $output .= '</a>';
  $output .= "<div class='meta'>";
  $output .= "<span class='cinema'><span>سمت: </span>{$content['name']}</span>";
  $output .= "<span class='fl'><span>استان</span> {$content['stateName']} </span> ";
  $output .= "</div>"; // .meta
  $output .= "<div class='description'>";
  $output .= "<p></p>";
  $output .= "</div>"; // .description
  $output .= "<div class='extra content'>";
  $output .= "<span class='right floated'>{$content['weight']} <span>کیلوگرم</span></span>";
  $output .= "<span class='left floated'>{$age} <span>سال</span></span>";
//  $output .= "<div class='ui label'><i class='globe icon'></i> Additional Languages</div>";
  $output .= "</div>"; // .extra
  $output .= "</div>"; // .content
  $output .= "</div>"; // .item

  return $output;
//  echo json_encode($content);die;
}

//////////// post
function theme_node_post_post__full($content) {
  global $cUser;
  $user      = user_load($content['user_id']);
  $full_name = $user->full_name();
  $date      = created_updated_date($content['created'], $content['updated']);
  $tags      = Tag::find_by_entity_id($content['id']);

  $output = '';
  $output .= '<div class="news_posts">';
  $output .= '<div class="news_part box_shadow" id="' . $content['id'] . '" >';
  $output .= '<div class="news_infos">';
  $output .= '<img class="img_profile news_circle" src="' . $user->pic . '" />';
  $output .= '<div class="news_timeline_name">';
  $output .= '<a data-rest-url = "min-prof/' . $content['user_id'] . '" class="profile_load" href="/' . $content['user_id'] . '">' . $full_name . '</a>';
  $output .= '</div>'; // news_timeline_name
  $output .= '<div class="news_timeline_date">';
  $output .= "<a href=\"{$content['link']}\">";
  $output .= "<span>{$date->text}</span>" . "<span class='date_time'>{$date->date}</span>";
  $output .= '</a>';
  $output .= '</div>'; // news_timeline_date
  $output .= '<div class="news_timeline_cat">';
  $output .= Tag::tags_link($tags);
  $output .= '</div>';
  $output .= '<div class="split_bottom split-gray"></div>';
  $output .= '</div>'; // news_infos
  if ($commenter = isset($cUser->user_id) && $content['user_id'] == $cUser->user_id) {
    $output .= '<ul class="editable">';
    $output .= '<li><span class="glyphicon glyphicon-chevron-down"></span></li>';
    $output .= '<li class="postEdit">ویرایش</li>';
    $output .= '<li class="postDelete">حذف کامل</li>';
    $output .= '<li class="postDone">اعمال تغییرات </li>';
    $output .= '</ul>';
  }
  $output .= "<article>" . strip_tags(nl2br($content['context']), '<br><b>') . "</article>";
  if ($images = Image::find_by_entity_id($content['id'], 'post')) {
//    var_dump($images);
    $output .= '<div class="news_imgHolder">';
    foreach ($images as $image) {
      $output .= "<a class='venobox' href='{$image->create_src('large')}'>";
      $output .= "<img class='box_shadow' src='{$image->create_src('large')}' alt='" . htmlentities($content['context']) . "' />";
      $output .= "</a>";
    }
  }
  else {
//    echo 'empty images';
  }

  $output .= '</div>'; // news_imgHolder
  $output .= post_like_part($content['id'], $commenter);
  $output .= post_comment_part($content['id'], $commenter, $content['user_id']);
  $output .= '</div>'; // news_part box_shadow
  $output .= '</div>'; // news_posts

  return $output;
}

function theme_node_post_post__teaser($content) {
  global $cUser;
  $rdmb      = $class = NULL;
  $user      = user_load($content['user_id']);
  $full_name = $user->full_name();
  $date      = created_updated_date($content['created'], $content['updated']);
  $tags      = Tag::find_by_entity_id($content['id']);

  $output = '';
  $output .= '<div class="news_posts">';
  $output .= '<div class="news_part box_shadow" id="' . $content['id'] . '" >';
  $output .= '<div class="news_infos">';
  $output .= '<img class="img_profile news_circle" src="' . $user->pic . '" />';
  $output .= '<div class="news_timeline_name">';
  $output .= '<a data-rest-url = "min-prof/' . $content['user_id'] . '" class="profile_load" href="/' . $content['user_id'] . '">' . $full_name . '</a>';
  $output .= '</div>'; // news_timeline_name
  $output .= '<div class="news_timeline_date">';
  $output .= "<a href=\"{$content['link']}\">";
  $output .= "<span>{$date->text}</span>" . "<span class='date_time'>{$date->date}</span>";
  $output .= '</a>';
  $output .= '</div>'; // news_timeline_date
  $output .= '<div class="news_timeline_cat">';
  $output .= Tag::tags_link($tags);
  $output .= '</div>';
  $output .= '<div class="split_bottom split-gray"></div>';
  $output .= '</div>'; // news_infos
  if ($commenter = isset($cUser->user_id) && $content['user_id'] == $cUser->user_id) {
    $output .= '<ul class="editable">';
    $output .= '<li><span class="glyphicon glyphicon-chevron-down"></span></li>';
    $output .= '<li class="postEdit">ویرایش</li>';
    $output .= '<li class="postDelete">حذف کامل</li>';
    $output .= '<li class="postDone">اعمال تغییرات </li>';
    $output .= '</ul>';
  }
  if (mb_strlen($content['context'], 'utf-8') > 710) {
    $class = 'rdmp';
    $rdmb  = '<button class="rdmb btn btn_blue"> ادامه مطلب </button>';
  }
  $output .= "<article class='{$class}'>" . strip_tags(nl2br($content['context']), '<br><b>') . $rdmb . "</article>";
  if ($images = Image::find_by_entity_id($content['id'], 'post')) {
    $output .= '<div class="news_imgHolder">';
    foreach ($images as $image) {
      $output .= "<a class='venobox' href='{$image->create_src('large')}'>";
      $output .= "<img class='box_shadow' src='{$image->create_src('large')}' alt='" . htmlentities($content['context']) . "' />";
      $output .= "</a>";
    }
    $output .= '</div>'; // news_imgHolder
  }
  $output .= post_like_part($content['id'], $commenter);
  $output .= post_comment_part($content['id'], $commenter, $content['user_id']);
  $output .= '</div>'; // news_part box_shadow
  $output .= '</div>'; // news_posts

  return $output;
}

////////////// image
function theme_node_image_image__profile_gallery($content) {
  $output = '';
  $output .= "<a data-gall ='prof-gall' class='venobox' href='{$content['source']['large']}'>";
  $output .= "<img src='{$content['source']['thumb']}' class='inner' alt=''>";
  $output .= "</a>";

  return $output;
}

function theme_node_class_class__full($content) {
  global $cUser, $week_day;
  $output = '';
  $output .= "<table class='ui selectable inverted table' id='{$content['class_id']}'>";
  $output .= '<thead>';

  if ($content['user_id'] == $cUser->user_id) {
    $output .= '<tr>';
    $output .= '<td style="position: relative;" colspan="3">';
    $output .= '<div class="edit-box" >';
    $output .= '<span class="glyphicon glyphicon-remove new_errors remove_class" ></span>';
    $output .= '</div>';
    $output .= '</td>';
    $output .= '</tr>';
  }

  if($content['field']) {
    $output .= '<tr>';
    $output .= '<th colspan = "3" >';
    $output .= '<h2>' . full_name(t('رشته', FALSE), $content['field']) . '</h2>';
    $output .= '</th>';
    $output .= '</tr>';
  }

  $output .= '<tr>';
  $output .= '<th>' . t('روز', FALSE) . '</th>';
  $output .= '<th>';
  $output .= t('از ساعت', FALSE) . '</th>';
  $output .= '<th>' . t('تا ساعت', FALSE) . '</th>';
  $output .= '</tr>';
  $output .= '</thead>';
  $output .= '<tbody>';

  foreach ($content['class_day_time_id'] as $class_day_time) {
    $output .= '<tr>';
    $output .= '<td>' . htmlentities($week_day[$class_day_time['day']]) . '</td>';
    $output .= '<td>' . htmlentities($class_day_time['start_time']) . '</td>';
    $output .= '<td>' . htmlentities($class_day_time['end_time']) . '</td>';
    $output .= '</tr>';
  }

  $output .= '</tbody>';
  $output .= '</table>';

  return $output;
}

function  theme_no_node_no_node__full($content) {
  $nodes_translate = array(
    'post' => t('پستی', FALSE),
    'picture' => t('تصویری', FALSE),
    'class' => t('کلاسی', FALSE),
    'user' => t('کاربری', FALSE),
  );

  $translated = isset($nodes_translate[$content['node_name']]) ? $nodes_translate[$content['node_name']] : $content['node_name'];
  $output = '';
  $output .= '<div class="ui segment inverted no-node">';
  $output .= t('هیچ ', FALSE) . $translated . t(' یافت نشد.', FALSE);
  $output .= '</div>'; // ui segment inverted

  return $output;
}